﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Elasticsearch.Net;
using MicroService.Customer.Data.ReadModel.Repository;
using MicroServices.Customer.Domain;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nest;

namespace MicroService.Customer.API.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerAccountController : ControllerBase
    {
        private readonly IESRepository<CustomerAccount> _readModelRepository;

        public CustomerAccountController(IESRepository<CustomerAccount> readModelRepository)
        {
            _readModelRepository = readModelRepository;
        }

        // GET: api/customerAccount/5
        [EnableCors("mypolicy")]
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(CustomerAccount), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<IEnumerable<CustomerAccount>> Get(Guid id)
        {
            var searchModel = new Model.ReadModelEntity.BaseSearchModel();
            searchModel.Fields = new Dictionary<string, string>();

            searchModel.Fields.Add("customerId", id.ToString());

            var result = _readModelRepository.Search(searchModel);

            return Ok(result);
        }
    }
}