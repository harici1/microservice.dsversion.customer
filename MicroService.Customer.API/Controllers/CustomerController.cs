﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Elasticsearch.Net;
using MediatR;
using MicroService.Customer.Data.ReadModel.Repository;
using MicroService.Customer.Model.CommandModel;
using MicroServices.Customer.Domain;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nest;

namespace MicroService.Customer.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        // GET: api/customer/5
        [EnableCors("mypolicy")]
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(MicroServices.Customer.Domain.Customer), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<IEnumerable<MicroServices.Customer.Domain.Customer>> Get(Guid id)
        {
            var searchModel = new Model.ReadModelEntity.BaseSearchModel();
            searchModel.Fields = new Dictionary<string, string>();

            searchModel.Fields.Add("id", id.ToString());

            var result = _readModelRepository.Search(searchModel);

            return Ok(result);
        }

        private readonly IMediator _mediator;
        private readonly IESRepository<MicroServices.Customer.Domain.Customer> _readModelRepository;
        public CustomerController(IMediator mediator, IESRepository<MicroServices.Customer.Domain.Customer> readRodelRepository)
        {
            _mediator = mediator;
            _readModelRepository = readRodelRepository;
        }
        [HttpPost("createcustomer")]
        public void CreateCustomer([FromBody] CreateCustomerCommand model)
        {
            var result = _mediator.Send(model).Result;
        }

        [HttpPost("addcustomerbillingaccountproduct")]
        public void AddCustomerBillingAccountProduct([FromBody] AddCustomerBillingAccountProductCommand model)
        {
            var result = _mediator.Send(model).Result;
        }

        [HttpPost("cancelcustomeraccount")]
        public void CancelCustomerAccount([FromBody] CancelCustomerAccountCommand model)
        {
            var result = _mediator.Send(model).Result;
        }

        [HttpPost("/cancelproductbundle")]
        public void CancelProductBundle([FromBody] CancelProductBundleCommand model)
        {
            var result = _mediator.Send(model).Result;
        }

        [HttpPost("cancelproduct")]
        public void CancelProduct([FromBody] CancelProductCommand model)
        {
            var result = _mediator.Send(model).Result;
        }

        [HttpPost("createaccountcontact")]
        public void CreateAccountContact([FromBody] CreateAccountContactCommand model)
        {
            var result = _mediator.Send(model).Result;
        }

        [HttpPost("createcustomeraccount")]
        public void CreateCustomerAccount([FromBody] CreateCustomerAccountCommand model)
        {
            var result = _mediator.Send(model).Result;
        }

        [HttpPost("createcustomerbillingaccount")]
        public void CreateCustomerBillingAccount([FromBody] CreateCustomerBillingAccountCommand model)
        {
            var result = _mediator.Send(model).Result;
        }

        [HttpPost("createproductbundle")]
        public void CreateProductBundle([FromBody] AddProductBundleCommand model)
        {
            var result = _mediator.Send(model).Result;
        }

        [HttpPost("createproduct")]
        public void CreateProduct([FromBody] AddProductCommand model)
        {
            var result = _mediator.Send(model).Result;
        }

        [HttpPost("removecustomerbillingaccountproduct")]
        public void RemoveCustomerBillingAccountProduct([FromBody] RemoveCustomerBillingAccountProductCommand model)
        {
            var result = _mediator.Send(model).Result;
        }
    }
}