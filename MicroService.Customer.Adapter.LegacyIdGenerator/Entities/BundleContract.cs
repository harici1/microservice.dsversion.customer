﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Adapter.LegacyIdGenerator.Entities
{
    public class BundleContract
    {
        public string DisplayName { get; set; }
        public int ContractId { get; set; }
        public int ContractDetailId { get; set; }
        public string DsisHesapNo { get; set; }
    }
}
