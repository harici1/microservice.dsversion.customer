﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MicroService.Customer.Adapter.LegacyIdGenerator.Entities.Dsis
{
    [Table("SMSLeasingContractExtensionError")]
    public class SMSLeasingContractExtensionError
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int? AnkaContractId { get; set; }
        public int? LeasingContractId { get; set; }
        public string Error { get; set; }
    }
}
