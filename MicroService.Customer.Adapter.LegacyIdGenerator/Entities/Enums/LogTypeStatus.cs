﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Adapter.LegacyIdGenerator.Entities.Enums
{
    public enum LogTypeStatus
    {
        Waiting = 0,
        InProgress = 1,
        Success = 2,
        Error = 3,
        CommandNotFound = 4,
        UnMigratableData = 5,
        SelectModelNotFound = 6,
        Test = 7
    }
}
