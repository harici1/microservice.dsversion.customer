﻿using Dapper;
using MicroService.Customer.Adapter.LegacyIdGenerator.Entities;
using MicroService.Customer.Adapter.LegacyIdGenerator.Interfaces;
using MicroService.Customer.Adapter.LegacyIdGenerator.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.Customer.Adapter.LegacyIdGenerator.Helpers
{
    public class AnkaLegacyDbDapper : IAnkaLegacyDb
    {
        private readonly AppSettings _appSettings;
        public AnkaLegacyDbDapper(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }
        public async Task<List<int>> GetContractIds()
        {
            try
            {
                using (var connection = new SqlConnection(_appSettings.ConnectionStrings.AnkaLegacyConnection))
                {
                    var data = await connection.QueryAsync<int>("SELECT ContractId FROM Contract");
                    return data.ToList();
                }
            }
            catch (Exception)
            {
                return new List<int>();
            }
        }

        public async Task<List<BundleContract>> GetBundleContracts()
        {
            try
            {
                using (var connection = new SqlConnection(_appSettings.ConnectionStrings.AnkaLegacyConnection))
                {
                    var query = @"SELECT 
                                    SPD.DisplayName,
                                    CD2.ContractId,
                                    CD2.ContractDetailId,
                                    CDSP.Value DsisHesapNo --leasingContractNumber
                                FROM dbo.ContractDetail AS CD2 (NOLOCK)
                                    INNER JOIN dbo.ContractDetailServiceParam AS CDSP (NOLOCK)
                                        ON CDSP.ContractDetailId = CD2.ContractDetailId
                                    INNER JOIN dbo.ServiceParamDetail AS SPD (NOLOCK)
                                        ON SPD.ServiceParamDetailId = CDSP.ServiceParamDetailId
                                    INNER JOIN dbo.ContractDetailStatus AS CDS (NOLOCK)
                                        ON CDS.ContractDetailStatusId = CD2.ContractDetailStatusId
                                           AND CDS.SelectPriority = 1
                                WHERE 
                                    CD2.CatalogItemId = 5106
                                    AND CD2.ContractDetailStatusId NOT IN ( 80, 81 )
                                    AND CDSP.ServiceParamDetailId = 382;
                                ";
                    var data = await connection.QueryAsync<BundleContract>(query);
                    return data.ToList();
                }
            }
            catch (Exception)
            {
                return new List<BundleContract>();
            }
        }

        public async Task<BundleContract> CheckBundleContractIfExist(int contractId)
        {
            try
            {
                using (var connection = new SqlConnection(_appSettings.ConnectionStrings.AnkaLegacyConnection))
                {
                    var query = $@"SELECT 
                                    SPD.DisplayName,
                                    CD2.ContractId,
                                    CD2.ContractDetailId,
                                    CDSP.Value DsisHesapNo --leasingContractNumber
                                FROM dbo.ContractDetail AS CD2 (NOLOCK)
                                    INNER JOIN dbo.ContractDetailServiceParam AS CDSP (NOLOCK)
                                        ON CDSP.ContractDetailId = CD2.ContractDetailId
                                    INNER JOIN dbo.ServiceParamDetail AS SPD (NOLOCK)
                                        ON SPD.ServiceParamDetailId = CDSP.ServiceParamDetailId
                                    INNER JOIN dbo.ContractDetailStatus AS CDS (NOLOCK)
                                        ON CDS.ContractDetailStatusId = CD2.ContractDetailStatusId
                                           AND CDS.SelectPriority = 1
                                WHERE CD2.ContractId = {contractId}
                                    AND CD2.CatalogItemId = 5106
                                    AND CD2.ContractDetailStatusId NOT IN ( 80, 81 )
                                    AND CDSP.ServiceParamDetailId = 382;";
                    return await connection.QueryFirstOrDefaultAsync<BundleContract>(query);
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
