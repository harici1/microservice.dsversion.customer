﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Dapper.FastCrud;
using MicroService.Customer.Adapter.LegacyIdGenerator.Entities.Anka;
using MicroService.Customer.Adapter.LegacyIdGenerator.Interfaces;
using MicroService.Customer.Adapter.LegacyIdGenerator.Models;
using Microsoft.Extensions.Options;

namespace MicroService.Customer.Adapter.LegacyIdGenerator.Helpers
{
    public class AnkaMigrationDbDapper : IAnkaMigrationDb
    {
        private readonly AppSettings _appSettings;
        public AnkaMigrationDbDapper(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }
        public async Task<List<Contract>> GetContractsByIdGeneratorStatus(int status)
        {
            try
            {
                using (var connection = new SqlConnection(_appSettings.ConnectionStrings.AnkaLegacyConnection))
                {
                    var data = await connection.QueryAsync<Contract>($"SELECT * FROM Contract WHERE LogType='INSERT' AND IdGeneratorStatus = {status}");
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<Contract>();
            }
        }

        public async Task UpdateContractIdGeneratorStatus(List<Contract> contracts, int status)
        {
            try
            {
                using (var connection = new SqlConnection(_appSettings.ConnectionStrings.AnkaLegacyConnection))
                {
                    //await connection.BulkUpdateAsync<Contract>(new Contract { IdGeneratorStatus = 1 }, st => st
                    //.Where($"{nameof(Contract.LogId):C} in (@IdIn)")
                    //.WithParameters(new { IdIn = contracts.Select(x => x.LogId).ToList() })
                    //);
                    var query = $"UPDATE Contract SET IdGeneratorStatus = {status} WHERE LogId in ({string.Format(",", contracts.Select(x => x.LogId).ToList())})";
                    await connection.ExecuteAsync(query);
                }
            }
            catch (Exception ex)
            {
            }
        }

        public async Task UpdateContract(Contract contract)
        {
            try
            {
                using (var connection = new SqlConnection(_appSettings.ConnectionStrings.AnkaLegacyConnection))
                {
                    _ = await connection.UpdateAsync(contract);
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
