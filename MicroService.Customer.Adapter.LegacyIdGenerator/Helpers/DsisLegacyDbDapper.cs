﻿using MicroService.Customer.Adapter.LegacyIdGenerator.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Dapper.FastCrud;
using Dapper;
using MicroService.Customer.Adapter.LegacyIdGenerator.Models;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.Customer.Adapter.LegacyIdGenerator.Helpers
{
    public class DsisLegacyDbDapper : IDsisLegacyDb
    {
        private readonly AppSettings _appSettings;
        public DsisLegacyDbDapper(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public async Task<int> GetLeasingContractIdByContractNumber(string dsisContractNumber)
        {
            try
            {
                using (var connection = new SqlConnection(_appSettings.ConnectionStrings.DsisLegacyConnection))
                {
                    var data = await connection.QueryAsync<int>($"SELECT LeasingContractID FROM dsLeasingContract WHERE LeasingContractNumber = {dsisContractNumber}");
                    return data.FirstOrDefault();
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public async Task<List<int>> GetLeasingContractIds()
        {
            try
            {
                using (var connection = new SqlConnection(_appSettings.ConnectionStrings.DsisLegacyConnection))
                {
                    var data = await connection.QueryAsync<int>("SELECT LeasingContractID FROM dsLeasingContract");
                    return data.ToList();
                }
            }
            catch (Exception)
            {
                return new List<int>();
            }
        }
    }
}
