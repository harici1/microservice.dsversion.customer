﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Dapper.FastCrud;
using MicroService.Customer.Adapter.LegacyIdGenerator.Entities.Dsis;
using MicroService.Customer.Adapter.LegacyIdGenerator.Interfaces;
using MicroService.Customer.Adapter.LegacyIdGenerator.Models;
using Microsoft.Extensions.Options;

namespace MicroService.Customer.Adapter.LegacyIdGenerator.Helpers
{
    public class DsisMigrationDbDapper : IDsisMigrationDb
    {
        private readonly AppSettings _appSettings;
        public DsisMigrationDbDapper(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }
        public async Task BulkInsertSmsleasingContractExtension(List<int> ids, bool isDsis)
        {
            try
            {
                if (ids != null && ids.Any())
                {
                    using (var connection = new SqlConnection(_appSettings.ConnectionStrings.DsisMigrationDbConnection))
                    {
                        int count = 1000;
                        for (int i = 0; i < (ids.Count / count) + 1; i++)
                        {
                            string query = isDsis ?
                                                "INSERT INTO dbo.SMSLeasingContractExtension (ProductBundleId, LeasingContractId) VALUES " :
                                                "INSERT INTO dbo.SMSLeasingContractExtension (ProductBundleId, AnkaContractId) VALUES ";

                            foreach (var id in ids.Skip(i * count).Take(count).ToList())
                            {
                                query += $"('{Guid.NewGuid()}', {id}),";
                            }
                            query = query.Remove(query.Length - 1);
                            await connection.ExecuteAsync(query);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
            }
        }
        public async Task SmsleasingContractExtensionByLeasingContractId(int leasingContractId, int ankaContractId)
        {
            try
            {
                using (var connection = new SqlConnection(_appSettings.ConnectionStrings.DsisMigrationDbConnection))
                {
                    string query = $"Update  dbo.SMSLeasingContractExtension SET AnkaContractId = {ankaContractId} WHERE LeasingContractId = {leasingContractId} ; SELECT @@ROWCOUNT ";
                    var res = await connection.ExecuteScalarAsync<int>(query);
                    if (res == 0)
                    {
                        await AddError(new SMSLeasingContractExtensionError
                        {
                            AnkaContractId = ankaContractId,
                            LeasingContractId = leasingContractId,
                            Error = "Item Not Found for Update!"
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                await AddError(new SMSLeasingContractExtensionError
                {
                    AnkaContractId = ankaContractId,
                    LeasingContractId = leasingContractId,
                    Error = "Error Detail = " + ex.Message
                });
            }
        }
        public async Task<List<int>> GetListContractIds(bool isDsis)
        {
            try
            {
                using (var connection = new SqlConnection(_appSettings.ConnectionStrings.DsisMigrationDbConnection))
                {
                    string query = isDsis ?
                                            "SELECT LeasingContractId FROM dbo.SMSLeasingContractExtension WHERE LeasingContractId IS NOT NULL" :
                                            "SELECT AnkaContractId FROM dbo.SMSLeasingContractExtension WHERE AnkaContractId IS NOT NULL";

                    var data = await connection.QueryAsync<int>(query);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<int>();
            }
        }
        public async Task<List<LeasingContract>> GetLeasingContractsByIdGeneratorStatus(int status)
        {
            try
            {
                using (var connection = new SqlConnection(_appSettings.ConnectionStrings.DsisMigrationDbConnection))
                {
                    var data = await connection.QueryAsync<LeasingContract>($"SELECT * FROM LeasingContract WHERE LogType='Insert' AND IdGeneratorStatus = {status}");
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<LeasingContract>();
            }
        }
        public async Task UpdateContractIdGeneratorStatus(List<LeasingContract> contracts, int status)
        {
            try
            {
                if (contracts != null && contracts.Any())
                {
                    using (var connection = new SqlConnection(_appSettings.ConnectionStrings.DsisMigrationDbConnection))
                    {
                        var query = $"UPDATE LeasingContract SET IdGeneratorStatus = {status} WHERE LogId in ({string.Format(",", contracts.Select(x => x.LogId).ToList())})";
                        await connection.ExecuteAsync(query);

                        //int count = 1000;
                        //for (int i = 0; i < (contracts.Count / count) + 1; i++)
                        //{
                        //    var query = $"UPDATE Contract SET IdGeneratorStatus = 1 WHERE LogId in ({string.Format(",", contracts.Skip(i * count).Take(count).Select(x => x.LogId).ToList())})";
                        //    await connection.ExecuteAsync(query);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        public async Task UpdateContract(LeasingContract contract)
        {
            try
            {
                using (var connection = new SqlConnection(_appSettings.ConnectionStrings.DsisMigrationDbConnection))
                {
                    _ = await connection.UpdateAsync(contract);
                }
            }
            catch (Exception ex)
            {

            }
        }

        public async Task AddError(SMSLeasingContractExtensionError entity)
        {
            try
            {
                using (var connection = new SqlConnection(_appSettings.ConnectionStrings.DsisMigrationDbConnection))
                {
                    await connection.InsertAsync(entity);
                }
            }
            catch (Exception)
            {

            }
        }
    }
}
