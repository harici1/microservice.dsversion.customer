﻿using MicroService.Customer.Adapter.LegacyIdGenerator.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MicroService.Customer.Adapter.LegacyIdGenerator.Interfaces
{
    public interface IAnkaLegacyDb
    {
        Task<List<int>> GetContractIds();
        Task<List<BundleContract>> GetBundleContracts();
        Task<BundleContract> CheckBundleContractIfExist(int contractId);
    }
}
