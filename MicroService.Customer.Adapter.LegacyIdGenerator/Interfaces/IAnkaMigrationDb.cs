﻿using MicroService.Customer.Adapter.LegacyIdGenerator.Entities.Anka;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MicroService.Customer.Adapter.LegacyIdGenerator.Interfaces
{
    public interface IAnkaMigrationDb
    {
        Task<List<Contract>> GetContractsByIdGeneratorStatus(int status);
        Task UpdateContractIdGeneratorStatus(List<Contract> contracts, int status);
    }
}
