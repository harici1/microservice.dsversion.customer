﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Customer.Adapter.LegacyIdGenerator.Interfaces
{
    public interface IDsisLegacyDb
    {
        Task<List<int>> GetLeasingContractIds();
        Task<int> GetLeasingContractIdByContractNumber(string dsisContractNumber);
    }
}
