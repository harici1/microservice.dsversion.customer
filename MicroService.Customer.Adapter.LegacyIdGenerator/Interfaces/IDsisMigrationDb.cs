﻿using MicroService.Customer.Adapter.LegacyIdGenerator.Entities.Dsis;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MicroService.Customer.Adapter.LegacyIdGenerator.Interfaces
{
    public interface IDsisMigrationDb
    {
        Task BulkInsertSmsleasingContractExtension(List<int> ids, bool isDsis);
        Task<List<int>> GetListContractIds(bool isDsis);
        Task SmsleasingContractExtensionByLeasingContractId(int LeasingContractId, int AnkaContractId);
        Task<List<LeasingContract>> GetLeasingContractsByIdGeneratorStatus(int status);
        Task UpdateContractIdGeneratorStatus(List<LeasingContract> contracts, int status);
        Task UpdateContract(LeasingContract contract);
        Task AddError(SMSLeasingContractExtensionError entity);
    }
}
