﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Adapter.LegacyIdGenerator.Models
{
    public class AppSettings
    {
        public AppSettingsConnectionStrings ConnectionStrings { get; set; }
    }
    public class AppSettingsConnectionStrings
    {
        public string AnkaMigrationConnection { get; set; }
        public string AnkaLegacyConnection { get; set; }
        public string DsisMigrationDbConnection { get; set; }
        public string DsisLegacyConnection { get; set; }
    }
}
