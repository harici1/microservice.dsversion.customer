﻿using MicroService.Customer.Adapter.LegacyIdGenerator.Helpers;
using MicroService.Customer.Adapter.LegacyIdGenerator.Interfaces;
using MicroService.Customer.Adapter.LegacyIdGenerator.Models;
using MicroService.Customer.Adapter.LegacyIdGenerator.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace MicroService.Customer.Adapter.LegacyIdGenerator
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var host = new HostBuilder()
                       .ConfigureLogging((hostContext, config) =>
                       {
                           config.AddConsole();
                           config.AddDebug();
                       })
                       .ConfigureHostConfiguration(config =>
                       {
                           config.AddEnvironmentVariables();
                       })
                       .ConfigureAppConfiguration((hostContext, config) =>
                       {
                           config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                           config.AddJsonFile($"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json", optional: true, reloadOnChange: true);
                           config.AddCommandLine(args);
                       })
                         .ConfigureServices((hostContext, services) =>
                         {
                             services.AddLogging();
                             services.Configure<AppSettings>(hostContext.Configuration);

                             services.AddScoped<IAnkaLegacyDb, AnkaLegacyDbDapper>();
                             services.AddScoped<IAnkaMigrationDb, AnkaMigrationDbDapper>();
                             services.AddScoped<IDsisLegacyDb, DsisLegacyDbDapper>();
                             services.AddScoped<IDsisMigrationDb, DsisMigrationDbDapper>();

                             services.AddHostedService<AdaptorWorkerTask>();

                         })
                       .UseConsoleLifetime()
                       .Build();

            using (host)
            {
                // Start the host
                await host.StartAsync();

                // Wait for the host to shutdown
                await host.WaitForShutdownAsync();
            }
        }
    }
}
