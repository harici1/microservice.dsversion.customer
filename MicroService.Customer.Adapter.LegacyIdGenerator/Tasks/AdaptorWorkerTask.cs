﻿using MicroService.Customer.Adapter.LegacyIdGenerator.Entities.Enums;
using MicroService.Customer.Adapter.LegacyIdGenerator.Interfaces;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MicroService.Customer.Adapter.LegacyIdGenerator.Tasks
{
    public class AdaptorWorkerTask : BackgroundService
    {
        private readonly ILogger _logger;
        private readonly IDsisLegacyDb _dsisLegacyDb;
        private readonly IDsisMigrationDb _dsisMigrationDb;
        private readonly IAnkaMigrationDb _ankaMigrationDb;
        private readonly IAnkaLegacyDb _ankaLegacyDb;
        public AdaptorWorkerTask(ILogger<AdaptorWorkerTask> logger, IDsisLegacyDb dsisLegacyDb, IDsisMigrationDb dsisMigrationDb, IAnkaMigrationDb ankaMigrationDb, IAnkaLegacyDb ankaLegacyDb)
        {
            _logger = logger;
            _dsisLegacyDb = dsisLegacyDb;
            _dsisMigrationDb = dsisMigrationDb;
            _ankaMigrationDb = ankaMigrationDb;
            _ankaLegacyDb = ankaLegacyDb;
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("LegacyIdGenerator started.");

            stoppingToken.Register(() => _logger.LogInformation($" LegacyIdGenerator background task is stopping."));

            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation($"LegacyIdGenerator task doing background work.");

                await CreateSmsleasingContractServiceExtensionFromDsisMigration();
                await CreateSmsleasingContractServiceExtensionFromAnkaMigration();

                //await CreateSmsleasingContractServiceExtensionFromDsisLegacy(); -- yapıldı
                //await CreateSmsleasingContractServiceExtensionFromAnkaLegacy(); -- yapıldı


                await Task.Delay(1000);

                _logger.LogInformation($"LegacyIdGenerator task finished background work.");

            }

            _logger.LogInformation("LegacyIdGenerator stopped.");
        }

        #region Migration Db Extension

        private async Task CreateSmsleasingContractServiceExtensionFromDsisMigration()
        {
            var list = await _dsisMigrationDb.GetLeasingContractsByIdGeneratorStatus((int)LogTypeStatus.Waiting);
            if (list != null && list.Any())
            {
                await _dsisMigrationDb.UpdateContractIdGeneratorStatus(list, (int)LogTypeStatus.InProgress);

                var listExist = (await _dsisMigrationDb.GetListContractIds(true)).ToHashSet();

                var listOfInsert = new List<int>();
                for (int i = 0; i < list.Count; i++)
                {
                    if (!listExist.Contains(list[i].LeasingContractId))
                    {
                        listOfInsert.Add(list[i].LeasingContractId);
                    }
                }
                await _dsisMigrationDb.BulkInsertSmsleasingContractExtension(listOfInsert, true);
                await _dsisMigrationDb.UpdateContractIdGeneratorStatus(list, (int)LogTypeStatus.Success);
            }
        }
        private async Task CreateSmsleasingContractServiceExtensionFromAnkaMigration()
        {
            var list = await _ankaMigrationDb.GetContractsByIdGeneratorStatus((int)LogTypeStatus.Waiting);
            if (list != null && list.Any())
            {
                await _ankaMigrationDb.UpdateContractIdGeneratorStatus(list, (int)LogTypeStatus.InProgress);
                var listExist = (await _dsisMigrationDb.GetListContractIds(false)).ToHashSet();

                var listOfInsert = new List<int>();
                for (int i = 0; i < list.Count; i++)
                {
                    if (!listExist.Contains(list[i].ContractId))
                    {
                        var itemBundle = await _ankaLegacyDb.CheckBundleContractIfExist(list[i].ContractId);
                        if (itemBundle != null)
                        {
                            var LeasingContractId = await _dsisLegacyDb.GetLeasingContractIdByContractNumber(itemBundle.DsisHesapNo);
                            await _dsisMigrationDb.SmsleasingContractExtensionByLeasingContractId(LeasingContractId, itemBundle.ContractId);
                        }
                        else
                        {
                            listOfInsert.Add(list[i].ContractId);
                        }
                    }
                }
                await _dsisMigrationDb.BulkInsertSmsleasingContractExtension(listOfInsert, false);

                await _ankaMigrationDb.UpdateContractIdGeneratorStatus(list, (int)LogTypeStatus.Success);
            }
        }

        #endregion

        #region Canlı Db Extension

        private async Task CreateSmsleasingContractServiceExtensionFromDsisLegacy()
        {
            var list = await _dsisLegacyDb.GetLeasingContractIds();
            if (list != null && list.Any())
            {
                await _dsisMigrationDb.BulkInsertSmsleasingContractExtension(list, true);
            }
        }

        private async Task CreateSmsleasingContractServiceExtensionFromAnkaLegacy()
        {
            var listContractIds = await _ankaLegacyDb.GetContractIds();
            if (listContractIds != null && listContractIds.Any())
            {
                var listOfBundleContract = (await _ankaLegacyDb.GetBundleContracts()).ToLookup(x => x.ContractId);
                var listExist = (await _dsisMigrationDb.GetListContractIds(false)).ToHashSet();
                var listOfInsertContractIds = new List<int>();
                foreach (var contractId in listContractIds)
                {
                    if (!listExist.Contains(contractId))
                    {
                        if (listOfBundleContract[contractId].Any())
                        {
                            var itemBundle = listOfBundleContract[contractId].FirstOrDefault();
                            await _dsisMigrationDb.SmsleasingContractExtensionByLeasingContractId(await _dsisLegacyDb.GetLeasingContractIdByContractNumber(itemBundle.DsisHesapNo), itemBundle.ContractId);
                        }
                        else
                        {
                            listOfInsertContractIds.Add(contractId);
                        }
                    }

                }
                await _dsisMigrationDb.BulkInsertSmsleasingContractExtension(listOfInsertContractIds, false);
            }
        }

        #endregion
    }
}
