﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Adapter
{
    public static class ConnectionStringHelper
    {
        public static string GetAnkaLegacyString()
        {
            return "Server=10.243.1.229; Database=ANKA; Trusted_Connection=True;";
        }
        public static string GetDsisLegacyString()
        {
            return "Server=10.66.24.7; Database=SMS; Trusted_Connection=True;";
        }
        public static string GetAnkaMigrationString()
        {
            return "Server=10.243.1.229; Database=CRM_MIGRATION; Trusted_Connection=True;";
        }
        public static string GetDsisMigrationString()
        {
            return "Server=10.66.24.6; Database=CRM_MIGRATION; Trusted_Connection=True;";
        }
    }
}
