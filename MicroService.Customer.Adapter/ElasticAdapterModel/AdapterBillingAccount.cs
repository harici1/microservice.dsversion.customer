﻿namespace MicroService.Customer.Adapter.ElasticAdapterModel
{
    public class AdapterBillingAccount
    {
        public string BillingAccountId { get; set; }
        public string LegacyDsisLEasingContractID { get; set; }
        public string LegacyDsisLEasingContractNumber { get; set; }
        public string LegacyAnkaContractID { get; set; }
        public string LegacyAnkaContractNumber { get; set; }
    }
}
