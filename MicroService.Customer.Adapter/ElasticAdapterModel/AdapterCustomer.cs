﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Adapter.ElasticAdapterModel
{
    public class AdapterCustomer
    {
        public string CustomerID { get; set; }
        public string LegacyDsisPartyID { get; set; }
        public string LegacyAnkaCustomerID { get; set; }


    }
}
