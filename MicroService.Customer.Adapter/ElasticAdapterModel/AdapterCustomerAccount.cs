﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Adapter.ElasticAdapterModel
{
    public class AdapterCustomerAccount
    {
        public string CustomerAccountID { get; set; }
        public string LegacyDsisLEasingContractID { get; set; }
        public string LegacyDsisLEasingContractNumber { get; set; }
        public string LegacyAnkaContractID { get; set; }
        public string LegacyAnkaContractNumber { get; set; }
    }
}
