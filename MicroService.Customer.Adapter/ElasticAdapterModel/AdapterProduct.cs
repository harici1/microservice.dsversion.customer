﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Adapter.ElasticAdapterModel
{
    public class AdapterProduct
    {
        public string ProductID { get; set; }
        public string LegacyDsisLeasingContractServiceID { get; set; }
        public string LegacyAnkaContractDetailID { get; set; }
    }
}
