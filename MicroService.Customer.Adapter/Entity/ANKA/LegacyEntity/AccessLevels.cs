﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class AccessLevels
    {
        public int Id { get; set; }
        public string AccesslevelName { get; set; }
    }
}
