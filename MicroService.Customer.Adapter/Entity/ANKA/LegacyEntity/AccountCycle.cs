﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class AccountCycle
    {
        public int AccountCycleId { get; set; }
        public string DisplayName { get; set; }
        public DateTime CycleStartDate { get; set; }
        public DateTime CycleEndDate { get; set; }
        public bool? Active { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool? Visibility { get; set; }
    }
}
