﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class AccountManager
    {
        public int AccountManagerId { get; set; }
        public int ProvinceId { get; set; }
        public int CityId { get; set; }
        public string Username { get; set; }
        public DateTime CreateDate { get; set; }
        public bool? Active { get; set; }
        public string ManagerName { get; set; }
        public string Region { get; set; }
        public int RepId { get; set; }
    }
}
