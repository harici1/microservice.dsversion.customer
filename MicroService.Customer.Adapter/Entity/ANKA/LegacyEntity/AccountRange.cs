﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class AccountRange
    {
        public int Id { get; set; }
        public int? AccountRangeTypeId { get; set; }
        public int? AccountRangeId { get; set; }
        public int? InitialValue { get; set; }
        public int? EndValue { get; set; }
        public double? Amount { get; set; }
        public bool? DefaultRange { get; set; }
        public double? Bundle { get; set; }
        public DateTime? RangeStartDate { get; set; }
        public DateTime? RangeEndDate { get; set; }
        public string ControlType { get; set; }
        public string Description { get; set; }
        public int? FirstPeriod { get; set; }
        public int? LastPeriod { get; set; }
        public bool IsBundle { get; set; }
    }
}
