﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class AccountRangeType
    {
        public int AccountRangeTypeId { get; set; }
        public string DisplayName { get; set; }
        public string RangeGroup { get; set; }
    }
}
