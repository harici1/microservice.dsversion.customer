﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class AccountType
    {
        public int AccountTypeId { get; set; }
        public string DisplayName { get; set; }
        public bool Active { get; set; }
    }
}
