﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Acs
    {
        public int AcsId { get; set; }
        public int ContractDetailId { get; set; }
        public string UniqueIdentifierNo { get; set; }
        public string RealmName { get; set; }
        public string DomainName { get; set; }
        public string PppUserName { get; set; }
        public string PppPassword { get; set; }
        public bool VoipEnable { get; set; }
        public string VoipUname { get; set; }
        public string VoipPword { get; set; }
        public string VoipUri { get; set; }
        public bool WifiEnable { get; set; }
        public string WifiPreshared { get; set; }
        public string WifiSecurity { get; set; }
        public string WifiSsid { get; set; }
        public string ServiceName { get; set; }
        public string CommandName { get; set; }
        public string StatusName { get; set; }
        public bool? Status { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
