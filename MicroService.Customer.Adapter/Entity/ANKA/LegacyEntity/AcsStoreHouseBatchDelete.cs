﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class AcsStoreHouseBatchDelete
    {
        public int Id { get; set; }
        public string UniqueIdentifierNo { get; set; }
        public int Period { get; set; }
        public bool Status { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
