﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class AcsStorehouse
    {
        public int Id { get; set; }
        public string UniqueIdentifierNo { get; set; }
        public int? ContractDetailId { get; set; }
        public bool Status { get; set; }
        public bool? AcsLinkExists { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public bool IsWireless { get; set; }
        public string Notes { get; set; }
    }
}
