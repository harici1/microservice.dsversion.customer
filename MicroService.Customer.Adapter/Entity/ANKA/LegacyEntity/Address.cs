﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Address
    {
        public Address()
        {
            AddressDetail = new HashSet<AddressDetail>();
            Contract = new HashSet<Contract>();
            ServiceRequest = new HashSet<ServiceRequest>();
            XdslTransferNewAddress = new HashSet<XdslTransfer>();
            XdslTransferOrigAddress = new HashSet<XdslTransfer>();
        }

        public int AddressId { get; set; }
        public string RefType { get; set; }
        public string RefParameter { get; set; }
        public string StreetAddress { get; set; }
        public string ZipCode { get; set; }
        public int? CityId { get; set; }
        public int? ProvinceId { get; set; }
        public int? CountryId { get; set; }
        public bool DefaultAdress { get; set; }
        public bool? Active { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public string ModifyBy { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public int? DistrictId { get; set; }

        public virtual ICollection<AddressDetail> AddressDetail { get; set; }
        public virtual ICollection<Contract> Contract { get; set; }
        public virtual ICollection<ServiceRequest> ServiceRequest { get; set; }
        public virtual ICollection<XdslTransfer> XdslTransferNewAddress { get; set; }
        public virtual ICollection<XdslTransfer> XdslTransferOrigAddress { get; set; }
    }
}
