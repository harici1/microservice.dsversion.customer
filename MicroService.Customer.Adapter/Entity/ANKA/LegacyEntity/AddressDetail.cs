﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class AddressDetail
    {
        public int AddressDetailId { get; set; }
        public int AddressId { get; set; }
        public int AddressDetailTypeId { get; set; }
        public string Value { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? LastModifyDate { get; set; }

        public virtual Address Address { get; set; }
        public virtual AddressDetailType AddressDetailType { get; set; }
    }
}
