﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class AddressDetailType
    {
        public AddressDetailType()
        {
            AddressDetail = new HashSet<AddressDetail>();
        }

        public int AddressDetailTypeId { get; set; }
        public string TypeName { get; set; }
        public string Description { get; set; }

        public virtual ICollection<AddressDetail> AddressDetail { get; set; }
    }
}
