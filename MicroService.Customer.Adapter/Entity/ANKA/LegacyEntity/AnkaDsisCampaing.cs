﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class AnkaDsisCampaing
    {
        public int Id { get; set; }
        public int AnkaCatalogItemId { get; set; }
        public int AnkaCampaingId { get; set; }
        public int? DsmartPackageId { get; set; }
        public int DsmartCampaingId { get; set; }
        public bool? Sd { get; set; }
        public bool? Sdplus { get; set; }
        public bool? Sdpvr { get; set; }
        public bool? Hd { get; set; }
        public bool? Hdpvr { get; set; }
        public bool? Cam { get; set; }
        public string Attribute1 { get; set; }
        public string Attribute2 { get; set; }
        public DateTime StartDate { get; set; }
        public string CreateBy { get; set; }
        public string ModifyBy { get; set; }
        public bool? Active { get; set; }
    }
}
