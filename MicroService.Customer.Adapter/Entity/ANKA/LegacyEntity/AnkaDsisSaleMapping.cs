﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class AnkaDsisSaleMapping
    {
        public int Id { get; set; }
        public string AnkaCustomerNo { get; set; }
        public string AnkaContractNo { get; set; }
        public string DsmartCustomerNo { get; set; }
        public bool Status { get; set; }
        public bool MappingRelationUpdate { get; set; }
        public DateTime ProcessStartDate { get; set; }
        public DateTime? ProcessFinishDate { get; set; }
        public string HostIp { get; set; }
        public string XmlObject { get; set; }
        public string ProcessResult { get; set; }
        public bool Dirty { get; set; }
    }
}
