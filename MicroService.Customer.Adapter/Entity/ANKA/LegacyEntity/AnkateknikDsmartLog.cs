﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class AnkateknikDsmartLog
    {
        public int Rid { get; set; }
        public string IslemAyraci { get; set; }
        public string IslemFaturaDonemiKontrolNotu { get; set; }
        public string IslemiYapanKisi { get; set; }
        public string IslemTipi { get; set; }
        public bool? IslemeUygunMu { get; set; }
        public bool? Islendi { get; set; }
        public string IslemAciklama { get; set; }
        public DateTime? IslemeTarihi { get; set; }
        public DateTime? KayitTarihi { get; set; }
        public string HesapNo { get; set; }
        public string FatutaIcinGecerliTarih { get; set; }
        public int? SonFaturaYili { get; set; }
        public int? SonFaturaAyi { get; set; }
        public int? ContractId { get; set; }
        public int? ContractStatusId { get; set; }
        public int? DsmartContractDetailId { get; set; }
        public int? DsmartCatalogItemId { get; set; }
        public int? DsmartContractDetailStatusId { get; set; }
        public int? DsmartContractDetailPricePlanId { get; set; }
        public int? DsmartContractDetailCampaignId { get; set; }
        public int? AdslContractDetailId { get; set; }
        public int? AdslCatalogItemId { get; set; }
        public int? AdslContractDetailStatusId { get; set; }
        public int? AdslContractDetailPricePlanId { get; set; }
        public int? AdslContractDetailCampaignId { get; set; }
        public bool? AdslVarMi { get; set; }
        public bool? AdslTemsiliVarMi { get; set; }
        public int? ModemContractDetailId { get; set; }
        public int? ModemCatalogItemId { get; set; }
        public int? ModemContractDetailStatusId { get; set; }
        public int? ModemContractDetailPricePlanId { get; set; }
        public int? ModemContractDetailCampaignId { get; set; }
        public bool? ModemVarMi { get; set; }
        public bool? ModemGonderilecekMi { get; set; }
        public int? ModemDeliveryId { get; set; }
        public int? ModemDeliveryDetailId { get; set; }
        public int? BtipiContractDetailId { get; set; }
        public int? EmailContractDetailId { get; set; }
        public int? NewDsmartContractDetailId { get; set; }
        public int? NewDsmartCatalogItemId { get; set; }
        public int? NewDsmartContractDetailStatusId { get; set; }
        public int? NewDsmartContractDetailPricePlanId { get; set; }
        public int? NewAdslContractDetailId { get; set; }
        public int? NewAdslCatalogItemId { get; set; }
        public int? NewAdslContractDetailStatusId { get; set; }
        public int? NewAdslContractDetailPricePlanId { get; set; }
        public string NewAdslParamPhoneNum { get; set; }
        public bool? NewAdslParamPhoneNumAnkaControl { get; set; }
        public string NewAdslParamDomain { get; set; }
        public string NewAdslParamUser { get; set; }
        public string NewAdslParamPassword { get; set; }
        public bool? NewAdslRadiusControl { get; set; }
        public bool? NewAdslCrmControl { get; set; }
        public int? NewModemContractDetailId { get; set; }
        public int? NewModemCatalogItemId { get; set; }
        public int? NewModemContractDetailStatusId { get; set; }
        public int? NewModemContractDetailPricePlanId { get; set; }
        public int? NewModemDeliveryId { get; set; }
        public int? NewModemDeliveryDetailId { get; set; }
        public int? NewModemAddressId { get; set; }
        public int? NewBtipiCatalogItemId { get; set; }
        public int? NewBtipiContractDetailId { get; set; }
        public int? NewBtipiContractDetailStatusId { get; set; }
        public int? NewBtipiContractDetailPricePlanId { get; set; }
        public int? NewEmailCatalogItemId { get; set; }
        public int? NewEmailContractDetailId { get; set; }
        public int? NewEmailContractDetailStatusId { get; set; }
        public int? NewEmailContractDetailPricePlanId { get; set; }
        public string NewEmailAddress { get; set; }
        public string NewEmailPassword { get; set; }
    }
}
