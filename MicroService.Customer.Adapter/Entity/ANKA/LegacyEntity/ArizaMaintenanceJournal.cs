﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ArizaMaintenanceJournal
    {
        public int Id { get; set; }
        public int? PusulaTicketId { get; set; }
        public int? AnkaTicketId { get; set; }
        public string PstnNo { get; set; }
        public string XdslNo { get; set; }
        public int? IlId { get; set; }
        public int? AdslStatusId { get; set; }
        public string ReferansNo { get; set; }
        public string ArizaDurumu { get; set; }
        public DateTime? BildirimZamani { get; set; }
        public string KaynakSistem { get; set; }
        public string KapamaKodu { get; set; }
        public DateTime? KapamaZamani { get; set; }
        public DateTime? SorguZamani { get; set; }
        public string TeyitSayisi { get; set; }
        public string IslemSonucKodu { get; set; }
        public string GecenSure { get; set; }
        public string RezervasyonId { get; set; }
        public DateTime? RezervasyonZamani { get; set; }
        public string OnMuayeneKodu { get; set; }
        public string SonMuayeneKodu { get; set; }
        public string TeyitDurumu { get; set; }
        public string SorguStatus { get; set; }
        public string ResponseReferansNo { get; set; }
        public string RandevuRezervasyonId { get; set; }
        public DateTime? RandevuRezervasyonDate { get; set; }
        public bool? RandevuGerekli { get; set; }
        public string RandevuKapamaKodu { get; set; }
        public string RandevuIsletmeciArizaId { get; set; }
        public string RandevuXdslNo { get; set; }
        public string RfsType { get; set; }
        public string Operation { get; set; }
        public string Ekip { get; set; }
        public string Gorev { get; set; }
        public DateTime? GorevBildirimZamani { get; set; }
        public string IsEmriId { get; set; }
        public string IslemAciklamasi { get; set; }
        public DateTime? RandevuZamani { get; set; }
        public DateTime? TamamlanmaZamani { get; set; }
        public string EkipTipi { get; set; }
        public string IslemSonucKoduGrubu { get; set; }
        public string MusterininBagliOlduguMudurluk { get; set; }
        public int? SlaIci { get; set; }
        public string IslemKalemleri { get; set; }
    }
}
