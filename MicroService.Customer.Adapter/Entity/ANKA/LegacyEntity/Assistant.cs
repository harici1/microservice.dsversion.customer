﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Assistant
    {
        public int Id { get; set; }
        public int ModuleId { get; set; }
        public int ModuleDetailId { get; set; }
        public int? RoleId { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public bool? Active { get; set; }
        public DateTime CreateDate { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public int? ModifyBy { get; set; }
    }
}
