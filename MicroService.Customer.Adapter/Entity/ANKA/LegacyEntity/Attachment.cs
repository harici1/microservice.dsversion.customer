﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Attachment
    {
        public Attachment()
        {
            WfqueueTaskAttachment = new HashSet<WfqueueTaskAttachment>();
        }

        public int Id { get; set; }
        public string AttachName { get; set; }
        public double? AttachLenght { get; set; }
        public string AttachType { get; set; }
        public DateTime? CreateDate { get; set; }
        public string AttachPath { get; set; }

        public virtual ICollection<WfqueueTaskAttachment> WfqueueTaskAttachment { get; set; }
    }
}
