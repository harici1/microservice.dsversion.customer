﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class AttrepTruncationSafeguard
    {
        public string LatchTaskName { get; set; }
        public string LatchMachineGuid { get; set; }
        public string LatchKey { get; set; }
        public DateTime LatchLocker { get; set; }
    }
}
