﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Authentication
    {
        public int Id { get; set; }
        public int? ModuleId { get; set; }
        public int? ModuleDetailId { get; set; }
        public int? RoleId { get; set; }
        public int? AuthSelect { get; set; }
        public int? AuthInsert { get; set; }
        public int? AuthUpdate { get; set; }
        public int? AuthApprove { get; set; }
        public int? AuthDelete { get; set; }
        public int? Status { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
