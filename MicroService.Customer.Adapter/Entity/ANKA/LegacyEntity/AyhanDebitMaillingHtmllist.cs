﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class AyhanDebitMaillingHtmllist
    {
        public int Id { get; set; }
        public string ContractNo { get; set; }
        public string AboneNo { get; set; }
        public string Title { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public string Cycle { get; set; }
        public string InvoiceAmount { get; set; }
        public string EmailHtmlText { get; set; }
        public bool SentStatus { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? FinishDate { get; set; }
    }
}
