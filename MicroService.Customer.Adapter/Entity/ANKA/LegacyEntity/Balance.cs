﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Balance
    {
        public long BalanceId { get; set; }
        public int? RefParameter { get; set; }
        public string RefType { get; set; }
        public int CustomerId { get; set; }
        public int? ContractId { get; set; }
        public decimal? Amount { get; set; }
        public decimal? CurrentAmount { get; set; }
        public string Description { get; set; }
        public DateTime? ProcessDate { get; set; }
        public int? Status { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
