﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class BalanceProblemFix
    {
        public int Id { get; set; }
        public int ContractId { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsProcess { get; set; }
    }
}
