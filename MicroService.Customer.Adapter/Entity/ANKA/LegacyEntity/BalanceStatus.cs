﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class BalanceStatus
    {
        public int BalanceStatusId { get; set; }
        public string Description { get; set; }
    }
}
