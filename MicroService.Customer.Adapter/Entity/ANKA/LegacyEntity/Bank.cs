﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Bank
    {
        public Bank()
        {
            BankCreditCardInstallmentOption = new HashSet<BankCreditCardInstallmentOption>();
        }

        public int BankId { get; set; }
        public string BankCode { get; set; }
        public string BankName { get; set; }
        public bool? Active { get; set; }

        public virtual ICollection<BankCreditCardInstallmentOption> BankCreditCardInstallmentOption { get; set; }
    }
}
