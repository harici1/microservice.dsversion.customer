﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class BankAgreementRequestLog
    {
        public int RequestId { get; set; }
        public int BankCode { get; set; }
        public DateTime AgreementDate { get; set; }
        public int PaymentUnit { get; set; }
        public decimal PaymentTotal { get; set; }
        public int CancelUnit { get; set; }
        public decimal CancelTotal { get; set; }
        public int DirectionUnit { get; set; }
        public decimal DirectionTotal { get; set; }
        public DateTime RecordDate { get; set; }
    }
}
