﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class BankAgreementResponseLog
    {
        public int ResponseId { get; set; }
        public int RequestId { get; set; }
        public DateTime AgreementDate { get; set; }
        public int PaymentUnit { get; set; }
        public decimal PaymentTotal { get; set; }
        public int CancelUnit { get; set; }
        public decimal CancelTotal { get; set; }
        public int DirectionUnit { get; set; }
        public decimal DirectionTotal { get; set; }
        public string ReturnCode { get; set; }
        public string ReturnDesc { get; set; }
        public DateTime RecordDate { get; set; }
    }
}
