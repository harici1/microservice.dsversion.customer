﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class BankCreditCardInstallmentOption
    {
        public int BankId { get; set; }
        public int InstallmentCount { get; set; }

        public virtual Bank Bank { get; set; }
    }
}
