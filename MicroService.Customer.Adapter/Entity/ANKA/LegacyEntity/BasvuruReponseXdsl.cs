﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class BasvuruReponseXdsl
    {
        public int BasvuruReponseXdslid { get; set; }
        public long? KurumId { get; set; }
        public string Il { get; set; }
        public string Mudurluk { get; set; }
        public string Sira { get; set; }
        public string MusteriNo { get; set; }
        public string KullaniciId { get; set; }
        public DateTime? BasvuruTarihi { get; set; }
        public DateTime? SonGuncellemeTarihi { get; set; }
        public string XdslNo { get; set; }
        public string StaticIp { get; set; }
        public long? Hiz { get; set; }
        public string IptalTesisBasvuru { get; set; }
        public int? BasvuruAsama { get; set; }
        public int? IsTuru { get; set; }
        public string AltIsTuru { get; set; }
        public int? PaketId { get; set; }
        public int? TarifeTuru { get; set; }
        public int? TransactionId { get; set; }
        public int? IptalSebepId { get; set; }
        public int? IptalSebepKodu { get; set; }
        public string IptalAciklama { get; set; }
        public string Kurulum { get; set; }
        public string ReklamasyonKodu { get; set; }
        public string ReklamasyonAciklama { get; set; }
        public string Speed { get; set; }
        public string NoiseMargin { get; set; }
        public string Attenuation { get; set; }
        public string EmailAddress { get; set; }
        public string Gsmno { get; set; }
        public string HatFizikselDurum { get; set; }
        public string HatYonetimselDurum { get; set; }
        public string DownloadNoiseMargin { get; set; }
        public string UploadNoiseMargin { get; set; }
        public string DownloadCurrentSpeed { get; set; }
        public string UploadCurrentSpeed { get; set; }
        public string DownloadCapacitySpeed { get; set; }
        public string UploadCapacitySpeed { get; set; }
        public string DownloadThroughput { get; set; }
        public string UploadThroughput { get; set; }
        public string DownloadAttenuation { get; set; }
        public string UploadAttenuation { get; set; }
        public string NmsprofileSpeed { get; set; }
        public bool? OlcumCihaziYok { get; set; }
        public bool? MusteriModemiYok { get; set; }
        public bool? MusteriModemiArizali { get; set; }
        public bool? PortTestiYapilamiyor { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool? IsControlled { get; set; }
        public DateTime? ControlDate { get; set; }
    }
}
