﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Batch
    {
        public int BatchId { get; set; }
        public string Name { get; set; }
        public int BatchTypeId { get; set; }
        public DateTime BatchPeriod { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public int? BatchStatusId { get; set; }
    }
}
