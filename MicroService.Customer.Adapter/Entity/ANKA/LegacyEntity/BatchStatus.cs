﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class BatchStatus
    {
        public int BatchStatusId { get; set; }
        public string Description { get; set; }
    }
}
