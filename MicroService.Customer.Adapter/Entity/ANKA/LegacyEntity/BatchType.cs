﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class BatchType
    {
        public int BatchTypeId { get; set; }
        public string Name { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public int? BatchTypeCategoryId { get; set; }
        public int? BatchTypeCategoryGroupId { get; set; }
    }
}
