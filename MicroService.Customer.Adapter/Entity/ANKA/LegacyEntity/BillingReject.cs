﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class BillingReject
    {
        public int Id { get; set; }
        public string RejectType { get; set; }
        public string RejectRefParameter { get; set; }
        public string ErrorMessage { get; set; }
        public int? CustomerId { get; set; }
        public int? ContractId { get; set; }
        public int? CycleId { get; set; }
        public string Username { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
