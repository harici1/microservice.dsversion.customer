﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Bonus2
    {
        public int BonusId { get; set; }
        public int? ParentId { get; set; }
        public int? CustomerId { get; set; }
        public int? ContractId { get; set; }
        public int? ContractDetailId { get; set; }
        public int? BonusTypeId { get; set; }
        public int? InvoiceAssessmentId { get; set; }
        public int? PricePlanDetailId { get; set; }
        public int? BonusPricePlanDetailId { get; set; }
        public decimal? Amount { get; set; }
        public decimal? UsedAmount { get; set; }
        public string Description { get; set; }
        public int? Status { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string ModifyBy { get; set; }
    }
}
