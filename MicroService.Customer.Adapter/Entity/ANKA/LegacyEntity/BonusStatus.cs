﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class BonusStatus
    {
        public int Status { get; set; }
        public string Name { get; set; }
    }
}
