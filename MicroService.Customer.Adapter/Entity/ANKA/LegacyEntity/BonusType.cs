﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class BonusType
    {
        public int BonusTypeId { get; set; }
        public string BonusTypeName { get; set; }
        public string ComputionType { get; set; }
        public decimal? ComputionAmount { get; set; }
        public int? Priority { get; set; }
        public decimal? Quantity { get; set; }
        public string Active { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string ModifyBy { get; set; }
    }
}
