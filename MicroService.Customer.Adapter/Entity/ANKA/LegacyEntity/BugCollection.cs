﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class BugCollection
    {
        public int Id { get; set; }
        public string BugName { get; set; }
        public string BugUrl { get; set; }
        public string Problem { get; set; }
        public string FileName { get; set; }
        public string Solution { get; set; }
        public int Status { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string CloseBy { get; set; }
        public DateTime? CloseDate { get; set; }
    }
}
