﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class BulkContractDocumentSuspend
    {
        public int RowId { get; set; }
        public int ContractId { get; set; }
        public DateTime? InsertDate { get; set; }
        public int? ContractStatusId { get; set; }
        public int InterimState { get; set; }
        public bool IsSmsSend { get; set; }
        public bool IsSignature { get; set; }
        public bool IsSuspend { get; set; }
        public bool IsCancel { get; set; }
        public bool IsCcActive { get; set; }
        public bool IsProcess { get; set; }
        public bool IsFinalize { get; set; }
        public DateTime? SmsSendDate { get; set; }
        public DateTime? SuspendDate { get; set; }
        public DateTime? CancelDate { get; set; }
        public DateTime? CcActiveDate { get; set; }
        public DateTime? ProcessDate { get; set; }
        public DateTime? FinalizeDate { get; set; }
        public string Result { get; set; }
        public string ModifyBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public bool? IsDelivery { get; set; }
    }
}
