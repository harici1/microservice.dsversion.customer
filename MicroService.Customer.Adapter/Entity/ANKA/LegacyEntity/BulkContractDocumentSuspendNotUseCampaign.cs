﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class BulkContractDocumentSuspendNotUseCampaign
    {
        public int CampaignId { get; set; }
        public string CampaignName { get; set; }
    }
}
