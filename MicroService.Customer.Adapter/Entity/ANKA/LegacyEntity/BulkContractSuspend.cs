﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class BulkContractSuspend
    {
        public int RowId { get; set; }
        public int ContractId { get; set; }
        public DateTime? InsertDate { get; set; }
        public DateTime? ProcessDate { get; set; }
        public bool Processed { get; set; }
        public int? ContractStatusId { get; set; }
        public int? XDaysPastDueDateOpenInvoiceCount { get; set; }
        public decimal? XDaysPastDueDateRemainingAmount { get; set; }
        public int InterimState { get; set; }
        public bool TriggeredSuspend { get; set; }
        public string ContractNo { get; set; }
        public bool? Migrated { get; set; }
        public bool? Dsmart { get; set; }
        public bool? Pe { get; set; }
    }
}
