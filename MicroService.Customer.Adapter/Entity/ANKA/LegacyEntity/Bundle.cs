﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Bundle
    {
        public int CatalogItemId { get; set; }
        public int? CampaignId { get; set; }
        public int BundledItemId { get; set; }
        public int PricePlanId { get; set; }
        public bool IsBoundToCampaign { get; set; }
        public string BundlePolicy { get; set; }
        public string UnbundlePolicy { get; set; }
        public string RebundlePolicy { get; set; }
        public bool? Active { get; set; }
        public int Id { get; set; }

        public virtual CatalogItem BundledItem { get; set; }
        public virtual Campaign Campaign { get; set; }
        public virtual CatalogItem CatalogItem { get; set; }
        public virtual PricePlan PricePlan { get; set; }
    }
}
