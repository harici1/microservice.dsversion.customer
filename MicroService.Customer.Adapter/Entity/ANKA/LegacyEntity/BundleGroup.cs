﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class BundleGroup
    {
        public int Id { get; set; }
        public int BundleGroupId { get; set; }
        public int CatalogItemId { get; set; }
        public int CampaignId { get; set; }
    }
}
