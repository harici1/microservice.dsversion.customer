﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CallTree
    {
        public CallTree()
        {
            TicketProblem = new HashSet<Ticket>();
            TicketResolution = new HashSet<Ticket>();
            TicketRouting = new HashSet<TicketRouting>();
        }

        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? TypeId { get; set; }
        public bool? Active { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual CallTreeItemType Type { get; set; }
        public virtual TicketEscalation TicketEscalation { get; set; }
        public virtual ICollection<Ticket> TicketProblem { get; set; }
        public virtual ICollection<Ticket> TicketResolution { get; set; }
        public virtual ICollection<TicketRouting> TicketRouting { get; set; }
    }
}
