﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CallTreeItemType
    {
        public CallTreeItemType()
        {
            CallTree = new HashSet<CallTree>();
        }

        public int Id { get; set; }
        public string TypeName { get; set; }
        public string ImageUrl { get; set; }

        public virtual ICollection<CallTree> CallTree { get; set; }
    }
}
