﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CampaignCommitment
    {
        public int Id { get; set; }
        public int CampaignId { get; set; }
        public bool? Active { get; set; }
        public bool? IsCommitmentControl { get; set; }
        public int? RequestTypeId { get; set; }

        public virtual Campaign Campaign { get; set; }
        public virtual RequestType RequestType { get; set; }
    }
}
