﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CampaignContract
    {
        public int CampaignContractId { get; set; }
        public int? ContractId { get; set; }
        public int? CampaignId { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? CampaignPricePlanId { get; set; }
    }
}
