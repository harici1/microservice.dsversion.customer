﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CampaignDetail
    {
        public int CampaignId { get; set; }
        public int CatalogItemId { get; set; }
        public int PricePlanId { get; set; }

        public virtual Campaign Campaign { get; set; }
        public virtual PricePlan PricePlan { get; set; }
    }
}
