﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CampaignGroup
    {
        public int CampaignGroupId { get; set; }
        public string WebDescription { get; set; }
        public bool WebVisibility { get; set; }
        public int WebOrderId { get; set; }
    }
}
