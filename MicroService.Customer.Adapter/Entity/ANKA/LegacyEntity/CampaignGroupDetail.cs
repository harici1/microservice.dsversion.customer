﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CampaignGroupDetail
    {
        public int CampaignGroupId { get; set; }
        public int CampaignId { get; set; }
        public bool? HaveModem { get; set; }
        public bool? HaveWirelessModem { get; set; }
        public bool? Active { get; set; }
        public string CampaignWebDisplayName { get; set; }
    }
}
