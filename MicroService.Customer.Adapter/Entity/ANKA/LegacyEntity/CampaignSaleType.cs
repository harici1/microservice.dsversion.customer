﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CampaignSaleType
    {
        public int CampaignSaleTypeId { get; set; }
        public int? RelationCampaignId { get; set; }
        public int? IsBundle { get; set; }
        public string CampaignType { get; set; }
    }
}
