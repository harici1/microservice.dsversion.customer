﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CamundaProcessContractRelation
    {
        public int CamundaProcessContractRelationId { get; set; }
        public string CamundaProcessId { get; set; }
        public int ContractId { get; set; }
        public int ContractDetailId { get; set; }
        public int ProcessStatusId { get; set; }
    }
}
