﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CargoIntegrationDeliveryReport
    {
        public int Id { get; set; }
        public int? QueueTaskId { get; set; }
        public string CargoCompany { get; set; }
        public string DeliveryNo { get; set; }
        public string ArrivalTrCenterName { get; set; }
        public string ArrivalTrCenterUnitId { get; set; }
        public string ArrivalUnitId { get; set; }
        public string ArrivalUnitName { get; set; }
        public string CargoEventExplanation { get; set; }
        public string CargoEventId { get; set; }
        public string CargoKey { get; set; }
        public string CargoReasonExplanation { get; set; }
        public string CargoReasonId { get; set; }
        public string CargoType { get; set; }
        public string CargoTypeExplanation { get; set; }
        public string CargoContractId { get; set; }
        public string DelEmpId { get; set; }
        public string DelEmpName { get; set; }
        public string DelInfoDeliveryFlag { get; set; }
        public string DelInfoDelUnitId { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public string DeliveryType { get; set; }
        public string DeliveryTypeExplanation { get; set; }
        public string DeliveryUnitId { get; set; }
        public string DeliveryUnitName { get; set; }
        public string DeliveryUnitType { get; set; }
        public string DelUnitTypeExplanation { get; set; }
        public string DepartureTrCenterName { get; set; }
        public string DepartureTrCenterUnitId { get; set; }
        public string DepartureUnitId { get; set; }
        public string DepartureUnitName { get; set; }
        public string DocId { get; set; }
        public string DocNumber { get; set; }
        public string DocType { get; set; }
        public string DocTypeExplanation { get; set; }
        public DateTime? DocumentDate { get; set; }
        public string DocumentDebitId { get; set; }
        public string DocumentDelFlag { get; set; }
        public string DocumentEventExplanation { get; set; }
        public string DocumentEventId { get; set; }
        public string DocumentReasonExplanation { get; set; }
        public string DocumentReasonId { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceDate { get; set; }
        public string PickupType { get; set; }
        public string PickupTypeExplanation { get; set; }
        public string Product { get; set; }
        public string ReceiverAddressTxt { get; set; }
        public string ReceiverCustId { get; set; }
        public string ReceiverCustName { get; set; }
        public string ReceiverInfo { get; set; }
        public string DeliveredTo { get; set; }
        public string SenderAddressTxt { get; set; }
        public string SenderCustId { get; set; }
        public bool? IsDrawback { get; set; }
        public string PaymentType { get; set; }
        public string RejectDescription { get; set; }
        public string RejectReasonExplanation { get; set; }
        public string RejectStatus { get; set; }
        public string RejectStatusExplanation { get; set; }
        public DateTime? ReturnDeliveryDate { get; set; }
        public string ReturnDocId { get; set; }
        public DateTime? ReturnDocumentDate { get; set; }
        public string ReturnStatus { get; set; }
        public decimal? TotalAmount { get; set; }
        public int? TotalCargo { get; set; }
        public decimal? TotalDesi { get; set; }
        public decimal? TotalDesiKg { get; set; }
        public decimal? TotalKg { get; set; }
        public decimal? TotalPrice { get; set; }
        public decimal? TotalVat { get; set; }
        public string WayBillNo { get; set; }
        public DateTime RecordDate { get; set; }
    }
}
