﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Catalog
    {
        public Catalog()
        {
            CatalogItem = new HashSet<CatalogItem>();
        }

        public int CatalogId { get; set; }
        public int CompanyId { get; set; }

        public virtual Company Company { get; set; }
        public virtual ICollection<CatalogItem> CatalogItem { get; set; }
    }
}
