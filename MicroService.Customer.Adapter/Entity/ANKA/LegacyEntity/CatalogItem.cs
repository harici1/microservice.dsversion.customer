﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CatalogItem
    {
        public CatalogItem()
        {
            BundleBundledItem = new HashSet<Bundle>();
            BundleCatalogItem = new HashSet<Bundle>();
            CatalogItemCategoryDetail = new HashSet<CatalogItemCategoryDetail>();
            CatalogItemPropertyValue = new HashSet<CatalogItemPropertyValue>();
            InvoiceAssessment = new HashSet<InvoiceAssessment>();
            PricePlan = new HashSet<PricePlan>();
            ProductAddonAddonCatalogItem = new HashSet<ProductAddon>();
            ProductAddonCatalogItem = new HashSet<ProductAddon>();
            VasAddOn = new HashSet<VasAddOn>();
            WfcontractDetailEventProcess = new HashSet<WfcontractDetailEventProcess>();
        }

        public int CatalogItemId { get; set; }
        public int ItemTypeId { get; set; }
        public string CatalogItemName { get; set; }
        public int CatalogId { get; set; }
        public bool? Active { get; set; }
        public bool List { get; set; }
        public bool? Intangible { get; set; }
        public int? PropertyTemplateId { get; set; }
        public int? ServiceParamId { get; set; }
        public string AdslSaleType { get; set; }
        public bool RequiresDelivery { get; set; }
        public bool? SetupServiceOffered { get; set; }
        public bool? SetupServiceMandatory { get; set; }
        public int? DisplayOrder { get; set; }
        public int? Grade { get; set; }
        public bool? Suspend { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public string WebDisplayCatalogItemName { get; set; }
        public string InvoicePrintDisplayCatalogItemName { get; set; }
        public string InvoicePrintDisplayOldCatalogItemName { get; set; }

        public virtual Catalog Catalog { get; set; }
        public virtual CatalogItemType ItemType { get; set; }
        public virtual PropertyTemplate PropertyTemplate { get; set; }
        public virtual ServiceParam ServiceParam { get; set; }
        public virtual ICollection<Bundle> BundleBundledItem { get; set; }
        public virtual ICollection<Bundle> BundleCatalogItem { get; set; }
        public virtual ICollection<CatalogItemCategoryDetail> CatalogItemCategoryDetail { get; set; }
        public virtual ICollection<CatalogItemPropertyValue> CatalogItemPropertyValue { get; set; }
        public virtual ICollection<InvoiceAssessment> InvoiceAssessment { get; set; }
        public virtual ICollection<PricePlan> PricePlan { get; set; }
        public virtual ICollection<ProductAddon> ProductAddonAddonCatalogItem { get; set; }
        public virtual ICollection<ProductAddon> ProductAddonCatalogItem { get; set; }
        public virtual ICollection<VasAddOn> VasAddOn { get; set; }
        public virtual ICollection<WfcontractDetailEventProcess> WfcontractDetailEventProcess { get; set; }
    }
}
