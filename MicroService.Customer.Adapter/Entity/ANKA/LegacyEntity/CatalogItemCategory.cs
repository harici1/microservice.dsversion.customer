﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CatalogItemCategory
    {
        public CatalogItemCategory()
        {
            CatalogItemCategoryDetail = new HashSet<CatalogItemCategoryDetail>();
        }

        public int CategoryId { get; set; }
        public string DisplayName { get; set; }
        public int? ParentCategoryId { get; set; }
        public bool? Active { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual ICollection<CatalogItemCategoryDetail> CatalogItemCategoryDetail { get; set; }
    }
}
