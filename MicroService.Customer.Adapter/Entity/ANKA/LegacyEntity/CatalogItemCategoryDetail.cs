﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CatalogItemCategoryDetail
    {
        public int CategoryId { get; set; }
        public int CatalogItemId { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual CatalogItem CatalogItem { get; set; }
        public virtual CatalogItemCategory Category { get; set; }
    }
}
