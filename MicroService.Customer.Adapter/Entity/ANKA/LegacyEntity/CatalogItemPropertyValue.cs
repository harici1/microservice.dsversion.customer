﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CatalogItemPropertyValue
    {
        public int CatalogItemPropertyValueId { get; set; }
        public int CatalogItemId { get; set; }
        public int PropertyId { get; set; }
        public string PropertyValue { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual CatalogItem CatalogItem { get; set; }
        public virtual Property Property { get; set; }
    }
}
