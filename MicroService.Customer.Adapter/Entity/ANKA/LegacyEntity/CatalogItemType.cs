﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CatalogItemType
    {
        public CatalogItemType()
        {
            CatalogItem = new HashSet<CatalogItem>();
        }

        public int ItemTypeId { get; set; }
        public string CatalogItemTypeName { get; set; }

        public virtual ICollection<CatalogItem> CatalogItem { get; set; }
    }
}
