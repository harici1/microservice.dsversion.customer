﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Cdrconfiguration
    {
        public int CdrconfigurationId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
