﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Cdrdata
    {
        public int CdrdataId { get; set; }
        public string Status { get; set; }
        public int? FailedRowCount { get; set; }
        public string TthesapTipi { get; set; }
        public string TthesapNo { get; set; }
        public string TtmusteriId { get; set; }
        public string TthesapKullanimDurumu { get; set; }
        public string TtadiSoyadi { get; set; }
        public string TtgercekTuzel { get; set; }
        public string TtfaturaDonemi { get; set; }
        public string TtfaturaId { get; set; }
        public string TtfaturaNo { get; set; }
        public string TttebligatIl { get; set; }
        public string TttebligatIlce { get; set; }
        public string TttebligatPosta { get; set; }
        public DateTime? TtfaturalamaTarihi { get; set; }
        public DateTime? TtsonrakiFaturalamaTarihi { get; set; }
        public DateTime? TtsonOdemeTarihi { get; set; }
        public DateTime? TtsonrakiSonOdemeTarihi { get; set; }
        public decimal? TtsabitUcreti { get; set; }
        public decimal? TtbaglantiUcreti { get; set; }
        public decimal? TtnakilUcreti { get; set; }
        public decimal? TtunvanDegisikligiUcreti { get; set; }
        public decimal? TtdevirUcreti { get; set; }
        public decimal? TtdevreHazirlamaUcreti { get; set; }
        public decimal? TtkotaAsimUcreti { get; set; }
        public string TtheiGunlukKullanim { get; set; }
        public decimal? TtkullanimUcreti { get; set; }
        public decimal? TtstatikIpucreti { get; set; }
        public decimal? TtndslAylikErisimUcreti { get; set; }
        public decimal? TthatDondurmaUcreti { get; set; }
        public decimal? TthatDondurmaKistUcreti { get; set; }
        public string Ttipdegisikligi { get; set; }
        public decimal? TthizTarifeDegisikligi { get; set; }
        public string TtkullaniciKimlikDegisikligi { get; set; }
        public decimal? TtndslerisimKistUcreti { get; set; }
        public decimal? TtndslgecisUcreti { get; set; }
        public decimal? TtelittSplitterUcreti { get; set; }
        public decimal? TtelittAnkastreUcreti { get; set; }
        public decimal? TtelittKablolamaUcreti { get; set; }
        public decimal? TtelittIscilikUcreti { get; set; }
        public decimal? TtelittModemUcreti { get; set; }
        public decimal? TthizDeneyimUcreti { get; set; }
        public decimal? TtmuteferrikUcretler { get; set; }
        public decimal? TtmahsupUcreti { get; set; }
        public decimal? TtgecikmeUcreti { get; set; }
        public decimal? TtacmaKapamaUcreti { get; set; }
        public decimal? TtkampanyaCezaUcreti { get; set; }
        public decimal? TtkampanyaIskontoUcreti { get; set; }
        public decimal? TtkampanyaCihazUcreti { get; set; }
        public decimal? TthstGeriOdemeUcreti { get; set; }
        public decimal? TtkistUcreti { get; set; }
        public decimal? TtiskontoUcreti { get; set; }
        public decimal? TtdamgaVergisiUcreti { get; set; }
        public decimal? TtkampanyaDamgaVergisiUcreti { get; set; }
        public decimal? TtportUcreti { get; set; }
        public decimal? Ttl2tpyedeklilikUcreti { get; set; }
        public decimal? Ttkdv18Ucreti { get; set; }
        public decimal? Ttoiv15Ucreti { get; set; }
        public decimal? Ttoiv5Ucreti { get; set; }
        public decimal? TtmatrahUcreti { get; set; }
        public decimal? TtgenelToplamUcreti { get; set; }
        public decimal? TtaraToplamUcreti { get; set; }
        public decimal? TtodenecekBorcUcreti { get; set; }
        public string TtbaglantiTaksitBedeliMahsup { get; set; }
        public decimal? TtbaglantiTaksitBedeli { get; set; }
        public decimal? TtelittUcreti { get; set; }
        public decimal? TttarifeDegNedFarkUcreti { get; set; }
        public decimal? TtkampanyaIskontoUcreti2 { get; set; }
        public decimal? TtndslErisimKistUcreti2 { get; set; }
        public decimal? TtkistStatikAylikSabitUcret { get; set; }
        public string TtdsliptalKist { get; set; }
        public decimal? TthataliArizaBildirimUcreti { get; set; }
        public decimal? TttopluGecisUcreti { get; set; }
        public decimal? TttopluBasvuruUcreti { get; set; }
        public decimal? TtekAknUcreti { get; set; }
        public decimal? TttekilGecisUcreti { get; set; }
        public decimal? TtkullanimIskontoUcreti { get; set; }
        public decimal? TtkurulumUcreti { get; set; }
        public decimal? Ttoiv75ucreti { get; set; }
    }
}
