﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ChannelType
    {
        public ChannelType()
        {
            CustomerInteraction = new HashSet<CustomerInteraction>();
        }

        public int ChannelTypeId { get; set; }
        public string ChannelTypeName { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual ICollection<CustomerInteraction> CustomerInteraction { get; set; }
    }
}
