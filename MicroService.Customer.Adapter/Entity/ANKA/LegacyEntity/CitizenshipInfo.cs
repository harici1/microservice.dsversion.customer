﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CitizenshipInfo
    {
        public string TckimlikNo { get; set; }
        public DateTime? CreateDate { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string AnaAd { get; set; }
        public string AnaSoyad { get; set; }
        public string BabaAd { get; set; }
        public string BabaSoyad { get; set; }
        public string Cinsiyet { get; set; }
        public int? DogumTarihYil { get; set; }
        public int? DogumTarihAy { get; set; }
        public int? DogumTarihGun { get; set; }
        public string DogumYer { get; set; }
        public string KizlikSoyad { get; set; }
        public int? OzurOran { get; set; }
        public int? AileSiraNo { get; set; }
        public int? BireySiraNo { get; set; }
        public string CiltAd { get; set; }
        public int? CiltKod { get; set; }
        public string IlAd { get; set; }
        public string IlceAd { get; set; }
        public int? IlceKod { get; set; }
        public int? IlKod { get; set; }
        public string Din { get; set; }
        public string Durum { get; set; }
        public string MedeniHal { get; set; }
        public int? OlumTarihYil { get; set; }
        public int? OlumTarihAy { get; set; }
        public int? OlumTarihGun { get; set; }
        public string OlumYer { get; set; }
        public string KayipCuzdanNo { get; set; }
        public string KayipCuzdanSeri { get; set; }
        public int? KayipTarihYil { get; set; }
        public int? KayipTarihAy { get; set; }
        public int? KayipTarihGun { get; set; }
        public string KayipTur { get; set; }
        public string YeniCuzdanNo { get; set; }
        public string YeniCuzdanSeri { get; set; }
        public string YeniCuzdanVerenIlceAd { get; set; }
        public string YeniCuzdanVerenIlceKod { get; set; }
        public int? YeniCuzdanVerilmeTarihYil { get; set; }
        public int? YeniCuzdanVerilmeTarihAy { get; set; }
        public int? YeniCuzdanVerilmeTarihGun { get; set; }
    }
}
