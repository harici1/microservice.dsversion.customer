﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Company
    {
        public Company()
        {
            Catalog = new HashSet<Catalog>();
            Customer = new HashSet<Customer>();
            Department = new HashSet<Department>();
        }

        public int Id { get; set; }
        public int ParentId { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string CompanyType { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string MobilePhone { get; set; }
        public string TaxOffice { get; set; }
        public string TaxNumber { get; set; }
        public string AccountManager { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public string ModifyBy { get; set; }
        public bool? Active { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual ICollection<Catalog> Catalog { get; set; }
        public virtual ICollection<Customer> Customer { get; set; }
        public virtual ICollection<Department> Department { get; set; }
    }
}
