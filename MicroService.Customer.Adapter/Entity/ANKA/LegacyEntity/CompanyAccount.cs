﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CompanyAccount
    {
        public int AccountId { get; set; }
        public int AccountCycleId { get; set; }
        public int AccountTypeId { get; set; }
        public string CompanyCode { get; set; }
        public int? AccountNumber { get; set; }
        public decimal? AmountBeforeTax { get; set; }
        public decimal? Amount { get; set; }
        public int? Sp { get; set; }
        public int? Spb { get; set; }
        public string NoteInfo { get; set; }
        public bool Canceled { get; set; }
        public decimal? TaxAmount { get; set; }
        public bool IsInvoice { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string ModifyBy { get; set; }
        public string InvoiceNo { get; set; }
        public string Description { get; set; }
        public decimal? TaxRate { get; set; }
        public DateTime? RecordDate { get; set; }
        public string InvoiceStatus { get; set; }
        public bool IsTransfer { get; set; }
    }
}
