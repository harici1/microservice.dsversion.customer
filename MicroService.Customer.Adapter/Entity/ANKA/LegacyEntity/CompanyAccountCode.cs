﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CompanyAccountCode
    {
        public string Ankacode { get; set; }
        public string Sapcode { get; set; }
        public int Id { get; set; }
    }
}
