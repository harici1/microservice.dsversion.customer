﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CompanyAccountData
    {
        public int Id { get; set; }
        public int AccountCycleId { get; set; }
        public int AccountTypeId { get; set; }
        public int ContractId { get; set; }
        public string ContractNo { get; set; }
        public int ContractStatusId { get; set; }
        public string ContractStatusName { get; set; }
        public int? CampaignId { get; set; }
        public string CampaignName { get; set; }
        public int ContractDetailId { get; set; }
        public string ContractDetailCreateBy { get; set; }
        public string ContractDetailCompanyDistributorCode { get; set; }
        public string ContractDetailCompanyDistributorName { get; set; }
        public string ContractDetailCompanyVendorCode { get; set; }
        public string ContractDetailCompanyVendorName { get; set; }
        public string ContractDetailCompanySubVendorCode { get; set; }
        public string ContractDetailCompanySubVendorName { get; set; }
        public int CustomerId { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerCreateBy { get; set; }
        public string CustomerCompanyDistributorCode { get; set; }
        public string CustomerCompanyDistributorName { get; set; }
        public string CustomerCompanyVendorCode { get; set; }
        public string CustomerCompanyVendorName { get; set; }
        public string CustomerCompanySubVendorCode { get; set; }
        public string CustomerCompanySubVendorName { get; set; }
        public string CargoByHand { get; set; }
        public DateTime? ActiveDate { get; set; }
        public bool? IsPremium { get; set; }
        public decimal? CiroValue { get; set; }
        public int? Commitment { get; set; }
        public bool? IsContractDocument { get; set; }
    }
}
