﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CompanyAccountDetail
    {
        public int AccountDetailId { get; set; }
        public int AccountId { get; set; }
        public int AccountCycleId { get; set; }
        public int AccountTypeId { get; set; }
        public string AccountExplanationLevel { get; set; }
        public string CompanyCode { get; set; }
        public int ContractId { get; set; }
        public string ContractNo { get; set; }
        public int ContractStatusId { get; set; }
        public int? ContractDetailId { get; set; }
        public int? CampaignId { get; set; }
        public string CampaignName { get; set; }
        public string CampaignGroup { get; set; }
        public decimal? AmountBeforeTax { get; set; }
        public decimal? Amount { get; set; }
        public int? Sp { get; set; }
        public string CargoByHand { get; set; }
        public bool Canceled { get; set; }
        public decimal? TaxAmount { get; set; }
        public decimal? TaxRate { get; set; }
        public DateTime? ActiveDate { get; set; }
        public string Description { get; set; }
        public int? RelatedAccountDetailId { get; set; }
        public int? AccountStatuId { get; set; }
        public int? AccountRangeId { get; set; }
        public int? PremiumInstallment { get; set; }
        public string DescriptionDetail { get; set; }
        public string DsmartCatalogItemName { get; set; }
        public decimal? CiroAmount { get; set; }
        public int? Commitment { get; set; }
        public string DescriptionDetail2 { get; set; }
        public string RefType { get; set; }
        public int? RefId { get; set; }
        public bool? IsPremium { get; set; }
    }
}
