﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CompanyDeviceStorehouse
    {
        public int Id { get; set; }
        public string CompanyCode { get; set; }
        public string UniqueIdentifierNo { get; set; }
        public int? DeliveryId { get; set; }
        public bool Status { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string ModifyBy { get; set; }
        public string ModifyDate { get; set; }
    }
}
