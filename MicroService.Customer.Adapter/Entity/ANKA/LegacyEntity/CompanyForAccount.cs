﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CompanyForAccount
    {
        public int Id { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string DistributorCompanyCode { get; set; }
        public string DistributorCompanyName { get; set; }
        public string VendorCompanyCode { get; set; }
        public string VendorCompanyName { get; set; }
        public string SubVendorCompanyCode { get; set; }
        public string SubVendorCompanyName { get; set; }
        public int? AccountCycleId { get; set; }
        public string AskSaha { get; set; }
    }
}
