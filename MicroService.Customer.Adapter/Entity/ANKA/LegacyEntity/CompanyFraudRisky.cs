﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CompanyFraudRisky
    {
        public string CompanyCode { get; set; }
        public bool? FraudRiskyActive { get; set; }
    }
}
