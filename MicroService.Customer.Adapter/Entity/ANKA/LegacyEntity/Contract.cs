﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Contract
    {
        public Contract()
        {
            ContractDetail = new HashSet<ContractDetail>();
            ContractDocument = new HashSet<ContractDocument>();
            CustomerRequest = new HashSet<CustomerRequest>();
            InvoiceAssessment = new HashSet<InvoiceAssessment>();
            ServiceRequest = new HashSet<ServiceRequest>();
            Ticket = new HashSet<Ticket>();
        }

        public int ContractId { get; set; }
        public string ContractNo { get; set; }
        public int CustomerId { get; set; }
        public int? ContractStatusId { get; set; }
        public int? InvoiceAddressId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? TerminationDate { get; set; }
        public bool AutoChargeCc { get; set; }
        public int? AutoChargeCcid { get; set; }
        public bool? SendInvoiceByMail { get; set; }
        public bool? SendInvoiceByEmail { get; set; }
        public bool? SendInvoiceBySms { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public string CreateBy { get; set; }
        public string ModifyBy { get; set; }
        public int? ReplacedContractId { get; set; }
        public int? BillingPlatformId { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public string ContractAlias { get; set; }
        public DateTime? WrittenCancelRequestDate { get; set; }

        public virtual ContractStatus ContractStatus { get; set; }
        public virtual UserData CreateByNavigation { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Address InvoiceAddress { get; set; }
        public virtual UserData ModifyByNavigation { get; set; }
        public virtual ICollection<ContractDetail> ContractDetail { get; set; }
        public virtual ICollection<ContractDocument> ContractDocument { get; set; }
        public virtual ICollection<CustomerRequest> CustomerRequest { get; set; }
        public virtual ICollection<InvoiceAssessment> InvoiceAssessment { get; set; }
        public virtual ICollection<ServiceRequest> ServiceRequest { get; set; }
        public virtual ICollection<Ticket> Ticket { get; set; }
    }
}
