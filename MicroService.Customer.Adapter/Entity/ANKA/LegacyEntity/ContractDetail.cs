﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractDetail
    {
        public ContractDetail()
        {
            ContractDetailServiceParam = new HashSet<ContractDetailServiceParam>();
            ContractDetailServiceStatusJournal = new HashSet<ContractDetailServiceStatusJournal>();
            ContractDetailStatusJournal = new HashSet<ContractDetailStatusJournal>();
            InvoiceAssessment = new HashSet<InvoiceAssessment>();
            VasAuthenticationToken = new HashSet<VasAuthenticationToken>();
            XdslTransferNewContractDetail = new HashSet<XdslTransfer>();
            XdslTransferOrigContractDetail = new HashSet<XdslTransfer>();
        }

        public int ContractDetailId { get; set; }
        public int ContractId { get; set; }
        public int CatalogItemId { get; set; }
        public int? ParentContractDetailId { get; set; }
        public int? PricePlanId { get; set; }
        public int? CampaignId { get; set; }
        public int? ContractDetailStatusId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public string CreateBy { get; set; }
        public string ModifyBy { get; set; }
        public bool Bundle { get; set; }
        public int? ReplacedContractDetailId { get; set; }
        public int? ReplacementTypeId { get; set; }
        public int? PreviousContractId { get; set; }
        public decimal PreBilledPeriodCount { get; set; }
        public decimal PostBilledPeriodCount { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual Campaign Campaign { get; set; }
        public virtual Contract Contract { get; set; }
        public virtual PricePlan PricePlan { get; set; }
        public virtual ICollection<ContractDetailServiceParam> ContractDetailServiceParam { get; set; }
        public virtual ICollection<ContractDetailServiceStatusJournal> ContractDetailServiceStatusJournal { get; set; }
        public virtual ICollection<ContractDetailStatusJournal> ContractDetailStatusJournal { get; set; }
        public virtual ICollection<InvoiceAssessment> InvoiceAssessment { get; set; }
        public virtual ICollection<VasAuthenticationToken> VasAuthenticationToken { get; set; }
        public virtual ICollection<XdslTransfer> XdslTransferNewContractDetail { get; set; }
        public virtual ICollection<XdslTransfer> XdslTransferOrigContractDetail { get; set; }
    }
}
