﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractDetailDiscount
    {
        public int ContractDetailDiscountId { get; set; }
        public int? ContractDetailId { get; set; }
        public int? PricePlanDetailId { get; set; }
        public int? GivenCount { get; set; }
        public int? AppliedCount { get; set; }
        public string DiscountType { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
    }
}
