﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractDetailPowerUp
    {
        public int ContractDetailPowerUpId { get; set; }
        public int ContractDetailId { get; set; }
        public int CatalogItemId { get; set; }
        public DateTime? ExpireDate { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public decimal? CurrentDownload { get; set; }
    }
}
