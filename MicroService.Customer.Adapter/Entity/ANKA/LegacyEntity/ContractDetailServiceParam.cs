﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractDetailServiceParam
    {
        public int ContractDetailServiceParamId { get; set; }
        public int ContractDetailId { get; set; }
        public int ServiceParamDetailId { get; set; }
        public string Value { get; set; }
        public string PendingValue { get; set; }
        public bool? Active { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public DateTime? CreateDate { get; set; }

        public virtual ContractDetail ContractDetail { get; set; }
        public virtual ServiceParamDetail ServiceParamDetail { get; set; }
    }
}
