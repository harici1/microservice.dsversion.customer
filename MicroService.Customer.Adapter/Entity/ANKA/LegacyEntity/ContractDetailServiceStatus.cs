﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractDetailServiceStatus
    {
        public ContractDetailServiceStatus()
        {
            ContractDetailServiceStatusJournal = new HashSet<ContractDetailServiceStatusJournal>();
        }

        public int ContractDetailServiceStatusId { get; set; }
        public int ContractDetailServiceStatusTypeId { get; set; }
        public string DisplayName { get; set; }
        public bool Active { get; set; }
        public bool IsOut { get; set; }
        public bool ServiceUsage { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual ICollection<ContractDetailServiceStatusJournal> ContractDetailServiceStatusJournal { get; set; }
    }
}
