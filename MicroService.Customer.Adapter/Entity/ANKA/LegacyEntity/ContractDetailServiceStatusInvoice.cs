﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractDetailServiceStatusInvoice
    {
        public int ContractDetailServiceStatusInvoiceId { get; set; }
        public int ContractDetailServiceStatusId { get; set; }
        public bool Billable { get; set; }
        public int? ArizaSla { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
