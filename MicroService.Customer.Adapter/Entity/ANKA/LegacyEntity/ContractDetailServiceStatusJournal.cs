﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractDetailServiceStatusJournal
    {
        public int ContractDetailServiceStatusJournalId { get; set; }
        public int ContractDetailId { get; set; }
        public int ContractDetailServiceStatusId { get; set; }
        public DateTime StatusChangeDate { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public bool? Billable { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string ModifyBy { get; set; }

        public virtual ContractDetail ContractDetail { get; set; }
        public virtual ContractDetailServiceStatus ContractDetailServiceStatus { get; set; }
    }
}
