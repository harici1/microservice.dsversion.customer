﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractDetailServiceStatusType
    {
        public int ContractDetailServiceStatusTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
