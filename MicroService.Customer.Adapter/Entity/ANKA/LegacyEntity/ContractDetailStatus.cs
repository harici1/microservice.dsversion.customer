﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractDetailStatus
    {
        public ContractDetailStatus()
        {
            ContractDetailStatusJournal = new HashSet<ContractDetailStatusJournal>();
        }

        public int ContractDetailStatusId { get; set; }
        public string DisplayName { get; set; }
        public bool Active { get; set; }
        public bool Billable { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public bool Interim { get; set; }
        public bool Final { get; set; }
        public bool Error { get; set; }
        public bool SelectPriority { get; set; }
        public bool CommitmentExtend { get; set; }

        public virtual ICollection<ContractDetailStatusJournal> ContractDetailStatusJournal { get; set; }
    }
}
