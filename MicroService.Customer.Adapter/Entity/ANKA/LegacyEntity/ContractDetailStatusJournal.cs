﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractDetailStatusJournal
    {
        public int ContractDetailStatusJournalId { get; set; }
        public int ContractDetailId { get; set; }
        public int ContractDetailStatusId { get; set; }
        public DateTime StatusChangeDate { get; set; }
        public int PricePlanId { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public bool? Billable { get; set; }
        public bool? CommitmentExtend { get; set; }

        public virtual ContractDetail ContractDetail { get; set; }
        public virtual ContractDetailStatus ContractDetailStatus { get; set; }
    }
}
