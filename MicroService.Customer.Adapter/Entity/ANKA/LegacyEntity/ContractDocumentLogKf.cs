﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractDocumentLogKf
    {
        public int Id { get; set; }
        public string ContractDocumentNo { get; set; }
        public int? ContractId { get; set; }
        public byte? Status { get; set; }
        public string RelatedFile { get; set; }
        public DateTime? SignDate { get; set; }
        public byte? SoftArchiveStatus { get; set; }
        public Guid? MsreplTranVersion { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
    }
}
