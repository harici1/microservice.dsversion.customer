﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractPreference
    {
        public int ContractPreferenceId { get; set; }
        public string ContractPreferenceName { get; set; }
        public string ContractPreferenceDescription { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool? Active { get; set; }
        public string PreferenceHeader { get; set; }
        public string PreferenceDescription { get; set; }
    }
}
