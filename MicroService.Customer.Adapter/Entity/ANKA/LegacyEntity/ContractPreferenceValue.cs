﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractPreferenceValue
    {
        public int ContractPreferenceValueId { get; set; }
        public int? ContractPreferenceId { get; set; }
        public int? ContractId { get; set; }
        public int? ContractPreferenceValueOptionId { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifyBy { get; set; }
    }
}
