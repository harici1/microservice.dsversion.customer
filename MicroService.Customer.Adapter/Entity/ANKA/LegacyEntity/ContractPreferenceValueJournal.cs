﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractPreferenceValueJournal
    {
        public int ContractPreferenceValueJournalId { get; set; }
        public int ContractPreferenceValueId { get; set; }
        public DateTime ChangeDate { get; set; }
        public string OldValue { get; set; }
        public int? ContractPreferenceId { get; set; }
        public int? NewValue { get; set; }
    }
}
