﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractPreferenceValueOption
    {
        public int ContractPreferenceValueOptionId { get; set; }
        public int ContractPreferenceId { get; set; }
        public string OptionValue { get; set; }
        public bool Active { get; set; }
        public bool? DefaultPreference { get; set; }
        public string OptionText { get; set; }
    }
}
