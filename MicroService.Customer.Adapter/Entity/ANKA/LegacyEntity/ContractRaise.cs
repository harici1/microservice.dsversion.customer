﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractRaise
    {
        public int ContractRaiseId { get; set; }
        public int ContractRaiseStatusId { get; set; }
        public int ContractId { get; set; }
        public int ContractStatusId { get; set; }
        public decimal TotalPrice { get; set; }
        public string Bandwidth { get; set; }
        public string Akn { get; set; }
        public string Quota { get; set; }
        public DateTime? CommitmentStartDate { get; set; }
        public DateTime? CommitmentEndDate { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime Cycle { get; set; }
        public string Description { get; set; }
        public decimal? RaiseAmount { get; set; }
        public string RaiseTypeId { get; set; }
    }
}
