﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractRaiseDetail
    {
        public int ContractRaiseDetailId { get; set; }
        public int? ContractRaiseId { get; set; }
        public int ContractId { get; set; }
        public int ContractDetailId { get; set; }
        public int ContractDetailStatusId { get; set; }
        public int CatalogItemId { get; set; }
        public int? CampaignId { get; set; }
        public int PricePlanId { get; set; }
        public DateTime Cycle { get; set; }
        public int? NewPricePlanId { get; set; }
    }
}
