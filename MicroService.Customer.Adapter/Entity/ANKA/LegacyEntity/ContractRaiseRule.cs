﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractRaiseRule
    {
        public int ContractRaiseRuleId { get; set; }
        public decimal PriceRangeStart { get; set; }
        public decimal PriceRangeEnd { get; set; }
        public string Bandwidth { get; set; }
        public string Akn { get; set; }
        public string Quota { get; set; }
        public int RaiseTypeId { get; set; }
        public decimal RaiseAmount { get; set; }
    }
}
