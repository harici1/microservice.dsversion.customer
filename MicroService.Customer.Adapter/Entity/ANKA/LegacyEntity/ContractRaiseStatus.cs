﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractRaiseStatus
    {
        public int ContractRaiseStatusId { get; set; }
        public string Name { get; set; }
    }
}
