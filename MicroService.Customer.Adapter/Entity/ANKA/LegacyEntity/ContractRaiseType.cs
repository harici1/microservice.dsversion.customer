﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractRaiseType
    {
        public int ContractRaiseTypeId { get; set; }
        public string Name { get; set; }
    }
}
