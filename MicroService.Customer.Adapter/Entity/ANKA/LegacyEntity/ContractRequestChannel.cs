﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractRequestChannel
    {
        public int ContractRequestChannelId { get; set; }
        public int ContractId { get; set; }
        public int RequestChannelId { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public Guid MsreplTranVersion { get; set; }
    }
}
