﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractStatus
    {
        public ContractStatus()
        {
            Contract = new HashSet<Contract>();
        }

        public int ContractStatusId { get; set; }
        public string DisplayName { get; set; }
        public bool PaymentAllowed { get; set; }
        public bool WarnForNewSales { get; set; }
        public bool? Active { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual ICollection<Contract> Contract { get; set; }
    }
}
