﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractStatusJournal
    {
        public int ContractJournalId { get; set; }
        public DateTime JournalDate { get; set; }
        public int ContractId { get; set; }
        public int ContractStatusId { get; set; }
        public Guid MsreplTranVersion { get; set; }
    }
}
