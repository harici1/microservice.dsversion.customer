﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ContractToInvoice
    {
        public int ContractToInvoiceId { get; set; }
        public int? ContractId { get; set; }
        public int? ContractStatusId { get; set; }
        public int? BatchId { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool? IsInvoice { get; set; }
        public int? DontInvoice { get; set; }
        public string Note { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public bool? IsOkInvoice { get; set; }
        public bool? IsOkAssesment { get; set; }
        public int? CampaignId { get; set; }
        public bool Process { get; set; }
        public byte? IsBundle { get; set; }
        public bool? IsExpense { get; set; }
    }
}
