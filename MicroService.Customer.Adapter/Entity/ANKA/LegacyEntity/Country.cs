﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Country
    {
        public Country()
        {
            Province = new HashSet<Province>();
        }

        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public bool? Active { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual ICollection<Province> Province { get; set; }
    }
}
