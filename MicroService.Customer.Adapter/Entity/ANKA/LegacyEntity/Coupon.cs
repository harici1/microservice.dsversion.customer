﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Coupon
    {
        public int CouponId { get; set; }
        public string CouponNo { get; set; }
        public int? CouponTypeId { get; set; }
        public int? CouponStatusId { get; set; }
        public int? ContractId { get; set; }
        public DateTime? AssignDate { get; set; }
        public DateTime? UseDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
    }
}
