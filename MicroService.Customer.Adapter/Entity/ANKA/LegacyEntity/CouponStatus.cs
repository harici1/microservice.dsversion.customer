﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CouponStatus
    {
        public int CouponStatusId { get; set; }
        public string DisplayName { get; set; }
    }
}
