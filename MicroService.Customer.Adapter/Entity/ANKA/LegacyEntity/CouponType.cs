﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CouponType
    {
        public int CouponTypeId { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public int? LifeTime { get; set; }
        public bool? Foreign { get; set; }
        public bool? PreAssigned { get; set; }
        public int? AllowedUsageDayCount { get; set; }
    }
}
