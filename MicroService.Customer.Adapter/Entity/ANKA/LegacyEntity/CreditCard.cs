﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CreditCard
    {
        public int CreditCardId { get; set; }
        public string RefType { get; set; }
        public string RefParameter { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public byte[] CardNo { get; set; }
        public string ExpireMonth { get; set; }
        public string ExpireYear { get; set; }
        public bool? Active { get; set; }
        public string CreateBy { get; set; }
        public int? ContractId { get; set; }
        public DateTime? CreateDate { get; set; }
        public string Channel { get; set; }
        public string ModifyBy { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
