﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CustomPricePlanDetailDescription
    {
        public int CustomPricePlanDetailDescriptionId { get; set; }
        public int PricePlanDetailId { get; set; }
        public string CustomDescription { get; set; }
        public Guid MsreplTranVersion { get; set; }
    }
}
