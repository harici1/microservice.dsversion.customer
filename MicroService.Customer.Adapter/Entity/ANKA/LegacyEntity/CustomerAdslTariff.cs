﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CustomerAdslTariff
    {
        public int Id { get; set; }
        public string ContractNo { get; set; }
        public string PstnNumber { get; set; }
        public string OldServiceType { get; set; }
        public int? OldBandwidth { get; set; }
        public int? OldPacketTypeId { get; set; }
        public int? OldTariffTypeId { get; set; }
        public string NewServiceType { get; set; }
        public int NewBandwidth { get; set; }
        public int NewPacketTypeId { get; set; }
        public int NewTariffTypeId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ProcessFinishDate { get; set; }
        public int? ProcessResultId { get; set; }
        public string ProcessResultInfo { get; set; }
        public bool Status { get; set; }
        public bool Dirty { get; set; }
    }
}
