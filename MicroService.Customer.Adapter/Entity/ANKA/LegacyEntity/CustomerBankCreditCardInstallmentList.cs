﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CustomerBankCreditCardInstallmentList
    {
        public int CustomerBankCreditCardInstallmentListId { get; set; }
        public string CustomerNo { get; set; }
        public int CustomerId { get; set; }
        public string ContractNo { get; set; }
        public int? ContractId { get; set; }
        public string InvoiceNo { get; set; }
        public int? InvoiceId { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public bool? Active { get; set; }
        public int InstallmentCount { get; set; }
    }
}
