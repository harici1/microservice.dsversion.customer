﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CustomerDocument
    {
        public int CustomerDocumentId { get; set; }
        public int CustomerId { get; set; }
        public int DocumentTypeId { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public bool Valid { get; set; }
        public bool Approval { get; set; }
    }
}
