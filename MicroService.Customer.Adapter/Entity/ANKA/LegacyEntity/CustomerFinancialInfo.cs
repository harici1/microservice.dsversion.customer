﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CustomerFinancialInfo
    {
        public int CustomerFinancialInfoId { get; set; }
        public int CustomerId { get; set; }
        public int ContractId { get; set; }
        public bool? EinvoiceInfo { get; set; }
        public bool? PhoneInfo { get; set; }
        public bool? InvoiceInfo { get; set; }
        public bool? EmailInfo { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
    }
}
