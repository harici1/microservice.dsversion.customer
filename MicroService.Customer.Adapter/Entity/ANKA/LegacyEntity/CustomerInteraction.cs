﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CustomerInteraction
    {
        public CustomerInteraction()
        {
            TicketCustomerInteraction = new HashSet<TicketCustomerInteraction>();
        }

        public int CustomerInteractionId { get; set; }
        public int CustomerId { get; set; }
        public string Direction { get; set; }
        public int ChannelType { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public int? Status { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual ChannelType ChannelTypeNavigation { get; set; }
        public virtual UserData CreateByNavigation { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual CustomerInteractionStatus StatusNavigation { get; set; }
        public virtual ICollection<TicketCustomerInteraction> TicketCustomerInteraction { get; set; }
    }
}
