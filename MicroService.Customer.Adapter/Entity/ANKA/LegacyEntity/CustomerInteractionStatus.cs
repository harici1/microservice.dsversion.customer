﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CustomerInteractionStatus
    {
        public CustomerInteractionStatus()
        {
            CustomerInteraction = new HashSet<CustomerInteraction>();
        }

        public int CustInterStatusId { get; set; }
        public string Description { get; set; }
        public bool? Active { get; set; }

        public virtual ICollection<CustomerInteraction> CustomerInteraction { get; set; }
    }
}
