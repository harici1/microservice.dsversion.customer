﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CustomerJournal
    {
        public int CustomerJournalId { get; set; }
        public int? CustomerId { get; set; }
        public string RefType { get; set; }
        public int? RefId { get; set; }
        public string EntryType { get; set; }
        public string EntryText { get; set; }
        public DateTime? EntryDate { get; set; }
        public string CreateBy { get; set; }
    }
}
