﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CustomerLogin
    {
        public int CustomerId { get; set; }
        public string WebUsername { get; set; }
        public string WebPassword { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public short? WrongPasswordCounter { get; set; }
    }
}
