﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CustomerPreferenceValue
    {
        public int CustomerPreferenceValueId { get; set; }
        public int? CustomerPreferenceId { get; set; }
        public int? CustomerId { get; set; }
        public int? CustomerPreferenceValueOptionId { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifyBy { get; set; }
    }
}
