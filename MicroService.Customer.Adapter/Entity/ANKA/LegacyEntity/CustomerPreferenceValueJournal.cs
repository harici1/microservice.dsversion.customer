﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CustomerPreferenceValueJournal
    {
        public int CustomerPreferenceValueJournalId { get; set; }
        public int CustomerPreferenceValueId { get; set; }
        public DateTime ChangeDate { get; set; }
        public string OldValue { get; set; }
        public int? CustomerPreferenceId { get; set; }
        public int? NewValue { get; set; }
        public string ModifyBy { get; set; }
    }
}
