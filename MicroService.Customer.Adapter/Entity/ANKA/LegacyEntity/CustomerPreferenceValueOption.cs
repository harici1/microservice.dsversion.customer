﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CustomerPreferenceValueOption
    {
        public int CustomerPreferenceValueOptionId { get; set; }
        public int CustomerPreferenceId { get; set; }
        public string OptionValue { get; set; }
        public bool Active { get; set; }
        public bool? DefaultPreference { get; set; }
        public string OptionText { get; set; }
    }
}
