﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CustomerPreferences
    {
        public int CustomerPreferenceId { get; set; }
        public string CustomerPreferenceName { get; set; }
        public string CustomerPreferenceDescription { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool? Active { get; set; }
        public string PreferenceHeader { get; set; }
        public string PreferenceDescription { get; set; }
    }
}
