﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CustomerRequest
    {
        public CustomerRequest()
        {
            CustomerRequestStatusJournal = new HashSet<CustomerRequestStatusJournal>();
        }

        public int CustomerRequestId { get; set; }
        public int CustomerId { get; set; }
        public int? ContractId { get; set; }
        public int RequestTypeId { get; set; }
        public int RequestReasonId { get; set; }
        public int RequestStatusId { get; set; }
        public int? RequestResolutionId { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string ModifyBy { get; set; }
        public string Parameters { get; set; }
        public DateTime? DueDate { get; set; }
        public bool IsConditionalRequest { get; set; }

        public virtual Contract Contract { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual RequestReason RequestReason { get; set; }
        public virtual RequestResolution RequestResolution { get; set; }
        public virtual RequestStatus RequestStatus { get; set; }
        public virtual RequestType RequestType { get; set; }
        public virtual ICollection<CustomerRequestStatusJournal> CustomerRequestStatusJournal { get; set; }
    }
}
