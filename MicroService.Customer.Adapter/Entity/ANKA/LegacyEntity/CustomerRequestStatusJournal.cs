﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CustomerRequestStatusJournal
    {
        public int JournalId { get; set; }
        public DateTime? JournalDate { get; set; }
        public int? CustomerRequestId { get; set; }
        public string RequestStatusId { get; set; }
        public int? RequestResolutionId { get; set; }
        public string By { get; set; }

        public virtual CustomerRequest CustomerRequest { get; set; }
    }
}
