﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class CustomerServices
    {
        public int CustomerId { get; set; }
        public int ServiceId { get; set; }
    }
}
