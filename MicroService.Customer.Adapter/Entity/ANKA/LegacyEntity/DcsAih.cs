﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class DcsAih
    {
        public int Id { get; set; }
        public int ContractId { get; set; }
        public int LineNumber { get; set; }
        public int LineStatus { get; set; }
        public int LineStatusCode { get; set; }
        public string ServiceType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Recipe { get; set; }
        public string SpeedProfile { get; set; }
        public string ServiceProvider { get; set; }
        public string PopInformation { get; set; }
        public string CountryA { get; set; }
        public string BorderCrossingPointA { get; set; }
        public string ServiceEndpointB { get; set; }
        public string CountryB { get; set; }
        public string CityB { get; set; }
        public string CountyB { get; set; }
        public string DistrictB { get; set; }
        public string StreetB { get; set; }
        public string OutsideDoorNumberB { get; set; }
        public string InsideDoorNumberB { get; set; }
        public string BorderCrossingPointB { get; set; }
        public string CircuitDesignation { get; set; }
        public string CircuitItinerary { get; set; }
        public string ActivationDealerName { get; set; }
        public string ActivationDealerAddress { get; set; }
        public string ActivationUser { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModificationDate { get; set; }
        public bool Deleted { get; set; }
    }
}
