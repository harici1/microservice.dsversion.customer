﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class DcsConstants
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IdentityNumber { get; set; }
        public int ContractId { get; set; }
        public string PhoneNumber { get; set; }
        public string PreLineNumber { get; set; }
        public DateTime? FrozenDate { get; set; }
        public DateTime? RestrictionDate { get; set; }
        public bool AbroadActive { get; set; }
        public bool VoiceCallActive { get; set; }
        public bool DirectoryActive { get; set; }
        public bool ClirActive { get; set; }
        public bool DataActive { get; set; }
        public bool IntercityActive { get; set; }
        public string CentralBuilding { get; set; }
        public string CentralType { get; set; }
        public string NetworkServiceNumber { get; set; }
        public string PilotNumber { get; set; }
        public string DdiServiceNumber { get; set; }
        public string VisibleNumber { get; set; }
        public string ReferenceNumber { get; set; }
        public string HomeWorkPlace { get; set; }
        public int? SubscriberId { get; set; }
        public string ServiceNumber { get; set; }
        public string InternalNumber { get; set; }
        public string AlphanumericTitle { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModificationDate { get; set; }
        public bool Deleted { get; set; }
    }
}
