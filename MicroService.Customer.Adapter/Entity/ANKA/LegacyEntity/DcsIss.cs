﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class DcsIss
    {
        public int Id { get; set; }
        public int ContractId { get; set; }
        public string SpeedProfile { get; set; }
        public string UserName { get; set; }
        public string PopInfo { get; set; }
        public int? SubscriberId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModificationDate { get; set; }
        public bool Deleted { get; set; }
    }
}
