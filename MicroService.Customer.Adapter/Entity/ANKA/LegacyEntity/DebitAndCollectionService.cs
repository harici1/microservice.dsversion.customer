﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class DebitAndCollectionService
    {
        public int Id { get; set; }
        public string BankName { get; set; }
        public string FtpAdress { get; set; }
        public bool? IsSsh { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ContainsName { get; set; }
        public string DebitPath { get; set; }
        public string DebitFileFormat { get; set; }
        public string DebitFileName { get; set; }
        public string CollectionPath { get; set; }
        public string CollectionFileFormat { get; set; }
        public string CollectionFileName { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDatetime { get; set; }
        public string ModifyBy { get; set; }
        public DateTime? ModifyDatetime { get; set; }
        public bool IsUpload { get; set; }
    }
}
