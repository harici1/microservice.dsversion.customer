﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class DebitAndCollectionServiceReport
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public string BankName { get; set; }
        public string FileName { get; set; }
        public DateTime StatusDatetime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDatetime { get; set; }
        public bool IsMailSend { get; set; }
        public string Description { get; set; }
    }
}
