﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class DeleteInvoiceOperationLog
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public int BalanceId { get; set; }
        public int RefParameter { get; set; }
        public string RefType { get; set; }
        public int CustomerId { get; set; }
        public int ContractId { get; set; }
        public decimal Amount { get; set; }
        public decimal CurrentAmount { get; set; }
        public string Description { get; set; }
        public DateTime ProcessDate { get; set; }
        public int Status { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string DeleteByUser { get; set; }
        public DateTime DeleteDate { get; set; }
    }
}
