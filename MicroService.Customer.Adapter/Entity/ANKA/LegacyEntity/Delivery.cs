﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Delivery
    {
        public Delivery()
        {
            DeliveryDetail = new HashSet<DeliveryDetail>();
            ServiceRequest = new HashSet<ServiceRequest>();
        }

        public int DeliveryId { get; set; }
        public string DeliveryNo { get; set; }
        public string DeliveryInquiryNo { get; set; }
        public int? ContractId { get; set; }
        public int? DeliveryCompanyId { get; set; }
        public int? DeliveryStatusId { get; set; }
        public int? DeliveryAddressId { get; set; }
        public int? DeliveryContactId { get; set; }
        public bool? TwoWay { get; set; }
        public bool? Indirect { get; set; }
        public DateTime? HandOverDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public string WayBillNo { get; set; }
        public bool? Canceled { get; set; }
        public string CompanyCode { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime CreateDate { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual DeliveryCompany DeliveryCompany { get; set; }
        public virtual DeliveryStatus DeliveryStatus { get; set; }
        public virtual ICollection<DeliveryDetail> DeliveryDetail { get; set; }
        public virtual ICollection<ServiceRequest> ServiceRequest { get; set; }
    }
}
