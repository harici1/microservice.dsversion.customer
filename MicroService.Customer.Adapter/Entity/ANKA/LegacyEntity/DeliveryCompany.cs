﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class DeliveryCompany
    {
        public DeliveryCompany()
        {
            Delivery = new HashSet<Delivery>();
        }

        public int DeliveryCompanyId { get; set; }
        public string Title { get; set; }
        public string DeliveryInquiryLink { get; set; }
        public bool? Active { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public int? WareHouseId { get; set; }
        public string UserNameOneWay { get; set; }
        public string UserNameTwoWay { get; set; }
        public string PassWordOneWay { get; set; }
        public string PassWordTwoWay { get; set; }
        public string WebServiceUrl { get; set; }

        public virtual ICollection<Delivery> Delivery { get; set; }
    }
}
