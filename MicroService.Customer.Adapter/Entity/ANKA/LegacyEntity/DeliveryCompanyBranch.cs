﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class DeliveryCompanyBranch
    {
        public int DeliveryCompanyBranchId { get; set; }
        public int? DeliveryCompanyId { get; set; }
        public int? WareHouseId { get; set; }
        public string Title { get; set; }
        public bool? Active { get; set; }
        public string WebServiceAddress { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
