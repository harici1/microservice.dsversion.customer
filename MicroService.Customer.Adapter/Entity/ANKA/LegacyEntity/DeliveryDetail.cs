﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class DeliveryDetail
    {
        public int DeliveryDetailId { get; set; }
        public int DeliveryId { get; set; }
        public int? ContractDetailId { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual Delivery Delivery { get; set; }
    }
}
