﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class DeliveryStatus
    {
        public DeliveryStatus()
        {
            Delivery = new HashSet<Delivery>();
        }

        public int DeliveryStatusId { get; set; }
        public string DisplayName { get; set; }
        public bool? Active { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual ICollection<Delivery> Delivery { get; set; }
    }
}
