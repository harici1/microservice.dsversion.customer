﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Department
    {
        public Department()
        {
            Division = new HashSet<Division>();
        }

        public int Id { get; set; }
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public bool? Active { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual Company Company { get; set; }
        public virtual ICollection<Division> Division { get; set; }
    }
}
