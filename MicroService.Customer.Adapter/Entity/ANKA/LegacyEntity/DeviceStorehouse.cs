﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class DeviceStorehouse
    {
        public int DeviceStorehouseId { get; set; }
        public string UniqueIdentifierNo { get; set; }
        public int CustomerId { get; set; }
        public int ContractId { get; set; }
        public int ContractDetailId { get; set; }
        public bool IsEk1Form { get; set; }
        public bool IsContract { get; set; }
        public bool IsActive { get; set; }
        public string SalesType { get; set; }
        public string SalesBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
