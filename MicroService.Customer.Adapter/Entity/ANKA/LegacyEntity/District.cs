﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class District
    {
        public int DistrictId { get; set; }
        public int CityId { get; set; }
        public string Mahalle { get; set; }
        public bool? Active { get; set; }
    }
}
