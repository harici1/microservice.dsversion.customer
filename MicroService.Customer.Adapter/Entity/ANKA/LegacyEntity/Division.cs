﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Division
    {
        public Division()
        {
            Section = new HashSet<Section>();
        }

        public int Id { get; set; }
        public int DepartmentId { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }

        public virtual Department Department { get; set; }
        public virtual ICollection<Section> Section { get; set; }
    }
}
