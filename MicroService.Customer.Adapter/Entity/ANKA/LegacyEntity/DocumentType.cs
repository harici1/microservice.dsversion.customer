﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class DocumentType
    {
        public int DocumentTypeId { get; set; }
        public string DisplayName { get; set; }
        public int ValidityPeriod { get; set; }
        public string CustomerType { get; set; }
        public bool? Status { get; set; }
    }
}
