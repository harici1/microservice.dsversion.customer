﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class DpiNotifyCallback
    {
        public int DpiNotifyId { get; set; }
        public int DpiNotifyCallbackProcessStatusId { get; set; }
        public int ReTryCount { get; set; }
        public int DpiUserId { get; set; }
        public string AdslUserName { get; set; }
        public string NotificationType { get; set; }
        public string NotificationKey { get; set; }
        public string NotificationRange { get; set; }
        public string UsageDataUnit { get; set; }
        public int UsageTotalDownload { get; set; }
        public int UsageTotalUpload { get; set; }
        public int UsageTotalFreeDownload { get; set; }
        public int UsageTotalFreeUpload { get; set; }
        public DateTime UsageNotifyDate { get; set; }
        public DateTime CreateDate { get; set; }
        public Guid TransactionNumber { get; set; }
        public DateTime? LastProcessDate { get; set; }
    }
}
