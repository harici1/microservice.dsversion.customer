﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class DpiNotifyCallbackProcessStatus
    {
        public int DpiNotifyCallbackProcessStatusId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
