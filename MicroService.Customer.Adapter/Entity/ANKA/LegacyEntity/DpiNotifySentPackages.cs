﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class DpiNotifySentPackages
    {
        public int DpiNotifySentPackagesId { get; set; }
        public int DpiNotifyId { get; set; }
        public int CreatedPackageId { get; set; }
        public string PackageDataValues { get; set; }
        public string PackageType { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
