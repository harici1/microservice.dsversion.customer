﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class DsEinvoiceResponseLog
    {
        public int EinvoiceResponseLogId { get; set; }
        public string InvoiceNumber { get; set; }
        public string Response { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
