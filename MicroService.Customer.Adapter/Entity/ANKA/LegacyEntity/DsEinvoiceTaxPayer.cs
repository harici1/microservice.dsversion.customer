﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class DsEinvoiceTaxPayer
    {
        public int EinvoiceTaxPayerId { get; set; }
        public string IdVkn { get; set; }
        public string Aliass { get; set; }
        public string CreateDate { get; set; }
        public string CreateTime { get; set; }
        public string Title { get; set; }
        public string PartnerType { get; set; }
        public string ValidityStart { get; set; }
        public string ValidityEnd { get; set; }
        public string InactiveInd { get; set; }
        public string ValdtyStrtTime { get; set; }
        public string ValdtyEndTime { get; set; }
        public string LastChangeDate { get; set; }
        public string LastChangeTime { get; set; }
    }
}
