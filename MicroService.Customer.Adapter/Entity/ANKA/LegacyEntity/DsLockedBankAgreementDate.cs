﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class DsLockedBankAgreementDate
    {
        public int LockedBankAgreementDateId { get; set; }
        public int BankCode { get; set; }
        public string LockedDate { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
