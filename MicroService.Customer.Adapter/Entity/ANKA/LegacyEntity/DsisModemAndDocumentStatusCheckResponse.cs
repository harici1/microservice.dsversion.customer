﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class DsisModemAndDocumentStatusCheckResponse
    {
        public int Id { get; set; }
        public int? DsisCustomerId { get; set; }
        public string DsisUserCodeId { get; set; }
        public int? SubjectId { get; set; }
        public int? DsisWorkOrderId { get; set; }
        public int? StatusId { get; set; }
        public string Description { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool? ProcessStatusId { get; set; }
        public int? StatusDetailId { get; set; }
    }
}
