﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class DsmartContractListForInvoice
    {
        public int ContractId { get; set; }
        public string ContractNo { get; set; }
        public int? ContractStatusId { get; set; }
        public DateTime CreateDate { get; set; }
        public bool? HasInvoice { get; set; }
        public string CycleMonth { get; set; }
        public int DsmartContractListForInvoice1 { get; set; }
    }
}
