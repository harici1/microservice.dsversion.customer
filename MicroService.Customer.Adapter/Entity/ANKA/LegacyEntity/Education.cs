﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Education
    {
        public Education()
        {
            Customer = new HashSet<Customer>();
        }

        public int EducationId { get; set; }
        public string EducationName { get; set; }
        public bool? Active { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual ICollection<Customer> Customer { get; set; }
    }
}
