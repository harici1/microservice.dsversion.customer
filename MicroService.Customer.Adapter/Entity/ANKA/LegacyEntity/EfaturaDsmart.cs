﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class EfaturaDsmart
    {
        public int Id { get; set; }
        public string InvoiceNo { get; set; }
        public bool Another { get; set; }
    }
}
