﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class EinvoiceBatchService
    {
        public string InvoiceNo { get; set; }
        public bool IsCreate { get; set; }
        public bool IsSend { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int Id { get; set; }
    }
}
