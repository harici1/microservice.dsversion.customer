﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Enum
    {
        public int EnumId { get; set; }
        public string TableName { get; set; }
        public string EnumType { get; set; }
        public string DisplayName { get; set; }
        public bool? Active { get; set; }
    }
}
