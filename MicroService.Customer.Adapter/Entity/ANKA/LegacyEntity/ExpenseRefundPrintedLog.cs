﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ExpenseRefundPrintedLog
    {
        public int ExpenseRefundPrintedLogId { get; set; }
        public int InvoiceId { get; set; }
        public int OldBatchId { get; set; }
        public DateTime OldInvoiceDate { get; set; }
        public int OldCycleId { get; set; }
        public int NewBatchId { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
