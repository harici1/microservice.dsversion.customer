﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ExternalTaskReference
    {
        public int ExternalTaskReferenceId { get; set; }
        public int ExternalTaskReferenceTypeId { get; set; }
        public string ReferenceValue { get; set; }
    }
}
