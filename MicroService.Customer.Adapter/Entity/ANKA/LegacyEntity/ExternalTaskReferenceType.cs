﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ExternalTaskReferenceType
    {
        public int ExternalTaskReferenceTypeId { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
    }
}
