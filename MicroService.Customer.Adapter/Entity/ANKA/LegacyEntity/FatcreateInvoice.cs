﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class FatcreateInvoice
    {
        public int InvoiceId { get; set; }
        public string InvoiceNo { get; set; }
        public string InvoiceTypeId { get; set; }
        public int? InvoiceCycleId { get; set; }
        public int CustomerId { get; set; }
        public int ContractId { get; set; }
        public string InvoiceTitle { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string Cycle { get; set; }
        public DateTime LastBillingDate { get; set; }
        public string Currency { get; set; }
        public decimal AmountBeforeTax { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal InvoiceAmount { get; set; }
        public decimal PreviusMonthAmount { get; set; }
        public decimal? NextMonthAmount { get; set; }
        public decimal BillingAmount { get; set; }
        public decimal PaymentAmount { get; set; }
        public string TaxOffice { get; set; }
        public string TaxNumber { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string ZipCode { get; set; }
        public string AmountText { get; set; }
        public string AdMessage { get; set; }
        public int Status { get; set; }
        public int PrintStatus { get; set; }
        public bool Canceled { get; set; }
        public decimal? PreCyclePositiveAmount { get; set; }
        public string ModifyBy { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public int Version { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? BatchId { get; set; }
        public int? PrintTypeId { get; set; }
        public DateTime? PrintDate { get; set; }
        public string GibinvoiceId { get; set; }
        public DateTime? CancelDate { get; set; }
    }
}
