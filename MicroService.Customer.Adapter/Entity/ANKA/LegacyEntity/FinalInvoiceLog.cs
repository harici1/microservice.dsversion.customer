﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class FinalInvoiceLog
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public int? ContractId { get; set; }
        public int? ContractNo { get; set; }
        public int? CycleId { get; set; }
        public int? InvoiceId { get; set; }
        public DateTime? CancelDate { get; set; }
        public string Result { get; set; }
        public int? ReasonId { get; set; }
        public string Channel { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public int? ContractDetailId { get; set; }
    }
}
