﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class GuvenliInternetProfile
    {
        public int ProfileId { get; set; }
        public int? ParentProfileId { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }
    }
}
