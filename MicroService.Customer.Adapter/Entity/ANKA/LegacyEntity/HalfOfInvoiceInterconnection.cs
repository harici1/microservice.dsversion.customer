﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class HalfOfInvoiceInterconnection
    {
        public int RowId { get; set; }
        public int ContractId { get; set; }
        public int AssessmentId { get; set; }
        public int BonusId { get; set; }
        public decimal GivenAmount { get; set; }
        public decimal RemainingAmount { get; set; }
        public bool IsCanceled { get; set; }
        public DateTime CreationDate { get; set; }
        public string Creator { get; set; }
        public string Hostname { get; set; }
        public string Notes { get; set; }
    }
}
