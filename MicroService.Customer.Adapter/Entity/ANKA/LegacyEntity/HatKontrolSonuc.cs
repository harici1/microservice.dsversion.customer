﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class HatKontrolSonuc
    {
        public int HatKontrolSonucId { get; set; }
        public int? QueueTaskId { get; set; }
        public int ContractDetailId { get; set; }
        public DateTime CreateDate { get; set; }
        public string AttenuationDown { get; set; }
        public string AttenuationUp { get; set; }
        public string CardName { get; set; }
        public int? CardNo { get; set; }
        public int? CardType { get; set; }
        public string CrmSpeedValue { get; set; }
        public string CurrentDown { get; set; }
        public string CurrentUp { get; set; }
        public decimal? DkIndoorMaxDown { get; set; }
        public decimal? DkIndoorMaxUp { get; set; }
        public decimal? DkIndoorMinDown { get; set; }
        public decimal? DkIndoorMinUp { get; set; }
        public decimal? DkOutdoorMaxDown { get; set; }
        public decimal? DkOutdoorMaxUp { get; set; }
        public decimal? DkOutdoorMinDown { get; set; }
        public decimal? DkOutdoorMinUp { get; set; }
        public int? DslamNo { get; set; }
        public int? DslamType { get; set; }
        public string DslamTypeName { get; set; }
        public string DwnOutputPower { get; set; }
        public string MaxDown { get; set; }
        public string MaxUp { get; set; }
        public string NmsSpeedValue { get; set; }
        public string OperationalStatus { get; set; }
        public string Port { get; set; }
        public int? PortNo { get; set; }
        public decimal? SdIndoorMaxDown { get; set; }
        public decimal? SdIndoorMaxUp { get; set; }
        public decimal? SdIndoorMinDown { get; set; }
        public decimal? SdIndoorMinUp { get; set; }
        public decimal? SdOutdoorMaxDown { get; set; }
        public decimal? SdOutdoorMaxUp { get; set; }
        public decimal? SdOutdoorMinDown { get; set; }
        public string ServiceStatus { get; set; }
        public string ShelfName { get; set; }
        public int? ShelfNo { get; set; }
        public string SnrDown { get; set; }
        public string SnrUp { get; set; }
        public string UpOutputPower { get; set; }
    }
}
