﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class IcSolInvoice201811
    {
        public int RowId { get; set; }
        public string FaturaMüşteriAdı { get; set; }
        public string FaturaDetayıDönemi { get; set; }
        public string KullanıcıAdı { get; set; }
        public string SözleşmeSunuAdı { get; set; }
        public string FaturaDetayServisAçıklama { get; set; }
        public string FaturaDetayıÜrünAdı { get; set; }
        public string FaturaDetayıÜrünTipiLevel3 { get; set; }
        public string FaturaDetayıÜrünGamıKırılımı { get; set; }
        public string FaturaDetayıÜrünGamı { get; set; }
        public string HizmetStatüsü { get; set; }
        public DateTime? FaturaBaşlangıçTarihi { get; set; }
        public DateTime? SözleşmeDetayıBitişTarihi { get; set; }
        public decimal? NetTutarTl { get; set; }
    }
}
