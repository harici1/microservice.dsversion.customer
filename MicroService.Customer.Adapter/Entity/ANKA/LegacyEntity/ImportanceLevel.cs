﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ImportanceLevel
    {
        public int ImportanceLevelId { get; set; }
        public string DisplayName { get; set; }
        public bool? Active { get; set; }
    }
}
