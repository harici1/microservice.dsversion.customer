﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class InvoiceCycle
    {
        public int InvoiceCycleId { get; set; }
        public string InvoicePeriodCode { get; set; }
        public string InvoicePrefixCode { get; set; }
        public string FeeType { get; set; }
        public DateTime? CycleStartDate { get; set; }
        public DateTime? CycleEndDate { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public DateTime? LastBillingDate { get; set; }
        public bool? OpenCycle { get; set; }
        public string VoiceCycle { get; set; }
        public string QuotaCycle { get; set; }
        public int? InvoiceCycleTypeId { get; set; }
    }
}
