﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class InvoiceDetail
    {
        public int InvoiceDetailId { get; set; }
        public int? InvoiceId { get; set; }
        public int? InvoiceAssessmentId { get; set; }
        public int? ContractId { get; set; }
        public int? ContractDetailId { get; set; }
        public int? CatalogItemId { get; set; }
        public int? PricePlanDetailId { get; set; }
        public int? InvoiceCycleId { get; set; }
        public DateTime? InvoiceStartDate { get; set; }
        public DateTime? InvoiceEndDate { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? UnitAmount { get; set; }
        public decimal? AmountBeforeTax { get; set; }
        public decimal? VatRate { get; set; }
        public decimal? VatAmount { get; set; }
        public decimal? OivRate { get; set; }
        public decimal? OivAmount { get; set; }
        public decimal? TotalAmount { get; set; }
        public string ItemName { get; set; }
        public string Description { get; set; }
        public decimal? NumericValue1 { get; set; }
        public string VarcharValue1 { get; set; }
        public string VarcharValue2 { get; set; }
        public DateTime? CreateDate { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual Invoice Invoice { get; set; }
    }
}
