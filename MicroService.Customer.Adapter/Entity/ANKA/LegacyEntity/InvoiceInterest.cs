﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class InvoiceInterest
    {
        public int InvoiceInterestId { get; set; }
        public int? CustomerId { get; set; }
        public int? ContractId { get; set; }
        public int? InvoiceAssessmentId { get; set; }
        public int? OpenInvoiceId { get; set; }
        public int? InvoiceCycleId { get; set; }
        public DateTime? LastBillingDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public decimal? InvoiceAmount { get; set; }
        public decimal? PaymentAmount { get; set; }
        public decimal? RemainingAmount { get; set; }
        public decimal? InterestAmount { get; set; }
        public int? DeptDay { get; set; }
        public decimal? InterestRate { get; set; }
        public decimal? InterestValue { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool? Canceled { get; set; }
    }
}
