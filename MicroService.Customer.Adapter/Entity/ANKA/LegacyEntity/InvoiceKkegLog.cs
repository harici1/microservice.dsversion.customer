﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class InvoiceKkegLog
    {
        public int LogId { get; set; }
        public DateTime LogDate { get; set; }
        public string LogUser { get; set; }
        public int ContractId { get; set; }
        public int InvoiceId { get; set; }
        public string InvoiceNo { get; set; }
        public decimal OldDebitAmount { get; set; }
        public bool Canceled { get; set; }
        public string ModifyBy { get; set; }
        public DateTime? LastModifyDate { get; set; }
    }
}
