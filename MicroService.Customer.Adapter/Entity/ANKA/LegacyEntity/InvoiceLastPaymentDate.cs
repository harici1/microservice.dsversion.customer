﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class InvoiceLastPaymentDate
    {
        public int Id { get; set; }
        public DateTime? CycleDate { get; set; }
        public DateTime? LastPaymentDate { get; set; }
        public string InvoiceType { get; set; }
    }
}
