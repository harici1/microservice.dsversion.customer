﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class InvoiceListRequestLog
    {
        public int RequestId { get; set; }
        public int BankCode { get; set; }
        public string ContractNo { get; set; }
        public DateTime RecordDate { get; set; }
    }
}
