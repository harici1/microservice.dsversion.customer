﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class InvoiceListResponseLog
    {
        public int ResponseId { get; set; }
        public int RequestId { get; set; }
        public string ContractNo { get; set; }
        public string NameSurname { get; set; }
        public string InvoiceNo { get; set; }
        public decimal BillingAmount { get; set; }
        public DateTime LastBillingDate { get; set; }
        public string Cycle { get; set; }
        public string ReturnCode { get; set; }
        public string ReturnDesc { get; set; }
        public string SelectDb { get; set; }
        public DateTime RecordDate { get; set; }
    }
}
