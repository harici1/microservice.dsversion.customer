﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class InvoiceLog
    {
        public int Id { get; set; }
        public int? BatchId { get; set; }
        public int? CycleId { get; set; }
        public int? CustomerId { get; set; }
        public int? ContractId { get; set; }
        public int? CaseId { get; set; }
        public string CaseDescription { get; set; }
        public decimal? VatAmount { get; set; }
        public decimal? OivRate { get; set; }
        public decimal? OivAmount { get; set; }
        public decimal? TotalAmount { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
    }
}
