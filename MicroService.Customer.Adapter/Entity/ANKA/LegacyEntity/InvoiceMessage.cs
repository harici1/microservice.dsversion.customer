﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class InvoiceMessage
    {
        public int RowId { get; set; }
        public int ContractId { get; set; }
        public string ContractNo { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string MessageText { get; set; }
        public int SortOrder { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public string AdditionalInfo { get; set; }
    }
}
