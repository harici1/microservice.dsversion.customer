﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class InvoicePeriod
    {
        public string InvoicePeriodCode { get; set; }
        public string FeeType { get; set; }
        public string PeriodName { get; set; }
        public string Description { get; set; }
        public int CycleDay { get; set; }
        public int ProcessDay { get; set; }
        public DateTime? LastCycleDate { get; set; }
        public string CycleType { get; set; }
        public string InvoicePaymentStatus { get; set; }
    }
}
