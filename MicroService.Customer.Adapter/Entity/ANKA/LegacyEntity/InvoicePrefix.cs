﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class InvoicePrefix
    {
        public string PrefixCode { get; set; }
        public int? MaxNumber { get; set; }
        public int? Length { get; set; }
    }
}
