﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class InvoicePrintStatus
    {
        public int PrintStatusId { get; set; }
        public string Description { get; set; }
    }
}
