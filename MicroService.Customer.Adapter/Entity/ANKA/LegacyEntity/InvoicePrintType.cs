﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class InvoicePrintType
    {
        public int PrintTypeId { get; set; }
        public string Description { get; set; }
    }
}
