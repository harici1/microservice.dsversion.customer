﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class InvoiceStatus
    {
        public int InvoiceStatusId { get; set; }
        public string Description { get; set; }
    }
}
