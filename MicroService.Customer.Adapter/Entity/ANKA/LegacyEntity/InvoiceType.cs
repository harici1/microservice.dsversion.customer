﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class InvoiceType
    {
        public string InvoiceTypeId { get; set; }
        public string InvoiceTypeName { get; set; }
    }
}
