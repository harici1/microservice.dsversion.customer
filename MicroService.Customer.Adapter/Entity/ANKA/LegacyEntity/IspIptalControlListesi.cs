﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class IspIptalControlListesi
    {
        public string Value { get; set; }
        public DateTime? ListeOlusmaTarihi { get; set; }
        public int Id { get; set; }
        public DateTime? KontrolTarihi { get; set; }
        public int? ContractId { get; set; }
        public int? ContractDetailId { get; set; }
        public int? ContractDetailServiceParamId { get; set; }
        public string KontrolSonuc { get; set; }
    }
}
