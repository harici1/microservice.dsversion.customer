﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ItftLegitimateProceedingBatch
    {
        public int LegitimateProceedingBatchId { get; set; }
        public string BatchName { get; set; }
        public DateTime? CreationDate { get; set; }
        public string CreateBy { get; set; }
    }
}
