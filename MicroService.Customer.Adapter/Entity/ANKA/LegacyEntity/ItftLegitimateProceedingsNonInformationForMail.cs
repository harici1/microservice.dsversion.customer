﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ItftLegitimateProceedingsNonInformationForMail
    {
        public int LegitimateProceedingsNonInformationForMailId { get; set; }
        public string LastContractNo { get; set; }
        public int? LastContractId { get; set; }
        public bool IsSent { get; set; }
        public bool IsNonCellPhone { get; set; }
        public bool IsNonXdslNo { get; set; }
        public bool IsNonSubscriberNo { get; set; }
        public string CreatedById { get; set; }
        public DateTime CreateDate { get; set; }
        public string LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public string SubscriberNo { get; set; }
        public string XdslNo { get; set; }
        public string CellPhoneNumber { get; set; }
    }
}
