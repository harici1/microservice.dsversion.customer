﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ItftLegitimateProceedingsProcess
    {
        public long LegitimateProceedingsProcessId { get; set; }
        public int ContractId { get; set; }
        public string ContractNo { get; set; }
        public int LastContractId { get; set; }
        public string LastContractNo { get; set; }
        public int? SmslogId { get; set; }
        public int LegitimateProceedingsProcessTypeId { get; set; }
        public int LegitimateProceedingBatchId { get; set; }
        public byte Status { get; set; }
        public int? InvoiceCount { get; set; }
        public decimal TotalDebitAmount { get; set; }
        public bool? IsBundle { get; set; }
        public string Comment { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public string LastUpdatedById { get; set; }
    }
}
