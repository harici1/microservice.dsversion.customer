﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ItftLegitimateProceedingsProcessContractInformation
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int ContractId { get; set; }
        public int LastContractId { get; set; }
        public int? ContractStatusId { get; set; }
        public string SubscriberNo { get; set; }
        public string XdslNo { get; set; }
        public DateTime? CancelDate { get; set; }
        public string CellPhoneNumber { get; set; }
        public string CreatedById { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
