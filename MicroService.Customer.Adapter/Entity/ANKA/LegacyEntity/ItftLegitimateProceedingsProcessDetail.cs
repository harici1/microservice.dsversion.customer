﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ItftLegitimateProceedingsProcessDetail
    {
        public int LegitimateProceedingsProcessDetailId { get; set; }
        public int LegitimateProceedingsProcessId { get; set; }
        public int LastContractId { get; set; }
        public int ContractId { get; set; }
        public int InvoiceId { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public decimal InvoiceAmount { get; set; }
        public int? LegitimateProceedingsProcessTypeId { get; set; }
        public string CreatedById { get; set; }
        public DateTime CreateDate { get; set; }
        public string LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
    }
}
