﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ItftLegitimateProceedingsSmsDateDiffValues
    {
        public int ItftLegitimateProceedingsSmsDateDiffValuesId { get; set; }
        public int LegitimateProceedingsProcessTypeId { get; set; }
        public string Description { get; set; }
        public int DateDiffValue { get; set; }
        public DateTime? CreationDate { get; set; }
        public string CreateBy { get; set; }
    }
}
