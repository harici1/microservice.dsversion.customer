﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class IvrrequestLog
    {
        public int RequestId { get; set; }
        public string Pstnno { get; set; }
        public DateTime? RequestStartDate { get; set; }
        public DateTime? RequestEndDate { get; set; }
        public int? CustomerId { get; set; }
        public string ContractNo { get; set; }
    }
}
