﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class IvrsmileAdslIntegration
    {
        public int Id { get; set; }
        public string Adslno { get; set; }
        public string HesapNo { get; set; }
        public string Ani { get; set; }
        public DateTime? DateTime { get; set; }
        public string Ivrresult { get; set; }
        public string Inout { get; set; }
        public string IvrgirisTip { get; set; }
    }
}
