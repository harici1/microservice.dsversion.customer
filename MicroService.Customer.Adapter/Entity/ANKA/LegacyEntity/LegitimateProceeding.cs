﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class LegitimateProceeding
    {
        public int LegitimateProceedingId { get; set; }
        public int CustomerId { get; set; }
        public int ContractId { get; set; }
        public string ContractNo { get; set; }
        public string LawyerUserName { get; set; }
        public string PhoneNumber { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public string LastUpdateBy { get; set; }
        public bool? Active { get; set; }
        public string AnkaUserName { get; set; }
    }
}
