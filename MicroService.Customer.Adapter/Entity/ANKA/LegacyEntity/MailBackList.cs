﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class MailBackList
    {
        public string Mailadr { get; set; }
        public bool? Smile { get; set; }
    }
}
