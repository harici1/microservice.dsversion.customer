﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class MarsResponsePortBilgisi
    {
        public int MarsResponsePortBilgisi1 { get; set; }
        public int ArizaAnkaTicketId { get; set; }
        public string DownloadAttenuation { get; set; }
        public string DownloadCikisGucu { get; set; }
        public string DownloadKapasiteHizi { get; set; }
        public string DownloadMevcutHiz { get; set; }
        public string DownloadNoiseMargin { get; set; }
        public string HatFizikselDurum { get; set; }
        public string HatYonetimselDurum { get; set; }
        public string TestTeyitZamani { get; set; }
        public string NmsHizProfili { get; set; }
        public string UploadAttenuation { get; set; }
        public string UploadCikisGucu { get; set; }
        public string UploadKapasiteHizi { get; set; }
        public string UploadMevcutHiz { get; set; }
        public string UploadNoiseMargin { get; set; }
        public string WorkOrderId { get; set; }
    }
}
