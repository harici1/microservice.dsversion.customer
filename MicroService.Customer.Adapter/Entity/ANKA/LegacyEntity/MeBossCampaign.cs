﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class MeBossCampaign
    {
        public int Id { get; set; }
        public int? ContractId { get; set; }
        public int? CustomerId { get; set; }
        public string Name { get; set; }
        public int? SubscriptionId { get; set; }
    }
}
