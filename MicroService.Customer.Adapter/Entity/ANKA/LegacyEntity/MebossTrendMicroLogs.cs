﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class MebossTrendMicroLogs
    {
        public int TrendMicroLogId { get; set; }
        public string Message { get; set; }
        public DateTime? OperationDate { get; set; }
        public string Serial { get; set; }
        public int? SubscriptionId { get; set; }
        public int? Type { get; set; }
        public int? TrendMicroType { get; set; }
    }
}
