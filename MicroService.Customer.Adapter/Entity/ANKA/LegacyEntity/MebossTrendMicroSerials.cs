﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class MebossTrendMicroSerials
    {
        public int TrendMicroSerialId { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? SaleDate { get; set; }
        public string Serial { get; set; }
        public int? Status { get; set; }
        public int? ContractId { get; set; }
        public int? TrendMicroType { get; set; }
    }
}
