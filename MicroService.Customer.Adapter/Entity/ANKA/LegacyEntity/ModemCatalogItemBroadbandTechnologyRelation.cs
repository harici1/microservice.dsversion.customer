﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ModemCatalogItemBroadbandTechnologyRelation
    {
        public int ModemCatalogItemBroadbandTechnologyRelationId { get; set; }
        public int ModemCatalogItemId { get; set; }
        public string BroadbandTechnologyName { get; set; }
        public DateTime CreateDate { get; set; }
        public bool Active { get; set; }
    }
}
