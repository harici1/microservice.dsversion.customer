﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ModuleDetail
    {
        public int Id { get; set; }
        public int? ModuleId { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public int? SequenceNo { get; set; }
        public int? AuthSelect { get; set; }
        public int? AuthInsert { get; set; }
        public int? AuthUpdate { get; set; }
        public int? AuthApprove { get; set; }
        public int? AuthDelete { get; set; }
        public bool? Active { get; set; }
    }
}
