﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class MoreumDocument
    {
        public int MoreumDocumentId { get; set; }
        public string InstanceId { get; set; }
        public int MoreumDocumentTypeId { get; set; }
        public int MoreumDocumentStatusId { get; set; }
        public string UniqueIdentifier { get; set; }
        public string DocumentBarcode { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime LastUpdatedDate { get; set; }
    }
}
