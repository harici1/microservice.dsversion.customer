﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class MoreumDocumentStatusReason
    {
        public int MoreumDocumentStatusReasonId { get; set; }
        public int? MoreumDocumentStatusId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
