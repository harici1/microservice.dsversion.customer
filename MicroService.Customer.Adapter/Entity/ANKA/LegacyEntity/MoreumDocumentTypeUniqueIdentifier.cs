﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class MoreumDocumentTypeUniqueIdentifier
    {
        public int Id { get; set; }
        public int MoreumDocumentTypeId { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }
    }
}
