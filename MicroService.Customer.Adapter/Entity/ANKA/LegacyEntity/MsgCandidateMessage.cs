﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class MsgCandidateMessage
    {
        public int MessageId { get; set; }
        public int CustomerId { get; set; }
        public int ContractId { get; set; }
        public int MessageTemplateId { get; set; }
        public bool Bulk { get; set; }
        public string To { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime DueDate { get; set; }
        public string DataQueryParameters { get; set; }
    }
}
