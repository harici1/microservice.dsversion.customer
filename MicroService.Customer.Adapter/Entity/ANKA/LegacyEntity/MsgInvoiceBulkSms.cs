﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class MsgInvoiceBulkSms
    {
        public int Id { get; set; }
        public int MessageTemplateId { get; set; }
        public int CustomerId { get; set; }
        public int ContractId { get; set; }
        public string ContractNo { get; set; }
        public string Title { get; set; }
        public string MobilePhone { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime LastBillingDate { get; set; }
        public decimal RemainingAmount { get; set; }
    }
}
