﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class MsgMessageDeliveryStatus
    {
        public int MessageDeliveryStatusId { get; set; }
        public string DisplayName { get; set; }
    }
}
