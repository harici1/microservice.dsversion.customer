﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class MsgMessageSendingStatus
    {
        public int MessageSendingStatusId { get; set; }
        public string DisplayName { get; set; }
        public string WebDisplayName { get; set; }
    }
}
