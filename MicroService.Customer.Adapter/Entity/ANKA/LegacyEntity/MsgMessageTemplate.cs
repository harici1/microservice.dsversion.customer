﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class MsgMessageTemplate
    {
        public int MessageTemplateId { get; set; }
        public string DisplayName { get; set; }
        public int MessageTypeId { get; set; }
        public bool Static { get; set; }
        public bool External { get; set; }
        public string Template { get; set; }
        public string DataQuery { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public int SendingRate { get; set; }
        public string BeginTime { get; set; }
        public string EndTime { get; set; }
    }
}
