﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class NbBasicRateDetails
    {
        public decimal BasicRateDetailsId { get; set; }
        public decimal BasicRatesId { get; set; }
        public string CountryCode { get; set; }
        public decimal PerMinFeeFirstUnit { get; set; }
        public decimal PerMinFeeNextUnit { get; set; }
    }
}
