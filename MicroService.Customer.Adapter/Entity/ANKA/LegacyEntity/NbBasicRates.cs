﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class NbBasicRates
    {
        public decimal BasicRatesId { get; set; }
        public string Name { get; set; }
        public string Decription { get; set; }
        public DateTime ExpirationDate { get; set; }
        public decimal ConnectFee { get; set; }
        public decimal FirstUnitSec { get; set; }
        public decimal NextUnitSec { get; set; }
    }
}
