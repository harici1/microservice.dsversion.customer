﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class NbCampaign
    {
        public NbCampaign()
        {
            NbCampaignCommitment = new HashSet<NbCampaignCommitment>();
            NbCampaignDetail = new HashSet<NbCampaignDetail>();
        }

        public int CampaignId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public byte? Status { get; set; }
        public string EligibilityPolicy { get; set; }
        public string TransferPolicy { get; set; }
        public string IndirectTransferPolicy { get; set; }
        public string ExitPolicy { get; set; }
        public bool SaleRequiresCc { get; set; }
        public bool AutoChargeCcrequired { get; set; }
        public bool? UpgradeAllowed { get; set; }
        public bool? DowngradeAllowed { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public string WebSaleDescription { get; set; }
        public bool? WebVisibility { get; set; }
        public string WebsaleTitle { get; set; }
        public string ReportDescription { get; set; }
        public byte? AvalaibleForSaleType { get; set; }

        public virtual ICollection<NbCampaignCommitment> NbCampaignCommitment { get; set; }
        public virtual ICollection<NbCampaignDetail> NbCampaignDetail { get; set; }
    }
}
