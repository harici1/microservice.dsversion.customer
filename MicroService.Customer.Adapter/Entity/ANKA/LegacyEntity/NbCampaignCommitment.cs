﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class NbCampaignCommitment
    {
        public int Id { get; set; }
        public int CampaignId { get; set; }
        public bool? Active { get; set; }
        public bool? IsCommitmentControl { get; set; }

        public virtual NbCampaign Campaign { get; set; }
    }
}
