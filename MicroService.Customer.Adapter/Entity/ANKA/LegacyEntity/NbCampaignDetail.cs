﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class NbCampaignDetail
    {
        public int CampaignId { get; set; }
        public int CatalogItemId { get; set; }
        public int PricePlanId { get; set; }

        public virtual NbCampaign Campaign { get; set; }
        public virtual NbCatalogItem CatalogItem { get; set; }
        public virtual NbPricePlan PricePlan { get; set; }
    }
}
