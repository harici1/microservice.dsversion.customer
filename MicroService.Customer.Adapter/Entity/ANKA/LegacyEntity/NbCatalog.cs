﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class NbCatalog
    {
        public NbCatalog()
        {
            NbCatalogItem = new HashSet<NbCatalogItem>();
        }

        public int CatalogId { get; set; }
        public int CompanyId { get; set; }

        public virtual ICollection<NbCatalogItem> NbCatalogItem { get; set; }
    }
}
