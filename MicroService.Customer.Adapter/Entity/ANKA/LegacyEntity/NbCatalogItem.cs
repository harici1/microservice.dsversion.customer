﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class NbCatalogItem
    {
        public NbCatalogItem()
        {
            NbCampaignDetail = new HashSet<NbCampaignDetail>();
            NbCatalogItemCategoryDetail = new HashSet<NbCatalogItemCategoryDetail>();
            NbCatalogItemPropertyValue = new HashSet<NbCatalogItemPropertyValue>();
            NbPricePlan = new HashSet<NbPricePlan>();
            NbProductAddonAddonCatalogItem = new HashSet<NbProductAddon>();
            NbProductAddonCatalogItem = new HashSet<NbProductAddon>();
        }

        public int CatalogItemId { get; set; }
        public int ItemTypeId { get; set; }
        public string CatalogItemName { get; set; }
        public int CatalogId { get; set; }
        public bool? Active { get; set; }
        public bool List { get; set; }
        public bool? Intangible { get; set; }
        public int? PropertyTemplateId { get; set; }
        public int? ServiceParamId { get; set; }
        public string AdslSaleType { get; set; }
        public bool RequiresDelivery { get; set; }
        public bool? SetupServiceOffered { get; set; }
        public bool? SetupServiceMandatory { get; set; }
        public int? DisplayOrder { get; set; }
        public int? Grade { get; set; }
        public bool? Suspend { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public string WebDisplayCatalogItemName { get; set; }

        public virtual NbCatalog Catalog { get; set; }
        public virtual NbCatalogItemType ItemType { get; set; }
        public virtual NbPropertyTemplate PropertyTemplate { get; set; }
        public virtual ICollection<NbCampaignDetail> NbCampaignDetail { get; set; }
        public virtual ICollection<NbCatalogItemCategoryDetail> NbCatalogItemCategoryDetail { get; set; }
        public virtual ICollection<NbCatalogItemPropertyValue> NbCatalogItemPropertyValue { get; set; }
        public virtual ICollection<NbPricePlan> NbPricePlan { get; set; }
        public virtual ICollection<NbProductAddon> NbProductAddonAddonCatalogItem { get; set; }
        public virtual ICollection<NbProductAddon> NbProductAddonCatalogItem { get; set; }
    }
}
