﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class NbCatalogItemCategoryDetail
    {
        public int CategoryId { get; set; }
        public int CatalogItemId { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual NbCatalogItem CatalogItem { get; set; }
        public virtual NbCatalogItemCategory Category { get; set; }
    }
}
