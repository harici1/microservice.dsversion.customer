﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class NbCatalogItemType
    {
        public NbCatalogItemType()
        {
            NbCatalogItem = new HashSet<NbCatalogItem>();
        }

        public int ItemTypeId { get; set; }
        public string CatalogItemTypeName { get; set; }

        public virtual ICollection<NbCatalogItem> NbCatalogItem { get; set; }
    }
}
