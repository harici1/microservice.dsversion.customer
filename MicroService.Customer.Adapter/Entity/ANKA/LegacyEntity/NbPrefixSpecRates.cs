﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class NbPrefixSpecRates
    {
        public decimal PrefixSpecRatesId { get; set; }
        public decimal RateId { get; set; }
        public string Name { get; set; }
        public string Decription { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}
