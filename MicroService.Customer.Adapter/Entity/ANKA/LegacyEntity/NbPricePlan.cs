﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class NbPricePlan
    {
        public NbPricePlan()
        {
            NbCampaignDetail = new HashSet<NbCampaignDetail>();
            NbPricePlanDetail = new HashSet<NbPricePlanDetail>();
        }

        public int PricePlanId { get; set; }
        public int? CatalogItemId { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public bool? Active { get; set; }
        public bool? Periodic { get; set; }
        public int? PeriodCountingPricePlanDetailId { get; set; }
        public int? Commitment { get; set; }

        public virtual NbCatalogItem CatalogItem { get; set; }
        public virtual ICollection<NbCampaignDetail> NbCampaignDetail { get; set; }
        public virtual ICollection<NbPricePlanDetail> NbPricePlanDetail { get; set; }
    }
}
