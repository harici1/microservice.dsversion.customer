﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class NbProductAddon
    {
        public int ProductAddonId { get; set; }
        public int CatalogItemId { get; set; }
        public int AddonCatalogItemId { get; set; }
        public int? MaxCount { get; set; }

        public virtual NbCatalogItem AddonCatalogItem { get; set; }
        public virtual NbCatalogItem CatalogItem { get; set; }
    }
}
