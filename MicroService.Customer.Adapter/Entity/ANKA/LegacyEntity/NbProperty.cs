﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class NbProperty
    {
        public NbProperty()
        {
            NbCatalogItemPropertyValue = new HashSet<NbCatalogItemPropertyValue>();
        }

        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
        public string DisplayName { get; set; }

        public virtual ICollection<NbCatalogItemPropertyValue> NbCatalogItemPropertyValue { get; set; }
    }
}
