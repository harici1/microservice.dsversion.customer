﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class NbPropertyTemplate
    {
        public NbPropertyTemplate()
        {
            NbCatalogItem = new HashSet<NbCatalogItem>();
        }

        public int PropertyTemplateId { get; set; }
        public string PropertyTemplateName { get; set; }
        public string Description { get; set; }

        public virtual ICollection<NbCatalogItem> NbCatalogItem { get; set; }
    }
}
