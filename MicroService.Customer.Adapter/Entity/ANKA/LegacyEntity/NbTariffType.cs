﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class NbTariffType
    {
        public decimal TariffTypeId { get; set; }
        public string TariffName { get; set; }
        public string TariffDescription { get; set; }
        public decimal TariffSetupFee { get; set; }
        public decimal TariffFixedCharge { get; set; }
        public decimal TariffFixedChargePeriod { get; set; }
        public byte RateTypeId { get; set; }
        public decimal RateId { get; set; }
        public byte Restricted { get; set; }
        public byte Active { get; set; }
    }
}
