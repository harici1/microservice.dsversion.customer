﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Notes
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Domain { get; set; }
        public string UserName { get; set; }
        public string Title { get; set; }
        public string Info { get; set; }
        public string FilePath { get; set; }
        public string FullFilePath { get; set; }
        public DateTime CrateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string RefType { get; set; }
        public int RefId { get; set; }
        public string RefParameter { get; set; }
        public string UserOpine { get; set; }
        public byte Active { get; set; }

        public virtual UserData UserNameNavigation { get; set; }
    }
}
