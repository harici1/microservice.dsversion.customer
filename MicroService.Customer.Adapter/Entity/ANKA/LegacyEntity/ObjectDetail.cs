﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ObjectDetail
    {
        public int Id { get; set; }
        public int? ObjectId { get; set; }
        public int? ControlId { get; set; }
        public int? SequenceNo { get; set; }
        public string Name { get; set; }
        public int? MenuGroupId { get; set; }
        public int? ProcessForm { get; set; }
        public int? Status { get; set; }
        public int? AuthSelect { get; set; }
        public int? AuthInsert { get; set; }
        public int? AuthUpdate { get; set; }
        public int? AuthApprove { get; set; }
        public int? AuthDelete { get; set; }
    }
}
