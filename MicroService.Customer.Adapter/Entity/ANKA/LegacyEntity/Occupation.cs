﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Occupation
    {
        public Occupation()
        {
            Customer = new HashSet<Customer>();
        }

        public int OccupationId { get; set; }
        public string OccupationName { get; set; }
        public bool? Active { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual ICollection<Customer> Customer { get; set; }
    }
}
