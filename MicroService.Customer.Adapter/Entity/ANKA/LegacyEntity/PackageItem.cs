﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class PackageItem
    {
        public int PackageItemId { get; set; }
        public int CatalogItemId { get; set; }
        public int? ComponentCatalogItemId { get; set; }
        public int? ParentCatalogItemId { get; set; }
        public bool? IsMigratable { get; set; }
    }
}
