﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class PayBackContractPoolDetailLog
    {
        public int PayBackDetailId { get; set; }
        public int? PayBackId { get; set; }
        public int? PayBackTypeId { get; set; }
        public int? CustomerId { get; set; }
        public int? ContractId { get; set; }
        public int? OrgContractId { get; set; }
        public int? ContractDetailId { get; set; }
        public int? StatusJournalId { get; set; }
        public int? StatusId { get; set; }
        public int? CatalogItemId { get; set; }
        public int? PricePlanDetailId { get; set; }
        public int? ServiceParamId { get; set; }
        public DateTime? StatusChangeStartDate { get; set; }
        public DateTime? StatusChangeEndDate { get; set; }
        public int Status { get; set; }
        public int? InvoiceId { get; set; }
        public int? InvoiceAssessmentId { get; set; }
        public int? InvoiceCycleId { get; set; }
        public DateTime PayBackStartDate { get; set; }
        public DateTime PayBackEndDate { get; set; }
        public long? PayBackBinaryDecimalDate { get; set; }
        public decimal? Amount { get; set; }
        public decimal? OivRate { get; set; }
        public string Description { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string ModifyBy { get; set; }
        public byte[] HashField { get; set; }
    }
}
