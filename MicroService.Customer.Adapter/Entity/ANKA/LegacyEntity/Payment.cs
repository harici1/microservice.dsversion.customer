﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Payment
    {
        public int PaymentId { get; set; }
        public int? FirstPaymentId { get; set; }
        public int? CustomerId { get; set; }
        public int? ContractId { get; set; }
        public int? InvoiceId { get; set; }
        public decimal? Amount { get; set; }
        public string PaymentMethod { get; set; }
        public string PaymentPlace { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string Description { get; set; }
        public string UserName { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool Canceled { get; set; }
        public string ModifyBy { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public int InstallmentCount { get; set; }
        public string OrderId { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
