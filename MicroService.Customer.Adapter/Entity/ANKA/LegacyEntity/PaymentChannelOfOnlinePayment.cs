﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class PaymentChannelOfOnlinePayment
    {
        public long Id { get; set; }
        public int? PaymentId { get; set; }
        public string PaymentChannel { get; set; }
    }
}
