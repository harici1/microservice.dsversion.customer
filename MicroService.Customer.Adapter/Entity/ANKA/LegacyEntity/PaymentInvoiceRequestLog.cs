﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class PaymentInvoiceRequestLog
    {
        public int RequestId { get; set; }
        public int BankCode { get; set; }
        public string ContractNo { get; set; }
        public string InvoiceNo { get; set; }
        public string Stan { get; set; }
        public decimal PaymentAmount { get; set; }
        public DateTime PaymentDate { get; set; }
        public string BankRefNo { get; set; }
        public string BankBranchNo { get; set; }
        public DateTime RecordDate { get; set; }
    }
}
