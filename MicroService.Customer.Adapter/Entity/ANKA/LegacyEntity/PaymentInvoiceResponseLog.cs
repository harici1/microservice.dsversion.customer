﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class PaymentInvoiceResponseLog
    {
        public int ResponseId { get; set; }
        public int RequestId { get; set; }
        public string ReturnCode { get; set; }
        public string ReturnDesc { get; set; }
        public string SelectDb { get; set; }
        public DateTime RecordDate { get; set; }
    }
}
