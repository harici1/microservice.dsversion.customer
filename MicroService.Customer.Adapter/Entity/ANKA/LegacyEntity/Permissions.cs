﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Permissions
    {
        public int Id { get; set; }
        public string PermissionName { get; set; }
    }
}
