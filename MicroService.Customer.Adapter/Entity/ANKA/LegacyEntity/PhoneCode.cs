﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class PhoneCode
    {
        public int PhoneCodeId { get; set; }
        public int ProvinceId { get; set; }
        public string PhoneCode1 { get; set; }
        public bool? Active { get; set; }
        public Guid MsreplTranVersion { get; set; }
    }
}
