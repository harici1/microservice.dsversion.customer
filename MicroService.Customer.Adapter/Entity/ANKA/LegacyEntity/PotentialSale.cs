﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class PotentialSale
    {
        public int PotentialSaleId { get; set; }
        public int? CustomerId { get; set; }
        public int? CatalogItemId { get; set; }
        public int? CampaignId { get; set; }
        public string Phone { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? CreateBy { get; set; }
    }
}
