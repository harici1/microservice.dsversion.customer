﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class PricePlan
    {
        public PricePlan()
        {
            Bundle = new HashSet<Bundle>();
            CampaignDetail = new HashSet<CampaignDetail>();
            ContractDetail = new HashSet<ContractDetail>();
            PricePlanDetail = new HashSet<PricePlanDetail>();
        }

        public int PricePlanId { get; set; }
        public int? CatalogItemId { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public bool? Active { get; set; }
        public bool? Periodic { get; set; }
        public int? PeriodCountingPricePlanDetailId { get; set; }
        public int? Commitment { get; set; }

        public virtual CatalogItem CatalogItem { get; set; }
        public virtual ICollection<Bundle> Bundle { get; set; }
        public virtual ICollection<CampaignDetail> CampaignDetail { get; set; }
        public virtual ICollection<ContractDetail> ContractDetail { get; set; }
        public virtual ICollection<PricePlanDetail> PricePlanDetail { get; set; }
    }
}
