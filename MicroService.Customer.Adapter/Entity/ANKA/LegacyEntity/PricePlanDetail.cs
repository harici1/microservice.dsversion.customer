﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class PricePlanDetail
    {
        public PricePlanDetail()
        {
            InvoiceAssessment = new HashSet<InvoiceAssessment>();
        }

        public int PricePlanDetailId { get; set; }
        public int? PricePlanId { get; set; }
        public string CalculationType { get; set; }
        public string FeeType { get; set; }
        public string ChargingType { get; set; }
        public string ChargingPolicy { get; set; }
        public bool? ChargePartial { get; set; }
        public string ExitPolicy { get; set; }
        public decimal? ExitPrice { get; set; }
        public int? Commitment { get; set; }
        public string PeriodUnit { get; set; }
        public string ChargePoint { get; set; }
        public string FirstPeriod { get; set; }
        public string LastPeriod { get; set; }
        public decimal? PriceBeforeTax { get; set; }
        public decimal? Price { get; set; }
        public decimal? UnitStart { get; set; }
        public decimal? UnitEnd { get; set; }
        public decimal? LowerSaturation { get; set; }
        public decimal? UpperSaturation { get; set; }
        public string Currency { get; set; }
        public string Vat { get; set; }
        public string Oiv { get; set; }
        public bool? TaxIncluded { get; set; }
        public string InvListingPolicy { get; set; }
        public string Description { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public int? ExtendLastPeriod { get; set; }

        public virtual PricePlan PricePlan { get; set; }
        public virtual ICollection<InvoiceAssessment> InvoiceAssessment { get; set; }
    }
}
