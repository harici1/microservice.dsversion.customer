﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ProductAddon
    {
        public int ProductAddonId { get; set; }
        public int CatalogItemId { get; set; }
        public int AddonCatalogItemId { get; set; }
        public int? MaxCount { get; set; }

        public virtual CatalogItem AddonCatalogItem { get; set; }
        public virtual CatalogItem CatalogItem { get; set; }
    }
}
