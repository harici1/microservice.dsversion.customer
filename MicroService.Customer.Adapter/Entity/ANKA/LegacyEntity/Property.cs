﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Property
    {
        public Property()
        {
            CatalogItemPropertyValue = new HashSet<CatalogItemPropertyValue>();
        }

        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
        public string DisplayName { get; set; }

        public virtual ICollection<CatalogItemPropertyValue> CatalogItemPropertyValue { get; set; }
    }
}
