﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class PropertyTemplate
    {
        public PropertyTemplate()
        {
            CatalogItem = new HashSet<CatalogItem>();
        }

        public int PropertyTemplateId { get; set; }
        public string PropertyTemplateName { get; set; }
        public string Description { get; set; }

        public virtual ICollection<CatalogItem> CatalogItem { get; set; }
    }
}
