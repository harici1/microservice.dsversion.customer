﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Province
    {
        public Province()
        {
            City = new HashSet<City>();
        }

        public int ProvinceId { get; set; }
        public string ProvinceCode { get; set; }
        public string ProvinceName { get; set; }
        public int CountryId { get; set; }
        public bool? Active { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual Country Country { get; set; }
        public virtual ICollection<City> City { get; set; }
    }
}
