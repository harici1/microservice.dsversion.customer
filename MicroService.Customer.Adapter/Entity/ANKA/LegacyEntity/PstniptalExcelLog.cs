﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class PstniptalExcelLog
    {
        public int Id { get; set; }
        public string ExcelName { get; set; }
        public int? Process { get; set; }
    }
}
