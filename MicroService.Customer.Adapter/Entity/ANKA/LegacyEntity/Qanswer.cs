﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Qanswer
    {
        public int AnswerId { get; set; }
        public int QuestionId { get; set; }
        public string RefType { get; set; }
        public int RefId { get; set; }
        public DateTime AnswerDate { get; set; }
        public string CreateBy { get; set; }
    }
}
