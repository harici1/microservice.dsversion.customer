﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class QanswerDetail
    {
        public int AnswerDetailId { get; set; }
        public int AnswerId { get; set; }
        public int? AnswerOptionId { get; set; }
    }
}
