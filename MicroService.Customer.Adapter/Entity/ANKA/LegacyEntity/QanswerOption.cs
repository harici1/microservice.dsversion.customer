﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class QanswerOption
    {
        public int AnswerOptionId { get; set; }
        public int QuestionId { get; set; }
        public string AnswerOptionText { get; set; }
        public int DisplayOrder { get; set; }
        public bool Active { get; set; }
    }
}
