﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class QcontractDetailQuestion
    {
        public int ContractDetailQuestionId { get; set; }
        public int QuestionId { get; set; }
        public int CatalogItemId { get; set; }
    }
}
