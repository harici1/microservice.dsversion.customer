﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Qquestion
    {
        public int QuestionId { get; set; }
        public string RefType { get; set; }
        public string QuestionText { get; set; }
        public int MaxAnswerOptionCount { get; set; }
        public int DisplayOrder { get; set; }
        public bool Active { get; set; }
    }
}
