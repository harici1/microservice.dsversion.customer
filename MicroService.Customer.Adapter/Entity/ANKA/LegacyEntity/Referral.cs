﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Referral
    {
        public int ReferralId { get; set; }
        public int ReferralContractId { get; set; }
        public string ReferringEntitySource { get; set; }
        public string ReferringEntityType { get; set; }
        public int? ReferringEntityId { get; set; }
        public string OriginalReferringEntitySource { get; set; }
        public int? OriginalReferringEntitiyId { get; set; }
        public int ReferralProgramId { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
    }
}
