﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ReferralProgram
    {
        public int ReferralProgramId { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
    }
}
