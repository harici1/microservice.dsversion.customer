﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ReferralProgramCampaign
    {
        public int Id { get; set; }
        public int RelationCampaignId { get; set; }
        public int RelationCatalogItemId { get; set; }
        public DateTime StartDate { get; set; }
        public string CreateBy { get; set; }
        public bool? Active { get; set; }
    }
}
