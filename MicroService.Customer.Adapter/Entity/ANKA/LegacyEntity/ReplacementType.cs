﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ReplacementType
    {
        public int ReplacementTypeId { get; set; }
        public string Description { get; set; }
    }
}
