﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ReportBanned
    {
        public int Id { get; set; }
        public string UserCode { get; set; }
        public int ReportType { get; set; }
    }
}
