﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class RequestChannel
    {
        public int RequestChannelId { get; set; }
        public string DisplayName { get; set; }
        public bool Active { get; set; }
        public Guid MsreplTranVersion { get; set; }
    }
}
