﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class RequestParam
    {
        public RequestParam()
        {
            RequestParamValue = new HashSet<RequestParamValue>();
        }

        public int RequestParamId { get; set; }
        public int RequestTypeId { get; set; }
        public string DisplayName { get; set; }
        public string DataType { get; set; }

        public virtual RequestType RequestType { get; set; }
        public virtual ICollection<RequestParamValue> RequestParamValue { get; set; }
    }
}
