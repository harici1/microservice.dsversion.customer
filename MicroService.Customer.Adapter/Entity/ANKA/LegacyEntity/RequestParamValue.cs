﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class RequestParamValue
    {
        public int RequestParamValueId { get; set; }
        public int CustomerRequestId { get; set; }
        public int RequestParamId { get; set; }
        public string Value { get; set; }
        public bool? Active { get; set; }
        public string DisplayValue { get; set; }

        public virtual RequestParam RequestParam { get; set; }
    }
}
