﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class RequestReason
    {
        public RequestReason()
        {
            CustomerRequest = new HashSet<CustomerRequest>();
        }

        public int RequestReasonId { get; set; }
        public int RequestTypeId { get; set; }
        public string DisplayName { get; set; }
        public int DisplayOrder { get; set; }
        public bool? Active { get; set; }

        public virtual RequestType RequestType { get; set; }
        public virtual ICollection<CustomerRequest> CustomerRequest { get; set; }
    }
}
