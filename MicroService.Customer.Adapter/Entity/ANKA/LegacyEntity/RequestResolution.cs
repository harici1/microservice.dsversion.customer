﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class RequestResolution
    {
        public RequestResolution()
        {
            CustomerRequest = new HashSet<CustomerRequest>();
        }

        public int RequestResolutionId { get; set; }
        public int RequestTypeId { get; set; }
        public int RequestStatusId { get; set; }
        public string DisplayName { get; set; }
        public int DisplayOrder { get; set; }
        public bool Active { get; set; }
        public int? AvailableUserType { get; set; }

        public virtual RequestStatus RequestStatus { get; set; }
        public virtual RequestType RequestType { get; set; }
        public virtual ICollection<CustomerRequest> CustomerRequest { get; set; }
    }
}
