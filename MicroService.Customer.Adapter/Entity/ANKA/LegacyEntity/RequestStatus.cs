﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class RequestStatus
    {
        public RequestStatus()
        {
            CustomerRequest = new HashSet<CustomerRequest>();
            RequestResolution = new HashSet<RequestResolution>();
            RequestStatusManualUpdateMapNewStatus = new HashSet<RequestStatusManualUpdateMap>();
            RequestStatusManualUpdateMapRequestStatus = new HashSet<RequestStatusManualUpdateMap>();
        }

        public int RequestStatusId { get; set; }
        public int RequestTypeId { get; set; }
        public string DisplayName { get; set; }
        public int DisplayOrder { get; set; }
        public bool Closed { get; set; }
        public bool Active { get; set; }
        public bool Initial { get; set; }

        public virtual RequestType RequestType { get; set; }
        public virtual ICollection<CustomerRequest> CustomerRequest { get; set; }
        public virtual ICollection<RequestResolution> RequestResolution { get; set; }
        public virtual ICollection<RequestStatusManualUpdateMap> RequestStatusManualUpdateMapNewStatus { get; set; }
        public virtual ICollection<RequestStatusManualUpdateMap> RequestStatusManualUpdateMapRequestStatus { get; set; }
    }
}
