﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class RequestStatusManualUpdateMap
    {
        public int RequestStatusId { get; set; }
        public int NewStatusId { get; set; }

        public virtual RequestStatus NewStatus { get; set; }
        public virtual RequestStatus RequestStatus { get; set; }
    }
}
