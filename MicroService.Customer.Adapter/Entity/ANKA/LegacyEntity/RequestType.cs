﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class RequestType
    {
        public RequestType()
        {
            CampaignCommitment = new HashSet<CampaignCommitment>();
            CustomerRequest = new HashSet<CustomerRequest>();
            RequestParam = new HashSet<RequestParam>();
            RequestReason = new HashSet<RequestReason>();
            RequestResolution = new HashSet<RequestResolution>();
            RequestStatus = new HashSet<RequestStatus>();
        }

        public int RequestTypeId { get; set; }
        public string DisplayName { get; set; }
        public int DisplayOrder { get; set; }
        public bool Active { get; set; }
        public bool? CloseableWhenContractCancel { get; set; }

        public virtual ICollection<CampaignCommitment> CampaignCommitment { get; set; }
        public virtual ICollection<CustomerRequest> CustomerRequest { get; set; }
        public virtual ICollection<RequestParam> RequestParam { get; set; }
        public virtual ICollection<RequestReason> RequestReason { get; set; }
        public virtual ICollection<RequestResolution> RequestResolution { get; set; }
        public virtual ICollection<RequestStatus> RequestStatus { get; set; }
    }
}
