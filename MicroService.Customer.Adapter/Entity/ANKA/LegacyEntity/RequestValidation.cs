﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class RequestValidation
    {
        public int RequestValidationId { get; set; }
        public int RequestTypeId { get; set; }
        public string Description { get; set; }
        public string StoredProcedure { get; set; }
        public bool Disabled { get; set; }
    }
}
