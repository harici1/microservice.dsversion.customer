﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Reseller
    {
        public int ResellerId { get; set; }
        public string ResellerName { get; set; }
        public string ContactName { get; set; }
        public string ResellerType { get; set; }
        public int? ParentId { get; set; }
        public bool? Active { get; set; }
    }
}
