﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class RoleMember
    {
        public int Id { get; set; }
        public int? RoleId { get; set; }
        public string UserName { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreatedBy { get; set; }
        public Guid MsreplTranVersion { get; set; }
    }
}
