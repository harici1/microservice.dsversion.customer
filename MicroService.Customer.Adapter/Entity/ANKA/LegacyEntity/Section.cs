﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Section
    {
        public int Id { get; set; }
        public int? DivisionId { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }

        public virtual Division Division { get; set; }
    }
}
