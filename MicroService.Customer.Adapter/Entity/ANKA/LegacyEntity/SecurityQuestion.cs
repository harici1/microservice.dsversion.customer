﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class SecurityQuestion
    {
        public int SecurityQuestionId { get; set; }
        public string SecurityQuestionText { get; set; }
        public bool? Active { get; set; }
    }
}
