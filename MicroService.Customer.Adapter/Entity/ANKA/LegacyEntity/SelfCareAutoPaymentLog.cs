﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class SelfCareAutoPaymentLog
    {
        public int Id { get; set; }
        public int LoginId { get; set; }
        public int ContractId { get; set; }
        public int CreditCardId { get; set; }
        public DateTime AutoPaymentDate { get; set; }
    }
}
