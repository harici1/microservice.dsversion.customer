﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class SelfCareLoginList
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public DateTime LoginDate { get; set; }
        public Guid MsreplTranVersion { get; set; }
    }
}
