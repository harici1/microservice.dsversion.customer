﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class SelfCareLoginPaymentLog
    {
        public int Id { get; set; }
        public int LoginId { get; set; }
        public decimal? PaymentAmount { get; set; }
        public DateTime? PaymentDate { get; set; }
        public int? InvoiceId { get; set; }
        public int? ContractId { get; set; }
        public int? CreditCardId { get; set; }
    }
}
