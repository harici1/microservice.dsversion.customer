﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class SelfCareWebUserInfoChangeList
    {
        public int Rid { get; set; }
        public int? CustomerId { get; set; }
        public bool? CustomerUsername { get; set; }
        public bool? CustomerPassword { get; set; }
        public bool? ChangeCustomerUsername { get; set; }
        public bool? ChangeCustomerPassword { get; set; }
    }
}
