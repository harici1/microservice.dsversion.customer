﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class SelfcareAuthenticationLog
    {
        public int LogId { get; set; }
        public int LogTypeId { get; set; }
        public DateTime LogDate { get; set; }
        public int? CustomerId { get; set; }
        public string Value1 { get; set; }
        public string Value2 { get; set; }
    }
}
