﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class SelfcareAuthenticationLogtype
    {
        public int LogTypeId { get; set; }
        public string Description { get; set; }
    }
}
