﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ServiceParam
    {
        public ServiceParam()
        {
            CatalogItem = new HashSet<CatalogItem>();
            ServiceParamDetail = new HashSet<ServiceParamDetail>();
        }

        public int ServiceParamId { get; set; }
        public string ServiceParamName { get; set; }

        public virtual ICollection<CatalogItem> CatalogItem { get; set; }
        public virtual ICollection<ServiceParamDetail> ServiceParamDetail { get; set; }
    }
}
