﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ServiceParamDetail
    {
        public ServiceParamDetail()
        {
            ContractDetailServiceParam = new HashSet<ContractDetailServiceParam>();
        }

        public int ServiceParamDetailId { get; set; }
        public int ServiceParamId { get; set; }
        public string DisplayName { get; set; }
        public int? DisplayOrder { get; set; }
        public bool? Primary { get; set; }
        public bool? IsUnique { get; set; }
        public bool? RequiredForSale { get; set; }
        public bool? UpdateAllowed { get; set; }
        public string DefaultValue { get; set; }
        public bool? OverrideDefaultAllowed { get; set; }
        public bool? IntegrationKey { get; set; }
        public string DataType { get; set; }
        public string FilterType { get; set; }
        public int? MaxLength { get; set; }
        public string Regex { get; set; }
        public string RegexWarningMessage { get; set; }

        public virtual ServiceParam ServiceParam { get; set; }
        public virtual ICollection<ContractDetailServiceParam> ContractDetailServiceParam { get; set; }
    }
}
