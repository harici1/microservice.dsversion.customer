﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ServiceParameterValueDetail
    {
        public int ServiceParameterValueDetailId { get; set; }
        public int ServiceParameterId { get; set; }
        public string ServiceParameterValue { get; set; }
        public string Description { get; set; }
    }
}
