﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ServiceRequest
    {
        public int ServiceRequestId { get; set; }
        public int? CustomerId { get; set; }
        public int? ContractId { get; set; }
        public int? DeliveryId { get; set; }
        public int? AddressId { get; set; }
        public int? ServiceRequestStatusId { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime? RendezvousDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public string ProblemDescription { get; set; }
        public string ResolutionDescripton { get; set; }
        public bool? ChargeCustomer { get; set; }
        public bool? Setup { get; set; }
        public string ForwardedUser { get; set; }
        public bool? Canceled { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual Address Address { get; set; }
        public virtual Contract Contract { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Delivery Delivery { get; set; }
        public virtual ServiceRequestStatus ServiceRequestStatus { get; set; }
    }
}
