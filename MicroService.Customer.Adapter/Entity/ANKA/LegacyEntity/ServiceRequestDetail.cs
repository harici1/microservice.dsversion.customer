﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ServiceRequestDetail
    {
        public int ServiceRequestDetailId { get; set; }
        public int ServiceRequestId { get; set; }
        public int ContractDetailId { get; set; }
    }
}
