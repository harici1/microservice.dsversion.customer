﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class ServiceRequestStatus
    {
        public ServiceRequestStatus()
        {
            ServiceRequest = new HashSet<ServiceRequest>();
        }

        public int ServiceRequestStatusId { get; set; }
        public string DisplayName { get; set; }
        public bool Active { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual ICollection<ServiceRequest> ServiceRequest { get; set; }
    }
}
