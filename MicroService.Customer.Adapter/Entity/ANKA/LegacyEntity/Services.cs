﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Services
    {
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
    }
}
