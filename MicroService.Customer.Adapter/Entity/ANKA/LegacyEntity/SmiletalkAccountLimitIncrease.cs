﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class SmiletalkAccountLimitIncrease
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int ContractDetailId { get; set; }
        public bool AccountLimitIncrease { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
    }
}
