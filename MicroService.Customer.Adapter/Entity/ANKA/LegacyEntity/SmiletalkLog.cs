﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class SmiletalkLog
    {
        public int SmiletalkLogId { get; set; }
        public string PhoneNumer { get; set; }
        public string SmiletalkNo { get; set; }
        public string ReferenceId { get; set; }
        public string MindContractNo { get; set; }
        public string UserName { get; set; }
        public string PinNumber { get; set; }
        public string EventName { get; set; }
        public bool? Active { get; set; }
        public bool IsFinalize { get; set; }
        public int? ResultId { get; set; }
        public int? FinalizeReservationId { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
