﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class SoladdressAutomateLog
    {
        public int Id { get; set; }
        public int ContractDetaild { get; set; }
        public int? TaskStatusId { get; set; }
        public int? TaskStatusDetailId { get; set; }
        public string UnknownErrorDescription { get; set; }
    }
}
