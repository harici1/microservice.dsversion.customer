﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class StsformCollectionRequest
    {
        public int RequestId { get; set; }
        public int ContractId { get; set; }
        public int RequestStatusId { get; set; }
        public string ForwardedToUser { get; set; }
        public int? AddressId { get; set; }
        public DateTime? AppointmentDate { get; set; }
        public string InfoNote { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime LastUpdatedDate { get; set; }
    }
}
