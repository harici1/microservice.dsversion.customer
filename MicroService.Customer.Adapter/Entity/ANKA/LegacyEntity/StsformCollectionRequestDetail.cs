﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class StsformCollectionRequestDetail
    {
        public int RequestDetailId { get; set; }
        public int RequestId { get; set; }
        public int MoreumDocumentTypeIdToCollect { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
