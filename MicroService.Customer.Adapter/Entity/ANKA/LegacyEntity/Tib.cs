﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Tib
    {
        public string Isim { get; set; }
        public string Soyad { get; set; }
        public string Adres { get; set; }
        public string PostaKod { get; set; }
        public string IlceSemt { get; set; }
        public string Sehir { get; set; }
        public string Ulke { get; set; }
        public string AboneNo { get; set; }
        public int MusteriId { get; set; }
        public string DslTelefonNo { get; set; }
        public string KullaniciAd { get; set; }
        public string HizProfil { get; set; }
        public string HizmeTuru { get; set; }
        public string PopBilgi { get; set; }
        public string StatikIp { get; set; }
        public string Eposta { get; set; }
        public string FaturaAdres { get; set; }
        public string TcKimlikNo { get; set; }
        public string Telefon1 { get; set; }
        public string Telefon2 { get; set; }
        public string AbonelikTarih { get; set; }
        public string IptalTarih { get; set; }
        public string HizmetDurum { get; set; }
    }
}
