﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Tiblog
    {
        public long Id { get; set; }
        public string Output { get; set; }
        public DateTime? Createdate { get; set; }
    }
}
