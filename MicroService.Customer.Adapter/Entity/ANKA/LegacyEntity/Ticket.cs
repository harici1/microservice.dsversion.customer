﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Ticket
    {
        public Ticket()
        {
            TicketCustomerInteraction = new HashSet<TicketCustomerInteraction>();
            TicketForwardLog = new HashSet<TicketForwardLog>();
            TicketNote = new HashSet<TicketNote>();
        }

        public int TicketId { get; set; }
        public string TicketNo { get; set; }
        public int CustomerId { get; set; }
        public int? ProblemId { get; set; }
        public int? ResolutionId { get; set; }
        public int? Status { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CloseBy { get; set; }
        public DateTime? CloseDate { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public string ModifyBy { get; set; }
        public int? ForwardedGroup { get; set; }
        public string ForwardedUser { get; set; }
        public string CurrentUser { get; set; }
        public int? MamId { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public int? ContractId { get; set; }
        public string CompanyCode { get; set; }

        public virtual UserData CloseByNavigation { get; set; }
        public virtual Contract Contract { get; set; }
        public virtual UserData CreateByNavigation { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual UserData ModifyByNavigation { get; set; }
        public virtual CallTree Problem { get; set; }
        public virtual CallTree Resolution { get; set; }
        public virtual TicketStatus StatusNavigation { get; set; }
        public virtual ICollection<TicketCustomerInteraction> TicketCustomerInteraction { get; set; }
        public virtual ICollection<TicketForwardLog> TicketForwardLog { get; set; }
        public virtual ICollection<TicketNote> TicketNote { get; set; }
    }
}
