﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class TicketCustomerInteraction
    {
        public int TicketId { get; set; }
        public int CustomerInteractionId { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual CustomerInteraction CustomerInteraction { get; set; }
        public virtual Ticket Ticket { get; set; }
    }
}
