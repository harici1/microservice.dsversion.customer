﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class TicketEscalation
    {
        public int ProblemId { get; set; }
        public int? MaxOpen { get; set; }
        public int? MaxProgress { get; set; }
        public int? MaxForwarded { get; set; }
        public string NotifiedUser { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual CallTree Problem { get; set; }
    }
}
