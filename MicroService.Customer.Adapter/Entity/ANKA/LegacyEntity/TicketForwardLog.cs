﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class TicketForwardLog
    {
        public int Id { get; set; }
        public int? TicketId { get; set; }
        public string FromUser { get; set; }
        public int? ToGroup { get; set; }
        public string ToUser { get; set; }
        public DateTime? ForwardDate { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual Ticket Ticket { get; set; }
    }
}
