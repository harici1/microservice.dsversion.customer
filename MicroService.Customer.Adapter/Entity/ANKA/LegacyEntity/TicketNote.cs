﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class TicketNote
    {
        public int TicketNoteId { get; set; }
        public int? TicketId { get; set; }
        public string Text { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool? Popup { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual UserData CreateByNavigation { get; set; }
        public virtual Ticket Ticket { get; set; }
    }
}
