﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class TicketRouting
    {
        public int ProblemId { get; set; }
        public int GroupId { get; set; }

        public virtual CallTree Problem { get; set; }
    }
}
