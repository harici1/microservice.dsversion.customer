﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class TicketStatus
    {
        public TicketStatus()
        {
            Ticket = new HashSet<Ticket>();
        }

        public int TicketStatusId { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual ICollection<Ticket> Ticket { get; set; }
    }
}
