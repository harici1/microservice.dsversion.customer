﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class TrendMicroStatus
    {
        public int TrendMicroStatusId { get; set; }
        public string Alias { get; set; }
        public string DisplayName { get; set; }
    }
}
