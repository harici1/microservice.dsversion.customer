﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class TrendMicroStatusJournal
    {
        public int TrendMicroJournalId { get; set; }
        public DateTime JournalDate { get; set; }
        public int TrendMicroSerialKeyId { get; set; }
        public int Status { get; set; }
    }
}
