﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class TssisCatalogItemPropertyValue
    {
        public int CatalogItemPropertyValueId { get; set; }
        public int CatalogItemId { get; set; }
        public int PropertyId { get; set; }
        public string PropertyValue { get; set; }
        public Guid MsreplTranVersion { get; set; }
    }
}
