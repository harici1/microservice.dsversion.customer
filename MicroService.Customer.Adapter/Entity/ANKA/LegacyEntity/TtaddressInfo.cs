﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class TtaddressInfo
    {
        public int TtaddressInfoId { get; set; }
        public string AcikAdres { get; set; }
        public string BbolumKodu { get; set; }
        public string BinaKodu { get; set; }
        public string BlokAdi { get; set; }
        public string BucakAdi { get; set; }
        public string BucakKodu { get; set; }
        public string Csbmadi { get; set; }
        public string Csbmkodu { get; set; }
        public string DisKapiNo { get; set; }
        public string IcKapiNo { get; set; }
        public string IlAdi { get; set; }
        public string IlKodu { get; set; }
        public string IlceAdi { get; set; }
        public string IlceKodu { get; set; }
        public string KoyAdi { get; set; }
        public string KoyKodu { get; set; }
        public string MahalleAdi { get; set; }
        public string MahalleKodu { get; set; }
        public string PostaKodu { get; set; }
        public string SiteAdi { get; set; }
        public string UlkeAdi { get; set; }
    }
}
