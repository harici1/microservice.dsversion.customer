﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class TtcallBackKopru
    {
        public int TtcallBackId { get; set; }
        public string TtxdslNo { get; set; }
        public int TtcontractDetailId { get; set; }
        public int TtsebepId { get; set; }
        public DateTime? TtkopruGirisTarihi { get; set; }
        public DateTime? TtkopruCikisTarihi { get; set; }
        public int KopruStatus { get; set; }
        public int ProcessStatus { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ProcessStatusLastModifyDate { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
