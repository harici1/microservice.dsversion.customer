﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class TtcallBackKopruLog
    {
        public int LogId { get; set; }
        public int? ContractDetailId { get; set; }
        public string HizmetNo { get; set; }
        public DateTime? TtkopruCikisTarihi { get; set; }
        public int? ProcessStatus { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string ModifyBy { get; set; }
    }
}
