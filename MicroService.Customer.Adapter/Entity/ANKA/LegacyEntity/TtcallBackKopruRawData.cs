﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class TtcallBackKopruRawData
    {
        public int TtcallBackId { get; set; }
        public string TtxdslNo { get; set; }
        public string TtpstnNo { get; set; }
        public int? TtsebepId { get; set; }
        public string TtkopruStatuTarih { get; set; }
        public int CallbackTypeId { get; set; }
        public int ProcessStatusId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ProcessStatusLastModifyDate { get; set; }
        public DateTime? LastModifyDate { get; set; }
    }
}
