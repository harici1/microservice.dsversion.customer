﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class TtcallBackPstnIptal
    {
        public int TtcallBackId { get; set; }
        public int? TtcontractDetailId { get; set; }
        public string TthizmetNo { get; set; }
        public DateTime? TtbasvuruTarihi { get; set; }
        public DateTime? TttamamlanmaZamani { get; set; }
        public string TtiptalSebebi { get; set; }
        public int? TtisTuru { get; set; }
        public string TtaltIsTuru { get; set; }
        public string TtaltIsTuruAciklama { get; set; }
        public int? ProcessStatus { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ProcessStatusLastModifyDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int FileBatchId { get; set; }
    }
}
