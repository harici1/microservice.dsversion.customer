﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class TtreclaimationCode
    {
        public int TtreclaimationCodeId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string CodeType { get; set; }
        public string ForwardTo { get; set; }
        public string PlanlamaForwardTo { get; set; }
        public int? TaskStatusDetailId { get; set; }
    }
}
