﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class TtxdslarizaTicketLog
    {
        public int LogId { get; set; }
        public int? TicketId { get; set; }
        public int? ProblemId { get; set; }
        public string PstnNo { get; set; }
        public string AdslUserName { get; set; }
        public string TtXdslCrmServisDslamNo { get; set; }
        public string TtXdslCrmServisDslamTipAd { get; set; }
        public string TtXdslCrmServisDslamTipi { get; set; }
        public string TtXdslCrmServisKartAdi { get; set; }
        public string TtXdslCrmServisKartNo { get; set; }
        public string TtXdslCrmServisKartTipi { get; set; }
        public string TtXdslCrmServisPopAdi { get; set; }
        public string TtXdslCrmServisPopNo { get; set; }
        public string TtXdslCrmServisPortDurum { get; set; }
        public string TtXdslCrmServisPortNo { get; set; }
        public string TtXdslCrmServisPortSahibi { get; set; }
        public string TtXdslCrmServisPortSatisModeli { get; set; }
        public string TtXdslCrmServisRedbackAdi { get; set; }
        public string TtXdslCrmServisRedbackId { get; set; }
        public string TtXdslCrmServisShelfAdi { get; set; }
        public string TtXdslCrmServisShelfNo { get; set; }
        public string DtbrassIp { get; set; }
        public string Dttunnel { get; set; }
    }
}
