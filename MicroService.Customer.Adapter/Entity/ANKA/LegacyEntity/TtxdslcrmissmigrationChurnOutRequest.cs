﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class TtxdslcrmissmigrationChurnOutRequest
    {
        public string TransactionId { get; set; }
        public int? Asama { get; set; }
        public string AboneTur { get; set; }
        public string MusteriGercekTuzel { get; set; }
        public string TcNo { get; set; }
        public string VergiNo { get; set; }
        public string MusteriAd { get; set; }
        public string MusteriSoyadUnvan { get; set; }
        public string DomainTip { get; set; }
        public int? Hiz { get; set; }
        public string PaketId { get; set; }
        public string PromosyonId { get; set; }
        public string TarifeId { get; set; }
        public string BasvuruTarihi { get; set; }
        public string XdslNo { get; set; }
        public string XdslModemPswd { get; set; }
        public string XdslModemUserName { get; set; }
        public string BasvuruYapanYeniMusteriId { get; set; }
        public string BasvuruAlanKullaniciId { get; set; }
        public string ContractNo { get; set; }
        public int? ContracDetailId { get; set; }
        public string PstnNumarasi { get; set; }
        public int? IslemStatuId { get; set; }
        public int? IslemDurumStatuId { get; set; }
        public int? IslemDetayStatuId { get; set; }
        public DateTime? IslemTarihi { get; set; }
        public DateTime? IsleminTekrarCalisacagiTarih { get; set; }
        public string IslemiYapan { get; set; }
        public DateTime? SonIslemTarihi { get; set; }
        public string Extra { get; set; }
        public int Id { get; set; }
    }
}
