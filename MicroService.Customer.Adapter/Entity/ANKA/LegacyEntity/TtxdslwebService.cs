﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class TtxdslwebService
    {
        public int TtxdslwebServiceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Wsdlurl { get; set; }
    }
}
