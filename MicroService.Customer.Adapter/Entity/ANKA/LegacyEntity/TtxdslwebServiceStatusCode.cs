﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class TtxdslwebServiceStatusCode
    {
        public int TtxdslwebServiceId { get; set; }
        public int StatusCodeId { get; set; }
        public string Description { get; set; }
    }
}
