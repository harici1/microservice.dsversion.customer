﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class TurkTelekomPortSuspendProcessLog
    {
        public int TurkTelekomPortSuspendProcessLogId { get; set; }
        public int? ContractId { get; set; }
        public int? AdslContractDetailId { get; set; }
        public string AdslXdslNo { get; set; }
        public DateTime? PortSuspendExecuteDate { get; set; }
        public string PortStatus { get; set; }
    }
}
