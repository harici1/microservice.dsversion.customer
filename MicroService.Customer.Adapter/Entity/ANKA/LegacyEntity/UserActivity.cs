﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class UserActivity
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public DateTime ProccessDate { get; set; }
        public string ProccessTitle { get; set; }
        public string ProccessInfo { get; set; }
        public string Type { get; set; }
        public string RefType { get; set; }
        public int? RefId { get; set; }
        public string RefParameter { get; set; }

        public virtual UserData UserNameNavigation { get; set; }
    }
}
