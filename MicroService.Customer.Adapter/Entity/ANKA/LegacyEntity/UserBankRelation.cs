﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class UserBankRelation
    {
        public int UserBankRelationId { get; set; }
        public string Username { get; set; }
        public int BankId { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public bool Active { get; set; }
    }
}
