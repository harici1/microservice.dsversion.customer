﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class UserData
    {
        public UserData()
        {
            ContractCreateByNavigation = new HashSet<Contract>();
            ContractModifyByNavigation = new HashSet<Contract>();
            CustomerInteraction = new HashSet<CustomerInteraction>();
            Notes = new HashSet<Notes>();
            TicketCloseByNavigation = new HashSet<Ticket>();
            TicketCreateByNavigation = new HashSet<Ticket>();
            TicketModifyByNavigation = new HashSet<Ticket>();
            TicketNote = new HashSet<TicketNote>();
            UserActivity = new HashSet<UserActivity>();
        }

        public string Username { get; set; }
        public string Domain { get; set; }
        public string Password { get; set; }
        public int? UserTypeId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string IdentityNo { get; set; }
        public string Gender { get; set; }
        public DateTime Birthdate { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int EducationId { get; set; }
        public bool Active { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime LastModifyDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public int? WrongPasswordCount { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public DateTime? PasswordExpireDate { get; set; }

        public virtual UserType UserType { get; set; }
        public virtual ICollection<Contract> ContractCreateByNavigation { get; set; }
        public virtual ICollection<Contract> ContractModifyByNavigation { get; set; }
        public virtual ICollection<CustomerInteraction> CustomerInteraction { get; set; }
        public virtual ICollection<Notes> Notes { get; set; }
        public virtual ICollection<Ticket> TicketCloseByNavigation { get; set; }
        public virtual ICollection<Ticket> TicketCreateByNavigation { get; set; }
        public virtual ICollection<Ticket> TicketModifyByNavigation { get; set; }
        public virtual ICollection<TicketNote> TicketNote { get; set; }
        public virtual ICollection<UserActivity> UserActivity { get; set; }
    }
}
