﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class UserLoginAuthorization
    {
        public int LoginAuthorizationId { get; set; }
        public int? UserTypeId { get; set; }
        public string UserName { get; set; }
        public string HostIp { get; set; }
        public bool? AccessStatus { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
