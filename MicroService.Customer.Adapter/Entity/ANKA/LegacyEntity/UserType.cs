﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class UserType
    {
        public UserType()
        {
            UserData = new HashSet<UserData>();
        }

        public int UserTypeId { get; set; }
        public string DisplayName { get; set; }
        public int? DefaultRoleId { get; set; }
        public string ServerUrl { get; set; }

        public virtual Role DefaultRole { get; set; }
        public virtual ICollection<UserData> UserData { get; set; }
    }
}
