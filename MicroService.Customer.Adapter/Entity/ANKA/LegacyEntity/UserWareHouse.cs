﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class UserWareHouse
    {
        public int UserWareHouseId { get; set; }
        public string UserName { get; set; }
        public int? WareHouseId { get; set; }
    }
}
