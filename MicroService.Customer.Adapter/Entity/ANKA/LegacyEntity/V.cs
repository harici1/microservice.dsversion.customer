﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class V
    {
        public int Rid { get; set; }
        public int? CustomerId { get; set; }
        public DateTime? SendDate { get; set; }
    }
}
