﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class V3EslesmemeAnalizBazRaporuAnkaIlkerCilingir
    {
        public int RowId { get; set; }
        public int? CustomerId { get; set; }
        public string Tckn { get; set; }
        public int? ContractId { get; set; }
        public string ContractNo { get; set; }
        public int? ReplacedContractId { get; set; }
        public string LeasingContractNumber { get; set; }
        public int? ContractStatusId { get; set; }
        public string ContractStatusName { get; set; }
        public DateTime? ContractStartDate { get; set; }
        public DateTime? ContractEndDate { get; set; }
        public int? ContractCommitment { get; set; }
        public DateTime? ContractCancelDate { get; set; }
        public int? ContractDetailId { get; set; }
        public int? CampaignId { get; set; }
        public string CampaignName { get; set; }
        public DateTime? ActivationDateOrCampaignChangeDate { get; set; }
        public DateTime? LastUsageDate { get; set; }
        public DateTime? LastPositiveUsageDate { get; set; }
        public decimal? OpenAssessmentTotal { get; set; }
        public decimal? ErişimBedeli { get; set; }
        public decimal? ErişimKampanyaİndirimi { get; set; }
        public decimal? YalınAdslHizmetBedeli { get; set; }
        public decimal? YalınAdslHizmetBedeliİndirimi { get; set; }
        public decimal? ModemBedeli { get; set; }
        public decimal? ModemKullanımBedeli { get; set; }
        public decimal? ModemBedeliİndirimi { get; set; }
        public DateTime? LastInvoiceDate { get; set; }
        public decimal? LastInvoiceAmount { get; set; }
        public DateTime? JournalDate { get; set; }
        public int? JournalStatusId { get; set; }
        public string JournalStatusName { get; set; }
    }
}
