﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class VasAddOn
    {
        public VasAddOn()
        {
            VasAuthenticationToken = new HashSet<VasAuthenticationToken>();
        }

        public int AddOnId { get; set; }
        public string AddOnCode { get; set; }
        public string AddOnName { get; set; }
        public int ProviderId { get; set; }
        public int CatalogItemId { get; set; }
        public int MaximumAllowed { get; set; }
        public int AuthenticationTokenExpiresInMinutes { get; set; }
        public string PostUrl { get; set; }
        public string AllowedIpAddresses { get; set; }
        public string SmilePageName { get; set; }

        public virtual CatalogItem CatalogItem { get; set; }
        public virtual VasProvider Provider { get; set; }
        public virtual ICollection<VasAuthenticationToken> VasAuthenticationToken { get; set; }
    }
}
