﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class VasAuthenticationToken
    {
        public Guid TokenId { get; set; }
        public int AddOnId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public int ContractDetailId { get; set; }
        public byte Status { get; set; }

        public virtual VasAddOn AddOn { get; set; }
        public virtual ContractDetail ContractDetail { get; set; }
    }
}
