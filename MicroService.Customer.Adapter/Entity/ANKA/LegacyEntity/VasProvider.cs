﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class VasProvider
    {
        public VasProvider()
        {
            VasAddOn = new HashSet<VasAddOn>();
        }

        public int ProviderId { get; set; }
        public Guid ProviderCode { get; set; }
        public string ProviderName { get; set; }

        public virtual ICollection<VasAddOn> VasAddOn { get; set; }
    }
}
