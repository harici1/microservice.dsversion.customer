﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class VasServiceLog
    {
        public int LogId { get; set; }
        public DateTime? CreateDate { get; set; }
        public string ProcessName { get; set; }
        public string ProviderCode { get; set; }
        public string AddOnCode { get; set; }
        public string Token { get; set; }
        public string ContractDetailId { get; set; }
        public string ReturnValue { get; set; }
        public string IpAddress { get; set; }
    }
}
