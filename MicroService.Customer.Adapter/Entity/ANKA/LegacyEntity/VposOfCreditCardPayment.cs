﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class VposOfCreditCardPayment
    {
        public long VposOfCreditCardPaymentId { get; set; }
        public int? PaymentId { get; set; }
        public string VposBankName { get; set; }
    }
}
