﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WareHouse
    {
        public int WareHouseId { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
    }
}
