﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WareHouseProvince
    {
        public int WareHouseProvinceId { get; set; }
        public int? WareHouseId { get; set; }
        public int? ProvinceId { get; set; }
        public int? CityId { get; set; }
    }
}
