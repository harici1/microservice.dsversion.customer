﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WebSubscriberAnka
    {
        public int SubscriberUserId { get; set; }
        public int CustomerId { get; set; }
        public bool? IsActive { get; set; }
    }
}
