﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfcontractDetailEventProcess
    {
        public int EventId { get; set; }
        public int CatalogItemId { get; set; }
        public int? ProcessId { get; set; }

        public virtual CatalogItem CatalogItem { get; set; }
        public virtual Wfevent Event { get; set; }
        public virtual Wfprocess Process { get; set; }
    }
}
