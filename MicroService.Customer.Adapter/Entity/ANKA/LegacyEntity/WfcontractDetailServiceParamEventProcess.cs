﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfcontractDetailServiceParamEventProcess
    {
        public int EventId { get; set; }
        public int ServiceParamDetailId { get; set; }
        public int ProcessId { get; set; }
    }
}
