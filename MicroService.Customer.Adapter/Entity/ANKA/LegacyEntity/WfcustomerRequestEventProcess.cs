﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfcustomerRequestEventProcess
    {
        public int RequestTypeId { get; set; }
        public int RequestStatusId { get; set; }
        public int ProcessId { get; set; }
    }
}
