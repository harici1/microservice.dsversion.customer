﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Wfevent
    {
        public Wfevent()
        {
            WfcontractDetailEventProcess = new HashSet<WfcontractDetailEventProcess>();
        }

        public int EventId { get; set; }
        public string EventName { get; set; }

        public virtual WfgeneralEventProcess WfgeneralEventProcess { get; set; }
        public virtual ICollection<WfcontractDetailEventProcess> WfcontractDetailEventProcess { get; set; }
    }
}
