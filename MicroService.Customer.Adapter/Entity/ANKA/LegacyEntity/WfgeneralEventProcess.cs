﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfgeneralEventProcess
    {
        public int EventId { get; set; }
        public int ProcessId { get; set; }

        public virtual Wfevent Event { get; set; }
        public virtual Wfprocess Process { get; set; }
    }
}
