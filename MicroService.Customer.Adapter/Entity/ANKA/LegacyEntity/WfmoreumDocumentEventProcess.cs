﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfmoreumDocumentEventProcess
    {
        public int EventId { get; set; }
        public int MoreumDocumentTypeId { get; set; }
        public int ProcessId { get; set; }
    }
}
