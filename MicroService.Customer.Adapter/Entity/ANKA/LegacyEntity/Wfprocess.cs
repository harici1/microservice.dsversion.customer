﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Wfprocess
    {
        public Wfprocess()
        {
            WfcontractDetailEventProcess = new HashSet<WfcontractDetailEventProcess>();
            WfgeneralEventProcess = new HashSet<WfgeneralEventProcess>();
            WfprocessNode = new HashSet<WfprocessNode>();
            WfqueueTask = new HashSet<WfqueueTask>();
        }

        public int ProcessId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<WfcontractDetailEventProcess> WfcontractDetailEventProcess { get; set; }
        public virtual ICollection<WfgeneralEventProcess> WfgeneralEventProcess { get; set; }
        public virtual ICollection<WfprocessNode> WfprocessNode { get; set; }
        public virtual ICollection<WfqueueTask> WfqueueTask { get; set; }
    }
}
