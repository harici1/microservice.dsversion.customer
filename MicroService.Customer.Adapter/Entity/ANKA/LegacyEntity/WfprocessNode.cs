﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfprocessNode
    {
        public WfprocessNode()
        {
            WfprocessNodeEvent = new HashSet<WfprocessNodeEvent>();
            WfqueueTask = new HashSet<WfqueueTask>();
        }

        public int ProcessNodeId { get; set; }
        public int ProcessId { get; set; }
        public string Name { get; set; }
        public int TaskId { get; set; }
        public int? ParentProcessNodeId { get; set; }

        public virtual Wfprocess Process { get; set; }
        public virtual Wftask Task { get; set; }
        public virtual ICollection<WfprocessNodeEvent> WfprocessNodeEvent { get; set; }
        public virtual ICollection<WfqueueTask> WfqueueTask { get; set; }
    }
}
