﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfprocessNodeEvent
    {
        public int ProcessNodeId { get; set; }
        public int TaskStatusId { get; set; }
        public string EventListenerName { get; set; }
        public string EventName { get; set; }

        public virtual WfprocessNode ProcessNode { get; set; }
        public virtual WftaskStatus TaskStatus { get; set; }
    }
}
