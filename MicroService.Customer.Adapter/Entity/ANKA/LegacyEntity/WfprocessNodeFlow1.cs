﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfprocessNodeFlow1
    {
        public int WfprocessNodeFlowId { get; set; }
        public int ProcessNodeId { get; set; }
        public int TaskStatusId { get; set; }
        public string Action { get; set; }
        public int AffectedProcessNodeId { get; set; }
        public string Parameters { get; set; }
        public int? TaskStatusDetailId { get; set; }
        public int? UpdateCurrentQueueTaskStatusId { get; set; }
    }
}
