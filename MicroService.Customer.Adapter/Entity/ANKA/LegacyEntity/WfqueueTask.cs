﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfqueueTask
    {
        public int QueueTaskId { get; set; }
        public int? ParentQueueTaskId { get; set; }
        public int? QueueTaskStatusId { get; set; }
        public int? ProcessId { get; set; }
        public int? ProcessNodeId { get; set; }
        public int? TaskId { get; set; }
        public int? TaskStatusId { get; set; }
        public int? TaskStatusDetailId { get; set; }
        public string HandlingType { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? DueDate { get; set; }
        public string LastProcessBy { get; set; }
        public DateTime? LastProcessDate { get; set; }
        public string ForwardedGroup { get; set; }
        public string ForwardedUser { get; set; }
        public string CurrentUser { get; set; }
        public string RefType { get; set; }
        public int? RefId { get; set; }
        public string Parameters { get; set; }
        public bool? Dirty { get; set; }
        public Guid MsreplTranVersion { get; set; }

        public virtual Wfprocess Process { get; set; }
        public virtual WfprocessNode ProcessNode { get; set; }
        public virtual WfqueueTaskStatus QueueTaskStatus { get; set; }
        public virtual Wftask Task { get; set; }
        public virtual WftaskStatus TaskStatus { get; set; }
        public virtual WftaskStatusDetail TaskStatusDetail { get; set; }
    }
}
