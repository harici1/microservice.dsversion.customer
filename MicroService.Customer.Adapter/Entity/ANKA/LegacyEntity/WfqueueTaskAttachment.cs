﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfqueueTaskAttachment
    {
        public int QueueAttachId { get; set; }
        public int AttachmentId { get; set; }
        public DateTime? AttachmentDate { get; set; }
        public bool? Deleted { get; set; }
        public DateTime? DeletedDate { get; set; }

        public virtual Attachment Attachment { get; set; }
    }
}
