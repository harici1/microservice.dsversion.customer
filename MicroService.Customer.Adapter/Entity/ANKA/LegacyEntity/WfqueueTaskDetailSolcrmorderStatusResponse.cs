﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfqueueTaskDetailSolcrmorderStatusResponse
    {
        public int QueueTaskId { get; set; }
        public string RefId { get; set; }
        public string OrderId { get; set; }
        public string ResultCode { get; set; }
        public string ResultDescription { get; set; }
        public DateTime ResponseDate { get; set; }
        public string FiberUserName { get; set; }
        public string Password { get; set; }
        public string ModemSerialNo { get; set; }
        public string OntmodemSerialNo { get; set; }
        public string Status { get; set; }
        public string WorkflowStatus { get; set; }
        public DateTime? ActivationDate { get; set; }
        public string WorkflowDescription { get; set; }
        public string WorkflowSubStatus { get; set; }
        public string AtataskNo { get; set; }
        public string AtataskResult { get; set; }
        public DateTime? HoldEndDate { get; set; }
        public string WorkflowStatusCode { get; set; }
        public int? AramaKuyruğuQueueTaskId { get; set; }
        public DateTime? AramaKuyruğuRandevuTarihi { get; set; }
        public string AramaKuyruğuMusteriAciklama { get; set; }
        public string AramaKuyruğuYetkiliKisi { get; set; }
        public int Id { get; set; }
    }
}
