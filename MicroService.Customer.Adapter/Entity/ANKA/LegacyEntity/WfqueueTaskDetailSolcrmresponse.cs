﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfqueueTaskDetailSolcrmresponse
    {
        public int QueueTaskId { get; set; }
        public string OrderId { get; set; }
        public string ResultCode { get; set; }
        public string ResultDescription { get; set; }
        public string Extra { get; set; }
        public DateTime ResponseDate { get; set; }
        public string RefId { get; set; }
        public string SoapRequestData { get; set; }
        public string SoapResponseData { get; set; }
    }
}
