﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfqueueTaskDetailTtxdslcrmArizaResponseTicket
    {
        public int TicketId { get; set; }
        public string AdslNo { get; set; }
        public int? SikayetId { get; set; }
        public string IrtibatTel { get; set; }
        public string CepTel { get; set; }
        public string BilgiNotu { get; set; }
        public int? TtxdslarizaId { get; set; }
        public int? TtxdslresponseCode { get; set; }
        public string TtxdslresponseMessage { get; set; }
    }
}
