﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfqueueTaskDetailTtxdslcrmIssMigrationChurnInResponse
    {
        public int QueueTaskId { get; set; }
        public string TransactionId { get; set; }
        public string PreviousXdslNo { get; set; }
        public DateTime ResponseDate { get; set; }
        public string CurrentXdslNo { get; set; }
    }
}
