﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfqueueTaskDetailTtxdslcrmMarsArizaResponseTicket
    {
        public int TicketId { get; set; }
        public string AdslNo { get; set; }
        public int? HizmetTuruId { get; set; }
        public string SikayetKodu { get; set; }
        public string IrtibatTel { get; set; }
        public string TeknikIrtibatAdi { get; set; }
        public string TeknikIrtibatSoyadi { get; set; }
        public string AgentAdi { get; set; }
        public string AgentSoyadi { get; set; }
        public string IrtibatAdi { get; set; }
        public string IrtibatSoyadi { get; set; }
        public string IrtibatEposta { get; set; }
        public string IrtibatGsmnumarasi { get; set; }
        public string RezervasyonId { get; set; }
        public string TeknikIrtibatTelefonu { get; set; }
        public int? ElittTeklifTuruId { get; set; }
        public int? OncelikTuruId { get; set; }
        public string BilgiNotu { get; set; }
        public DateTime? RequestDate { get; set; }
        public string ReferansNo { get; set; }
        public string AuditUuid { get; set; }
        public string BolumKodu { get; set; }
        public string IslemKodu { get; set; }
        public string JobId { get; set; }
        public long? TakipId { get; set; }
        public int? TtxdslmarsResponseCode { get; set; }
        public string TtxdslmarsResponseMessage { get; set; }
        public DateTime? ResponseDate { get; set; }
        public DateTime? IhbarTarihi { get; set; }
        public DateTime? SonIslahTarihi { get; set; }
        public DateTime? TeyitTarihi { get; set; }
        public string KapamaKodu { get; set; }
        public string XdslNo { get; set; }
        public string IlId { get; set; }
        public bool? Hab { get; set; }
        public string Habdescription { get; set; }
        public string MesafeOlcumu { get; set; }
        public string DownloadAttenuation { get; set; }
        public string DownloadCikisGucu { get; set; }
        public string DownloadKapasiteHizi { get; set; }
        public string DownloadMevcutHiz { get; set; }
        public string DownloadNoiseMargin { get; set; }
        public string HatFizikselDurum { get; set; }
        public string HatYonetimselDurum { get; set; }
        public string TestTeyitZamani { get; set; }
        public string NmsHizProfili { get; set; }
        public string UploadAttenuation { get; set; }
        public string UploadCikisGucu { get; set; }
        public string UploadKapasiteHizi { get; set; }
        public string UploadMevcutHiz { get; set; }
        public string UploadNoiseMargin { get; set; }
        public string WorkOrderId { get; set; }
    }
}
