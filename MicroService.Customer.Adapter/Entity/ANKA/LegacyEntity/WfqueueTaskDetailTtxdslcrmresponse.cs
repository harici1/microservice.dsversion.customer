﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfqueueTaskDetailTtxdslcrmresponse
    {
        public int QueueTaskId { get; set; }
        public int? IlId { get; set; }
        public int? MudurlukId { get; set; }
        public string SiraNumarasi { get; set; }
        public string Mesaj { get; set; }
        public string SonucKodu { get; set; }
        public string Extra { get; set; }
        public DateTime ResponseDate { get; set; }
        public string MusteriNo { get; set; }
    }
}
