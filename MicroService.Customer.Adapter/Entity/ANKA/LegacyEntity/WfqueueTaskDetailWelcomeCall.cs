﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfqueueTaskDetailWelcomeCall
    {
        public int QueueTaskId { get; set; }
        public DateTime? AppointmentDate { get; set; }
        public int? SpooledMessageId { get; set; }
        public bool? AppointmentMailSent { get; set; }
    }
}
