﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfqueueTaskDsmartServiceResponse
    {
        public int QueueTaskId { get; set; }
        public string ResponseValue { get; set; }
        public string ResponseType { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
