﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfqueueTaskServiceConfig
    {
        public int WfqueueTaskServiceConfigId { get; set; }
        public int PerformerId { get; set; }
        public bool MultiThread { get; set; }
        public int MaxAsyncProcessCount { get; set; }
        public int WaitTimeInMilliSecond { get; set; }
    }
}
