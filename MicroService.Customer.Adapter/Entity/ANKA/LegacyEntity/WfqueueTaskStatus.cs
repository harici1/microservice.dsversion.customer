﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfqueueTaskStatus
    {
        public WfqueueTaskStatus()
        {
            WfqueueTask = new HashSet<WfqueueTask>();
        }

        public int QueueTaskStatusId { get; set; }
        public string DisplayName { get; set; }

        public virtual ICollection<WfqueueTask> WfqueueTask { get; set; }
    }
}
