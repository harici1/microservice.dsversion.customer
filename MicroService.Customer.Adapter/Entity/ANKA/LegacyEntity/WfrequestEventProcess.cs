﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfrequestEventProcess
    {
        public int WfrequestEventProcessId { get; set; }
        public int EventId { get; set; }
        public int RequestTypeId { get; set; }
        public int? RequestReasonId { get; set; }
        public int? InitialRequestStatusId { get; set; }
    }
}
