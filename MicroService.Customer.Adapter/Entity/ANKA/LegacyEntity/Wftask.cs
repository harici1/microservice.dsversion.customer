﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Wftask
    {
        public Wftask()
        {
            WfprocessNode = new HashSet<WfprocessNode>();
            WfqueueTask = new HashSet<WfqueueTask>();
            WftaskRole = new HashSet<WftaskRole>();
            WftaskStatusDetail = new HashSet<WftaskStatusDetail>();
        }

        public int TaskId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string HandlingType { get; set; }
        public string AssemblyName { get; set; }
        public string ClassName { get; set; }
        public string MethodName { get; set; }
        public string InterfaceName { get; set; }
        public bool Implemented { get; set; }
        public string OrigAssemblyName { get; set; }
        public string OrigClassName { get; set; }
        public string OrigMethodName { get; set; }
        public bool LogTaskCreate { get; set; }
        public bool LogTaskStatusChange { get; set; }
        public bool CreateFromDirty { get; set; }
        public bool AutoRetry { get; set; }
        public int RetryTime { get; set; }
        public int RetryTimeout { get; set; }

        public virtual ICollection<WfprocessNode> WfprocessNode { get; set; }
        public virtual ICollection<WfqueueTask> WfqueueTask { get; set; }
        public virtual ICollection<WftaskRole> WftaskRole { get; set; }
        public virtual ICollection<WftaskStatusDetail> WftaskStatusDetail { get; set; }
    }
}
