﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WftaskExtension
    {
        public int TaskId { get; set; }
        public int TaskPriority { get; set; }
        public int? TaskGroupId { get; set; }
    }
}
