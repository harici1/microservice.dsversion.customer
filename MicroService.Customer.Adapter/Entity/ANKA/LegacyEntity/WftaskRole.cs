﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WftaskRole
    {
        public int TaskId { get; set; }
        public int RoleId { get; set; }

        public virtual Wftask Task { get; set; }
    }
}
