﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WftaskStatus
    {
        public WftaskStatus()
        {
            WfprocessNodeEvent = new HashSet<WfprocessNodeEvent>();
            WfqueueTask = new HashSet<WfqueueTask>();
            WftaskStatusDetail = new HashSet<WftaskStatusDetail>();
        }

        public int TaskStatusId { get; set; }
        public string DisplayName { get; set; }
        public bool Display { get; set; }
        public string Description { get; set; }

        public virtual ICollection<WfprocessNodeEvent> WfprocessNodeEvent { get; set; }
        public virtual ICollection<WfqueueTask> WfqueueTask { get; set; }
        public virtual ICollection<WftaskStatusDetail> WftaskStatusDetail { get; set; }
    }
}
