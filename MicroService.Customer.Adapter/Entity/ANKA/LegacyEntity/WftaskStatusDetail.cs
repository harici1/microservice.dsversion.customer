﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WftaskStatusDetail
    {
        public WftaskStatusDetail()
        {
            WfqueueTask = new HashSet<WfqueueTask>();
        }

        public int TaskStatusDetailId { get; set; }
        public int? TaskId { get; set; }
        public int? TaskStatusId { get; set; }
        public string Description { get; set; }
        public string DetailCode { get; set; }

        public virtual Wftask Task { get; set; }
        public virtual WftaskStatus TaskStatus { get; set; }
        public virtual ICollection<WfqueueTask> WfqueueTask { get; set; }
    }
}
