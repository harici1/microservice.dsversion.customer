﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WfticketProblemEventProcess
    {
        public int EventId { get; set; }
        public int ProblemId { get; set; }
        public int ProcessId { get; set; }
    }
}
