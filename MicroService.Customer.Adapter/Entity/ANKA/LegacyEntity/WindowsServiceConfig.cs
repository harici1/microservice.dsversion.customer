﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class WindowsServiceConfig
    {
        public string ServiceName { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
