﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class Workflow
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool? AuthReq { get; set; }
        public byte? Accesslevel { get; set; }
    }
}
