﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class XdslTransfer
    {
        public int Id { get; set; }
        public int OrigContractDetailId { get; set; }
        public int NewContractDetailId { get; set; }
        public bool AdslPhoneChange { get; set; }
        public bool AddressChange { get; set; }
        public string OriginalAni { get; set; }
        public string NewAni { get; set; }
        public int? OrigAddressId { get; set; }
        public int? NewAddressId { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public string AlternateContactPhone { get; set; }
        public int? NewTtbbkid { get; set; }
        public string NewSollocationId { get; set; }
        public string NewSollocationUnitId { get; set; }
        public int? NewCatalogItemId { get; set; }
        public bool? IsDestinationNdsl { get; set; }

        public virtual Address NewAddress { get; set; }
        public virtual ContractDetail NewContractDetail { get; set; }
        public virtual Address OrigAddress { get; set; }
        public virtual ContractDetail OrigContractDetail { get; set; }
    }
}
