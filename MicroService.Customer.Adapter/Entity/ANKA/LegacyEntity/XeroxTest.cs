﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.LegacyEntity
{
    public partial class XeroxTest
    {
        public int Id { get; set; }
        public string Xdslno { get; set; }
        public int Spid { get; set; }
    }
}
