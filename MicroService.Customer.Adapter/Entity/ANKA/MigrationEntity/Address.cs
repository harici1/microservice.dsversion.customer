﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.MigrationEntity
{
    public partial class Address
    {
        public int LogId { get; set; }
        public DateTime LogDate { get; set; }
        public string LogUser { get; set; }
        public string LogHostname { get; set; }
        public string LogType { get; set; }
        public int MigrationStatus { get; set; }
        public int AddressId { get; set; }
        public string RefType { get; set; }
        public string RefParameter { get; set; }
        public string StreetAddress { get; set; }
        public string ZipCode { get; set; }
        public int? CityId { get; set; }
        public int? ProvinceId { get; set; }
        public int? CountryId { get; set; }
        public bool DefaultAdress { get; set; }
        public bool? Active { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public string ModifyBy { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public int? DistrictId { get; set; }
    }
}
