﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.MigrationEntity
{
    public partial class Company
    {
        public int LogId { get; set; }
        public DateTime LogDate { get; set; }
        public string LogUser { get; set; }
        public string LogHostname { get; set; }
        public string LogType { get; set; }
        public int MigrationStatus { get; set; }
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string CompanyType { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string MobilePhone { get; set; }
        public string TaxOffice { get; set; }
        public string TaxNumber { get; set; }
        public string AccountManager { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public string ModifyBy { get; set; }
        public bool Active { get; set; }
        public Guid MsreplTranVersion { get; set; }
    }
}
