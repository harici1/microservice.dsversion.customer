﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.MigrationEntity
{
    public partial class Contact
    {
        public int LogId { get; set; }
        public DateTime LogDate { get; set; }
        public string LogUser { get; set; }
        public string LogHostname { get; set; }
        public string LogType { get; set; }
        public int MigrationStatus { get; set; }
        public int ContactId { get; set; }
        public string RefType { get; set; }
        public int? RefId { get; set; }
        public string RefParameter { get; set; }
        public string ContactName { get; set; }
        public string ContactSurname { get; set; }
        public string Eposta1 { get; set; }
        public string Eposta2 { get; set; }
        public string Phone { get; set; }
        public string Ext { get; set; }
        public string PocketPhone { get; set; }
        public string Fax { get; set; }
        public string ContactType { get; set; }
        public Guid MsreplTranVersion { get; set; }
    }
}
