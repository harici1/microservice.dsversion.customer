﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.MigrationEntity
{
    public partial class Contract
    {
        public int LogId { get; set; }
        public DateTime LogDate { get; set; }
        public string LogUser { get; set; }
        public string LogHostname { get; set; }
        public string LogType { get; set; }
        public int MigrationStatus { get; set; }
        public int ContractId { get; set; }
        public string ContractNo { get; set; }
        public int CustomerId { get; set; }
        public int? ContractStatusId { get; set; }
        public int? InvoiceAddressId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? TerminationDate { get; set; }
        public bool AutoChargeCc { get; set; }
        public int? AutoChargeCcid { get; set; }
        public bool? SendInvoiceByMail { get; set; }
        public bool? SendInvoiceByEmail { get; set; }
        public bool? SendInvoiceBySms { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public string CreateBy { get; set; }
        public string ModifyBy { get; set; }
        public int? ReplacedContractId { get; set; }
        public int? BillingPlatformId { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public string ContractAlias { get; set; }
        public DateTime? WrittenCancelRequestDate { get; set; }
    }
}
