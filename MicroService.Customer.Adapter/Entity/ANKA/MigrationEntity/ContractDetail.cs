﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.MigrationEntity
{
    public partial class ContractDetail
    {
        public int LogId { get; set; }
        public DateTime LogDate { get; set; }
        public string LogUser { get; set; }
        public string LogHostname { get; set; }
        public string LogType { get; set; }
        public int MigrationStatus { get; set; }
        public int ContractDetailId { get; set; }
        public int ContractId { get; set; }
        public int CatalogItemId { get; set; }
        public int? ParentContractDetailId { get; set; }
        public int? PricePlanId { get; set; }
        public int? CampaignId { get; set; }
        public int? ContractDetailStatusId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public string CreateBy { get; set; }
        public string ModifyBy { get; set; }
        public bool Bundle { get; set; }
        public int? ReplacedContractDetailId { get; set; }
        public int? ReplacementTypeId { get; set; }
        public int? PreviousContractId { get; set; }
        public decimal PreBilledPeriodCount { get; set; }
        public decimal PostBilledPeriodCount { get; set; }
        public Guid MsreplTranVersion { get; set; }
    }
}
