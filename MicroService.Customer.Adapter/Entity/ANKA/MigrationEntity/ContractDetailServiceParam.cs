﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.MigrationEntity
{
    public partial class ContractDetailServiceParam
    {
        public int LogId { get; set; }
        public DateTime LogDate { get; set; }
        public string LogUser { get; set; }
        public string LogHostname { get; set; }
        public string LogType { get; set; }
        public int MigrationStatus { get; set; }
        public int ContractDetailServiceParamId { get; set; }
        public int ContractDetailId { get; set; }
        public int ServiceParamDetailId { get; set; }
        public string Value { get; set; }
        public string PendingValue { get; set; }
        public bool Active { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
