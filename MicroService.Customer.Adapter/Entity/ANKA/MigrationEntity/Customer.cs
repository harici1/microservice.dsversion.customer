﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.ANKA.MigrationEntity
{
    public partial class Customer
    {
        public int LogId { get; set; }
        public DateTime LogDate { get; set; }
        public string LogUser { get; set; }
        public string LogHostname { get; set; }
        public string LogType { get; set; }
        public int MigrationStatus { get; set; }
        public int CustomerId { get; set; }
        public string CustomerNo { get; set; }
        public string Title { get; set; }
        public string CustomerName { get; set; }
        public string CustomerSurname { get; set; }
        public int? CompanyId { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public string OfficePhone { get; set; }
        public string Ext1 { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public bool? Active { get; set; }
        public string CustomerType { get; set; }
        public string CitizienshipNo { get; set; }
        public string TaxOffice { get; set; }
        public string TaxNo { get; set; }
        public bool? Exempt { get; set; }
        public int? RoleType { get; set; }
        public int? ParentId { get; set; }
        public string Fax { get; set; }
        public bool? Potential { get; set; }
        public DateTime? Birthdate { get; set; }
        public string Gender { get; set; }
        public int? EducationId { get; set; }
        public int? OccupationId { get; set; }
        public int? SecurityQuestionId { get; set; }
        public string SecurityAnswerText { get; set; }
        public string MothersMaidenName { get; set; }
        public string WebSite { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? LastModifyDate { get; set; }
        public string ModifyBy { get; set; }
        public string WebUsername { get; set; }
        public string WebPassword { get; set; }
        public int? ImportanceLevelId { get; set; }
        public string ImportanceLevelDescription { get; set; }
        public Guid MsreplTranVersion { get; set; }
        public bool? IsGroup { get; set; }
        public string SapCode { get; set; }
        public bool IsEmailOk { get; set; }
        public bool Einvoicer { get; set; }
        public bool EinvoicerOk { get; set; }
        public bool? MernisCheck { get; set; }
        public DateTime? MernisCheckDate { get; set; }
    }
}
