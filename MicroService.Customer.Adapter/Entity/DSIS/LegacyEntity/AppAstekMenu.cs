﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class AppAstekMenu
    {
        public AppAstekMenu()
        {
            InverseParent = new HashSet<AppAstekMenu>();
        }

        public int MenuId { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public int? ParentId { get; set; }
        public bool? Status { get; set; }
        public int OrderNo { get; set; }
        public int? AuthorizationId { get; set; }
        public bool HasLink { get; set; }
        public int AcessObjectId { get; set; }
        public string SystemCode { get; set; }
        public string AccesObjectDescription { get; set; }

        public virtual DsMenu Authorization { get; set; }
        public virtual AppAstekMenu Parent { get; set; }
        public virtual ICollection<AppAstekMenu> InverseParent { get; set; }
    }
}
