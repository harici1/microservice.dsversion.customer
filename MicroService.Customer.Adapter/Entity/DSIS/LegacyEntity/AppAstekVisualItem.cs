﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class AppAstekVisualItem
    {
        public int AstekVisualItemId { get; set; }
        public string Name { get; set; }
        public string ClassName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int AuthorizationId { get; set; }
        public string Description { get; set; }

        public virtual DsMenu Authorization { get; set; }
    }
}
