﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class AppBettingMenu
    {
        public AppBettingMenu()
        {
            AppBettingMenuVisualItem = new HashSet<AppBettingMenuVisualItem>();
        }

        public int MenuId { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public int? ParentId { get; set; }
        public bool? Status { get; set; }
        public int OrderNo { get; set; }
        public int? AuthorizationId { get; set; }
        public bool HasLink { get; set; }
        public int AcessObjectId { get; set; }

        public virtual DsMenu Authorization { get; set; }
        public virtual ICollection<AppBettingMenuVisualItem> AppBettingMenuVisualItem { get; set; }
    }
}
