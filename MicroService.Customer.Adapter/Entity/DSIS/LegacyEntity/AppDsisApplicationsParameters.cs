﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class AppDsisApplicationsParameters
    {
        public int ApplicationParameterId { get; set; }
        public string ParameterName { get; set; }
        public string ParameterValueType { get; set; }
        public int? ParameterValueInt { get; set; }
        public string ParameterValueString { get; set; }
        public DateTime? ParameterValueDateTime { get; set; }
        public bool? ParameterValueBool { get; set; }
    }
}
