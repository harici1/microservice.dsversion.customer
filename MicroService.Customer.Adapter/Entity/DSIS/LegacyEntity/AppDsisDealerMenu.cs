﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class AppDsisDealerMenu
    {
        public int DsisDealerMenuId { get; set; }
        public int DsisDealerMenuTypeId { get; set; }
        public string Link { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsVisible { get; set; }
        public int? OrderNo { get; set; }

        public virtual AppDsisDealerMenuType DsisDealerMenuType { get; set; }
    }
}
