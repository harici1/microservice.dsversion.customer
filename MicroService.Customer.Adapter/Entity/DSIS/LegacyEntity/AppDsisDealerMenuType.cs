﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class AppDsisDealerMenuType
    {
        public AppDsisDealerMenuType()
        {
            AppDsisDealerMenu = new HashSet<AppDsisDealerMenu>();
        }

        public int DsisDealerMenuTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<AppDsisDealerMenu> AppDsisDealerMenu { get; set; }
    }
}
