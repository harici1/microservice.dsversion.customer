﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class AppDsisMenu
    {
        public AppDsisMenu()
        {
            InverseParent = new HashSet<AppDsisMenu>();
        }

        public int MenuId { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public int? ParentId { get; set; }
        public bool? Status { get; set; }
        public int OrderNo { get; set; }
        public int? AuthorizationId { get; set; }
        public bool HasLink { get; set; }
        public int? AcessObjectId { get; set; }
        public string SystemCode { get; set; }

        public virtual DsMenu Authorization { get; set; }
        public virtual AppDsisMenu Parent { get; set; }
        public virtual ICollection<AppDsisMenu> InverseParent { get; set; }
    }
}
