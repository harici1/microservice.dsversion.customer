﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class AppDsmartComTrVisualItem
    {
        public int DsmartComTrVisualItemId { get; set; }
        public string Name { get; set; }
        public int AuthorizationId { get; set; }
    }
}
