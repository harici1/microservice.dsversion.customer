﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class AppWebRequest
    {
        public int AppWebRequestId { get; set; }
        public string Name { get; set; }
        public int AuthorizationId { get; set; }

        public virtual DsMenu Authorization { get; set; }
    }
}
