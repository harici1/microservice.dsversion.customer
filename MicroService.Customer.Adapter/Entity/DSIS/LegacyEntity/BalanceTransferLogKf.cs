﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class BalanceTransferLogKf
    {
        public int Id { get; set; }
        public int? TypeId { get; set; }
        public string RefType { get; set; }
        public int? RefId { get; set; }
        public int? DsisContractId { get; set; }
        public int? LeasingContractInstallmentId { get; set; }
        public int? AnkaContractId { get; set; }
        public string Description { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string ModifyBy { get; set; }
    }
}
