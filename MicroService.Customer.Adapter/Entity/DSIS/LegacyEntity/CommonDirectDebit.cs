﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class CommonDirectDebit
    {
        public int CommonDirectDebitId { get; set; }
        public int CommonDirectDebitTypeId { get; set; }
        public int? LeasingContractId { get; set; }
        public int? AnkaContractId { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
