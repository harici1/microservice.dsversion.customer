﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class CommonDirectDebitType
    {
        public int CommonDirectDebitTypeId { get; set; }
        public string Name { get; set; }
    }
}
