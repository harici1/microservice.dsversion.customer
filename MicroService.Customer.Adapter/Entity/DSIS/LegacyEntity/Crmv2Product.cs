﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2Product
    {
        public Crmv2Product()
        {
            Crmv2ProductCompositeToProduct = new HashSet<Crmv2ProductCompositeToProduct>();
            Crmv2ProductOfferPriceToProduct = new HashSet<Crmv2ProductOfferPriceToProduct>();
            Crmv2ProductPriceAlteration = new HashSet<Crmv2ProductPriceAlteration>();
        }

        public int ProductId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Crmv2ProductCompositeToProduct> Crmv2ProductCompositeToProduct { get; set; }
        public virtual ICollection<Crmv2ProductOfferPriceToProduct> Crmv2ProductOfferPriceToProduct { get; set; }
        public virtual ICollection<Crmv2ProductPriceAlteration> Crmv2ProductPriceAlteration { get; set; }
    }
}
