﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductAtomic
    {
        public int ProductAtomicId { get; set; }
        public int ProductComponentId { get; set; }
        public int ProductSpecificationCharacteristicValueId { get; set; }

        public virtual Crmv2ProductComponent ProductComponent { get; set; }
        public virtual Crmv2ProductSpecificationCharacteristicValue ProductSpecificationCharacteristicValue { get; set; }
    }
}
