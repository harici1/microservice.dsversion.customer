﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductComponent
    {
        public Crmv2ProductComponent()
        {
            Crmv2ProductAtomic = new HashSet<Crmv2ProductAtomic>();
            Crmv2ProductComponentEquivalentEquivalentProductComponent = new HashSet<Crmv2ProductComponentEquivalent>();
            Crmv2ProductComponentEquivalentProductComponent = new HashSet<Crmv2ProductComponentEquivalent>();
            Crmv2ProductComponentHierarchyParentProductComponent = new HashSet<Crmv2ProductComponentHierarchy>();
            Crmv2ProductComponentHierarchyProductComponent = new HashSet<Crmv2ProductComponentHierarchy>();
            Crmv2ProductComponentPrice = new HashSet<Crmv2ProductComponentPrice>();
            Crmv2ProductComponentToProductComposite = new HashSet<Crmv2ProductComponentToProductComposite>();
            Crmv2ProductOfferCatalogProductComponentPriceAlteration = new HashSet<Crmv2ProductOfferCatalogProductComponentPriceAlteration>();
        }

        public int ProductComponentId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Crmv2ProductAtomic> Crmv2ProductAtomic { get; set; }
        public virtual ICollection<Crmv2ProductComponentEquivalent> Crmv2ProductComponentEquivalentEquivalentProductComponent { get; set; }
        public virtual ICollection<Crmv2ProductComponentEquivalent> Crmv2ProductComponentEquivalentProductComponent { get; set; }
        public virtual ICollection<Crmv2ProductComponentHierarchy> Crmv2ProductComponentHierarchyParentProductComponent { get; set; }
        public virtual ICollection<Crmv2ProductComponentHierarchy> Crmv2ProductComponentHierarchyProductComponent { get; set; }
        public virtual ICollection<Crmv2ProductComponentPrice> Crmv2ProductComponentPrice { get; set; }
        public virtual ICollection<Crmv2ProductComponentToProductComposite> Crmv2ProductComponentToProductComposite { get; set; }
        public virtual ICollection<Crmv2ProductOfferCatalogProductComponentPriceAlteration> Crmv2ProductOfferCatalogProductComponentPriceAlteration { get; set; }
    }
}
