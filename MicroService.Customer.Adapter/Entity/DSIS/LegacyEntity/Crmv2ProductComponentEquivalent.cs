﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductComponentEquivalent
    {
        public int ProductComponentEquivalentId { get; set; }
        public int ProductComponentId { get; set; }
        public int EquivalentProductComponentId { get; set; }
        public bool AllowConvert { get; set; }

        public virtual Crmv2ProductComponent EquivalentProductComponent { get; set; }
        public virtual Crmv2ProductComponent ProductComponent { get; set; }
    }
}
