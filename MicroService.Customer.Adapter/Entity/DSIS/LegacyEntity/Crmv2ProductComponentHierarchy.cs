﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductComponentHierarchy
    {
        public Crmv2ProductComponentHierarchy()
        {
            Crmv2ProductComponentHierarchyPrice = new HashSet<Crmv2ProductComponentHierarchyPrice>();
        }

        public int ProductComponentHierarchyId { get; set; }
        public int ProductComponentId { get; set; }
        public int ParentProductComponentId { get; set; }

        public virtual Crmv2ProductComponent ParentProductComponent { get; set; }
        public virtual Crmv2ProductComponent ProductComponent { get; set; }
        public virtual ICollection<Crmv2ProductComponentHierarchyPrice> Crmv2ProductComponentHierarchyPrice { get; set; }
    }
}
