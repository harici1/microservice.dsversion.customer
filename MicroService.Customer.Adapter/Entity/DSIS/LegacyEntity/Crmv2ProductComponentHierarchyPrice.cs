﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductComponentHierarchyPrice
    {
        public int ProductComponentHierarchyPriceId { get; set; }
        public int ProductComponentHierarchyId { get; set; }
        public int ProductOfferPriceId { get; set; }
        public int ProductOfferCatalogId { get; set; }
        public int Version { get; set; }

        public virtual Crmv2ProductComponentHierarchy ProductComponentHierarchy { get; set; }
        public virtual Crmv2ProductOfferCatalog ProductOfferCatalog { get; set; }
        public virtual Crmv2ProductOfferPrice ProductOfferPrice { get; set; }
    }
}
