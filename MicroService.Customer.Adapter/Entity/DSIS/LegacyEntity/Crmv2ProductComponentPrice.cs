﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductComponentPrice
    {
        public int ProductComponentPriceId { get; set; }
        public int ProductComponentId { get; set; }
        public decimal MinPrice { get; set; }
        public decimal ListPrice { get; set; }
        public decimal WithoutContractPrice { get; set; }
        public int Version { get; set; }

        public virtual Crmv2ProductComponent ProductComponent { get; set; }
    }
}
