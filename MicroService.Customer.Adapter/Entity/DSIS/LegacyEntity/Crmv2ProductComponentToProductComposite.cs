﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductComponentToProductComposite
    {
        public int ProductComponentToProductCompositeId { get; set; }
        public int ProductCompositeId { get; set; }
        public int ProductComponentId { get; set; }

        public virtual Crmv2ProductComponent ProductComponent { get; set; }
        public virtual Crmv2ProductComposite ProductComposite { get; set; }
    }
}
