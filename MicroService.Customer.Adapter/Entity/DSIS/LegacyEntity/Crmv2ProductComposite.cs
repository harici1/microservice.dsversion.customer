﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductComposite
    {
        public Crmv2ProductComposite()
        {
            Crmv2ProductComponentToProductComposite = new HashSet<Crmv2ProductComponentToProductComposite>();
            Crmv2ProductCompositePriceAlteration = new HashSet<Crmv2ProductCompositePriceAlteration>();
            Crmv2ProductCompositeToCrmEquivalentProduct = new HashSet<Crmv2ProductCompositeToCrmEquivalentProduct>();
            Crmv2ProductCompositeToProduct = new HashSet<Crmv2ProductCompositeToProduct>();
        }

        public int ProductCompositeId { get; set; }
        public string Name { get; set; }
        public int ProductSpecificationId { get; set; }

        public virtual Crmv2ProductSpecification ProductSpecification { get; set; }
        public virtual ICollection<Crmv2ProductComponentToProductComposite> Crmv2ProductComponentToProductComposite { get; set; }
        public virtual ICollection<Crmv2ProductCompositePriceAlteration> Crmv2ProductCompositePriceAlteration { get; set; }
        public virtual ICollection<Crmv2ProductCompositeToCrmEquivalentProduct> Crmv2ProductCompositeToCrmEquivalentProduct { get; set; }
        public virtual ICollection<Crmv2ProductCompositeToProduct> Crmv2ProductCompositeToProduct { get; set; }
    }
}
