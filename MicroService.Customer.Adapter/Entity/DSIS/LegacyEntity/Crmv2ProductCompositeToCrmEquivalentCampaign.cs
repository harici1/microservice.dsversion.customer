﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductCompositeToCrmEquivalentCampaign
    {
        public int ProductCompositeToCrmEquivalentCampaignId { get; set; }
        public int ProductOfferId { get; set; }
        public int? DsisCampaignId { get; set; }
        public int? AnkaCampaignId { get; set; }

        public virtual Crmv2ProductOffer ProductOffer { get; set; }
    }
}
