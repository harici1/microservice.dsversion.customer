﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductCompositeToCrmEquivalentProduct
    {
        public int ProductCompositeToCrmEquivalentProductId { get; set; }
        public int ProductCompositeId { get; set; }
        public int? DsisProductId { get; set; }
        public int? AnkaProductId { get; set; }

        public virtual Crmv2ProductComposite ProductComposite { get; set; }
    }
}
