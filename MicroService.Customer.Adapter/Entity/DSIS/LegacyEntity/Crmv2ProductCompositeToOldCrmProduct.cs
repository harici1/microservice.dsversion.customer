﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductCompositeToOldCrmProduct
    {
        public int ProductCompositeToOldCrmProductId { get; set; }
        public int ProductCompositeId { get; set; }
        public int OldCrmProductId { get; set; }
        public int Version { get; set; }
    }
}
