﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductCompositeToProduct
    {
        public int ProductCompositeToProductId { get; set; }
        public int ProductCompositeId { get; set; }
        public int ProductId { get; set; }

        public virtual Crmv2Product Product { get; set; }
        public virtual Crmv2ProductComposite ProductComposite { get; set; }
    }
}
