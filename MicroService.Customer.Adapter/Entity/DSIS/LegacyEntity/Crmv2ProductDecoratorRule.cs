﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductDecoratorRule
    {
        public int ProductDecoratorRuleId { get; set; }
        public string Formula { get; set; }
        public int OfferScreenId { get; set; }
        public int ProductOfferCatalogId { get; set; }
        public string ClassName { get; set; }
        public string Arguments { get; set; }
        public int Version { get; set; }

        public virtual Crmv2ProductOfferCatalog ProductOfferCatalog { get; set; }
    }
}
