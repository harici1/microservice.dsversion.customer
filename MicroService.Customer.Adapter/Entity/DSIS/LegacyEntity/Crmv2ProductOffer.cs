﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductOffer
    {
        public Crmv2ProductOffer()
        {
            Crmv2ProductCompositeToCrmEquivalentCampaign = new HashSet<Crmv2ProductCompositeToCrmEquivalentCampaign>();
        }

        public int ProductOfferId { get; set; }
        public string Name { get; set; }
        public int ProductOfferCatalogId { get; set; }
        public int ProductOfferRuleId { get; set; }
        public int ProductOfferPriceId { get; set; }
        public int ProductOfferTermId { get; set; }
        public int? ProductOfferActionId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Enabled { get; set; }
        public int LastUpdateUser { get; set; }

        public virtual Crmv2ProductOfferAction ProductOfferAction { get; set; }
        public virtual Crmv2ProductOfferCatalog ProductOfferCatalog { get; set; }
        public virtual Crmv2ProductOfferPrice ProductOfferPrice { get; set; }
        public virtual Crmv2ProductOfferRule ProductOfferRule { get; set; }
        public virtual Crmv2ProductOfferTerm ProductOfferTerm { get; set; }
        public virtual ICollection<Crmv2ProductCompositeToCrmEquivalentCampaign> Crmv2ProductCompositeToCrmEquivalentCampaign { get; set; }
    }
}
