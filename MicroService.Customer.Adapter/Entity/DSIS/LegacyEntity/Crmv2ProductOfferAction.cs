﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductOfferAction
    {
        public Crmv2ProductOfferAction()
        {
            Crmv2ProductOffer = new HashSet<Crmv2ProductOffer>();
            Crmv2ProductOfferActionAtomic = new HashSet<Crmv2ProductOfferActionAtomic>();
        }

        public int ProductOfferActionId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Crmv2ProductOffer> Crmv2ProductOffer { get; set; }
        public virtual ICollection<Crmv2ProductOfferActionAtomic> Crmv2ProductOfferActionAtomic { get; set; }
    }
}
