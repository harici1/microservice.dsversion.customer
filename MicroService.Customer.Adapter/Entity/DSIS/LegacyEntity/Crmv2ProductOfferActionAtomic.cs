﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductOfferActionAtomic
    {
        public int ProductOfferActionAtomicId { get; set; }
        public int ProductOfferActionId { get; set; }
        public string ProductOfferActionType { get; set; }
        public string Value { get; set; }

        public virtual Crmv2ProductOfferAction ProductOfferAction { get; set; }
    }
}
