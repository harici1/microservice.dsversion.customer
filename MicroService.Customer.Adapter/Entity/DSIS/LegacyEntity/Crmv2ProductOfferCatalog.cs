﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductOfferCatalog
    {
        public Crmv2ProductOfferCatalog()
        {
            Crmv2ProductComponentHierarchyPrice = new HashSet<Crmv2ProductComponentHierarchyPrice>();
            Crmv2ProductDecoratorRule = new HashSet<Crmv2ProductDecoratorRule>();
            Crmv2ProductOffer = new HashSet<Crmv2ProductOffer>();
            Crmv2ProductOfferCatalogProductComponentPriceAlteration = new HashSet<Crmv2ProductOfferCatalogProductComponentPriceAlteration>();
            Crmv2ProductOfferCatalogToContractType = new HashSet<Crmv2ProductOfferCatalogToContractType>();
            Crmv2ProductOfferCatalogToOfferScreen = new HashSet<Crmv2ProductOfferCatalogToOfferScreen>();
            Crmv2ProductOfferCatalogToPriceVersion = new HashSet<Crmv2ProductOfferCatalogToPriceVersion>();
            Crmv2ProductOfferCatalogToProductSpecification = new HashSet<Crmv2ProductOfferCatalogToProductSpecification>();
            Crmv2ProductOfferCatalogToRole = new HashSet<Crmv2ProductOfferCatalogToRole>();
        }

        public int ProductOfferCatalogId { get; set; }
        public string Name { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<Crmv2ProductComponentHierarchyPrice> Crmv2ProductComponentHierarchyPrice { get; set; }
        public virtual ICollection<Crmv2ProductDecoratorRule> Crmv2ProductDecoratorRule { get; set; }
        public virtual ICollection<Crmv2ProductOffer> Crmv2ProductOffer { get; set; }
        public virtual ICollection<Crmv2ProductOfferCatalogProductComponentPriceAlteration> Crmv2ProductOfferCatalogProductComponentPriceAlteration { get; set; }
        public virtual ICollection<Crmv2ProductOfferCatalogToContractType> Crmv2ProductOfferCatalogToContractType { get; set; }
        public virtual ICollection<Crmv2ProductOfferCatalogToOfferScreen> Crmv2ProductOfferCatalogToOfferScreen { get; set; }
        public virtual ICollection<Crmv2ProductOfferCatalogToPriceVersion> Crmv2ProductOfferCatalogToPriceVersion { get; set; }
        public virtual ICollection<Crmv2ProductOfferCatalogToProductSpecification> Crmv2ProductOfferCatalogToProductSpecification { get; set; }
        public virtual ICollection<Crmv2ProductOfferCatalogToRole> Crmv2ProductOfferCatalogToRole { get; set; }
    }
}
