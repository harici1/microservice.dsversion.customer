﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductOfferCatalogProductComponentPriceAlteration
    {
        public int ProductOfferCatalogProductComponentPriceAlterationId { get; set; }
        public int ProductOfferCatalogId { get; set; }
        public int ProductComponentId { get; set; }
        public string AlterationType { get; set; }
        public string AlterationFormula { get; set; }
        public string AlterationApplyType { get; set; }
        public int Version { get; set; }

        public virtual Crmv2ProductComponent ProductComponent { get; set; }
        public virtual Crmv2ProductOfferCatalog ProductOfferCatalog { get; set; }
    }
}
