﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductOfferCatalogToContractType
    {
        public int ProductOfferCatalogToContractTypeId { get; set; }
        public int ProductOfferCatalogId { get; set; }
        public int ContractTypeId { get; set; }

        public virtual Crmv2ProductOfferCatalog ProductOfferCatalog { get; set; }
    }
}
