﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductOfferCatalogToOfferScreen
    {
        public int ProductOfferCatalogToOfferScreenId { get; set; }
        public int ProductOfferCatalogId { get; set; }
        public int OfferScreenId { get; set; }

        public virtual Crmv2ProductOfferCatalog ProductOfferCatalog { get; set; }
    }
}
