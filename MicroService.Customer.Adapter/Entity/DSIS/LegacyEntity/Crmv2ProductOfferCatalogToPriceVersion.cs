﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductOfferCatalogToPriceVersion
    {
        public int ProductOfferCatalogToPriceVersionId { get; set; }
        public int ProductOfferCatalogId { get; set; }
        public int Version { get; set; }

        public virtual Crmv2ProductOfferCatalog ProductOfferCatalog { get; set; }
    }
}
