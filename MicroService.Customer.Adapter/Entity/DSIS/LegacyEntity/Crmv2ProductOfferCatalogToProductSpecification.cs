﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductOfferCatalogToProductSpecification
    {
        public int ProductOfferCatalogToProductSpecificationId { get; set; }
        public int ProductOfferCatalogId { get; set; }
        public string ProductSpecificationId { get; set; }

        public virtual Crmv2ProductOfferCatalog ProductOfferCatalog { get; set; }
    }
}
