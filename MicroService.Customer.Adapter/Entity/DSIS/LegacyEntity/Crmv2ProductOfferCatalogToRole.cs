﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductOfferCatalogToRole
    {
        public int ProductOfferCatalogToRoleId { get; set; }
        public int ProductOfferCatalogId { get; set; }
        public int RoleId { get; set; }
        public int OfferScreenId { get; set; }

        public virtual Crmv2ProductOfferCatalog ProductOfferCatalog { get; set; }
        public virtual DsRole Role { get; set; }
    }
}
