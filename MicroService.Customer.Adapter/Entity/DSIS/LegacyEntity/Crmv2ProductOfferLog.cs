﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductOfferLog
    {
        public int ProductOfferLogId { get; set; }
        public int ProductOfferId { get; set; }
        public string Name { get; set; }
        public int ProductOfferCatalogId { get; set; }
        public int ProductOfferRuleId { get; set; }
        public int ProductOfferPriceId { get; set; }
        public int ProductOfferTermId { get; set; }
        public int? ProductOfferActionId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Enabled { get; set; }
        public DateTime? TriggerDate { get; set; }
        public string TriggerUserName { get; set; }
        public string TriggerHostName { get; set; }
        public string TriggerType { get; set; }
        public int? LastUpdateUser { get; set; }
    }
}
