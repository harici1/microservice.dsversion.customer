﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductOfferPolicy
    {
        public int ProductOfferPolicyId { get; set; }
        public string Formula { get; set; }
        public bool Enabled { get; set; }
        public string Action { get; set; }
        public string ActionParameters { get; set; }
        public int OfferScreenId { get; set; }
        public int ContractTypeId { get; set; }
        public int Version { get; set; }
    }
}
