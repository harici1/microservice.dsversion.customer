﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductOfferPrice
    {
        public Crmv2ProductOfferPrice()
        {
            Crmv2ProductComponentHierarchyPrice = new HashSet<Crmv2ProductComponentHierarchyPrice>();
            Crmv2ProductOffer = new HashSet<Crmv2ProductOffer>();
            Crmv2ProductOfferPriceToProduct = new HashSet<Crmv2ProductOfferPriceToProduct>();
        }

        public int ProductOfferPriceId { get; set; }
        public int ProductOfferPriceTypeId { get; set; }
        public string Formula { get; set; }
        public string Description { get; set; }

        public virtual Crmv2ProductOfferPriceType ProductOfferPriceType { get; set; }
        public virtual ICollection<Crmv2ProductComponentHierarchyPrice> Crmv2ProductComponentHierarchyPrice { get; set; }
        public virtual ICollection<Crmv2ProductOffer> Crmv2ProductOffer { get; set; }
        public virtual ICollection<Crmv2ProductOfferPriceToProduct> Crmv2ProductOfferPriceToProduct { get; set; }
    }
}
