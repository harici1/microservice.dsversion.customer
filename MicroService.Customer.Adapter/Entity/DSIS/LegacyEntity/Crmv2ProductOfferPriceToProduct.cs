﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductOfferPriceToProduct
    {
        public int ProductOfferPriceToProductId { get; set; }
        public int ProductOfferPriceId { get; set; }
        public int ProductId { get; set; }
        public string Formula { get; set; }

        public virtual Crmv2Product Product { get; set; }
        public virtual Crmv2ProductOfferPrice ProductOfferPrice { get; set; }
    }
}
