﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductOfferPriceType
    {
        public Crmv2ProductOfferPriceType()
        {
            Crmv2ProductOfferPrice = new HashSet<Crmv2ProductOfferPrice>();
        }

        public int ProductOfferPriceTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Crmv2ProductOfferPrice> Crmv2ProductOfferPrice { get; set; }
    }
}
