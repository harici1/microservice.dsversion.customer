﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductOfferRule
    {
        public Crmv2ProductOfferRule()
        {
            Crmv2ProductOffer = new HashSet<Crmv2ProductOffer>();
            Crmv2ProductOfferRuleToProductOfferRuleAtomic = new HashSet<Crmv2ProductOfferRuleToProductOfferRuleAtomic>();
        }

        public int ProductOfferRuleId { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Crmv2ProductOffer> Crmv2ProductOffer { get; set; }
        public virtual ICollection<Crmv2ProductOfferRuleToProductOfferRuleAtomic> Crmv2ProductOfferRuleToProductOfferRuleAtomic { get; set; }
    }
}
