﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductOfferRuleAtomic
    {
        public Crmv2ProductOfferRuleAtomic()
        {
            Crmv2ProductOfferRuleToProductOfferRuleAtomic = new HashSet<Crmv2ProductOfferRuleToProductOfferRuleAtomic>();
        }

        public int ProductOfferRuleAtomicId { get; set; }
        public string Formula { get; set; }

        public virtual ICollection<Crmv2ProductOfferRuleToProductOfferRuleAtomic> Crmv2ProductOfferRuleToProductOfferRuleAtomic { get; set; }
    }
}
