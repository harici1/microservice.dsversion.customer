﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductOfferRuleToProductOfferRuleAtomic
    {
        public int ProductOfferRuleToProductOfferRuleAtomicId { get; set; }
        public int ProductOfferRuleId { get; set; }
        public int ProductOfferRuleAtomicId { get; set; }

        public virtual Crmv2ProductOfferRule ProductOfferRule { get; set; }
        public virtual Crmv2ProductOfferRuleAtomic ProductOfferRuleAtomic { get; set; }
    }
}
