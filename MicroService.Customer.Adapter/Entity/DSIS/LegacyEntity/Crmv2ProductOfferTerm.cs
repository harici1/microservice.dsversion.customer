﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductOfferTerm
    {
        public Crmv2ProductOfferTerm()
        {
            Crmv2ProductOffer = new HashSet<Crmv2ProductOffer>();
        }

        public int ProductOfferTermId { get; set; }
        public string TermDurationType { get; set; }
        public string TermDuration { get; set; }

        public virtual ICollection<Crmv2ProductOffer> Crmv2ProductOffer { get; set; }
    }
}
