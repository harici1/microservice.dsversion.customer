﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductPriceAlteration
    {
        public int ProductPriceAlterationId { get; set; }
        public int ProductId { get; set; }
        public string AlterationType { get; set; }
        public string AlterationFormula { get; set; }
        public string AlterationApplyType { get; set; }
        public int Version { get; set; }

        public virtual Crmv2Product Product { get; set; }
    }
}
