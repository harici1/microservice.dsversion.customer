﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductSpecification
    {
        public Crmv2ProductSpecification()
        {
            Crmv2ProductComposite = new HashSet<Crmv2ProductComposite>();
            Crmv2ProductSpecificationToCharacteristic = new HashSet<Crmv2ProductSpecificationToCharacteristic>();
            Crmv2ProductSpecificationToProductFactory = new HashSet<Crmv2ProductSpecificationToProductFactory>();
        }

        public int ProductSpecificationId { get; set; }
        public int ProductSpecificationTypeId { get; set; }
        public string Name { get; set; }

        public virtual Crmv2ProductSpecificationType ProductSpecificationType { get; set; }
        public virtual ICollection<Crmv2ProductComposite> Crmv2ProductComposite { get; set; }
        public virtual ICollection<Crmv2ProductSpecificationToCharacteristic> Crmv2ProductSpecificationToCharacteristic { get; set; }
        public virtual ICollection<Crmv2ProductSpecificationToProductFactory> Crmv2ProductSpecificationToProductFactory { get; set; }
    }
}
