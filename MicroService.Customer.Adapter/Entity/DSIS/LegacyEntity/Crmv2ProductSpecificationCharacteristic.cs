﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductSpecificationCharacteristic
    {
        public Crmv2ProductSpecificationCharacteristic()
        {
            Crmv2ProductSpecificationCharacteristicToProductCompositeFactory = new HashSet<Crmv2ProductSpecificationCharacteristicToProductCompositeFactory>();
            Crmv2ProductSpecificationCharacteristicValue = new HashSet<Crmv2ProductSpecificationCharacteristicValue>();
            Crmv2ProductSpecificationToCharacteristic = new HashSet<Crmv2ProductSpecificationToCharacteristic>();
        }

        public int ProductSpecificationCharacteristicId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ValueType { get; set; }
        public int? MinCardinality { get; set; }
        public int? MaxCardinality { get; set; }
        public bool? Extensible { get; set; }
        public string DerivationFormula { get; set; }
        public string CreateUser { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ValidFor { get; set; }

        public virtual ICollection<Crmv2ProductSpecificationCharacteristicToProductCompositeFactory> Crmv2ProductSpecificationCharacteristicToProductCompositeFactory { get; set; }
        public virtual ICollection<Crmv2ProductSpecificationCharacteristicValue> Crmv2ProductSpecificationCharacteristicValue { get; set; }
        public virtual ICollection<Crmv2ProductSpecificationToCharacteristic> Crmv2ProductSpecificationToCharacteristic { get; set; }
    }
}
