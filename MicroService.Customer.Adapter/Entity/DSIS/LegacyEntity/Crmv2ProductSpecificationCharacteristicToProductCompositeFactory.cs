﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductSpecificationCharacteristicToProductCompositeFactory
    {
        public int ProductSpecificationCharacteristicToProductCompositeFactoryId { get; set; }
        public int ProductSpecificationCharacteristicId { get; set; }
        public string ClassName { get; set; }
        public bool AllowMultiple { get; set; }

        public virtual Crmv2ProductSpecificationCharacteristic ProductSpecificationCharacteristic { get; set; }
    }
}
