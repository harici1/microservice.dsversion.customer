﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductSpecificationCharacteristicValue
    {
        public Crmv2ProductSpecificationCharacteristicValue()
        {
            Crmv2ProductAtomic = new HashSet<Crmv2ProductAtomic>();
        }

        public int ProductSpecificationCharacteristicValueId { get; set; }
        public int ProductSpecificationCharacteristicId { get; set; }
        public string ValueType { get; set; }
        public bool? IsDefault { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string UnitOfMeasure { get; set; }
        public long? ValueFrom { get; set; }
        public long? ValueTo { get; set; }
        public bool? IsRangeInternal { get; set; }
        public string CreateUser { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ValidFor { get; set; }

        public virtual Crmv2ProductSpecificationCharacteristic ProductSpecificationCharacteristic { get; set; }
        public virtual ICollection<Crmv2ProductAtomic> Crmv2ProductAtomic { get; set; }
    }
}
