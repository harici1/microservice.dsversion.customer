﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductSpecificationToCharacteristic
    {
        public int ProductSpecificationToCharacteristicId { get; set; }
        public int ProductSpecificationId { get; set; }
        public int ProductSpecificationCharacteristicId { get; set; }

        public virtual Crmv2ProductSpecification ProductSpecification { get; set; }
        public virtual Crmv2ProductSpecificationCharacteristic ProductSpecificationCharacteristic { get; set; }
    }
}
