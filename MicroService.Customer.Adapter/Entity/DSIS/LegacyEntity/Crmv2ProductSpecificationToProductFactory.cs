﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductSpecificationToProductFactory
    {
        public int ProductSpecificationToProductFactoryId { get; set; }
        public int ProductSpecificationId { get; set; }
        public string ClassName { get; set; }

        public virtual Crmv2ProductSpecification ProductSpecification { get; set; }
    }
}
