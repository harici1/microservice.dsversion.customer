﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2ProductSpecificationType
    {
        public Crmv2ProductSpecificationType()
        {
            Crmv2ProductSpecification = new HashSet<Crmv2ProductSpecification>();
        }

        public int ProductSpecificationTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Crmv2ProductSpecification> Crmv2ProductSpecification { get; set; }
    }
}
