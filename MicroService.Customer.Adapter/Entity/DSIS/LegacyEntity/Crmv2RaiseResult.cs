﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2RaiseResult
    {
        public int RaiseResultId { get; set; }
        public int LeasingContractId { get; set; }
        public decimal CurrentPrice { get; set; }
        public decimal NewRaiseAmount { get; set; }
        public int OfferId { get; set; }
        public string AnkaContractNo { get; set; }
        public int? CatalogItemId { get; set; }
        public bool? Naked { get; set; }
        public string Debug { get; set; }
        public int DocumentTypeId { get; set; }
        public int DocumentId { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
