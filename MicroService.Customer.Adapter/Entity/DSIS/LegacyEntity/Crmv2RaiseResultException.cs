﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class Crmv2RaiseResultException
    {
        public int RaiseResultExceptionId { get; set; }
        public int? LeasingContractId { get; set; }
        public string AnkaContractNo { get; set; }
        public DateTime CreationDate { get; set; }
        public string ExceptionMessage { get; set; }
    }
}
