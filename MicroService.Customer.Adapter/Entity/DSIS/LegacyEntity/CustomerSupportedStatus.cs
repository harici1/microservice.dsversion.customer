﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class CustomerSupportedStatus
    {
        public int CustomerSupportedStatusId { get; set; }
        public int? CustomerId { get; set; }
        public string CustomerTckn { get; set; }
        public int? CustomerSupportedStatusTypeId { get; set; }
        public int? RalativeOfFamily { get; set; }
        public string RelativeTckn { get; set; }
        public bool? IsActive { get; set; }
        public int? CreateUserId { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? LastUpdateUserId { get; set; }
        public DateTime? LastUpdateDate { get; set; }
    }
}
