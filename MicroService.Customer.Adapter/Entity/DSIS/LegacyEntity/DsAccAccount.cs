﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsAccAccount
    {
        public DsAccAccount()
        {
            DsAccBatchItem = new HashSet<DsAccBatchItem>();
        }

        public int AccAccountId { get; set; }
        public string AccAccountCode { get; set; }
        public int? VposId { get; set; }
        public int? BankAccountId { get; set; }
        public int? PartyId { get; set; }
        public int PaymentTypeId { get; set; }
        public int AccOrganizationId { get; set; }
        public int AccBatchTypeId { get; set; }

        public virtual DsAccOrganization AccOrganization { get; set; }
        public virtual DsBankAccount BankAccount { get; set; }
        public virtual DsPaymentType PaymentType { get; set; }
        public virtual DsVposServiceProvider Vpos { get; set; }
        public virtual ICollection<DsAccBatchItem> DsAccBatchItem { get; set; }
    }
}
