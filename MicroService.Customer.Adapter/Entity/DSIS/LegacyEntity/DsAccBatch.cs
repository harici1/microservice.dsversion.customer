﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsAccBatch
    {
        public DsAccBatch()
        {
            DsAccBatchItem = new HashSet<DsAccBatchItem>();
        }

        public int AccBatchId { get; set; }
        public string Name { get; set; }
        public int AccBatchTypeId { get; set; }
        public int ProcessedItemCount { get; set; }
        public int? XmlBatchId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }

        public virtual DsAccBatchType AccBatchType { get; set; }
        public virtual ICollection<DsAccBatchItem> DsAccBatchItem { get; set; }
    }
}
