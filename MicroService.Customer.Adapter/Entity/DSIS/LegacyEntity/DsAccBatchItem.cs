﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsAccBatchItem
    {
        public int AccBatchItemId { get; set; }
        public int? AccBatchItemTypeId { get; set; }
        public int ItemId { get; set; }
        public int AccBatchId { get; set; }
        public int AccAccountId { get; set; }
        public int? AccPaymentTypeId { get; set; }
        public int? AccContactChannelId { get; set; }
        public string AccAccountCode { get; set; }
        public string AccPaymentTypeCode { get; set; }
        public string AccContactChannelCode { get; set; }
        public int? AccProductId { get; set; }
        public string AccProductCode { get; set; }
        public decimal? Amount { get; set; }
        public decimal? NetAmount { get; set; }
        public decimal? TaxAmount1 { get; set; }
        public decimal? TaxAmount2 { get; set; }
        public int NumberOfProduct { get; set; }
        public DateTime ProcessDate { get; set; }
        public string WayBillNo { get; set; }
        public int? InvoiceId { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? AccBatchTypeId { get; set; }
        public int? VposId { get; set; }
        public int? BankAccountId { get; set; }
        public int? InstallmentNumber { get; set; }
        public int? PaymentTypeId { get; set; }
        public int InvoicePromptDay { get; set; }
        public int? TempId { get; set; }
        public int? SaleCompensationId { get; set; }
        public int? Counter { get; set; }
        public string InvoiceNumber { get; set; }
        public int? XmlAccBatchId { get; set; }
        public bool? T1 { get; set; }
        public bool? T2 { get; set; }

        public virtual DsAccAccount AccAccount { get; set; }
        public virtual DsAccBatch AccBatch { get; set; }
        public virtual DsAccBatchItemType AccBatchItemType { get; set; }
        public virtual DsAccContactChannel AccContactChannel { get; set; }
        public virtual DsAccPaymentType AccPaymentType { get; set; }
    }
}
