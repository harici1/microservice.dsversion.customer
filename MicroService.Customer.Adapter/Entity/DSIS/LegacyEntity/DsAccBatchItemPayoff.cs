﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsAccBatchItemPayoff
    {
        public int AccBatchItemPayoffId { get; set; }
        public int? AccBatchId { get; set; }
        public int? SaleCompensationPeriodId { get; set; }
        public string DocumentType { get; set; }
        public string OrganizationCode { get; set; }
        public string BusinessField { get; set; }
        public int? SalecompensationLineId { get; set; }
        public string RevisionNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public string PeriodYear { get; set; }
        public string PeriodMonth { get; set; }
        public DateTime? RecordDate { get; set; }
        public DateTime? DocumentDate { get; set; }
        public DateTime? PromptDate { get; set; }
        public string AccountCode { get; set; }
        public string CurrencyCode { get; set; }
        public string ItemNo { get; set; }
        public decimal? ExpenseAmount { get; set; }
        public decimal? PayoffAmount { get; set; }
        public decimal? DealerAmount { get; set; }
        public string TaxType { get; set; }
        public decimal? Tax191Amount { get; set; }
        public decimal? OivAmount { get; set; }
        public decimal? Tax391Amount { get; set; }
        public string ProductCode { get; set; }
        public string PaymentType { get; set; }
        public int? SaleCompensationId { get; set; }
        public int? IsActive { get; set; }
    }
}
