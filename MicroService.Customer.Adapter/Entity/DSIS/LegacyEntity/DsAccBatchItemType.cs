﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsAccBatchItemType
    {
        public DsAccBatchItemType()
        {
            DsAccBatchItem = new HashSet<DsAccBatchItem>();
        }

        public int AccBatchItemTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsAccBatchItem> DsAccBatchItem { get; set; }
    }
}
