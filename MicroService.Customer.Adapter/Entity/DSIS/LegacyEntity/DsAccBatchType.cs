﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsAccBatchType
    {
        public DsAccBatchType()
        {
            DsAccBatch = new HashSet<DsAccBatch>();
        }

        public int AccBatchTypeId { get; set; }
        public string Name { get; set; }
        public string SystemCode { get; set; }
        public int AccOrganizationId { get; set; }

        public virtual ICollection<DsAccBatch> DsAccBatch { get; set; }
    }
}
