﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsAccContactChannel
    {
        public DsAccContactChannel()
        {
            DsAccBatchItem = new HashSet<DsAccBatchItem>();
        }

        public int AccContanctChannelId { get; set; }
        public string ChannelCode { get; set; }
        public string Name { get; set; }
        public int ContactChannelId { get; set; }

        public virtual DsContactChannel ContactChannel { get; set; }
        public virtual ICollection<DsAccBatchItem> DsAccBatchItem { get; set; }
    }
}
