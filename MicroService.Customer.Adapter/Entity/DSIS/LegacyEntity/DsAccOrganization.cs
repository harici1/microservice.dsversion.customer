﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsAccOrganization
    {
        public DsAccOrganization()
        {
            DsAccAccount = new HashSet<DsAccAccount>();
        }

        public int AccOrganizationId { get; set; }
        public string OrganizationCode { get; set; }
        public string Name { get; set; }
        public int CorporationId { get; set; }

        public virtual ICollection<DsAccAccount> DsAccAccount { get; set; }
    }
}
