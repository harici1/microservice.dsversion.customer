﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsAccPaymentType
    {
        public DsAccPaymentType()
        {
            DsAccBatchItem = new HashSet<DsAccBatchItem>();
        }

        public int AccPaymentTypeId { get; set; }
        public string PaymentCode { get; set; }
        public string Name { get; set; }
        public int PaymentTypeId { get; set; }

        public virtual DsPaymentType PaymentType { get; set; }
        public virtual ICollection<DsAccBatchItem> DsAccBatchItem { get; set; }
    }
}
