﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsAccProduct
    {
        public int AccProductId { get; set; }
        public int? InvoiceItemId { get; set; }
        public int? SaleCompensationTypeId { get; set; }
        public string ProductCode { get; set; }
        public int AccBatchItemTypeId { get; set; }
        public int AccBatchTypeId { get; set; }
        public bool IsVirtualProduct { get; set; }
        public bool TransferToSap { get; set; }

        public virtual DsInvoiceItem InvoiceItem { get; set; }
    }
}
