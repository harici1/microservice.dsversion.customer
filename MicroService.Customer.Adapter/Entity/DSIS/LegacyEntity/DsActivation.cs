﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsActivation
    {
        public DsActivation()
        {
            DsActivationDetail = new HashSet<DsActivationDetail>();
            DsActivationService = new HashSet<DsActivationService>();
            DsDealerStsActivation = new HashSet<DsDealerStsActivation>();
            DsWorkOrderActivationGeneric = new HashSet<DsWorkOrderActivationGeneric>();
            DsWorkOrderStbReturn = new HashSet<DsWorkOrderStbReturn>();
            DsWorkOrderSts = new HashSet<DsWorkOrderSts>();
        }

        public int ActivationId { get; set; }
        public int CustomerId { get; set; }
        public int? SmartCardId { get; set; }
        public int? StbId { get; set; }
        public int? RegionKeyId { get; set; }
        public int? BouquetId { get; set; }
        public int? OrderId { get; set; }
        public int? ParentalRatingId { get; set; }
        public string PersonelBits { get; set; }
        public int? ActivationStatusId { get; set; }
        public int? ActivationLogTypeId { get; set; }
        public int? AdditionalDisplayCount { get; set; }
        public int? ContactChannelId { get; set; }
        public int? DealerId { get; set; }
        public int? SalesSourceId { get; set; }
        public int? CampaignId { get; set; }
        public int? ReturnDealerId { get; set; }
        public int? TechnicalServiceDealerId { get; set; }
        public int? CancelReasonId { get; set; }
        public int? ReturnReasonId { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? CretionDate { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public int? LeasingContractId { get; set; }
        public bool? IsOk { get; set; }
        public int ActivationSalesModelId { get; set; }
        public int? SaledById { get; set; }
        public int? WorkOrderId { get; set; }
        public int SendingStbSkuId { get; set; }
        public bool? UpsIsOk { get; set; }
        public bool? IsIsOk { get; set; }
        public bool? LeasingContractIsOk { get; set; }
        public bool? UpsSignatureIsOk { get; set; }
        public bool? IsSignatureIsOk { get; set; }
        public bool? LeasingContractSignatureIsOk { get; set; }
        public string BacodeNumber { get; set; }
        public string LogisticsNote { get; set; }
        public int? LogisticsDealerId { get; set; }
        public int? LogicticsLocationId { get; set; }
        public DateTime? LogisticsDealerDeliveryDate { get; set; }
        public int? ActivationLogisticsPromotionSendingStatusId { get; set; }
        public bool? UpdatedByForm { get; set; }
        public int? LeasingContractAcceptedById { get; set; }
        public int? HdPvrHddId { get; set; }
        public int? ExternalEquipmentId { get; set; }
        public bool? IsSmartCity { get; set; }
        public int? SuspendBouquetId { get; set; }
        public int? ModemId { get; set; }
        public string ModemSerialNumber { get; set; }
        public int? RequestedBonusProductId { get; set; }
        public bool ValidateReactivation { get; set; }
        public int? LastOrderId { get; set; }
        public int? LastOrderTypeId { get; set; }
        public Guid UniqueId { get; set; }

        public virtual DsOrder Order { get; set; }
        public virtual DsStbSku SendingStbSku { get; set; }
        public virtual DsActivationAlias DsActivationAlias { get; set; }
        public virtual ICollection<DsActivationDetail> DsActivationDetail { get; set; }
        public virtual ICollection<DsActivationService> DsActivationService { get; set; }
        public virtual ICollection<DsDealerStsActivation> DsDealerStsActivation { get; set; }
        public virtual ICollection<DsWorkOrderActivationGeneric> DsWorkOrderActivationGeneric { get; set; }
        public virtual ICollection<DsWorkOrderStbReturn> DsWorkOrderStbReturn { get; set; }
        public virtual ICollection<DsWorkOrderSts> DsWorkOrderSts { get; set; }
    }
}
