﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsActivationAlias
    {
        public int ActivationAliasId { get; set; }
        public int ActivationId { get; set; }
        public string Alias { get; set; }

        public virtual DsActivation Activation { get; set; }
    }
}
