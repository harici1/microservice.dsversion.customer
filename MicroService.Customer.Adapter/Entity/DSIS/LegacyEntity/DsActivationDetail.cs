﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsActivationDetail
    {
        public int ActivationDetailId { get; set; }
        public int ActivationDetailTypeId { get; set; }
        public int ActivationId { get; set; }
        public string Value { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LeasingContractId { get; set; }

        public virtual DsActivation Activation { get; set; }
    }
}
