﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsActivationFeature
    {
        public int ActivationFeatureId { get; set; }
        public int ActivationId { get; set; }
        public int FeatureTypeId { get; set; }
        public string Value { get; set; }
    }
}
