﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsActivationLog
    {
        public int ActivationLogId { get; set; }
        public int? ActivationId { get; set; }
        public int? CustomerId { get; set; }
        public int? SmartCardId { get; set; }
        public int? StbId { get; set; }
        public int? RegionKeyId { get; set; }
        public int? BouquetId { get; set; }
        public int? ActivaionOrderId { get; set; }
        public int? ParentalRatingId { get; set; }
        public string PersonelBits { get; set; }
        public int? ActivationStatusId { get; set; }
        public int? AdditionalDisplayCount { get; set; }
        public int ActivationLogTypeId { get; set; }
        public int OrderId { get; set; }
        public int? ContactChannelId { get; set; }
        public int? DealerId { get; set; }
        public int? SalesSourceId { get; set; }
        public int? CampaignId { get; set; }
        public int? ReturnDealerId { get; set; }
        public int? TechnicalServiceDealerId { get; set; }
        public int? CancelReasonId { get; set; }
        public int? ReturnReasonId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public int? LeasingContractId { get; set; }
        public int? ActivationSalesModelId { get; set; }
        public int? SaledById { get; set; }
        public int? WorkOrderId { get; set; }
        public int? SendingStbSkuId { get; set; }
        public int? ChangeStbOrderId { get; set; }
        public int? ExternalEquipmentId { get; set; }
        public int? ModemId { get; set; }
        public string ModemSerialNumber { get; set; }
        public bool? IsSmartCity { get; set; }
    }
}
