﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsActivationLogType
    {
        public int ActivationLogTypeId { get; set; }
        public string Name { get; set; }
    }
}
