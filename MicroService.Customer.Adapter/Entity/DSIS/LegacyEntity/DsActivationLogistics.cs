﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsActivationLogistics
    {
        public int ActivationLogisticsId { get; set; }
        public int ActivationId { get; set; }
        public int? LeasingContractServiceId { get; set; }
        public int CampaignId { get; set; }
        public int CargoKey { get; set; }
        public int InvoiceKey { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverAddress { get; set; }
        public int? DistrictId { get; set; }
        public string ReceiverPhoneNumber { get; set; }
        public string ReceiverMobilePhoneNumber { get; set; }
        public string ReturnDocId { get; set; }
        public int ActivationLogisticsStatusId { get; set; }
        public int ActivationLogisticsTypeId { get; set; }
        public string Description { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public byte[] CargoKeyBarcode { get; set; }
        public int RowNumber { get; set; }
        public int? JobBatchId { get; set; }
        public DateTime BatchUpdateStartDate { get; set; }
        public int BatchSent { get; set; }
    }
}
