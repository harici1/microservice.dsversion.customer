﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsActivationLogisticsChangeLog
    {
        public int ActivationLogisticsChangeLogId { get; set; }
        public int ActivationLogisticsId { get; set; }
        public int? ActivationLogisticsStatusId { get; set; }
        public string ArrivalTrCenterName { get; set; }
        public int? ArrivalTrCenterUnitId { get; set; }
        public int? ArrivalUnitId { get; set; }
        public string ArrivalUnitName { get; set; }
        public string CargoEventId { get; set; }
        public string CargoEventExplanation { get; set; }
        public string CargoKey { get; set; }
        public string CargoReasonExplanation { get; set; }
        public string CargoReasonId { get; set; }
        public int? CargoType { get; set; }
        public string CargoTypeExplanation { get; set; }
        public string DelEmpName { get; set; }
        public int? DelEmpId { get; set; }
        public int? DelInfoDelUnitId { get; set; }
        public string DeliveryUnitName { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public int? DeliveryType { get; set; }
        public string DepartureTrCenterName { get; set; }
        public int? DepartureTrCenterUnitId { get; set; }
        public int? DepartureUnitId { get; set; }
        public int? DelInfoDeliveryFlag { get; set; }
        public string DepartureUnitName { get; set; }
        public DateTime? ReturnDeliveryDate { get; set; }
        public string ReturnDocId { get; set; }
        public DateTime? ReturnDocumentDate { get; set; }
        public string WaybillNo { get; set; }
        public int? ActiveFlag { get; set; }
        public string DocCargoId { get; set; }
        public string DocNumber { get; set; }
        public string DocType { get; set; }
        public string DocumentEventId { get; set; }
        public string DocumentExplanation { get; set; }
        public string DocumentReasonExplanation { get; set; }
        public string InvCustId { get; set; }
        public string InvCustName { get; set; }
        public string PickUpType { get; set; }
        public string PickUpTypeName { get; set; }
        public string ReceiverAddressTxt { get; set; }
        public string ReceiverCustId { get; set; }
        public string ReceiverCustName { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
