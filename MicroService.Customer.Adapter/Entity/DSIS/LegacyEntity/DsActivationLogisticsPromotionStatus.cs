﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsActivationLogisticsPromotionStatus
    {
        public int ActivationLogisticsPromotionStatusId { get; set; }
        public string Name { get; set; }
        public bool VisibleForDealer { get; set; }
    }
}
