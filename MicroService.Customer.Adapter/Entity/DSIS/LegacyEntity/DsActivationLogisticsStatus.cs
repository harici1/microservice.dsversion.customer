﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsActivationLogisticsStatus
    {
        public int ActivationLogisticsStatusId { get; set; }
        public string Name { get; set; }
    }
}
