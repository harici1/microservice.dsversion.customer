﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsActivationLogisticsType
    {
        public int ActivationLogisticsTypeId { get; set; }
        public string Name { get; set; }
        public bool IncludeUps { get; set; }
        public bool IncludeIs { get; set; }
        public bool IncludeContract { get; set; }
    }
}
