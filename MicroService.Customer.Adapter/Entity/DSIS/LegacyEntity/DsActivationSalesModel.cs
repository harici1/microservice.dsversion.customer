﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsActivationSalesModel
    {
        public DsActivationSalesModel()
        {
            DsRoleTableConstraint = new HashSet<DsRoleTableConstraint>();
        }

        public int ActivationSalesModelId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsRoleTableConstraint> DsRoleTableConstraint { get; set; }
    }
}
