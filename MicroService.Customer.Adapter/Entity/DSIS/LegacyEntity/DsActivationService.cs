﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsActivationService
    {
        public DsActivationService()
        {
            DsActivationServiceLog = new HashSet<DsActivationServiceLog>();
        }

        public int ActivationServiceId { get; set; }
        public int ActivationId { get; set; }
        public int? PpvActivationId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? ActivationServiceTypeId { get; set; }
        public int? ActivationServiceLogTypeId { get; set; }
        public int? ActivationServiceStatusId { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public bool FtvIsClosed { get; set; }

        public virtual DsActivation Activation { get; set; }
        public virtual DsActivationServiceLogType ActivationServiceLogType { get; set; }
        public virtual DsActivationServiceStatus ActivationServiceStatus { get; set; }
        public virtual DsActivationServiceType ActivationServiceType { get; set; }
        public virtual DsPpvActivation PpvActivation { get; set; }
        public virtual ICollection<DsActivationServiceLog> DsActivationServiceLog { get; set; }
    }
}
