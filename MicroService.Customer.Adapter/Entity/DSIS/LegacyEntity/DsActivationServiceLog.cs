﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsActivationServiceLog
    {
        public int ActivationServiceLogId { get; set; }
        public int? ActivationServiceId { get; set; }
        public int ActivationServiceLogTypeId { get; set; }
        public int ActivationServiceTypeId { get; set; }
        public int ActivationServiceStatusId { get; set; }
        public int? FromProductId { get; set; }
        public int? ToProductId { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? CreatedById { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? TmpActivationId { get; set; }
        public int? TmpPpvActivationId { get; set; }
        public int? Rank { get; set; }

        public virtual DsActivationService ActivationService { get; set; }
        public virtual DsActivationServiceLogType ActivationServiceLogType { get; set; }
        public virtual DsActivationServiceStatus ActivationServiceStatus { get; set; }
        public virtual DsActivationServiceType ActivationServiceType { get; set; }
        public virtual DsProduct FromProduct { get; set; }
        public virtual DsProduct ToProduct { get; set; }
    }
}
