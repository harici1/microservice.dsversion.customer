﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsActivationServiceStatus
    {
        public DsActivationServiceStatus()
        {
            DsActivationService = new HashSet<DsActivationService>();
            DsActivationServiceLog = new HashSet<DsActivationServiceLog>();
        }

        public int ActivationServiceStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsActivationService> DsActivationService { get; set; }
        public virtual ICollection<DsActivationServiceLog> DsActivationServiceLog { get; set; }
    }
}
