﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsActivationStatus
    {
        public int ActivationStatusId { get; set; }
        public string Name { get; set; }
    }
}
