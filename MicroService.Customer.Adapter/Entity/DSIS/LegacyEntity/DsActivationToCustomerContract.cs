﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsActivationToCustomerContract
    {
        public int ActivationToCustomerContractId { get; set; }
        public int ActivationId { get; set; }
        public string CustomerType { get; set; }
        public string PayPacket { get; set; }
        public string PaymentType { get; set; }
        public string Campaign { get; set; }
        public decimal Amount { get; set; }
        public string Bank { get; set; }
        public string BankAccountOwner { get; set; }
        public string BankCode { get; set; }
        public string AccountNumber { get; set; }
        public int ContractNumber { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
