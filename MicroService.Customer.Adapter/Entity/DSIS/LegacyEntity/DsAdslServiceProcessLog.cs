﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsAdslServiceProcessLog
    {
        public int AdslServiceProcessLogId { get; set; }
        public DateTime CreationDate { get; set; }
        public string ServiceProcessResult { get; set; }
        public string ServiceProcessCode { get; set; }
        public string MobilePhone1 { get; set; }
        public string AdslPhoneNumber { get; set; }
    }
}
