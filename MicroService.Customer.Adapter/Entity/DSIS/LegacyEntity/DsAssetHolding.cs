﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsAssetHolding
    {
        public int AssetHoldingId { get; set; }
        public string AssetHoldingName { get; set; }
        public bool? IsActive { get; set; }
        public decimal? AssetHoldingPrice { get; set; }
    }
}
