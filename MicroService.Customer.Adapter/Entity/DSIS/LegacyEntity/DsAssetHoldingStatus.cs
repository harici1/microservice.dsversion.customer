﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsAssetHoldingStatus
    {
        public int AssetHoldingStatusId { get; set; }
        public string Status { get; set; }
    }
}
