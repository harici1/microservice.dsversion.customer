﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsAssetHoldingToStatus
    {
        public int AssetHoldingToStatusId { get; set; }
        public int AssetHoldingId { get; set; }
        public int AssetHoldingStatusId { get; set; }
        public int RoleId { get; set; }
    }
}
