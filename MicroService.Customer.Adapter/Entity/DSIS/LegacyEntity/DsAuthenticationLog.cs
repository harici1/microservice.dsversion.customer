﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsAuthenticationLog
    {
        public int AuthenticationLogId { get; set; }
        public int TypeId { get; set; }
        public DateTime CreationDate { get; set; }
        public int? ApplicationId { get; set; }
        public string UserCode { get; set; }
        public string Password { get; set; }
        public string IpAddress { get; set; }
        public int? ContactChannelId { get; set; }
        public int? UserId { get; set; }
        public int? AuthenticationSigninTypeId { get; set; }

        public virtual DsContactChannel ContactChannel { get; set; }
        public virtual DsAuthenticationLogType Type { get; set; }
        public virtual DsUser User { get; set; }
    }
}
