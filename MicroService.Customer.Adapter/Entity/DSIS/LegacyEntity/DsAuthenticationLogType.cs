﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsAuthenticationLogType
    {
        public DsAuthenticationLogType()
        {
            DsAuthenticationLog = new HashSet<DsAuthenticationLog>();
        }

        public int AuthenticationLogTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<DsAuthenticationLog> DsAuthenticationLog { get; set; }
    }
}
