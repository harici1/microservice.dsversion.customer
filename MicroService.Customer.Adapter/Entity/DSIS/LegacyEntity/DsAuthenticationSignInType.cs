﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsAuthenticationSignInType
    {
        public int AuthenticationSingInTypeId { get; set; }
        public string AuthenticationSingInTypeName { get; set; }
    }
}
