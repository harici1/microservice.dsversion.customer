﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBank
    {
        public int BankId { get; set; }
        public string Name { get; set; }
        public bool IsForceIbanShortCodeValidation { get; set; }
        public string ShortName { get; set; }
    }
}
