﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBankAccount
    {
        public DsBankAccount()
        {
            DsAccAccount = new HashSet<DsAccAccount>();
            DsExpenseBillReturnPayment = new HashSet<DsExpenseBillReturnPayment>();
            DsInvoiceToBankChargeDetail = new HashSet<DsInvoiceToBankChargeDetail>();
            DsMoneyOrder = new HashSet<DsMoneyOrder>();
            DsRoleTableConstraint = new HashSet<DsRoleTableConstraint>();
        }

        public int BankAccountId { get; set; }
        public string Name { get; set; }
        public int BankId { get; set; }
        public string Description { get; set; }
        public bool AccountingIsForced { get; set; }
        public int? CorporationId { get; set; }

        public virtual DsCorporation Corporation { get; set; }
        public virtual ICollection<DsAccAccount> DsAccAccount { get; set; }
        public virtual ICollection<DsExpenseBillReturnPayment> DsExpenseBillReturnPayment { get; set; }
        public virtual ICollection<DsInvoiceToBankChargeDetail> DsInvoiceToBankChargeDetail { get; set; }
        public virtual ICollection<DsMoneyOrder> DsMoneyOrder { get; set; }
        public virtual ICollection<DsRoleTableConstraint> DsRoleTableConstraint { get; set; }
    }
}
