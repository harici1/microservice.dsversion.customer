﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBankAgreement
    {
        public int BankAgreementId { get; set; }
        public int BankCode { get; set; }
        public int RequestPaymentUnit { get; set; }
        public decimal RequestPaymentTotal { get; set; }
        public int RequestCancelUnit { get; set; }
        public decimal RequestCancelTotal { get; set; }
        public int ResponsePaymentUnit { get; set; }
        public decimal ResponsePaymentTotal { get; set; }
        public int ResponseCancelUnit { get; set; }
        public decimal ResponseCancelTotal { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseDesc { get; set; }
        public DateTime AggrementDate { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
