﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBankIban
    {
        public int BankIbanId { get; set; }
        public int BankId { get; set; }
        public string IbanCode { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public string Description { get; set; }
        public string CardOrganization { get; set; }
        public int? ExBankCode { get; set; }
        public string ExCountryCode { get; set; }
        public string CardClass { get; set; }
        public string ExBankName { get; set; }
        public int? InstallmentOptionBankId { get; set; }
        public bool? IsLocal { get; set; }
    }
}
