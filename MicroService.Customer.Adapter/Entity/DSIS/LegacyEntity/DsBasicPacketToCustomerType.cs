﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBasicPacketToCustomerType
    {
        public int BasicPacketToCustomerTypeId { get; set; }
        public int BasicPacketId { get; set; }
        public int CustomerTypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool AutoAuthorizeEnabled { get; set; }

        public virtual DsContentProduct BasicPacket { get; set; }
        public virtual DsCustomerType CustomerType { get; set; }
    }
}
