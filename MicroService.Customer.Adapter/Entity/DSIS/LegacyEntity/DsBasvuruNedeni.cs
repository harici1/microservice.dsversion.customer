﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBasvuruNedeni
    {
        public DsBasvuruNedeni()
        {
            DsDestekSinifi = new HashSet<DsDestekSinifi>();
        }

        public int? DomainId { get; set; }
        public int BasvuruId { get; set; }
        public string Tanimi { get; set; }
        public int? OlusturanKullanici { get; set; }
        public DateTime? OlusturmaTarihi { get; set; }
        public int? DuzeltenKullanici { get; set; }
        public DateTime? DuzeltmeTarihi { get; set; }
        public byte? Durumu { get; set; }

        public virtual DsDomain Domain { get; set; }
        public virtual ICollection<DsDestekSinifi> DsDestekSinifi { get; set; }
    }
}
