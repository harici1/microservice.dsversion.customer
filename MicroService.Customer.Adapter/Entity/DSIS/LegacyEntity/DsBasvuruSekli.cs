﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBasvuruSekli
    {
        public int BasvuruSekliId { get; set; }
        public int GrupId { get; set; }
        public string Aciklama { get; set; }
    }
}
