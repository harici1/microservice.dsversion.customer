﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBatch
    {
        public int BatchId { get; set; }
        public string Name { get; set; }
        public int? BatchTypeId { get; set; }
        public int? ProcessedItemCount { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? BatchPeriod { get; set; }
        public string Notes { get; set; }
        public int? PrintStatusId { get; set; }
        public DateTime? PrintDate { get; set; }

        public virtual DsBatchType BatchType { get; set; }
    }
}
