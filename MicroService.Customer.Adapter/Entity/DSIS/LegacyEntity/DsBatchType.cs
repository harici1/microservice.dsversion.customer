﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBatchType
    {
        public DsBatchType()
        {
            DsBatch = new HashSet<DsBatch>();
        }

        public int BatchTypeId { get; set; }
        public string Name { get; set; }
        public int? BatchTypeCategoryId { get; set; }
        public int? BatchTypeCategoryGroupId { get; set; }

        public virtual ICollection<DsBatch> DsBatch { get; set; }
    }
}
