﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBayi
    {
        public DsBayi()
        {
            DsDealerStsActivation = new HashSet<DsDealerStsActivation>();
            DsDealerToNeighbourhood = new HashSet<DsDealerToNeighbourhood>();
            DsSaleCompensation = new HashSet<DsSaleCompensation>();
            VendorParty = new HashSet<VendorParty>();
        }

        public int BayiId { get; set; }
        public string BayiKodu { get; set; }
        public string BayiAdi { get; set; }
        public string VergiNo { get; set; }
        public string VergiDaire { get; set; }
        public string Telefon1 { get; set; }
        public string Telefon2 { get; set; }
        public string CepTelefonu { get; set; }
        public string FaksNo { get; set; }
        public string YetkiliAdi { get; set; }
        public string EmailAdresi { get; set; }
        public DateTime YaratmaTarihi { get; set; }
        public DateTime? GuncellemeTarihi { get; set; }
        public int DealerTypeId { get; set; }
        public string ReferenceCode { get; set; }
        public string Address { get; set; }
        public bool? IsActive { get; set; }
        public int? SalesPersonId { get; set; }
        public string Description { get; set; }
        public int? BankId { get; set; }
        public string BankBranchName { get; set; }
        public string BankBranchCode { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankAccountName { get; set; }
        public string AccountNumber { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public string InvoiceTitle { get; set; }
        public string InvoiceAddress { get; set; }
        public string Iban { get; set; }
        public bool? IsStsAuthorized { get; set; }
        public string PasswordResetCode { get; set; }
        public DateTime? PasswordResetExpiryDate { get; set; }
        public decimal? CollateralAmount { get; set; }
        public string GarantiCode { get; set; }
        public int NeighbourhoodId { get; set; }
        public bool? IsWarehouse { get; set; }
        public bool VisibleToWeb { get; set; }
        public string Title { get; set; }
        public string Xaxis { get; set; }
        public string Yaxis { get; set; }
        public string DsmartWebEmail { get; set; }
        public string CloseNote { get; set; }
        public DateTime? CloseDate { get; set; }
        public int? CloseReasonId { get; set; }
        public int? CompanyTypeId { get; set; }
        public Guid DealerUid { get; set; }

        public virtual ICollection<DsDealerStsActivation> DsDealerStsActivation { get; set; }
        public virtual ICollection<DsDealerToNeighbourhood> DsDealerToNeighbourhood { get; set; }
        public virtual ICollection<DsSaleCompensation> DsSaleCompensation { get; set; }
        public virtual ICollection<VendorParty> VendorParty { get; set; }
    }
}
