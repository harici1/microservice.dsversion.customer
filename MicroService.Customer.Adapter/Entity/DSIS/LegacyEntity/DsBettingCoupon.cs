﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBettingCoupon
    {
        public int BettingCouponId { get; set; }
        public string CouponName { get; set; }
        public string CouponShortCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string UserDefineField1 { get; set; }
        public string UserDefineField2 { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int BettingCouponStatusId { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public bool? IsShowOnlyEroticChannel { get; set; }
        public int? BettingCouponTypeId { get; set; }
        public string SystemCode { get; set; }

        public virtual DsBettingCouponStatus BettingCouponStatus { get; set; }
    }
}
