﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBettingCouponChangeLog
    {
        public int BettingCouponChangeLogId { get; set; }
        public int BettingCouponChangeLogTypeId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public string OldValues { get; set; }
        public string NewValues { get; set; }
        public int BettingCouponId { get; set; }

        public virtual DsBettingCouponChangeLogType BettingCouponChangeLogType { get; set; }
    }
}
