﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBettingCouponChangeLogType
    {
        public DsBettingCouponChangeLogType()
        {
            DsBettingCouponChangeLog = new HashSet<DsBettingCouponChangeLog>();
        }

        public int BettingCouponChangeLogTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsBettingCouponChangeLog> DsBettingCouponChangeLog { get; set; }
    }
}
