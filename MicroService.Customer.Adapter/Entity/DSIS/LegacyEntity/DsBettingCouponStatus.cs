﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBettingCouponStatus
    {
        public DsBettingCouponStatus()
        {
            DsBettingCoupon = new HashSet<DsBettingCoupon>();
        }

        public int BettingCouponStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsBettingCoupon> DsBettingCoupon { get; set; }
    }
}
