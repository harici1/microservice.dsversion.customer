﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBettingCouponType
    {
        public int BettingCouponTypeId { get; set; }
        public string Name { get; set; }
    }
}
