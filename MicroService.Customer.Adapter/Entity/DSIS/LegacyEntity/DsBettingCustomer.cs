﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBettingCustomer
    {
        public DsBettingCustomer()
        {
            DsBettingCustomerChangeLog = new HashSet<DsBettingCustomerChangeLog>();
        }

        public int BettingCustomerId { get; set; }
        public string MobilePhone { get; set; }
        public string SocialIdentificationName { get; set; }
        public DateTime? BirthDate { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdateById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? SmartCardId { get; set; }

        public virtual DsSmartCard SmartCard { get; set; }
        public virtual ICollection<DsBettingCustomerChangeLog> DsBettingCustomerChangeLog { get; set; }
    }
}
