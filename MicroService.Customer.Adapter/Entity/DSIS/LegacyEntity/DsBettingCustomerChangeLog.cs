﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBettingCustomerChangeLog
    {
        public int BettingCustomerChangeLogId { get; set; }
        public int BettingCustomerId { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? CreatedById { get; set; }
        public string OldValues { get; set; }
        public string NewValues { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? DocumentId { get; set; }
        public int? BettingCustomerChangeLogTypeId { get; set; }

        public virtual DsBettingCustomer BettingCustomer { get; set; }
    }
}
