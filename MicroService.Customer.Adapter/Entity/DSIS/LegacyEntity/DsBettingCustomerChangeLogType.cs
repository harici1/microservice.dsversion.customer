﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBettingCustomerChangeLogType
    {
        public int BettingCustomerChangeLogTypeId { get; set; }
        public string Name { get; set; }
    }
}
