﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBettingMatch
    {
        public int BettingMatchId { get; set; }
        public string MatchCode { get; set; }
        public string Team1 { get; set; }
        public string Team2 { get; set; }
        public DateTime LastViewingDate { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
    }
}
