﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBettingOption
    {
        public int BettingOptionId { get; set; }
        public int BettingOptionTypeId { get; set; }
        public string UserDefineField1 { get; set; }
    }
}
