﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBettingOptionType
    {
        public int BettingOptionTypeId { get; set; }
        public string Name { get; set; }
    }
}
