﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBettingServiceCode
    {
        public int BettingServiceCodeId { get; set; }
        public int TypeId { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }

        public virtual DsBettingServiceCodeType Type { get; set; }
    }
}
