﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBettingServiceCodeType
    {
        public DsBettingServiceCodeType()
        {
            DsBettingServiceCode = new HashSet<DsBettingServiceCode>();
        }

        public int BettingServiceCodeTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsBettingServiceCode> DsBettingServiceCode { get; set; }
    }
}
