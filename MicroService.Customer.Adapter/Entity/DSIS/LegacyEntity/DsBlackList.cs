﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBlackList
    {
        public int BlackListId { get; set; }
        public string SocialIdentificationNumber { get; set; }
        public string Notes { get; set; }
        public bool IsActive { get; set; }
        public int BlackListTypeId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? DocumentId { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
    }
}
