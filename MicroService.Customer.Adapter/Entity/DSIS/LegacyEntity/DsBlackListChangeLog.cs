﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBlackListChangeLog
    {
        public int BlackListChangeLogId { get; set; }
        public int BlackListId { get; set; }
        public string Ssn { get; set; }
        public string Note { get; set; }
        public bool? IsActive { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? BlackListTypeId { get; set; }
    }
}
