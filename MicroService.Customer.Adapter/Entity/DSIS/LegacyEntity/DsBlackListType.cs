﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBlackListType
    {
        public int BlackListTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
