﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBluActivationCode
    {
        public int BluActivationCodeId { get; set; }
        public int PartyId { get; set; }
        public string ActivationCode { get; set; }
        public DateTime ExpirationDate { get; set; }
        public bool IsAuthorized { get; set; }
        public bool IsCancelled { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int ProductId { get; set; }
        public int? UsedByDocumentId { get; set; }
        public int? CreatedByDocumentId { get; set; }
        public int? CancelledByDocumentId { get; set; }
    }
}
