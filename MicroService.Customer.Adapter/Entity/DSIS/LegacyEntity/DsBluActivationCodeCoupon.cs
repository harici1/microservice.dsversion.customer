﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBluActivationCodeCoupon
    {
        public int BluActivationCodeCouponId { get; set; }
        public string ActivationCouponCode { get; set; }
        public string SerialNumber { get; set; }
        public DateTime? Startdate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public bool IsAuthorized { get; set; }
        public bool IsCancelled { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public string ClassName { get; set; }
        public int CampaignId { get; set; }
        public int? AdditionalDay { get; set; }
        public int? BonusProductId { get; set; }
        public decimal? DiscountFixAmount { get; set; }
        public decimal? DiscountRatio { get; set; }
        public int? UsedByDocumentId { get; set; }
        public int? CreatedByDocumentId { get; set; }
        public int? CancelledByDocumentId { get; set; }
        public int? Month { get; set; }
        public int? ProductId { get; set; }
    }
}
