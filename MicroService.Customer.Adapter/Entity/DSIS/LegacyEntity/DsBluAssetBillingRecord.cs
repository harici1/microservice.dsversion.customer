﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBluAssetBillingRecord
    {
        public int BluAssetBillingRecordId { get; set; }
        public int BluReportBackId { get; set; }
        public string ProviderId { get; set; }
        public string AssetId { get; set; }
        public string ProviderCode { get; set; }
        public string AssetTitle { get; set; }
        public int? ParentalRating { get; set; }
        public string Emmj { get; set; }
    }
}
