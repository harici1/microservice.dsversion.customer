﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBluBonusLog
    {
        public int BluBonusLogId { get; set; }
        public int BluBonusLogTypeId { get; set; }
        public int ProcessId { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
    }
}
