﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBluCampaign
    {
        public int BluCampaignId { get; set; }
        public int CampaignId { get; set; }
        public bool MultipleJoin { get; set; }

        public virtual DsCampaign Campaign { get; set; }
    }
}
