﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBluEntitlementMap
    {
        public int BluEntitlementMapId { get; set; }
        public int? MainPackageId { get; set; }
        public string EntitlementPackageId { get; set; }
        public int? EntitlementPackageTypeId { get; set; }
        public string WebDisplayName { get; set; }
        public int? AddOnProductId { get; set; }
    }
}
