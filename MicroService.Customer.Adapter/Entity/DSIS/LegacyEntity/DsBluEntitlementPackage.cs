﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBluEntitlementPackage
    {
        public int BluEntitlementPackageId { get; set; }
        public string EntitlementPackageId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
