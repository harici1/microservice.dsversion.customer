﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBluPaymentValidationLog
    {
        public int BluPaymentValidationLogId { get; set; }
        public int SubscriberUserId { get; set; }
        public bool ReturnStatus { get; set; }
        public string ReturnMessage { get; set; }
        public DateTime CreationDate { get; set; }
        public string UserId { get; set; }
    }
}
