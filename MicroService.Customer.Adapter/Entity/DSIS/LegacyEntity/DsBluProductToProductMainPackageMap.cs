﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBluProductToProductMainPackageMap
    {
        public int BluProductToProductMainPackageMap { get; set; }
        public int PpvProductMainPackageId { get; set; }
        public int BluProductMainPackageId { get; set; }
        public int? AddOnProductId { get; set; }
        public int? ForbiddenProductId { get; set; }
    }
}
