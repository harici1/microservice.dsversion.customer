﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBluReportBack
    {
        public int BluReportBackId { get; set; }
        public int NdsSubscriberId { get; set; }
        public int ActivationId { get; set; }
        public string Emm { get; set; }
        public DateTime? CreationDate { get; set; }
    }
}
