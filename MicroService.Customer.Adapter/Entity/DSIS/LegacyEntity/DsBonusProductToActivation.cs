﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBonusProductToActivation
    {
        public int BonusProductToActivationId { get; set; }
        public string Description { get; set; }
        public int MenuId { get; set; }
        public int ActivationId { get; set; }
        public int BonusProfileId { get; set; }
        public bool IsActive { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreationDate { get; set; }
        public string Note { get; set; }
        public int CreatedById { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
    }
}
