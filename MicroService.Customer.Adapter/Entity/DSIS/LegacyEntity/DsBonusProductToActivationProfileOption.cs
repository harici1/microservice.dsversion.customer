﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBonusProductToActivationProfileOption
    {
        public int BonusProfileOptionId { get; set; }
        public int BonusProfileId { get; set; }
        public int ProductMainPackageId { get; set; }
        public int ProductId { get; set; }
        public bool IsActive { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Note { get; set; }
        public DateTime CreationDate { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? LastUpdatedById { get; set; }
    }
}
