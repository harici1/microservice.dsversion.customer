﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBonusStatus
    {
        public DsBonusStatus()
        {
            DsBonuseski = new HashSet<DsBonuseski>();
        }

        public int BonusStatusId { get; set; }
        public int StatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsBonuseski> DsBonuseski { get; set; }
    }
}
