﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBonusType
    {
        public DsBonusType()
        {
            DsBonuseski = new HashSet<DsBonuseski>();
        }

        public int BonusTypeId { get; set; }
        public int InstallmentTypeId { get; set; }
        public int AvailableMonthlyDuration { get; set; }
        public string Name { get; set; }
        public int Priorty { get; set; }
        public bool Crmvisible { get; set; }
        public decimal? MaxBonusAmount { get; set; }
        public bool Active { get; set; }
        public string BonusProcessObjectName { get; set; }
        public string BonusProcessObjectInput { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreateDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }

        public virtual ICollection<DsBonuseski> DsBonuseski { get; set; }
    }
}
