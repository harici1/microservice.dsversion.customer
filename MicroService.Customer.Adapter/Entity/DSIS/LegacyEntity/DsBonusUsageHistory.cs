﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBonusUsageHistory
    {
        public int BonusUsageId { get; set; }
        public int BonusId { get; set; }
        public int LeasingContractInstallmentId { get; set; }
        public int LeasingContractId { get; set; }
        public int? ProductId { get; set; }
        public int? ServiceId { get; set; }
        public int? BonusProductId { get; set; }
        public int? BonusTypeId { get; set; }
        public decimal UsedAmount { get; set; }
        public int StatusId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreateDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }

        public virtual DsBonuseski Bonus { get; set; }
    }
}
