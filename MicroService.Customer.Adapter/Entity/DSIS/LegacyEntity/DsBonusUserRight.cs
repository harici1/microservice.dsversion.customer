﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBonusUserRight
    {
        public int RoleId { get; set; }
        public int UsageCount { get; set; }
        public int? ExpandAmountFactor { get; set; }
        public int Priority { get; set; }
        public bool IsAuthorized { get; set; }
        public bool BonusReasonRequired { get; set; }
        public bool BonusSubReasonRequired { get; set; }
        public bool Active { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreateDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }

        public virtual DsRole Role { get; set; }
    }
}
