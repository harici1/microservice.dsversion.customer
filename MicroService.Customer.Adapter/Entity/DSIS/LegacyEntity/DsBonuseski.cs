﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBonuseski
    {
        public DsBonuseski()
        {
            DsBonusUsageHistory = new HashSet<DsBonusUsageHistory>();
        }

        public int BonusId { get; set; }
        public int LeasingContractId { get; set; }
        public int? ProductId { get; set; }
        public int? ServiceId { get; set; }
        public int? ServiceTypeId { get; set; }
        public int? BonusProductId { get; set; }
        public int? BonusTypeId { get; set; }
        public int? BonusReasonId { get; set; }
        public int? BonusSubReasonId { get; set; }
        public decimal Amount { get; set; }
        public decimal RemainingAmount { get; set; }
        public string Description { get; set; }
        public int StatusId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? DailyExtension { get; set; }
        public int? MonthlyExtension { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreateDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }

        public virtual DsBonusType BonusType { get; set; }
        public virtual DsLeasingContract LeasingContract { get; set; }
        public virtual DsProduct Product { get; set; }
        public virtual DsBonusStatus Status { get; set; }
        public virtual ICollection<DsBonusUsageHistory> DsBonusUsageHistory { get; set; }
    }
}
