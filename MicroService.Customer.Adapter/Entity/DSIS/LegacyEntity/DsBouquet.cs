﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBouquet
    {
        public DsBouquet()
        {
            DsBouquetToCustomerType = new HashSet<DsBouquetToCustomerType>();
            DsCustomerType = new HashSet<DsCustomerType>();
            DsRoleTableConstraint = new HashSet<DsRoleTableConstraint>();
        }

        public int BouquetId { get; set; }
        public string BouquetCode { get; set; }
        public string Name { get; set; }
        public string HexCode { get; set; }
        public string BouquetCodeWithoutFreeToAirEnableBit { get; set; }

        public virtual ICollection<DsBouquetToCustomerType> DsBouquetToCustomerType { get; set; }
        public virtual ICollection<DsCustomerType> DsCustomerType { get; set; }
        public virtual ICollection<DsRoleTableConstraint> DsRoleTableConstraint { get; set; }
    }
}
