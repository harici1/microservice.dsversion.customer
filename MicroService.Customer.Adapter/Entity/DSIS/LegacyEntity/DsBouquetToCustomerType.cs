﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBouquetToCustomerType
    {
        public int BouquetId { get; set; }
        public int CustomerTypeId { get; set; }

        public virtual DsBouquet Bouquet { get; set; }
        public virtual DsCustomerType CustomerType { get; set; }
    }
}
