﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBouquetToStbType
    {
        public int BouquetToStbTypeId { get; set; }
        public int BouquetId { get; set; }
        public int StbTypeId { get; set; }
    }
}
