﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsBrand
    {
        public int BrandId { get; set; }
        public string Name { get; set; }
        public string DistCode { get; set; }
    }
}
