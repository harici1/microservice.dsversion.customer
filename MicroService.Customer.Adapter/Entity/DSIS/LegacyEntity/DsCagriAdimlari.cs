﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCagriAdimlari
    {
        public decimal CagriId { get; set; }
        public decimal AdimId { get; set; }
        public string Aciklama { get; set; }
        public DateTime? OlusturmaTarih { get; set; }
        public int? OlusturanId { get; set; }
        public string OlusturanAdi { get; set; }
        public DateTime? DuzeltmeTarih { get; set; }
        public int? DuzeltenId { get; set; }
        public string DuzeltenAdi { get; set; }
        public string AdimKodu { get; set; }
        public string KodAciklama { get; set; }
        public string AdimAciklama { get; set; }
        public DateTime? Tarih { get; set; }
        public DateTime? Zaman { get; set; }
        public string AcilisAciklamasi { get; set; }
        public string KapanisAciklamasi { get; set; }
        public DateTime? RezervasyonTarihi { get; set; }
    }
}
