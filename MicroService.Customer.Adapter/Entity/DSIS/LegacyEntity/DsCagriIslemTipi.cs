﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCagriIslemTipi
    {
        public int CagriIslemTipiId { get; set; }
        public string Name { get; set; }
    }
}
