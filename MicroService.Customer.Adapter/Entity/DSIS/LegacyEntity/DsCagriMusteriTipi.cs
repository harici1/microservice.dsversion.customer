﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCagriMusteriTipi
    {
        public byte CagriMusteriTipiId { get; set; }
        public string Ad { get; set; }
    }
}
