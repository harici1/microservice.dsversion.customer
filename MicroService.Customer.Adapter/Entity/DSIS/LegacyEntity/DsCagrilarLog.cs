﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCagrilarLog
    {
        public decimal LogId { get; set; }
        public decimal CagriId { get; set; }
        public DateTime? AcilisZamani { get; set; }
        public decimal? AcilisTarihi { get; set; }
        public int? AcanKullaniciId { get; set; }
        public string AcanKullaniciAdi { get; set; }
        public string AcilisAciklamasi { get; set; }
        public byte? BasvuruSekliId { get; set; }
        public string BasvuruSekli { get; set; }
        public int? BasvuruNedeniId { get; set; }
        public string BasvuruNedeni { get; set; }
        public DateTime? KapanisZamani { get; set; }
        public decimal? KapanisTarihi { get; set; }
        public int? KapatanKullaniciId { get; set; }
        public string KapatanKullaniciAdi { get; set; }
        public string KapanisAciklamasi { get; set; }
        public string Durum { get; set; }
        public string DurumAciklama { get; set; }
        public int? DestekSinifiId { get; set; }
        public string DestekSinifi { get; set; }
        public int? DestekKonusuId { get; set; }
        public string DestekKonusu { get; set; }
        public int? TanimliSorunId { get; set; }
        public string TanimliSorun { get; set; }
        public int? TanimliCozumId { get; set; }
        public string TanimliCozum { get; set; }
        public string MusteriTuru { get; set; }
        public decimal? MusteriId { get; set; }
        public string MusteriAdi { get; set; }
        public int? HavaleEdenId { get; set; }
        public string HavaleEdenAdi { get; set; }
        public int? HavaleEdilenId { get; set; }
        public string HavaleEdilenAdi { get; set; }
        public int? HavaleEdilenGrupId { get; set; }
        public string HavaleEdilenGrupAdi { get; set; }
        public decimal? HavaleTarihi { get; set; }
        public DateTime? HavaleZamani { get; set; }
        public string HavaleAciklamasi { get; set; }
        public int? DuzeltenId { get; set; }
        public string DuzeltenAdi { get; set; }
        public DateTime? DuzeltmeTarihi { get; set; }
        public int? BayiId { get; set; }
        public int? KutuId { get; set; }
        public string YeniAcilisAciklamasi { get; set; }
        public string YeniKapanisAciklamasi { get; set; }
        public byte? YeniBasvuruSekliId { get; set; }
        public int? YeniBasvuruNedeniId { get; set; }
        public int? YeniDestekSinifiId { get; set; }
        public int? YeniDestekKonusuId { get; set; }
        public int? YeniTanimliSorunId { get; set; }
        public int? YeniTanimliCozumId { get; set; }
        public int? DegisiklikYapanId { get; set; }
        public DateTime? YeniDuzeltmeTarihi { get; set; }
        public DateTime? RezervasyonTarihi { get; set; }
    }
}
