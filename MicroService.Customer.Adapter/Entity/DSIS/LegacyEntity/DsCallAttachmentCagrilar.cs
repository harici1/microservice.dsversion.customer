﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCallAttachmentCagrilar
    {
        public decimal CagriId { get; set; }
        public int DosyaId { get; set; }
        public int RefCagri { get; set; }

        public virtual DsCagrilar Cagri { get; set; }
    }
}
