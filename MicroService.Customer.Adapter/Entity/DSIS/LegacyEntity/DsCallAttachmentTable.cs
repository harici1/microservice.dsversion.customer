﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCallAttachmentTable
    {
        public int DosyaId { get; set; }
        public string DosyaAdi { get; set; }
        public string DosyaYolu { get; set; }
        public DateTime? EklenmeTarihi { get; set; }
        public double? DosyaBoyutu { get; set; }
        public string DosyaTuru { get; set; }
        public int? UserId { get; set; }
    }
}
