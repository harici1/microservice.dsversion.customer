﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCallOpenCallCounter
    {
        public decimal CagriId { get; set; }
        public int Counter { get; set; }
    }
}
