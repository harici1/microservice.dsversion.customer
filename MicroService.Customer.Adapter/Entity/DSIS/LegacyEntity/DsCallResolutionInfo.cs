﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCallResolutionInfo
    {
        public DsCallResolutionInfo()
        {
            DsCallResolutionInfoParameter = new HashSet<DsCallResolutionInfoParameter>();
            DsCallResolutionInfoToResolution = new HashSet<DsCallResolutionInfoToResolution>();
        }

        public int CallResolutionInfoId { get; set; }
        public string Header { get; set; }
        public string InnerHtml { get; set; }

        public virtual ICollection<DsCallResolutionInfoParameter> DsCallResolutionInfoParameter { get; set; }
        public virtual ICollection<DsCallResolutionInfoToResolution> DsCallResolutionInfoToResolution { get; set; }
    }
}
