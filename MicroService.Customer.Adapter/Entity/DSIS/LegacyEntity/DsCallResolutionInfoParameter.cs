﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCallResolutionInfoParameter
    {
        public int CallResolutionInfoParameterId { get; set; }
        public int CallResolutionInfoId { get; set; }
        public string Parameter { get; set; }

        public virtual DsCallResolutionInfo CallResolutionInfo { get; set; }
    }
}
