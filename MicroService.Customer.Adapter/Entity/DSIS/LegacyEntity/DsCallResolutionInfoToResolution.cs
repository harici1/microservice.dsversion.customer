﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCallResolutionInfoToResolution
    {
        public int CallResolutionInfoToResolutionId { get; set; }
        public int CallResolutionInfoId { get; set; }
        public int ResolutionId { get; set; }
        public bool Disabled { get; set; }

        public virtual DsCallResolutionInfo CallResolutionInfo { get; set; }
        public virtual DsCozumTanimi Resolution { get; set; }
    }
}
