﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaign
    {
        public DsCampaign()
        {
            DsBluCampaign = new HashSet<DsBluCampaign>();
            DsCampaignToDistrict = new HashSet<DsCampaignToDistrict>();
            DsCampaignToPaymentType = new HashSet<DsCampaignToPaymentType>();
            DsCampaignToProductPrice = new HashSet<DsCampaignToProductPrice>();
            DsCampaignToSmartCard = new HashSet<DsCampaignToSmartCard>();
            DsCampaignToStbSku = new HashSet<DsCampaignToStbSku>();
            DsCouponToCampaign = new HashSet<DsCouponToCampaign>();
            DsOfferType = new HashSet<DsOfferType>();
            DsRoleTableConstraint = new HashSet<DsRoleTableConstraint>();
        }

        public int CampaignId { get; set; }
        public string Name { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public string SystemCode { get; set; }
        public string ShortCode { get; set; }
        public int? ParticipationCondition { get; set; }
        public int? ProductId { get; set; }
        public string CustomReturnMessage { get; set; }
        public string CustomCampaignEndMessage { get; set; }
        public bool? IsAutoPickOffer { get; set; }
        public string ClassName { get; set; }
        public int? ContentCategoryId { get; set; }
        public int? FormHeight { get; set; }
        public int? FormWidht { get; set; }
        public int? TvGamesSensitivityNumber { get; set; }
        public int? TvGamesContentStartNumber { get; set; }
        public int? TvGamesContentEndNumber { get; set; }
        public bool HasForm { get; set; }
        public string ShortCode2 { get; set; }
        public int? CampaignCategoryId { get; set; }
        public string CampaignCode1Label { get; set; }
        public string CampaignCode2Label { get; set; }
        public string NameField1Label { get; set; }
        public string NameField2Label { get; set; }
        public string NameField3Label { get; set; }
        public bool? IsChangeable { get; set; }
        public bool SaleIsCancelable { get; set; }
        public int? TmpRelatedCampaignId { get; set; }
        public int? CampaignCompensationCategoryId { get; set; }
        public bool CancelToken { get; set; }
        public bool ContractIsRequired { get; set; }
        public string ContractText111 { get; set; }
        public string ContractCampaignInfo { get; set; }
        public bool BonusConfirmationIsRequired { get; set; }
        public bool? IsSendStb { get; set; }
        public bool FixedAssetSale { get; set; }
        public int InvoiceDelayInMonth { get; set; }
        public bool? IsStampDuty { get; set; }
        public bool IsModemAvailable { get; set; }
        public bool IsBundle { get; set; }
        public int DelayInvoice { get; set; }
        public string Notes { get; set; }
        public bool IsTermination { get; set; }
        public int IsDiscountAvailable { get; set; }
        public bool IsptaransferDiscount { get; set; }
        public int? IsNoFinalInvoice { get; set; }
        public string ContractTextAdsl111 { get; set; }
        public int? ContractTextVersion { get; set; }
        public bool InitialPartialPriceBonus { get; set; }

        public virtual ICollection<DsBluCampaign> DsBluCampaign { get; set; }
        public virtual ICollection<DsCampaignToDistrict> DsCampaignToDistrict { get; set; }
        public virtual ICollection<DsCampaignToPaymentType> DsCampaignToPaymentType { get; set; }
        public virtual ICollection<DsCampaignToProductPrice> DsCampaignToProductPrice { get; set; }
        public virtual ICollection<DsCampaignToSmartCard> DsCampaignToSmartCard { get; set; }
        public virtual ICollection<DsCampaignToStbSku> DsCampaignToStbSku { get; set; }
        public virtual ICollection<DsCouponToCampaign> DsCouponToCampaign { get; set; }
        public virtual ICollection<DsOfferType> DsOfferType { get; set; }
        public virtual ICollection<DsRoleTableConstraint> DsRoleTableConstraint { get; set; }
    }
}
