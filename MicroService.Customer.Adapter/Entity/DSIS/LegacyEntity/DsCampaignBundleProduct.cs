﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignBundleProduct
    {
        public int CampaignBundleProductId { get; set; }
        public int CampaignId { get; set; }
        public int ProductId { get; set; }
        public int CampaignCodeId { get; set; }
        public int NumberOfInstallment { get; set; }
        public int VposId { get; set; }
        public decimal Price { get; set; }
        public string ProductCode { get; set; }
        public string TsvposCode { get; set; }
        public string TsvposName { get; set; }
    }
}
