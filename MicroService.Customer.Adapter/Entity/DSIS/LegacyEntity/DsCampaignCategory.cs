﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignCategory
    {
        public int CampaignCategoryId { get; set; }
        public string Name { get; set; }
    }
}
