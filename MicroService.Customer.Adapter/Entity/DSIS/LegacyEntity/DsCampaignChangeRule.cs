﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignChangeRule
    {
        public int CampaignChangeRuleId { get; set; }
        public int FromCampaignId { get; set; }
        public int ToCampaignId { get; set; }
    }
}
