﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignCode
    {
        public DsCampaignCode()
        {
            DsCampaignResponseAdslProduct = new HashSet<DsCampaignResponseAdsl>();
            DsCampaignResponseAdslSmileProduct = new HashSet<DsCampaignResponseAdsl>();
        }

        public int CampignCodeId { get; set; }
        public int CampaignCodeTypeId { get; set; }
        public string Name { get; set; }
        public int? CampaignId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Description { get; set; }
        public int? SortColumn { get; set; }
        public string ExternalSystemCode { get; set; }
        public int? AnkaCampaignId { get; set; }

        public virtual DsCampaignCodeType CampaignCodeType { get; set; }
        public virtual ICollection<DsCampaignResponseAdsl> DsCampaignResponseAdslProduct { get; set; }
        public virtual ICollection<DsCampaignResponseAdsl> DsCampaignResponseAdslSmileProduct { get; set; }
    }
}
