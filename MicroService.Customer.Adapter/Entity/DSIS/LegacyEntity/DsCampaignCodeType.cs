﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignCodeType
    {
        public DsCampaignCodeType()
        {
            DsCampaignCode = new HashSet<DsCampaignCode>();
        }

        public int CampaignCodeTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<DsCampaignCode> DsCampaignCode { get; set; }
    }
}
