﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignCompensationCategory
    {
        public int CampaignCompensationCategoryId { get; set; }
        public string Name { get; set; }
    }
}
