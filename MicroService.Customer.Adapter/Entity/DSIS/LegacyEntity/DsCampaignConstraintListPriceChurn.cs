﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignConstraintListPriceChurn
    {
        public int Id { get; set; }
        public int LeasingContractTypeId { get; set; }
        public bool StbOwner { get; set; }
        public string StbType { get; set; }
        public int ProductId { get; set; }
        public int CampaignId { get; set; }
        public int SmileCatalogItemId { get; set; }
        public int SmileCatalogItemCampaignId { get; set; }
        public double ListPrice { get; set; }
    }
}
