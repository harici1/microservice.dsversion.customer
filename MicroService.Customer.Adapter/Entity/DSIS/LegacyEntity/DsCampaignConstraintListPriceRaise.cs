﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignConstraintListPriceRaise
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int SmileCatalogItemId { get; set; }
        public decimal ListPrice { get; set; }
    }
}
