﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignConstraintRefreshContract
    {
        public int Id { get; set; }
        public int MenuId { get; set; }
        public int WorkOrderSubjectId { get; set; }
        public int LeasingContractTypeId { get; set; }
        public int OldStbTypeId { get; set; }
        public int ProductMainPackageId { get; set; }
        public decimal PriceMin { get; set; }
        public decimal PriceMax { get; set; }
        public int CampaignId { get; set; }
        public string Note { get; set; }
    }
}
