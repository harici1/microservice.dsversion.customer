﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignConstraintReplaceHd
    {
        public int Id { get; set; }
        public int MenuId { get; set; }
        public int WorkOrderSubjectId { get; set; }
        public string OldStbType { get; set; }
        public int ProductMainPackageId { get; set; }
        public double PriceMin { get; set; }
        public double PriceMax { get; set; }
        public int CampaignId { get; set; }
    }
}
