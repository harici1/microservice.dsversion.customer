﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignConstraintResetContract
    {
        public int Id { get; set; }
        public int MenuId { get; set; }
        public int LeasingContractTypeId { get; set; }
        public int? OldStbTypeId { get; set; }
        public int ProductMainPackageId { get; set; }
        public int SmileCatalogItemId { get; set; }
        public decimal ListPrice { get; set; }
        public int DsiscampaignId { get; set; }
        public bool IsActive { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
