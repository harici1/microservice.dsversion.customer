﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignConstraintRetentionCampaignPrePostMap
    {
        public int Id { get; set; }
        public int PostpaidCampaignId { get; set; }
        public int PrepaidCampaignId { get; set; }
        public int OwnerId { get; set; }
    }
}
