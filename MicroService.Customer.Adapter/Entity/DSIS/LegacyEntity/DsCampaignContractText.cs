﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignContractText
    {
        public int CampaignContractTextId { get; set; }
        public int ContractTextVersion { get; set; }
        public int LeasingContractTypeId { get; set; }
        public string Name { get; set; }
        public string Decription { get; set; }
        public string ContractText { get; set; }
        public bool Active { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public string LastUpdateByUser { get; set; }
    }
}
