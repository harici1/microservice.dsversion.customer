﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignFromStbSkuForReplaceHd
    {
        public int CampaignFromStbSkuForReplaceHdid { get; set; }
        public int CampaignId { get; set; }
        public int StbSkuId { get; set; }
    }
}
