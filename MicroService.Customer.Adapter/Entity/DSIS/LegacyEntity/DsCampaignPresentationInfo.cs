﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignPresentationInfo
    {
        public int CampaignPresentationInfoId { get; set; }
        public int GenericCampaignId { get; set; }
        public int CampaignGroupId { get; set; }
        public int? RankNumber { get; set; }
        public string Description { get; set; }
        public string CampaignSource { get; set; }
        public int? RelatedCampaignId { get; set; }
    }
}
