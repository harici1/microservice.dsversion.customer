﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignResponse
    {
        public DsCampaignResponse()
        {
            DsCampaignResponseDetail = new HashSet<DsCampaignResponseDetail>();
        }

        public int CampaignResponseId { get; set; }
        public int CampaignId { get; set; }
        public int PartyId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? DocumentId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? OfferedById { get; set; }
        public int? CampaignCode1 { get; set; }
        public int? CampaignCode2 { get; set; }
        public string NameField1 { get; set; }
        public string NameField2 { get; set; }
        public bool? IsPostEventsProccessed { get; set; }
        public int? PartyRequestId { get; set; }
        public string NameField3 { get; set; }
        public int? AdslTypeId { get; set; }
        public string SocialIdentificationNumber { get; set; }
        public string AdslAddress { get; set; }
        public string IspName { get; set; }
        public string XdslNo { get; set; }
        public int? AnkaAdslPlanId { get; set; }
        public string SmileCampaignName { get; set; }
        public string SmileProductName { get; set; }
        public int? ActivationId { get; set; }

        public virtual ICollection<DsCampaignResponseDetail> DsCampaignResponseDetail { get; set; }
    }
}
