﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignResponseAdsl
    {
        public int CampaignResponseAdslId { get; set; }
        public int ProductId { get; set; }
        public int? SmileProductId { get; set; }
        public int DeliveryProvinceId { get; set; }
        public int InvoiceProvinceId { get; set; }
        public int DeliveryDistrictId { get; set; }
        public int InvoiceDistrictId { get; set; }
        public int? SecurityQuestId { get; set; }
        public string SecurityAnswer { get; set; }
        public string AdslPhoneNumber { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string MotherFirstName { get; set; }
        public int? PaymentTypeId { get; set; }
        public int? InstalmentTypeId { get; set; }
        public string InvoiceAddress { get; set; }
        public int? PreviousAdslOperatorId { get; set; }
        public string DeliveryAddress { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? CreationDate { get; set; }
        public string AdslPortRegistrationWebServiceResult { get; set; }
        public string AnkaPhoneRegistrationWebServiceResult { get; set; }
        public string UserNameRegistrationWebServiceResult { get; set; }
        public int? SmartCardId { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public bool? InstalmentIsRequired { get; set; }
        public bool? InvoiceAddressIsSameDeliveryAddress { get; set; }

        public virtual DsCampaignCode Product { get; set; }
        public virtual DsCampaignCode SmileProduct { get; set; }
    }
}
