﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignResponseAdslChangeLog
    {
        public int CampaignResponseAdslChangeLogId { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? CreationDate { get; set; }
        public int CampaignResponseAdslChangeLogTypeId { get; set; }
        public int PaymentTypeId { get; set; }
        public int? CampaignResponseId { get; set; }
    }
}
