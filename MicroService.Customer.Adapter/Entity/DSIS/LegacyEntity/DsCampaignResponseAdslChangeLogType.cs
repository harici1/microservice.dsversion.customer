﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignResponseAdslChangeLogType
    {
        public int CampaignResponseAdslChangeLogTypeId { get; set; }
        public string Name { get; set; }
    }
}
