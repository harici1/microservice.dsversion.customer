﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignResponseDetail
    {
        public int CampaignResponseDetailId { get; set; }
        public int CampaignResponseId { get; set; }
        public int CampaignResponseDetailTypeId { get; set; }
        public string Value { get; set; }

        public virtual DsCampaignResponse CampaignResponse { get; set; }
        public virtual DsCampaignResponseDetailType CampaignResponseDetailType { get; set; }
    }
}
