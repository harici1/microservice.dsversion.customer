﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignResponseDetailType
    {
        public DsCampaignResponseDetailType()
        {
            DsCampaignResponseDetail = new HashSet<DsCampaignResponseDetail>();
        }

        public int CampaignResponseDetailTypeId { get; set; }
        public string Name { get; set; }
        public int SystemCode { get; set; }

        public virtual ICollection<DsCampaignResponseDetail> DsCampaignResponseDetail { get; set; }
    }
}
