﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignSupplementaryDocumentDetail
    {
        public int CampaignSupplementaryDocumentDetailId { get; set; }
        public int CampaignId { get; set; }
        public int StbTypeId { get; set; }
        public int ProductId { get; set; }
        public decimal Price { get; set; }
        public decimal? MonthlyPrice { get; set; }
        public string Description { get; set; }
        public string MonthlyDescription { get; set; }
    }
}
