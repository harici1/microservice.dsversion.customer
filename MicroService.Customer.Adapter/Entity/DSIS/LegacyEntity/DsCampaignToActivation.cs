﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignToActivation
    {
        public int CampaignToActivationId { get; set; }
        public int CampaignId { get; set; }
        public int ActivationId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public bool? IsActive { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? CancelledByDocumentTypeId { get; set; }
        public int? CancelledByDocumentId { get; set; }
        public int? CancelledById { get; set; }
        public DateTime? CancelDate { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? DocumentId { get; set; }
    }
}
