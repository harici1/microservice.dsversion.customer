﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignToAddonProduct
    {
        public int CampaignToAddonProductId { get; set; }
        public int CampaignId { get; set; }
        public int ForcedProductId { get; set; }
        public int SelectedProductId { get; set; }
        public bool Disabled { get; set; }
    }
}
