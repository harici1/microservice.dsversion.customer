﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignToAdslCampaign
    {
        public int CampaignToAdslCampaignId { get; set; }
        public int DsmartCampaignId { get; set; }
        public int SmileCatalogItemId { get; set; }
        public int SmileCatalogItemCampaignId { get; set; }
        public bool V1fiber { get; set; }
        public bool T11fiber { get; set; }
        public bool SuperOnlineFiber { get; set; }
        public bool? VisibleToNewSale { get; set; }
        public bool ForbidToForeignCustomer { get; set; }
        public int? BandwidthLowerLimit { get; set; }
        public int? BandwidthUpperLimit { get; set; }
    }
}
