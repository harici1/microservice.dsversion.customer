﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignToDistrict
    {
        public int CampaignToDistrictId { get; set; }
        public int CampaignId { get; set; }
        public int DistrictId { get; set; }

        public virtual DsCampaign Campaign { get; set; }
    }
}
