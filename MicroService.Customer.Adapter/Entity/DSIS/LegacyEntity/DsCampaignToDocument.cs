﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignToDocument
    {
        public int CampaignToDocumentId { get; set; }
        public int CampaignId { get; set; }
        public string DocumentForm { get; set; }
    }
}
