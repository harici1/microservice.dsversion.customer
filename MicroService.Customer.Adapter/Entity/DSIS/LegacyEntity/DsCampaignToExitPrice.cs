﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignToExitPrice
    {
        public int CampaignToExitPriceId { get; set; }
        public int CampaignId { get; set; }
        public int ProductId { get; set; }
        public decimal Price { get; set; }
        public int? StbTypeId { get; set; }

        public virtual DsProduct Product { get; set; }
        public virtual DsStbType StbType { get; set; }
    }
}
