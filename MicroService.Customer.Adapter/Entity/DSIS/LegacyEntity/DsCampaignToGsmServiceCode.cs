﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignToGsmServiceCode
    {
        public int CampaignToGsmServiceCodeId { get; set; }
        public int CampaignId { get; set; }
        public string GsmServiceCode { get; set; }
    }
}
