﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignToPaymentType
    {
        public int CampaignToPaymentTypeId { get; set; }
        public int CampaignId { get; set; }
        public int PaymentTypeId { get; set; }

        public virtual DsCampaign Campaign { get; set; }
        public virtual DsPaymentType PaymentType { get; set; }
    }
}
