﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignToProductPrice
    {
        public int CampaignToProductPriceId { get; set; }
        public int CampaignId { get; set; }
        public int ProductPriceId { get; set; }

        public virtual DsCampaign Campaign { get; set; }
        public virtual DsProductPrice ProductPrice { get; set; }
    }
}
