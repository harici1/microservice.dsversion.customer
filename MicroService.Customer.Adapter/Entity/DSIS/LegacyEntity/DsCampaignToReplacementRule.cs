﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignToReplacementRule
    {
        public int CampaignToReplacementRuleId { get; set; }
        public string Description { get; set; }
        public int CampaignId { get; set; }
        public int MenuId { get; set; }
        public bool StbPriceExist { get; set; }
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
        public int WorkOrderSubjectId { get; set; }
        public int StbTypeId { get; set; }
    }
}
