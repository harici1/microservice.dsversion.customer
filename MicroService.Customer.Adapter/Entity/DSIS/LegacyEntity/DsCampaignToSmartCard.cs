﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignToSmartCard
    {
        public int CampaignToSmartCardId { get; set; }
        public int CampaignId { get; set; }
        public int? PartyId { get; set; }
        public int? SmartCardId { get; set; }
        public int? StbId { get; set; }
        public bool IsAuthorized { get; set; }
        public bool IsCancelled { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? DocumentId { get; set; }

        public virtual DsCampaign Campaign { get; set; }
        public virtual DsSmartCard SmartCard { get; set; }
        public virtual DsStb Stb { get; set; }
    }
}
