﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignToStbSku
    {
        public int CampaignToStbSkuId { get; set; }
        public int CampaignId { get; set; }
        public int StbSkuId { get; set; }

        public virtual DsCampaign Campaign { get; set; }
    }
}
