﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCampaignValidationRule
    {
        public int CampaignValidationRuleId { get; set; }
        public int CampaignId { get; set; }
        public string FieldType { get; set; }
        public string FieldLabel { get; set; }
        public string FieldDataSource { get; set; }
        public string ValidationSp { get; set; }
        public bool Disabled { get; set; }
    }
}
