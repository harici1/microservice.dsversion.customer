﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCellCallback
    {
        public int CellCallbackId { get; set; }
        public int CellCallbackTypeId { get; set; }
        public int CellCallbackStatusId { get; set; }
        public string RequestJson { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? LastProcessDate { get; set; }
    }
}
