﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCellCallbackStatus
    {
        public int CellCallbackStatusId { get; set; }
        public string Name { get; set; }
    }
}
