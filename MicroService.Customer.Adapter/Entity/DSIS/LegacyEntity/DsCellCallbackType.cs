﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCellCallbackType
    {
        public int CellCallbackTypeId { get; set; }
        public string Name { get; set; }
    }
}
