﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCellSaleDiscountRule
    {
        public int CellSaleDiscountRuleId { get; set; }
        public int ProductId { get; set; }
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
        public decimal Discount { get; set; }
        public int LeasingContractTypeId { get; set; }
    }
}
