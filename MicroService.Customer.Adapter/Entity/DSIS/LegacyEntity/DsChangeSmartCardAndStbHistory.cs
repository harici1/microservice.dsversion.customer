﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsChangeSmartCardAndStbHistory
    {
        public int ChangeSmartCardAndStbHistoryId { get; set; }
        public int? WorkOrderId { get; set; }
        public int OldStbId { get; set; }
        public int OldSmartCardId { get; set; }
        public int NewStbId { get; set; }
        public int NewSmartCardId { get; set; }
        public int CustomerId { get; set; }
        public int ActivationId { get; set; }
        public int? ChangeSmartCardAndStbReasonId { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
    }
}
