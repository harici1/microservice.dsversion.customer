﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsChangeSmartCardAndStbReason
    {
        public int ChangeSmartCardAndStbReasonId { get; set; }
        public string Name { get; set; }
    }
}
