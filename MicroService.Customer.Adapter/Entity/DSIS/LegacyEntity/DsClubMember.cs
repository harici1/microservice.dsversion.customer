﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsClubMember
    {
        public int ClubMemberId { get; set; }
        public string ClubMemberCode { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string FamilyName { get; set; }
        public DateTime BirthDate { get; set; }
        public int GenderTypeId { get; set; }
        public int NumberOfSiblings { get; set; }
        public int? FootballTeamId { get; set; }
        public string Hobbies { get; set; }
        public string Address { get; set; }
        public int ProvinceId { get; set; }
        public string DistrictName { get; set; }
        public string SchoolName { get; set; }
        public string Email { get; set; }
        public string ParentFullName { get; set; }
        public string ParentPhoneNumber { get; set; }
        public string ParentEmail { get; set; }
        public int? CustomerId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }

        public virtual DsGenderType GenderType { get; set; }
    }
}
