﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCommercialActivationToDealer
    {
        public int CommercialActivationToDealerId { get; set; }
        public int ActivationId { get; set; }
        public int DealerId { get; set; }
    }
}
