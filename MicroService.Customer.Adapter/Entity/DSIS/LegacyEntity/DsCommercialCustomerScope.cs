﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCommercialCustomerScope
    {
        public DsCommercialCustomerScope()
        {
            DsMusteriPotansiyel = new HashSet<DsMusteriPotansiyel>();
        }

        public int CommercialCustomerScopeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int OrderNum { get; set; }
        public bool Visible { get; set; }

        public virtual ICollection<DsMusteriPotansiyel> DsMusteriPotansiyel { get; set; }
    }
}
