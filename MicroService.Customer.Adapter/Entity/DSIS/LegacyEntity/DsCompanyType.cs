﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCompanyType
    {
        public int CompanyTypeId { get; set; }
        public string Name { get; set; }
    }
}
