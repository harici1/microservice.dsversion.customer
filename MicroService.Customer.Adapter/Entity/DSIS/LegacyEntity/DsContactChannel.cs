﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsContactChannel
    {
        public DsContactChannel()
        {
            DsAccContactChannel = new HashSet<DsAccContactChannel>();
            DsAuthenticationLog = new HashSet<DsAuthenticationLog>();
            DsOfferOrder = new HashSet<DsOfferOrder>();
            DsPopup = new HashSet<DsPopup>();
            DsSocialIdentificationRegistrationOrder = new HashSet<DsSocialIdentificationRegistrationOrder>();
            DsTransactionLog = new HashSet<DsTransactionLog>();
        }

        public int ContactChannelId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsMoneyOrder { get; set; }
        public bool? IsMailOrder { get; set; }

        public virtual DsPasswordPolicy DsPasswordPolicy { get; set; }
        public virtual ICollection<DsAccContactChannel> DsAccContactChannel { get; set; }
        public virtual ICollection<DsAuthenticationLog> DsAuthenticationLog { get; set; }
        public virtual ICollection<DsOfferOrder> DsOfferOrder { get; set; }
        public virtual ICollection<DsPopup> DsPopup { get; set; }
        public virtual ICollection<DsSocialIdentificationRegistrationOrder> DsSocialIdentificationRegistrationOrder { get; set; }
        public virtual ICollection<DsTransactionLog> DsTransactionLog { get; set; }
    }
}
