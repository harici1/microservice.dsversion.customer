﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsContentProduct
    {
        public DsContentProduct()
        {
            DsBasicPacketToCustomerType = new HashSet<DsBasicPacketToCustomerType>();
            DsContentProductActivation = new HashSet<DsContentProductActivation>();
            DsContentProductToStbModel = new HashSet<DsContentProductToStbModel>();
        }

        public int ContentProductId { get; set; }
        public string Name { get; set; }
        public int ServiceId { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public bool IsPinCodeRequired { get; set; }
        public string Description { get; set; }

        public virtual DsService Service { get; set; }
        public virtual ICollection<DsBasicPacketToCustomerType> DsBasicPacketToCustomerType { get; set; }
        public virtual ICollection<DsContentProductActivation> DsContentProductActivation { get; set; }
        public virtual ICollection<DsContentProductToStbModel> DsContentProductToStbModel { get; set; }
    }
}
