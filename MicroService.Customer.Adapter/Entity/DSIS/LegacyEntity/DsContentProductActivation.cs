﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsContentProductActivation
    {
        public int ContentProductActivationId { get; set; }
        public int ActivationId { get; set; }
        public int ContentProductId { get; set; }
        public bool IsAuthorized { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }

        public virtual DsContentProduct ContentProduct { get; set; }
    }
}
