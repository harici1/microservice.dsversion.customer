﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsContentProductActivationLogType
    {
        public int ContentProductActivationLogTypeId { get; set; }
        public string Name { get; set; }
    }
}
