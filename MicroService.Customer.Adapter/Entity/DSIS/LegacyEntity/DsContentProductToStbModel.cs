﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsContentProductToStbModel
    {
        public int ContentProductToStbModelId { get; set; }
        public int ContentProductId { get; set; }
        public int StbModelId { get; set; }
        public int StbTypeId { get; set; }

        public virtual DsContentProduct ContentProduct { get; set; }
        public virtual DsStbmodel StbModel { get; set; }
    }
}
