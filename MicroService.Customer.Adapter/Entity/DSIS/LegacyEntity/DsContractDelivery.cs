﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsContractDelivery
    {
        public DsContractDelivery()
        {
            DsContractDeliveryDetail = new HashSet<DsContractDeliveryDetail>();
        }

        public int ContractDeliveryId { get; set; }
        public string DeliveryNo { get; set; }
        public int ContractDeliveryStatusId { get; set; }
        public int LeasingContractId { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? LastUpdateById { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }

        public virtual DsContractDeliveryStatus ContractDeliveryStatus { get; set; }
        public virtual ICollection<DsContractDeliveryDetail> DsContractDeliveryDetail { get; set; }
    }
}
