﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsContractDeliveryDetail
    {
        public int ContractDeliveryDetailId { get; set; }
        public int ContractDeliveryId { get; set; }
        public int ContractDeliveryTypeId { get; set; }
        public int ContractDeliveryDetailStatusId { get; set; }
        public int LeasingContractServiceId { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public string LastUpdateById { get; set; }
        public DateTime? LastUpdateDate { get; set; }

        public virtual DsContractDelivery ContractDelivery { get; set; }
        public virtual DsContractDeliveryDetailStatus ContractDeliveryDetailStatus { get; set; }
        public virtual DsContractDeliveryType ContractDeliveryType { get; set; }
    }
}
