﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsContractDeliveryDetailStatus
    {
        public DsContractDeliveryDetailStatus()
        {
            DsContractDeliveryDetail = new HashSet<DsContractDeliveryDetail>();
        }

        public int ContractDeliveryDetailStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsContractDeliveryDetail> DsContractDeliveryDetail { get; set; }
    }
}
