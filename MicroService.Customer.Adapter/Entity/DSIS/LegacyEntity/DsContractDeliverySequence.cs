﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsContractDeliverySequence
    {
        public int ContractDeliverySequenceId { get; set; }
    }
}
