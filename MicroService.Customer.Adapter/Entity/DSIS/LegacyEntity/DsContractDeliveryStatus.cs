﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsContractDeliveryStatus
    {
        public DsContractDeliveryStatus()
        {
            DsContractDelivery = new HashSet<DsContractDelivery>();
        }

        public int ContractDeliveryStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsContractDelivery> DsContractDelivery { get; set; }
    }
}
