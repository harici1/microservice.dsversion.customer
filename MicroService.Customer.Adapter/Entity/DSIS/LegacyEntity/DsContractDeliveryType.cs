﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsContractDeliveryType
    {
        public DsContractDeliveryType()
        {
            DsContractDeliveryDetail = new HashSet<DsContractDeliveryDetail>();
        }

        public int ContractDeliveryTypeId { get; set; }
        public string Name { get; set; }
        public int? LeasingContractServiceTypeId { get; set; }

        public virtual ICollection<DsContractDeliveryDetail> DsContractDeliveryDetail { get; set; }
    }
}
