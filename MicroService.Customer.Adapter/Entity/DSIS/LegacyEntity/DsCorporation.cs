﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCorporation
    {
        public DsCorporation()
        {
            DsBankAccount = new HashSet<DsBankAccount>();
            DsExpenseBill = new HashSet<DsExpenseBill>();
            DsSaleCompensation = new HashSet<DsSaleCompensation>();
            DsVposServiceProvider = new HashSet<DsVposServiceProvider>();
        }

        public int CorporationId { get; set; }
        public int OrganizationId { get; set; }
        public string Name { get; set; }
        public string InvoiceTitle { get; set; }
        public string InvoiceAddress { get; set; }
        public string InvoiceDeliveryAddress { get; set; }
        public string InvoiceTaxOffice { get; set; }
        public string InvoiceTaxNumber { get; set; }
        public bool IsInvoiceCorporation { get; set; }

        public virtual ICollection<DsBankAccount> DsBankAccount { get; set; }
        public virtual ICollection<DsExpenseBill> DsExpenseBill { get; set; }
        public virtual ICollection<DsSaleCompensation> DsSaleCompensation { get; set; }
        public virtual ICollection<DsVposServiceProvider> DsVposServiceProvider { get; set; }
    }
}
