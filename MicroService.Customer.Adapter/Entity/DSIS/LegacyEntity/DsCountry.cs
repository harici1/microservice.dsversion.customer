﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCountry
    {
        public DsCountry()
        {
            DsCountryToCustomerType = new HashSet<DsCountryToCustomerType>();
        }

        public int CountryId { get; set; }
        public string CountryCode { get; set; }
        public string Name { get; set; }
        public int PhoneCode { get; set; }
        public string Temp { get; set; }

        public virtual ICollection<DsCountryToCustomerType> DsCountryToCustomerType { get; set; }
    }
}
