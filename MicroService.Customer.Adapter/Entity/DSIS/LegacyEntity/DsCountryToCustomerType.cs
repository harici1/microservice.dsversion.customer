﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCountryToCustomerType
    {
        public int CountryId { get; set; }
        public int CustomerTypeId { get; set; }

        public virtual DsCountry Country { get; set; }
        public virtual DsCustomerType CustomerType { get; set; }
    }
}
