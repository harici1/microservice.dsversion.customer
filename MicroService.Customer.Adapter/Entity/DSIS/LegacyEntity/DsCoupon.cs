﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCoupon
    {
        public int CouponId { get; set; }
        public int CouponTypeId { get; set; }
        public string CouponCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Active { get; set; }
        public bool? IsAuthorized { get; set; }
        public DateTime CreationDate { get; set; }
        public string Note { get; set; }
        public int NumberOfUsage { get; set; }

        public virtual DsCouponType CouponType { get; set; }
    }
}
