﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCouponClassType
    {
        public int CouponClassTypeId { get; set; }
        public string Name { get; set; }
    }
}
