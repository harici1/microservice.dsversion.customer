﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCouponLog
    {
        public int CouponLogId { get; set; }
        public int CouponLogTypeId { get; set; }
        public int CouponId { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? PartyId { get; set; }
    }
}
