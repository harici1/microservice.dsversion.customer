﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCouponToCampaign
    {
        public int CouponToCampaignId { get; set; }
        public int CouponId { get; set; }
        public int CampaignId { get; set; }

        public virtual DsCampaign Campaign { get; set; }
    }
}
