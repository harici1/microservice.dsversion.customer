﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCouponType
    {
        public DsCouponType()
        {
            DsCoupon = new HashSet<DsCoupon>();
        }

        public int CouponTypeId { get; set; }
        public string Name { get; set; }
        public int CouponClassTypeId { get; set; }
        public string Value { get; set; }
        public int Period { get; set; }
        public bool? IsActive { get; set; }

        public virtual ICollection<DsCoupon> DsCoupon { get; set; }
    }
}
