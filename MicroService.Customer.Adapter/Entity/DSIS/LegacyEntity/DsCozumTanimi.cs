﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCozumTanimi
    {
        public DsCozumTanimi()
        {
            DsCallResolutionInfoToResolution = new HashSet<DsCallResolutionInfoToResolution>();
        }

        public int SorunId { get; set; }
        public int CozumId { get; set; }
        public string Tanimi { get; set; }
        public string Aciklama { get; set; }
        public int? OlusturanKullanici { get; set; }
        public DateTime? OlusturmaTarihi { get; set; }
        public int? DuzeltenKullanici { get; set; }
        public DateTime? DuzeltmeTarihi { get; set; }
        public byte? Durumu { get; set; }
        public int CagriIslemTipiId { get; set; }
        public int CagriYonlendirmeGrupId { get; set; }
        public int CagriYonlendirmeGrup2Id { get; set; }
        public bool EkNot { get; set; }
        public string GridColor { get; set; }
        public string GridFontStyle { get; set; }

        public virtual DsSorunTanimi Sorun { get; set; }
        public virtual ICollection<DsCallResolutionInfoToResolution> DsCallResolutionInfoToResolution { get; set; }
    }
}
