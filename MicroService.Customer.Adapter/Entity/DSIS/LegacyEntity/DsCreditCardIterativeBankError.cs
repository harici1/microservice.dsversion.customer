﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCreditCardIterativeBankError
    {
        public int CreditCardIterativeBankErrorId { get; set; }
        public string ErrorCode { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}
