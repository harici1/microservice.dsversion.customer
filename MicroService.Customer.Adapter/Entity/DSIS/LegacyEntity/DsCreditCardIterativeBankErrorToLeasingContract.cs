﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCreditCardIterativeBankErrorToLeasingContract
    {
        public int CreditCardIterativeBankErrorToLeasingContractId { get; set; }
        public int LeasingContractId { get; set; }
        public string ErrorCode { get; set; }
        public string CardNumber { get; set; }
    }
}
