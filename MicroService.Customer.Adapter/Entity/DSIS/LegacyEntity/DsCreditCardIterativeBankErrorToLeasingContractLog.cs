﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCreditCardIterativeBankErrorToLeasingContractLog
    {
        public int CreditCardIterativeBankErrorToLeasingContractLogId { get; set; }
        public int LeasingContractId { get; set; }
        public string ErrorCode { get; set; }
        public string CardNumber { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
