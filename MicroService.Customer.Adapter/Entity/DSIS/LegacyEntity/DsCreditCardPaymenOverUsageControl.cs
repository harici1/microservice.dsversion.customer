﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCreditCardPaymenOverUsageControl
    {
        public int CreditCardPaymenOverUsageControlId { get; set; }
        public int CreditCardPaymentId { get; set; }
        public int ContactChannelId { get; set; }
        public int VposProviderId { get; set; }
        public string MaskedCardNumber { get; set; }
        public long MaskedCardNumberWithOutXs { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
