﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCreditCardPayment
    {
        public int CreditCardPaymentId { get; set; }
        public int VposServiceProviderId { get; set; }
        public string ChargeType { get; set; }
        public decimal Amount { get; set; }
        public string OrderNumber { get; set; }
        public string GroupId { get; set; }
        public string TransId { get; set; }
        public string ProvisionCode { get; set; }
        public string ReferenceNumber { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public decimal? ServiceCostAmount { get; set; }
        public int? NumberOfInstallments { get; set; }
        public decimal? InterestCost { get; set; }
        public bool? IsAccountingOk { get; set; }
        public decimal? DiscountAmount { get; set; }
        public int? LeasingContractId { get; set; }
        public bool? Old { get; set; }
        public byte[] CardNumberEncrypt { get; set; }
        public byte[] ExpirationMonthEncrypt { get; set; }
        public byte[] ExpirationYearEncrypt { get; set; }
        public byte[] BnameEncrypt { get; set; }
        public string IpAddress { get; set; }
    }
}
