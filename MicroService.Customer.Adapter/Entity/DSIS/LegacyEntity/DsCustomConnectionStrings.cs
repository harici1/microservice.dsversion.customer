﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomConnectionStrings
    {
        public int CustomConnectionStringId { get; set; }
        public string ConnectionStringKey { get; set; }
        public string Description { get; set; }
        public int? ConnectionStringType { get; set; }
        public byte[] ConnectionString { get; set; }
        public byte[] UserName { get; set; }
        public byte[] Password { get; set; }
        public bool? IsActive { get; set; }
    }
}
