﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerAdditionalInfoType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
