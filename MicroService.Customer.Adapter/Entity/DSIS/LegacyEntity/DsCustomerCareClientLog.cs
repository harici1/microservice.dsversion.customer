﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerCareClientLog
    {
        public int CustomerCareClientLogId { get; set; }
        public string Ip { get; set; }
        public string CalledMethodName { get; set; }
        public DateTime? CreationDate { get; set; }
    }
}
