﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerChangeLog
    {
        public int CustomerChangeLogId { get; set; }
        public int? CustomerId { get; set; }
        public int? TypeId { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? CreatedById { get; set; }
        public string OldValues { get; set; }
        public string NewValues { get; set; }
        public int? OrderId { get; set; }

        public virtual DsCustomerChangeLogType Type { get; set; }
    }
}
