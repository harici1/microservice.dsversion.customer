﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerChangeLogType
    {
        public DsCustomerChangeLogType()
        {
            DsCustomerChangeLog = new HashSet<DsCustomerChangeLog>();
        }

        public int CustomerChangeLogTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<DsCustomerChangeLog> DsCustomerChangeLog { get; set; }
    }
}
