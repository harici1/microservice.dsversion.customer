﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerCharacteristicValue
    {
        public int CustomerCharacteristicValueId { get; set; }
        public int CustomerSpecificationCharacteristicId { get; set; }
        public int CustomerId { get; set; }
        public string Value { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public int LastUpdatedById { get; set; }
        public int CustomerCharacteristicValueStatusId { get; set; }

        public virtual DsMusteri Customer { get; set; }
        public virtual DsCustomerCharacteristicValueStatus CustomerCharacteristicValueStatus { get; set; }
        public virtual DsCustomerSpecificationCharacteristic CustomerSpecificationCharacteristic { get; set; }
    }
}
