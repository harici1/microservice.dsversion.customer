﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerCharacteristicValueStatus
    {
        public DsCustomerCharacteristicValueStatus()
        {
            DsCustomerCharacteristicValue = new HashSet<DsCustomerCharacteristicValue>();
        }

        public int CustomerCharacteristicValueStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsCustomerCharacteristicValue> DsCustomerCharacteristicValue { get; set; }
    }
}
