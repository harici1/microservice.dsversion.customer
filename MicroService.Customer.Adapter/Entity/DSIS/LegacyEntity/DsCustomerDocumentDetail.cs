﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerDocumentDetail
    {
        public int CustomerDocumentDetailId { get; set; }
        public int CustomerDocumentId { get; set; }
        public int CustomerDocumentDetailTypeId { get; set; }
        public string Value { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdateById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
    }
}
