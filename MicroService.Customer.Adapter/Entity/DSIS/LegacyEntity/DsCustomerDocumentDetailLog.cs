﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerDocumentDetailLog
    {
        public int CustomerDocumentDetailLogId { get; set; }
        public int CustomerDocumentDetailId { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
    }
}
