﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerDocumentDetailType
    {
        public int CustomerDocumentDetailTypeId { get; set; }
        public int CustomerDocumentTypeId { get; set; }
        public string Name { get; set; }
        public string DefaultValue { get; set; }
    }
}
