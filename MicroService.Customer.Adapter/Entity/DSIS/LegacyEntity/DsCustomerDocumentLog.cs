﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerDocumentLog
    {
        public int CustomerDocumentLogId { get; set; }
        public int CustomerDocumentId { get; set; }
        public int CustomerDocumentTypeId { get; set; }
        public int CustomerDocumentLogTypeId { get; set; }
        public int ActivationId { get; set; }
        public int? LeasingContractServiceId { get; set; }
        public int CampaignId { get; set; }
        public int CustomerDocumentStatusId { get; set; }
        public bool? SignatureIsOk { get; set; }
        public string BacodeNumber { get; set; }
        public bool? IdentificationDocumentIsOk { get; set; }
        public bool? PaymentInstructionIsOk { get; set; }
        public bool? ContractDocumentIsOk { get; set; }
        public bool? OtherDocumentIsOk { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public string Note { get; set; }
        public int CustomerDocumentArchiveStatusId { get; set; }
        public int DocumentId { get; set; }
        public int? ResponsableDealerId { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public bool WaitForProcess { get; set; }
        public int? WaitingFromDealerId { get; set; }
        public bool IsScanned { get; set; }
    }
}
