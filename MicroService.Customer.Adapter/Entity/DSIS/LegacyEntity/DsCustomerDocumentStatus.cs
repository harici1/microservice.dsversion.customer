﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerDocumentStatus
    {
        public int CustomerDocumentStatusId { get; set; }
        public string Name { get; set; }
    }
}
