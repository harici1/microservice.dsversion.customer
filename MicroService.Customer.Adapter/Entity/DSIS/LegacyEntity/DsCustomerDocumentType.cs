﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerDocumentType
    {
        public int CustomerDocumentTypeId { get; set; }
        public string Name { get; set; }
        public bool? Updateable { get; set; }
        public bool IsVisible { get; set; }
        public bool IsDefault { get; set; }
        public bool Disabled { get; set; }
    }
}
