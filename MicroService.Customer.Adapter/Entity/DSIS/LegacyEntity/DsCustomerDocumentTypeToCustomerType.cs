﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerDocumentTypeToCustomerType
    {
        public int CustomerDocumentTypeToCustomerTypeId { get; set; }
        public int CustomerDocumentTypeId { get; set; }
        public int CustomerTypeId { get; set; }
    }
}
