﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerIdentificationType
    {
        public int CustomerIdentificationTypeId { get; set; }
        public string Name { get; set; }
        public bool ForceTaxOffice { get; set; }
    }
}
