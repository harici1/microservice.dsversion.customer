﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerInfoSmstext
    {
        public int CustomerInfoSmstextId { get; set; }
        public string Name { get; set; }
        public string StoredProcedure { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? CreationDate { get; set; }
    }
}
