﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerPrivacyBlockList
    {
        public int CustomerPrivacyBlockListId { get; set; }
        public int CustomerPrivacyBlockListTypeId { get; set; }
        public int? CustomerId { get; set; }
        public string BlockedChannel { get; set; }
        public bool IsActive { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? DocumentId { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? CustomerPrivacyBlockListReasonId { get; set; }
    }
}
