﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerPrivacyBlockListReason
    {
        public int CustomerPrivacyBlockListReasonId { get; set; }
        public string Name { get; set; }
        public bool IsVisible { get; set; }
    }
}
