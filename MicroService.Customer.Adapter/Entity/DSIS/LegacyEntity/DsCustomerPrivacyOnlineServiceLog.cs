﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerPrivacyOnlineServiceLog
    {
        public int CustomerPrivacyOnlineServiceLogId { get; set; }
        public int CustomerPrivacyOnlineServiceLogTypeId { get; set; }
        public string CustomerPrivacyOnlineServiceLog { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
