﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerPrivacyOnlineServiceLogType
    {
        public int CustomerPrivacyOnlineServiceLogTypeId { get; set; }
        public string CustomerPrivacyOnlineServiceLogTypt { get; set; }
    }
}
