﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerProfileInfo
    {
        public int CustomerProfileInfoId { get; set; }
        public int CustomerId { get; set; }
        public int CustomerProfileInfoTypeId { get; set; }
        public string Value { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
        public DateTime ExpriyDate { get; set; }
    }
}
