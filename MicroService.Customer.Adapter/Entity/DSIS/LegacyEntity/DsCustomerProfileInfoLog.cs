﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerProfileInfoLog
    {
        public int CustomerProfileInfoLogId { get; set; }
        public int CustomerId { get; set; }
        public int CustomerProfileInfoTypeId { get; set; }
        public string Value { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public int DocumentId { get; set; }
        public int DocumentTypeId { get; set; }
    }
}
