﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerRecordTag
    {
        public int CustomerRecordTagId { get; set; }
        public int? CustomerId { get; set; }
        public int? TypeId { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? CreatedById { get; set; }
        public string Description { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? StatusId { get; set; }
        public string XmlMessage { get; set; }

        public virtual DsCustomerRecordTagStatus Status { get; set; }
        public virtual DsCustomerRecordTagType Type { get; set; }
    }
}
