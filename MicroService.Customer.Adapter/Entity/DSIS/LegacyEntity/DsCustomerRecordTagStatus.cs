﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerRecordTagStatus
    {
        public DsCustomerRecordTagStatus()
        {
            DsCustomerRecordTag = new HashSet<DsCustomerRecordTag>();
        }

        public int CustomerRecordTagStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsCustomerRecordTag> DsCustomerRecordTag { get; set; }
    }
}
