﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerRecordTagType
    {
        public DsCustomerRecordTagType()
        {
            DsCustomerRecordTag = new HashSet<DsCustomerRecordTag>();
        }

        public int CustomerRecordTagTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Template { get; set; }
        public string ClassName { get; set; }

        public virtual ICollection<DsCustomerRecordTag> DsCustomerRecordTag { get; set; }
    }
}
