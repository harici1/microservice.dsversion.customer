﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerSpecificationCharacteristic
    {
        public DsCustomerSpecificationCharacteristic()
        {
            DsCustomerCharacteristicValue = new HashSet<DsCustomerCharacteristicValue>();
        }

        public int CustomerSpecificationCharacteristicId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsCustomerCharacteristicValue> DsCustomerCharacteristicValue { get; set; }
    }
}
