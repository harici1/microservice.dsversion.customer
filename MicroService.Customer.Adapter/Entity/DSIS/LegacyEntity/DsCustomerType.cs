﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerType
    {
        public DsCustomerType()
        {
            DsBasicPacketToCustomerType = new HashSet<DsBasicPacketToCustomerType>();
            DsBouquetToCustomerType = new HashSet<DsBouquetToCustomerType>();
            DsCountryToCustomerType = new HashSet<DsCountryToCustomerType>();
            DsRoleTableConstraint = new HashSet<DsRoleTableConstraint>();
            DsStbActivationUsingRule = new HashSet<DsStbActivationUsingRule>();
            DsStbTypeToProduct = new HashSet<DsStbTypeToProduct>();
            DsStbUsageTypeToCustomerType = new HashSet<DsStbUsageTypeToCustomerType>();
            VendorOrderConstraint = new HashSet<VendorOrderConstraint>();
        }

        public int CustomerTypeId { get; set; }
        public string Name { get; set; }
        public bool IsOrganization { get; set; }
        public bool AdditionalDataRequired { get; set; }
        public string ActivationCancelMessage { get; set; }
        public int CustomerTypeCategoryId { get; set; }
        public bool SocialIdentificationNumberIsForced { get; set; }
        public bool StbSale { get; set; }
        public int DefaultDeActivationBouquetId { get; set; }
        public int DebitDeActivationBouquetId { get; set; }
        public int? DefaultStbOwnerId { get; set; }
        public int? SuspendDeActivationBouquetId { get; set; }
        public bool IsBundle { get; set; }
        public bool InvoicedByDsmart { get; set; }
        public bool EnableTaxNumber { get; set; }
        public bool EnablePassPortNumber { get; set; }

        public virtual DsCustomerTypeCategory CustomerTypeCategory { get; set; }
        public virtual DsBouquet DefaultDeActivationBouquet { get; set; }
        public virtual ICollection<DsBasicPacketToCustomerType> DsBasicPacketToCustomerType { get; set; }
        public virtual ICollection<DsBouquetToCustomerType> DsBouquetToCustomerType { get; set; }
        public virtual ICollection<DsCountryToCustomerType> DsCountryToCustomerType { get; set; }
        public virtual ICollection<DsRoleTableConstraint> DsRoleTableConstraint { get; set; }
        public virtual ICollection<DsStbActivationUsingRule> DsStbActivationUsingRule { get; set; }
        public virtual ICollection<DsStbTypeToProduct> DsStbTypeToProduct { get; set; }
        public virtual ICollection<DsStbUsageTypeToCustomerType> DsStbUsageTypeToCustomerType { get; set; }
        public virtual ICollection<VendorOrderConstraint> VendorOrderConstraint { get; set; }
    }
}
