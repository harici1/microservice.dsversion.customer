﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsCustomerTypeCategory
    {
        public DsCustomerTypeCategory()
        {
            DsCustomerType = new HashSet<DsCustomerType>();
        }

        public int CustomerTypeCategoryId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsCustomerType> DsCustomerType { get; set; }
    }
}
