﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerAddInfo
    {
        public int DealerAddInfoId { get; set; }
        public int DealerId { get; set; }
        public int AddInfoTypeId { get; set; }
        public string AddInfo { get; set; }
        public string AddInfoShortDesc { get; set; }
        public int CreatedById { get; set; }
        public int LastUpdatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastUpdatedDate { get; set; }
        public string InfoIdentificationNumber { get; set; }
        public bool? IsImageAvailable { get; set; }
    }
}
