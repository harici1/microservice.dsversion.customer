﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerAddInfoType
    {
        public int DealerAddInfoTypeId { get; set; }
        public string AddInfoType { get; set; }
        public string AddInfoTypeHeader { get; set; }
    }
}
