﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerAssetHolding
    {
        public int DealerAssetHoldingId { get; set; }
        public int AssetHoldingId { get; set; }
        public int DealerId { get; set; }
        public int CreatedId { get; set; }
        public int? DealerAssetHoldingNumber { get; set; }
        public int AssetHoldingStatusId { get; set; }
        public decimal? AssetHoldingPrice { get; set; }
        public int LastUpdatedId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public string Note { get; set; }
    }
}
