﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerAssetHoldingHistory
    {
        public int DealerAssetHoldingHistoryId { get; set; }
        public int DealerAssetHoldingId { get; set; }
        public int AssetHoldingId { get; set; }
        public int DealerId { get; set; }
        public int AssetHoldingStatusId { get; set; }
        public string Note { get; set; }
        public decimal Price { get; set; }
        public int Amount { get; set; }
        public int CreatedId { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
