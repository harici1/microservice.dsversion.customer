﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerChangeLog
    {
        public int DealerChangeLogId { get; set; }
        public int? DealerId { get; set; }
        public int? TypeId { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? CreatedById { get; set; }
        public string OldValues { get; set; }
        public string NewValues { get; set; }

        public virtual DsDealerChangeLogType Type { get; set; }
    }
}
