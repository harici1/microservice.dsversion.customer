﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerChangeLogType
    {
        public DsDealerChangeLogType()
        {
            DsDealerChangeLog = new HashSet<DsDealerChangeLog>();
        }

        public int DealerChangeLogTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<DsDealerChangeLog> DsDealerChangeLog { get; set; }
    }
}
