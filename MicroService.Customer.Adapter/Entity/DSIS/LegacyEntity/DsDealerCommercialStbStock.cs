﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerCommercialStbStock
    {
        public int DealerCommercialStbStockId { get; set; }
        public int StbId { get; set; }
        public int DealerId { get; set; }
        public int? StbStatusId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
