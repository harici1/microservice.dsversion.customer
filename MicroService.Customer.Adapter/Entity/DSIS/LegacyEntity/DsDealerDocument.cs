﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerDocument
    {
        public DsDealerDocument()
        {
            DsDealerDocumentLog = new HashSet<DsDealerDocumentLog>();
        }

        public int DealerDocumentId { get; set; }
        public string Name { get; set; }
        public string FileName { get; set; }
        public string Extention { get; set; }
        public int DocumentCategoryId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public bool IsActive { get; set; }

        public virtual DsDealerDocumentCategory DocumentCategory { get; set; }
        public virtual ICollection<DsDealerDocumentLog> DsDealerDocumentLog { get; set; }
    }
}
