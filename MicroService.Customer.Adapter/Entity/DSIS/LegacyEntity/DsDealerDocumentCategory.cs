﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerDocumentCategory
    {
        public DsDealerDocumentCategory()
        {
            DsDealerDocument = new HashSet<DsDealerDocument>();
        }

        public int DocumentCategoryId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsDealerDocument> DsDealerDocument { get; set; }
    }
}
