﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerDocumentLog
    {
        public int DealerDocumentLogId { get; set; }
        public int DealerDocumentId { get; set; }
        public int UserId { get; set; }
        public DateTime CreationDate { get; set; }

        public virtual DsDealerDocument DealerDocument { get; set; }
    }
}
