﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerDocumentType
    {
        public int DocumentTypeId { get; set; }
        public string Name { get; set; }
    }
}
