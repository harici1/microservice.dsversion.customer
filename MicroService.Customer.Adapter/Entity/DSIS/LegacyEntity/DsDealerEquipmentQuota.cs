﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerEquipmentQuota
    {
        public DsDealerEquipmentQuota()
        {
            DsDealerEquipmentQuotaLog = new HashSet<DsDealerEquipmentQuotaLog>();
        }

        public int DealerEquipmentQuotaId { get; set; }
        public int DelaerId { get; set; }
        public int Minimum { get; set; }
        public int Maximum { get; set; }
        public int StbTypeId { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? LastUpdatedById { get; set; }

        public virtual ICollection<DsDealerEquipmentQuotaLog> DsDealerEquipmentQuotaLog { get; set; }
    }
}
