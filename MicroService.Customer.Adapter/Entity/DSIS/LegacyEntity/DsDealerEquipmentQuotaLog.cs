﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerEquipmentQuotaLog
    {
        public int DealerEquipmentQuotaLogId { get; set; }
        public int DealerEquipmentQuotaId { get; set; }
        public int DealerId { get; set; }
        public int Minimum { get; set; }
        public int Maximum { get; set; }
        public int StbTypeId { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? LastUpdatedById { get; set; }

        public virtual DsDealerEquipmentQuota DealerEquipmentQuota { get; set; }
    }
}
