﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerMateryal
    {
        public int DealerMateryalId { get; set; }
        public string Name { get; set; }
        public int DealerMateryalCategoryId { get; set; }
        public string PictureLink { get; set; }
        public bool IsActive { get; set; }
    }
}
