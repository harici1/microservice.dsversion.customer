﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerMateryalDetail
    {
        public int DealerMateryalInfoId { get; set; }
        public int DealerMateryalId { get; set; }
        public string PartNumber { get; set; }
        public string Info { get; set; }
        public decimal Price { get; set; }
        public bool IsActive { get; set; }
        public int DealerMateryalDetailId { get; set; }
    }
}
