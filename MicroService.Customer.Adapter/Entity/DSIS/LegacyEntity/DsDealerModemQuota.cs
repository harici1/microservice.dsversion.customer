﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerModemQuota
    {
        public DsDealerModemQuota()
        {
            DsDealerModemQuotaLog = new HashSet<DsDealerModemQuotaLog>();
        }

        public int DealerModemQuotaId { get; set; }
        public int DealerId { get; set; }
        public int Minimum { get; set; }
        public int Maximum { get; set; }
        public int ModemTypeId { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? LastUpdatedById { get; set; }

        public virtual ICollection<DsDealerModemQuotaLog> DsDealerModemQuotaLog { get; set; }
    }
}
