﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerPrimeAddOnQuota
    {
        public int DealerPrimeAddOnQuotaId { get; set; }
        public int SaleCompensationTypeId { get; set; }
        public int? CampaignId { get; set; }
        public int? CustomerTypeId { get; set; }
        public decimal Amount { get; set; }
    }
}
