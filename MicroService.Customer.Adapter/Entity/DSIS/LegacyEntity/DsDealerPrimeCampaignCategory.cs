﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerPrimeCampaignCategory
    {
        public int DealerPrimeCampaignCategoryId { get; set; }
        public int? CampaignId { get; set; }
        public int? DealerPrimeCampaignCategoryDefinationId { get; set; }
    }
}
