﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerPrimeCustomGenericData
    {
        public int DealerPrimeCustomGenericDataId { get; set; }
        public int? DealerPrimeCustomGenericDataListId { get; set; }
        public int? CampaignId { get; set; }
    }
}
