﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerPrimeCustomerTypeCategory
    {
        public int DealerPrimeCustomerTypeCategoryId { get; set; }
        public int? CustomerTypeId { get; set; }
        public int? DealerPrimeCustomerTypeCategoryDefinationId { get; set; }
    }
}
