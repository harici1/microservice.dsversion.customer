﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerPrimeDealerClass
    {
        public int DealerPrimeDealerClassId { get; set; }
        public int DealerId { get; set; }
        public int ClassId { get; set; }
    }
}
