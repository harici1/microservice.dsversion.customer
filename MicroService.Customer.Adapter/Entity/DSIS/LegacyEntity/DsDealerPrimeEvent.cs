﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerPrimeEvent
    {
        public int DealerPrimeEventId { get; set; }
        public int SaleCompensationTypeId { get; set; }
        public int? CustomerId { get; set; }
        public int? ActivationId { get; set; }
        public int? CampaignId { get; set; }
        public int? PartyId { get; set; }
        public int? EventCreatedById { get; set; }
        public int? CustomerTypeId { get; set; }
        public int? ProductMainPackageId { get; set; }
        public decimal? Amount { get; set; }
        public int? GroupCount { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? ActivationSalesModelId { get; set; }
        public int? StbTypeId { get; set; }
        public int? CorporationId { get; set; }
        public int? UserType { get; set; }
        public int? PpvActivationId { get; set; }
        public bool? IsPrime { get; set; }
        public int? AddOnCount { get; set; }
        public decimal? AddOnAmount { get; set; }
        public int? ToProductMainPackageId { get; set; }
        public int? FromProductMainPackageId { get; set; }
        public int? FromProductAddOnCount { get; set; }
        public int? ToProductAddOnCount { get; set; }
        public int? StsId { get; set; }
        public bool? ContractIsDelivered { get; set; }
        public int? SaleCompensationLineId { get; set; }
        public int? PpvActivationProductId { get; set; }
        public int? FromProductId { get; set; }
        public int? ToProductId { get; set; }
        public int? SaleCompensationPeriodId { get; set; }
        public DateTime? ProcessDate { get; set; }
        public int? DealerTypeId { get; set; }
        public int? LeasingContractServiceTypeId { get; set; }
        public int? SaledUserType { get; set; }
        public int? SaledPartyId { get; set; }
        public int? RealPartyId { get; set; }
        public int? StbIsDelivered { get; set; }
        public int? RealGroupCount { get; set; }
        public decimal? PackagePrice { get; set; }
        public decimal? PackageDiscount { get; set; }
        public decimal? Stbprice { get; set; }
        public decimal? Stbdiscount { get; set; }
        public int? CommitmentPeriod { get; set; }
        public decimal? LeasingContractServicePrice { get; set; }
        public int? InstallmentNumber { get; set; }
        public int? ProductId { get; set; }
        public string LeasingContractNumber { get; set; }
        public decimal? BundleSmileAmount { get; set; }
        public int? BundleSmileStatus { get; set; }
        public decimal? AdditionalSaleAmount { get; set; }
        public int? LeasingContractServiceCampaignId { get; set; }
        public decimal? NewPayPrice { get; set; }
        public decimal? HdOldPrePaidPayPrice { get; set; }
        public decimal? HdOldPostPaidPayPrice { get; set; }
        public DateTime? PostPaidContractStartDate { get; set; }
        public DateTime? PostPaidContractEndDate { get; set; }
        public decimal? Ciro { get; set; }
        public bool? DirectDebit { get; set; }
        public DateTime? CurrentLeasingContractEndDate { get; set; }
        public DateTime? PreviousLeasingContractEndDate { get; set; }
        public int? PreviousLeasingContractServiceCampaignId { get; set; }
    }
}
