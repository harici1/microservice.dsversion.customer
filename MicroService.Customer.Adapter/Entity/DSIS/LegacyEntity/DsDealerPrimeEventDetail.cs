﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerPrimeEventDetail
    {
        public int DealerPrimeEventDetailId { get; set; }
        public int DealerPrimeEventId { get; set; }
        public decimal? CurrentTotalPrice { get; set; }
        public decimal? OldTotalPrice { get; set; }
        public int? CommitmentLenght { get; set; }
        public int? ExtendCommitmentLenght { get; set; }
        public decimal? CalculatedRatioAmount { get; set; }
    }
}
