﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerPrimeGiro
    {
        public int DealerPrimeGiroId { get; set; }
        public int SaleCompensationTypeId { get; set; }
        public int? Ratio { get; set; }
        public decimal? Amount { get; set; }
    }
}
