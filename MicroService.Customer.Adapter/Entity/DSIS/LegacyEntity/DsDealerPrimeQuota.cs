﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerPrimeQuota
    {
        public int DealerPrimeQuotaId { get; set; }
        public int SaleCompensationTypeId { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
        public decimal Amount { get; set; }
        public int? ActivationSalesModelId { get; set; }
        public int? ProductMainPackageId { get; set; }
        public int? StbTypeId { get; set; }
        public int? CustomerTypeId { get; set; }
        public int? FromProductMainPackageId { get; set; }
        public int? ToProductMainPackageId { get; set; }
        public int? DealerTypeId { get; set; }
        public int? LeasingContractServiceTypeId { get; set; }
        public int? SaledUserType { get; set; }
        public int? ProductId { get; set; }
    }
}
