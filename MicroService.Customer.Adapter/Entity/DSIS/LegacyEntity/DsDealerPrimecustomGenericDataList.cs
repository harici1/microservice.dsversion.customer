﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerPrimecustomGenericDataList
    {
        public int DealerPrimeCustomGenericDataListId { get; set; }
        public string Description { get; set; }
    }
}
