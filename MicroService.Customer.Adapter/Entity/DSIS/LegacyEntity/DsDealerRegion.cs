﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerRegion
    {
        public int DealerRegionId { get; set; }
        public string Name { get; set; }
    }
}
