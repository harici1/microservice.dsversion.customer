﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerReport
    {
        public int DealerReportId { get; set; }
        public int? DealerId { get; set; }
        public string Description1 { get; set; }
        public string Description2 { get; set; }
        public DateTime? VisitDate { get; set; }
        public string DealerName { get; set; }
        public string DealerClass { get; set; }
        public string DealerAddress { get; set; }
        public int? DealerNeighbourHoodId { get; set; }
        public string DealerPhone { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? DealerReportQueueId { get; set; }
        public DateTime? VisitEndDate { get; set; }

        public virtual DsDealerReportQueue DealerReportQueue { get; set; }
    }
}
