﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerReportAnswer
    {
        public int DealerReportAnswerId { get; set; }
        public int DealerReportQuestionId { get; set; }
        public string Name { get; set; }
    }
}
