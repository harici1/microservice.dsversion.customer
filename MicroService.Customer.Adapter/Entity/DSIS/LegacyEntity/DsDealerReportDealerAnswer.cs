﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerReportDealerAnswer
    {
        public int DealerReportDealerAnswerId { get; set; }
        public int DealerReportId { get; set; }
        public int AnswerId { get; set; }
        public string Note { get; set; }
    }
}
