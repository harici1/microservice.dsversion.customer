﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerReportQuestion
    {
        public int DealerReportQuestionId { get; set; }
        public string Name { get; set; }
    }
}
