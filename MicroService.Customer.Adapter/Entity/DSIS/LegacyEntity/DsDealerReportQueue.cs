﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerReportQueue
    {
        public DsDealerReportQueue()
        {
            DsDealerReport = new HashSet<DsDealerReport>();
        }

        public int DealerReportQueueId { get; set; }
        public int? DealerId { get; set; }
        public string DealerName { get; set; }
        public bool Visited { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }

        public virtual ICollection<DsDealerReport> DsDealerReport { get; set; }
    }
}
