﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerStbStock
    {
        public int DealerStbStockId { get; set; }
        public int StbId { get; set; }
        public int? SmartCardId { get; set; }
        public int DealerId { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public string Notes { get; set; }
    }
}
