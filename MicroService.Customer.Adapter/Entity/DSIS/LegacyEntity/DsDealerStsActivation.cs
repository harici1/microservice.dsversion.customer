﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerStsActivation
    {
        public int DealerStsActivationId { get; set; }
        public int ActivationId { get; set; }
        public int DealerId { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }

        public virtual DsActivation Activation { get; set; }
        public virtual DsBayi Dealer { get; set; }
    }
}
