﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerToNeighbourhood
    {
        public int DealerToNeighbourhoodId { get; set; }
        public int NeighbourhoodId { get; set; }
        public int DealerId { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public int LastUpdatedById { get; set; }
        public bool? IsNewTemp { get; set; }

        public virtual DsBayi Dealer { get; set; }
        public virtual DsNeighbourhood Neighbourhood { get; set; }
    }
}
