﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerToNeighbourhoodWorkOrderCreate
    {
        public int DealerToNeighbourhoodWorkOrderCreateId { get; set; }
        public int SealerId { get; set; }
        public int NeighbourhoodId { get; set; }
        public int DealerId { get; set; }
        public bool IsActive { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int SortNumber { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public string Notes { get; set; }
    }
}
