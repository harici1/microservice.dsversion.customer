﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerType
    {
        public int DealerTypeId { get; set; }
        public string Name { get; set; }
    }
}
