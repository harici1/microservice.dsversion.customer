﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerUser
    {
        public int DealerUserId { get; set; }
        public int? DealerId { get; set; }
        public int UserId { get; set; }
        public string DealerUserCode { get; set; }
        public string Name { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsAdministrator { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string PasswordResetCode { get; set; }
        public DateTime? PasswordResetExpiryDate { get; set; }
        public int? NeighbourhoodId { get; set; }
        public int? DealerUserTypeId { get; set; }
        public string Ssotoken { get; set; }
        public DateTime? SsotokenExpiryDate { get; set; }
    }
}
