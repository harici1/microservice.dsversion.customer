﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDealerUserType
    {
        public int DealerUserTypeId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
