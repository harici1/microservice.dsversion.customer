﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDestekKonusu
    {
        public DsDestekKonusu()
        {
            DsSorunTanimi = new HashSet<DsSorunTanimi>();
        }

        public int SinifId { get; set; }
        public int KonuId { get; set; }
        public string Tanimi { get; set; }
        public int? OlusturanKullanici { get; set; }
        public DateTime? OlusturmaTarihi { get; set; }
        public int? DuzeltenKullanici { get; set; }
        public DateTime? DuzeltmeTarihi { get; set; }
        public byte? Durumu { get; set; }

        public virtual DsDestekSinifi Sinif { get; set; }
        public virtual ICollection<DsSorunTanimi> DsSorunTanimi { get; set; }
    }
}
