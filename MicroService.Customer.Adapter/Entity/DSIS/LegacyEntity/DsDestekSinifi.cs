﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDestekSinifi
    {
        public DsDestekSinifi()
        {
            DsDestekKonusu = new HashSet<DsDestekKonusu>();
        }

        public int SinifId { get; set; }
        public int? BasvuruId { get; set; }
        public string Tanimi { get; set; }
        public int? OlusturanKullanici { get; set; }
        public DateTime? OlusturmaTarihi { get; set; }
        public int? DuzeltenKullanici { get; set; }
        public DateTime? DuzeltmeTarihi { get; set; }
        public byte? Durumu { get; set; }

        public virtual DsBasvuruNedeni Basvuru { get; set; }
        public virtual ICollection<DsDestekKonusu> DsDestekKonusu { get; set; }
    }
}
