﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDirectDebitAgreement
    {
        public int DirectDebitAgreementId { get; set; }
        public int BankCode { get; set; }
        public int RequestDirectDebitUnit { get; set; }
        public int RequestDirectDebitCancelUnit { get; set; }
        public int ResponseDirectDebitUnit { get; set; }
        public int ResponseDirectDebitCancelUnit { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseDesc { get; set; }
        public DateTime AggrementDate { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
