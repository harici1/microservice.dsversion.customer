﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDiscountCoupon
    {
        public int DiscountCouponId { get; set; }
        public int? BatchId { get; set; }
        public string PinCode { get; set; }
        public int? PartyId { get; set; }
        public int? CampaignId { get; set; }
        public bool IsAuthorized { get; set; }
        public bool IsCancelled { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? DocumentId { get; set; }
        public int? ProductId { get; set; }
        public int? ContactChannelId { get; set; }
        public int? ActivationLengthInDays { get; set; }
        public int? DistirbutorId { get; set; }
        public decimal? DiscountAmount { get; set; }
        public string Notes { get; set; }
        public int? CreatedByDocumentTypeId { get; set; }
        public int? CreatedByDocumentId { get; set; }
        public string SerialNumber { get; set; }
    }
}
