﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDistrict
    {
        public DsDistrict()
        {
            DsGenericCustomer = new HashSet<DsGenericCustomer>();
            DsMagazineSubscription = new HashSet<DsMagazineSubscription>();
        }

        public int DistrictId { get; set; }
        public int ProvinceId { get; set; }
        public string Name { get; set; }
        public int? DistrictManagerId { get; set; }
        public int? DistrictSupervisorId { get; set; }
        public string DistrictCode { get; set; }
        public int? DealerRegionId { get; set; }
        public int? DistrictSalesTarget { get; set; }
        public int? TtsubProvinceId { get; set; }
        public string TtsubProvinceName { get; set; }

        public virtual ICollection<DsGenericCustomer> DsGenericCustomer { get; set; }
        public virtual ICollection<DsMagazineSubscription> DsMagazineSubscription { get; set; }
    }
}
