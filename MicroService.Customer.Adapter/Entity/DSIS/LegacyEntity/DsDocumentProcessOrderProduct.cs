﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDocumentProcessOrderProduct
    {
        public int DocumentProcessOrderProductId { get; set; }
        public int? OrderProductId { get; set; }
        public bool? IsActive { get; set; }
    }
}
