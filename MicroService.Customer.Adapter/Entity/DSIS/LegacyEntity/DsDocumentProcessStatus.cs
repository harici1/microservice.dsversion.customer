﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDocumentProcessStatus
    {
        public DsDocumentProcessStatus()
        {
            DsWorkOrderLogStatus = new HashSet<DsWorkOrderLogStatus>();
        }

        public int DocumentProcessStatusId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<DsWorkOrderLogStatus> DsWorkOrderLogStatus { get; set; }
    }
}
