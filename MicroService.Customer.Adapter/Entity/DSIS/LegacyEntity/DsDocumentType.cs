﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDocumentType
    {
        public DsDocumentType()
        {
            DsGenericCustomerChangeLog = new HashSet<DsGenericCustomerChangeLog>();
            DsGsmChargePayment = new HashSet<DsGsmChargePayment>();
        }

        public int DocumentTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsGenericCustomerChangeLog> DsGenericCustomerChangeLog { get; set; }
        public virtual ICollection<DsGsmChargePayment> DsGsmChargePayment { get; set; }
    }
}
