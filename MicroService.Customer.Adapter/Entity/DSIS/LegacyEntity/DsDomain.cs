﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDomain
    {
        public DsDomain()
        {
            DsBasvuruNedeni = new HashSet<DsBasvuruNedeni>();
        }

        public int DomainId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsBasvuruNedeni> DsBasvuruNedeni { get; set; }
    }
}
