﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsDsisneighbourhoodToAnkaDistrict
    {
        public int DsisneighbourhoodToAnkaDistrictId { get; set; }
        public int DsisneighbourhoodId { get; set; }
        public int AnkaDistrictId { get; set; }

        public virtual DsNeighbourhood Dsisneighbourhood { get; set; }
    }
}
