﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsEirequestLog
    {
        public int EirequestLogId { get; set; }
        public string SystemId { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
