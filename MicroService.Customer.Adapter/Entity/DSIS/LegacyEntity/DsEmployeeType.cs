﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsEmployeeType
    {
        public int EmployeeTypeId { get; set; }
        public string Name { get; set; }
    }
}
