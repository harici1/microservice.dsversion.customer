﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsEntitlement
    {
        public int EntitlementId { get; set; }
        public int TypeId { get; set; }
        public int? PartyId { get; set; }
        public int? SmartCardId { get; set; }
        public int? Stbid { get; set; }
        public int? RegionKeyId { get; set; }
        public int? ParentalRatingId { get; set; }
        public bool? IsFreeToAirEnableSet { get; set; }
        public int? NdsbouquetId { get; set; }
        public int? NdssubscriberId { get; set; }
        public int StatusId { get; set; }
        public int? PersonelBitNumber { get; set; }
        public bool? PersonelBitValue { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? DocumentId { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? CreationDate { get; set; }
        public string PinNumber { get; set; }
        public int? ServiceId { get; set; }
        public DateTime? OppvexpirationDate { get; set; }
        public DateTime? EntitlementDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public string ErrorCode { get; set; }
        public int? EmmgpacketSenderServiceId { get; set; }
        public int? BatchId { get; set; }
        public bool? IsCopyProtectionEnabled { get; set; }
        public string Note { get; set; }
        public int? ActivationId { get; set; }
        public int? RetryCount { get; set; }
        public int? ReportBackAvailability { get; set; }
    }
}
