﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsEntitlementLog
    {
        public int EntitlementLogId { get; set; }
        public int EntitlementId { get; set; }
        public string T020command { get; set; }
        public string T020response { get; set; }
        public string T020fullPacketResponse { get; set; }
        public string ErrorDescription { get; set; }
        public DateTime? CreationDate { get; set; }
    }
}
