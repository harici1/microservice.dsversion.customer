﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsEntitlementStatus
    {
        public int EntitlementStatusId { get; set; }
        public string Name { get; set; }
    }
}
