﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsEntitlementType
    {
        public int EntitlementTypeId { get; set; }
        public string TypeName { get; set; }
    }
}
