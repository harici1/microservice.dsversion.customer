﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsEpgchannel
    {
        public int EpgchannelId { get; set; }
        public int? EpgchannelTypeId { get; set; }
        public int? ChannelNumber { get; set; }
        public string ServiceName { get; set; }
        public int? SiServiceKey { get; set; }
        public bool? AllowFingerPrint { get; set; }
    }
}
