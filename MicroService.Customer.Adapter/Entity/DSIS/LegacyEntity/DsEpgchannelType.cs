﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsEpgchannelType
    {
        public int EpgchannelTypeId { get; set; }
        public string Name { get; set; }
    }
}
