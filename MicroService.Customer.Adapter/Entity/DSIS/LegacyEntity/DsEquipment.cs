﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsEquipment
    {
        public int EquipmentId { get; set; }
        public string SerialNumber { get; set; }
        public int? Capacity { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? DealerId { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
    }
}
