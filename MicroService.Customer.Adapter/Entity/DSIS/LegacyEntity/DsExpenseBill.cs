﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsExpenseBill
    {
        public DsExpenseBill()
        {
            DsExpenseBillLine = new HashSet<DsExpenseBillLine>();
            DsExpenseBillReturnPayment = new HashSet<DsExpenseBillReturnPayment>();
        }

        public int ExpenseBillId { get; set; }
        public int InvoiceId { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string ExpenseBillNumber { get; set; }
        public decimal Amount { get; set; }
        public int PartyId { get; set; }
        public string PartyName { get; set; }
        public string Address { get; set; }
        public string TaxOffice { get; set; }
        public string TaxNumber { get; set; }
        public int CorporationId { get; set; }
        public DateTime ExpenseBillDate { get; set; }
        public int DocumentId { get; set; }
        public int DocumentTypeId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }

        public virtual DsCorporation Corporation { get; set; }
        public virtual DsInvoice Invoice { get; set; }
        public virtual DsParty Party { get; set; }
        public virtual ICollection<DsExpenseBillLine> DsExpenseBillLine { get; set; }
        public virtual ICollection<DsExpenseBillReturnPayment> DsExpenseBillReturnPayment { get; set; }
    }
}
