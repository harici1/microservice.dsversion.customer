﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsExpenseBillLine
    {
        public int ExpenseBillLineId { get; set; }
        public int ExpenseBillId { get; set; }
        public int InvoiceItemId { get; set; }
        public string Description { get; set; }
        public decimal NetAmount { get; set; }
        public decimal TaxRate1 { get; set; }
        public decimal TaxAmount1 { get; set; }
        public decimal TaxRate2 { get; set; }
        public decimal TaxAmount2 { get; set; }
        public decimal Amount { get; set; }
        public int NumberOfProduct { get; set; }

        public virtual DsExpenseBill ExpenseBill { get; set; }
        public virtual DsInvoiceItem InvoiceItem { get; set; }
    }
}
