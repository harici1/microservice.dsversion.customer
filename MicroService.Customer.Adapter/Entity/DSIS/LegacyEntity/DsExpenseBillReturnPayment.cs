﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsExpenseBillReturnPayment
    {
        public int ExpenseBillReturnPaymentId { get; set; }
        public int ExpenseBillId { get; set; }
        public int InvoiceId { get; set; }
        public int? VposId { get; set; }
        public int? BankAccountId { get; set; }
        public int PaymentId { get; set; }
        public int PaymentTypeId { get; set; }
        public decimal Amount { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }

        public virtual DsBankAccount BankAccount { get; set; }
        public virtual DsExpenseBill ExpenseBill { get; set; }
        public virtual DsInvoice Invoice { get; set; }
        public virtual DsVposServiceProvider Vpos { get; set; }
    }
}
