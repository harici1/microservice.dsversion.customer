﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsExternalEquipmentType
    {
        public int ExternalEquipmentTypeId { get; set; }
        public string Name { get; set; }
    }
}
