﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsExternalSystem
    {
        public int ExternalSystemId { get; set; }
        public string Name { get; set; }

        public virtual DsParty ExternalSystem { get; set; }
    }
}
