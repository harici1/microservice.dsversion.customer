﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsFacadeService
    {
        public DsFacadeService()
        {
            DsIncomingWebMessageRequestType = new HashSet<DsIncomingWebMessageRequestType>();
            DsUserToFacadeService = new HashSet<DsUserToFacadeService>();
        }

        public int FacadeServiceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ClassName { get; set; }

        public virtual ICollection<DsIncomingWebMessageRequestType> DsIncomingWebMessageRequestType { get; set; }
        public virtual ICollection<DsUserToFacadeService> DsUserToFacadeService { get; set; }
    }
}
