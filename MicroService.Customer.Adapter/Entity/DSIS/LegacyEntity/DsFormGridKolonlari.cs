﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsFormGridKolonlari
    {
        public string FormAdi { get; set; }
        public int GridNo { get; set; }
        public int KolonNo { get; set; }
        public string KolonBasligi { get; set; }
        public string Gorunur { get; set; }
    }
}
