﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGarantiCollectedPayment
    {
        public int GarantiCollectedPaymentId { get; set; }
        public string CustomerNo { get; set; }
        public string InvoiceNo { get; set; }
        public string ActivityDate { get; set; }
        public string Amount { get; set; }
        public string Balance { get; set; }
        public string Explanation { get; set; }
        public string TransactionId { get; set; }
        public string TransactionReferenceId { get; set; }
        public string WrittenLine { get; set; }
        public DateTime CreationDate { get; set; }
        public string ProcessResult { get; set; }
        public int? RetryCount { get; set; }
        public DateTime? ProcessDate { get; set; }
        public int? ProcessStatus { get; set; }
        public DateTime? ProcessExpirationDate { get; set; }
    }
}
