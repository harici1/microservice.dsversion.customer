﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGarantiOnlinePreLog
    {
        public int GarantiOnlinePreLogId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string ActivityDate { get; set; }
        public string Amount { get; set; }
        public string Balance { get; set; }
        public string Explanation { get; set; }
        public string TransactionId { get; set; }
        public string TransactionReferenceId { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
