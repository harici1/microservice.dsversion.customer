﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGarantiSemiOnlineServiceLog
    {
        public int GarantiSemiOnlineServiceLogId { get; set; }
        public int GarantiSemiOnlineServiceLogTypeId { get; set; }
        public string GarantiSemiOnlineServiceLog { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
