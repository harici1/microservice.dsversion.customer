﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGarantiSemiOnlineServiceLogType
    {
        public int GarantiSemiOnlineServiceLogTypeId { get; set; }
        public string GarantiSemiOnlineServiceLogTypt { get; set; }
    }
}
