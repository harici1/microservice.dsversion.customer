﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGenderType
    {
        public DsGenderType()
        {
            DsClubMember = new HashSet<DsClubMember>();
            DsGenericCustomer = new HashSet<DsGenericCustomer>();
        }

        public int GenderTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsClubMember> DsClubMember { get; set; }
        public virtual ICollection<DsGenericCustomer> DsGenericCustomer { get; set; }
    }
}
