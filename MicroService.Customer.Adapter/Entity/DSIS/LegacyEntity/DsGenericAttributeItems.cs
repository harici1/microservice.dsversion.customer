﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGenericAttributeItems
    {
        public int GenericAttributeItemId { get; set; }
        public int? AttributeCode { get; set; }
        public string AttributeItemName { get; set; }
        public int? AttributeItemCode { get; set; }
    }
}
