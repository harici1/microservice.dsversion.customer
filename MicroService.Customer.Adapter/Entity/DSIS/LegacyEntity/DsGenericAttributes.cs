﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGenericAttributes
    {
        public int GenericAttributeId { get; set; }
        public string AttributeName { get; set; }
        public int AttributeCode { get; set; }
        public int? ParentAttributeCode { get; set; }
    }
}
