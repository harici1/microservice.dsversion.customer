﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGenericCustomer
    {
        public DsGenericCustomer()
        {
            DsGenericCustomerChangeLog = new HashSet<DsGenericCustomerChangeLog>();
        }

        public int GenericCustomerId { get; set; }
        public string Name { get; set; }
        public string FamilyName { get; set; }
        public string SocialIdentificationNumber { get; set; }
        public DateTime? BirthDate { get; set; }
        public int GenderTypeId { get; set; }
        public int DistrictId { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }
        public string PostalCode { get; set; }
        public int GenericCustomerTypeId { get; set; }
        public int? FootballTeamId { get; set; }
        public int? ProfessionId { get; set; }
        public string MaritalStatus { get; set; }
        public int? EducationLevelId { get; set; }
        public string Address { get; set; }
        public string CustomerCode { get; set; }
        public string Description { get; set; }
        public int? GsmserviceCodeId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public string CommercialCustomerName { get; set; }
        public int? PreviousAdslOperatorId { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? PreviousTelevisionOperatorId { get; set; }
        public int? AdslContractTypeId { get; set; }
        public int? CampaignRequestChannelId { get; set; }
        public int? AdslPhonePhysicalStatus { get; set; }

        public virtual DsDistrict District { get; set; }
        public virtual DsEducationLevel EducationLevel { get; set; }
        public virtual DsFootballTeam FootballTeam { get; set; }
        public virtual DsGenderType GenderType { get; set; }
        public virtual DsGenericCustomerType GenericCustomerType { get; set; }
        public virtual DsProfession Profession { get; set; }
        public virtual ICollection<DsGenericCustomerChangeLog> DsGenericCustomerChangeLog { get; set; }
    }
}
