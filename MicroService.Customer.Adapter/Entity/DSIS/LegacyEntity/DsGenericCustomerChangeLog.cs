﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGenericCustomerChangeLog
    {
        public int GenericCustomerChangeLogId { get; set; }
        public int? GenericCustomerId { get; set; }
        public int? TypeId { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? CreatedById { get; set; }
        public string OldValues { get; set; }
        public string NewValues { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? DocumentId { get; set; }

        public virtual DsDocumentType DocumentType { get; set; }
        public virtual DsGenericCustomer GenericCustomer { get; set; }
        public virtual DsGenericCustomerChangeLogType Type { get; set; }
    }
}
