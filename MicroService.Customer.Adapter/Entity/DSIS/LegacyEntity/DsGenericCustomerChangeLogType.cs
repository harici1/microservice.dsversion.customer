﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGenericCustomerChangeLogType
    {
        public DsGenericCustomerChangeLogType()
        {
            DsGenericCustomerChangeLog = new HashSet<DsGenericCustomerChangeLog>();
        }

        public int GenericCustomerChangeLogTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<DsGenericCustomerChangeLog> DsGenericCustomerChangeLog { get; set; }
    }
}
