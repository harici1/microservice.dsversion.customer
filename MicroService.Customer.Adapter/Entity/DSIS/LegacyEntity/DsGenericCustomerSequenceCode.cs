﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGenericCustomerSequenceCode
    {
        public int SequenceId { get; set; }
    }
}
