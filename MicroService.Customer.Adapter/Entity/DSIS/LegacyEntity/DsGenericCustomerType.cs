﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGenericCustomerType
    {
        public DsGenericCustomerType()
        {
            DsGenericCustomer = new HashSet<DsGenericCustomer>();
        }

        public int GenericCustomerTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsGenericCustomer> DsGenericCustomer { get; set; }
    }
}
