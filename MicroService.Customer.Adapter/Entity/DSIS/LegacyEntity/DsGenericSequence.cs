﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGenericSequence
    {
        public long SequenceId { get; set; }
    }
}
