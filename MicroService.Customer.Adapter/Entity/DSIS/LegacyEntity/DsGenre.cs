﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGenre
    {
        public int GenreId { get; set; }
        public string Name { get; set; }
    }
}
