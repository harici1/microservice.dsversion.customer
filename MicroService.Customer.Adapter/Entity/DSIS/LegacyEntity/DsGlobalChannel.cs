﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGlobalChannel
    {
        public int GlobalChannelId { get; set; }
        public int BouquetId { get; set; }
        public int SiServiceId { get; set; }
        public int SelectNum { get; set; }
        public bool? IsActive { get; set; }
    }
}
