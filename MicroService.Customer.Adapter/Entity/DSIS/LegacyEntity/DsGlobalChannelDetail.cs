﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGlobalChannelDetail
    {
        public int GlobalChannelDetailId { get; set; }
        public int SiServiceId { get; set; }
        public int? ChannelTypeId { get; set; }
        public string ChannelName { get; set; }
        public string SpotText { get; set; }
        public string ChannelDescription { get; set; }
        public string ImageUrl { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsTvGuide { get; set; }
        public bool? IsEurope { get; set; }
        public bool? IsHd { get; set; }
        public bool? IsPrimeTime { get; set; }
        public bool? IsPaid { get; set; }
        public string BouquetLock { get; set; }
    }
}
