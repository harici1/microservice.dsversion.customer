﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGlobalChannelType
    {
        public int GlobalChannelTypeId { get; set; }
        public string Dimension { get; set; }
    }
}
