﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGlobalPackageInfo
    {
        public int GlobalPackageInfoId { get; set; }
        public int GlobalPackageTypeId { get; set; }
        public string PackageName { get; set; }
        public string PackageDetail { get; set; }
        public string PackageImage { get; set; }
        public int? OrderNo { get; set; }
    }
}
