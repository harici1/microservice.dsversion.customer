﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGlobalPackageInfoToProduct
    {
        public int GlobalPackageInfoToProductId { get; set; }
        public int GlobalPackageInfoId { get; set; }
        public int ProductId { get; set; }
        public int StbTypeId { get; set; }
    }
}
