﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGlobalPackageInfoToSiService
    {
        public int GlobalPackageInfoToSiServiceId { get; set; }
        public int GlobalPackageInfoId { get; set; }
        public int BouquetId { get; set; }
        public int SiServiceId { get; set; }
    }
}
