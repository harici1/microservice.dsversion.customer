﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGlobalProgramRate
    {
        public int GlobalProgramRateId { get; set; }
        public int UserId { get; set; }
        public int BouquetId { get; set; }
        public int SiServiceId { get; set; }
        public int EventId { get; set; }
        public int Rate { get; set; }
        public int ContactChannelId { get; set; }
        public int ContactChannelSubDomainId { get; set; }
    }
}
