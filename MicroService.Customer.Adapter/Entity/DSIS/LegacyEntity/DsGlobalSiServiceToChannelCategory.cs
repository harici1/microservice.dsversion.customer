﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGlobalSiServiceToChannelCategory
    {
        public int GlobalSiServiceToChannelCategoryId { get; set; }
        public int SiService { get; set; }
        public int GlobalChannelCategoryId { get; set; }
    }
}
