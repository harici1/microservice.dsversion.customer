﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGrupKullanicilari
    {
        public int GrupId { get; set; }
        public int AgentId { get; set; }
    }
}
