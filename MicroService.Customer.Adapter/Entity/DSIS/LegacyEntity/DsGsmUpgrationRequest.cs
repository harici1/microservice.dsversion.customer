﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGsmUpgrationRequest
    {
        public int GsmUpgrationRequestId { get; set; }
        public int GsmMessageId { get; set; }
        public int ConfirmationMessageId { get; set; }
        public int PartyId { get; set; }
        public int LeasingContractId { get; set; }
        public int RequestedProductId { get; set; }
        public int RequestStatus { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastUpdateDate { get; set; }
    }
}
