﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGsmserviceCode
    {
        public int GsmserviceCodeId { get; set; }
        public string Name { get; set; }
    }
}
