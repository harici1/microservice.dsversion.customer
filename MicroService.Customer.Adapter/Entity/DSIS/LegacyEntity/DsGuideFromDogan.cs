﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsGuideFromDogan
    {
        public int Pkey { get; set; }
        public string SiServiceId { get; set; }
        public string SiTrafficKey { get; set; }
        public string AudienceGroups { get; set; }
        public string Planning { get; set; }
        public string Actor { get; set; }
        public string Year { get; set; }
        public string Id { get; set; }
        public string Subject { get; set; }
        public string ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string No { get; set; }
        public string CreatedBy { get; set; }
        public string AudioSubtitles { get; set; }
        public string Application { get; set; }
        public DateTime? StartDate { get; set; }
        public string Channel { get; set; }
        public string Description { get; set; }
        public DateTime? EndDate { get; set; }
        public string Director { get; set; }
        public string AudienceGroupName { get; set; }
        public string Genre { get; set; }
        public string ProgramName { get; set; }
        public string Day { get; set; }
        public string SubGenre { get; set; }
        public string ScreenViolence { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Id1 { get; set; }
    }
}
