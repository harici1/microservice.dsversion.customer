﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsIl
    {
        public DsIl()
        {
            DsMusteriPotansiyel = new HashSet<DsMusteriPotansiyel>();
            DsOrganization = new HashSet<DsOrganization>();
        }

        public int ProvinceId { get; set; }
        public int CountryId { get; set; }
        public string Adi { get; set; }
        public string Plaka { get; set; }
        public int? PhoneCode { get; set; }
        public string WeatherSeriviceCode { get; set; }
        public string AnkaName { get; set; }
        public int? RegionKeyId { get; set; }

        public virtual ICollection<DsMusteriPotansiyel> DsMusteriPotansiyel { get; set; }
        public virtual ICollection<DsOrganization> DsOrganization { get; set; }
    }
}
