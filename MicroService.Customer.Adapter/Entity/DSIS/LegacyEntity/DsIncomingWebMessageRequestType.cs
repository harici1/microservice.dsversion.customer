﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsIncomingWebMessageRequestType
    {
        public int IncomingWebMessageRequestTypeId { get; set; }
        public string Name { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? FacadeServiceId { get; set; }
        public string FormTitle { get; set; }
        public int? SystemCode { get; set; }

        public virtual DsFacadeService FacadeService { get; set; }
    }
}
