﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsIncomingWebMessageRequestTypeMappingType
    {
        public int IncomingWebMessageRequestTypeMappingTypeId { get; set; }
        public string Name { get; set; }
    }
}
