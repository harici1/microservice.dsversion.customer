﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInternetConnectionSpeed
    {
        public int InternetConnectionSpeedId { get; set; }
        public string Name { get; set; }
    }
}
