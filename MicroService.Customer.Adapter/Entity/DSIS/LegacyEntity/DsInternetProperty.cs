﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInternetProperty
    {
        public int InternetPropertyId { get; set; }
        public int PropertyId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int ShortCode { get; set; }
    }
}
