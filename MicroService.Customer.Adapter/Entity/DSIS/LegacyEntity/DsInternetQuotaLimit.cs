﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInternetQuotaLimit
    {
        public int InternetQuotaLimitId { get; set; }
        public string Name { get; set; }
    }
}
