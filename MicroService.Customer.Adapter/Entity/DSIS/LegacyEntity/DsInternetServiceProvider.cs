﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInternetServiceProvider
    {
        public int InternetServiceProviderId { get; set; }
        public string Name { get; set; }
    }
}
