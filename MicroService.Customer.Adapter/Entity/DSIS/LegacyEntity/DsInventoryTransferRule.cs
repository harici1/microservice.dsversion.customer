﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInventoryTransferRule
    {
        public int InventoryTransferRuleId { get; set; }
        public int FromParty { get; set; }
        public int TransactionTypeId { get; set; }
        public int ToParty { get; set; }
        public int ProductTypeId { get; set; }

        public virtual DsParty FromPartyNavigation { get; set; }
        public virtual DsProductType ProductType { get; set; }
        public virtual DsParty ToPartyNavigation { get; set; }
        public virtual DsTransactionType TransactionType { get; set; }
    }
}
