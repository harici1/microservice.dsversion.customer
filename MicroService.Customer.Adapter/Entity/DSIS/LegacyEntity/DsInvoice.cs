﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoice
    {
        public DsInvoice()
        {
            DsExpenseBill = new HashSet<DsExpenseBill>();
            DsExpenseBillReturnPayment = new HashSet<DsExpenseBillReturnPayment>();
            DsInvoiceToBankDebitDetail = new HashSet<DsInvoiceToBankDebitDetail>();
            DsLeasingContractPayment = new HashSet<DsLeasingContractPayment>();
        }

        public int InvoiceId { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public int? PartyId { get; set; }
        public string PartyName { get; set; }
        public string Address { get; set; }
        public string TaxOffice { get; set; }
        public string TaxNumber { get; set; }
        public int? PaymentTypeId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? DocumentId { get; set; }
        public int? BatchId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public bool? IsAccountingOk { get; set; }
        public int? BankAccountId { get; set; }
        public int? VposId { get; set; }
        public int? InstallmentNumber { get; set; }
        public int? WaybillPartyId { get; set; }
        public int? InvoiceCommercialTypeId { get; set; }
        public int? CorporationId { get; set; }
        public int? LeasingContractId { get; set; }
        public decimal? PaymentAmount { get; set; }
        public decimal? InvoiceAmount { get; set; }
        public decimal? DebitAmount { get; set; }
        public int? InvoiceDeliveryStatusId { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public bool? IsCancelled { get; set; }
        public string Note { get; set; }
        public int? InvoiceTypeId { get; set; }
        public decimal? PaymentAmountTemp { get; set; }
        public decimal? DebitAmountTemp { get; set; }
        public decimal? ClaimAmount { get; set; }
        public int? InvoicePromptDay { get; set; }
        public int? TempId { get; set; }
        public DateTime? CancelDate { get; set; }
        public int? InvoiceStatusId { get; set; }
        public int? LeasingContractToInvoiceProcessId { get; set; }
        public bool? IsCancelContractInvoice { get; set; }
        public decimal? SmileInvoiceAmount { get; set; }
        public int? ChangedAfterCreate { get; set; }
        public int? PrintStatusId { get; set; }
        public DateTime? PrintDate { get; set; }
        public int? InvoiceGroupId { get; set; }
        public int? PrintTypeId { get; set; }
        public string GibinvoiceId { get; set; }

        public virtual ICollection<DsExpenseBill> DsExpenseBill { get; set; }
        public virtual ICollection<DsExpenseBillReturnPayment> DsExpenseBillReturnPayment { get; set; }
        public virtual ICollection<DsInvoiceToBankDebitDetail> DsInvoiceToBankDebitDetail { get; set; }
        public virtual ICollection<DsLeasingContractPayment> DsLeasingContractPayment { get; set; }
    }
}
