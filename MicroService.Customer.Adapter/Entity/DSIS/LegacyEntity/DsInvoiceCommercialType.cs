﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceCommercialType
    {
        public int InvoiceCommercialTypeId { get; set; }
        public string Name { get; set; }
        public int VendorOrderCommercialTypeId { get; set; }
    }
}
