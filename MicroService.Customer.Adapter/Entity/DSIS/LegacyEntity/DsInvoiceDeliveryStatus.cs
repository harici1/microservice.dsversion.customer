﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceDeliveryStatus
    {
        public int InvoiceDeliveryStatusId { get; set; }
        public string Name { get; set; }
    }
}
