﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceFromAnka
    {
        public int InvoiceFromAnkaId { get; set; }
        public string AnkaAccountNumber { get; set; }
        public int? Id { get; set; }
        public long? RowNumber { get; set; }
        public int? CustomerId { get; set; }
        public int? ContractId { get; set; }
        public string ContractNo { get; set; }
        public int? InvoiceId { get; set; }
        public string InvoiceNo { get; set; }
        public string InvoiceTypeId { get; set; }
        public int? InvoiceCycleId { get; set; }
        public string InvoiceTitle { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string Cycle { get; set; }
        public DateTime? LastBillingDate { get; set; }
        public string Currency { get; set; }
        public decimal? AmountBeforeTax { get; set; }
        public decimal? TaxAmount { get; set; }
        public decimal? InvoiceAmount { get; set; }
        public decimal? PreviusMonthAmount { get; set; }
        public decimal? NextMonthAmount { get; set; }
        public decimal? BillingAmount { get; set; }
        public decimal? PaymentAmount { get; set; }
        public decimal? RemainingAmount { get; set; }
        public string TaxOffice { get; set; }
        public string TaxNumber { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string ZipCode { get; set; }
        public int? ProvinceId { get; set; }
        public string AmountText { get; set; }
        public string AdMessage { get; set; }
        public int? Status { get; set; }
        public int? PrintStatus { get; set; }
        public bool? Canceled { get; set; }
        public bool? AutoChargeCc { get; set; }
        public bool? SendInvoiceByMail { get; set; }
        public bool? SendInvoiceByEmail { get; set; }
        public bool? SendInvoiceBySms { get; set; }
        public decimal? PreviusMonthDebitAmount { get; set; }
        public decimal? TotalDebitAmount { get; set; }
        public decimal? AdslOverQuotaCharging { get; set; }
        public int? PrintCycle { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string CustomerType { get; set; }
        public string CustomerNo { get; set; }
        public decimal? PreCyclePositiveAmount { get; set; }
        public string InvoicePeriodCode { get; set; }
        public string AdslNo { get; set; }
        public string HizmetNo { get; set; }
        public string XdslNo { get; set; }
        public string WebUserName { get; set; }
        public string WebUserPassword { get; set; }
        public string AnkaCustomerNumber { get; set; }
        public int? DsmartInvoiceId { get; set; }
        public bool IsProcess { get; set; }
        public string DsmartMusteriNo { get; set; }
        public bool IsCancelCustomer { get; set; }
        public int? BatchId { get; set; }
    }
}
