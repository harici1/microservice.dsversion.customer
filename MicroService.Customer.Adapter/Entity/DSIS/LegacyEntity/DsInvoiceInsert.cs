﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceInsert
    {
        public int RowId { get; set; }
        public int LeasingContractId { get; set; }
        public string LeasingContractNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string InsertCode { get; set; }
        public int? InvoiceId { get; set; }
    }
}
