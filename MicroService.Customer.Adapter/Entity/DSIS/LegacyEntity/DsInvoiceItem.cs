﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceItem
    {
        public DsInvoiceItem()
        {
            DsAccProduct = new HashSet<DsAccProduct>();
            DsExpenseBillLine = new HashSet<DsExpenseBillLine>();
            DsProduct = new HashSet<DsProduct>();
            VendorProduct = new HashSet<VendorProduct>();
        }

        public int InvoiceItemId { get; set; }
        public string Name { get; set; }
        public decimal NetPrice { get; set; }
        public decimal TaxRate1 { get; set; }
        public decimal TaxAmount1 { get; set; }
        public decimal TaxRate2 { get; set; }
        public decimal TaxAmount2 { get; set; }
        public decimal Price { get; set; }
        public int SystemCode { get; set; }
        public string InvoiceItemAttribute1 { get; set; }
        public string InvoiceItemAttribute2 { get; set; }
        public bool IsGroupItem { get; set; }
        public bool IsSaleStb { get; set; }
        public string Description { get; set; }
        public bool IsDistrictInvoice { get; set; }
        public int? ExtraInvoiceItemId { get; set; }
        public bool? IsContent { get; set; }
        public int? InvoiceItemCategoryId { get; set; }
        public string ShortDescription { get; set; }
        public int? InvoiceItemTypeId { get; set; }
        public int? InvoiceItemDomainId { get; set; }
        public int? InvoiceItemPrintCategoryId { get; set; }
        public short? Priority { get; set; }
        public byte? StarCount { get; set; }

        public virtual ICollection<DsAccProduct> DsAccProduct { get; set; }
        public virtual ICollection<DsExpenseBillLine> DsExpenseBillLine { get; set; }
        public virtual ICollection<DsProduct> DsProduct { get; set; }
        public virtual ICollection<VendorProduct> VendorProduct { get; set; }
    }
}
