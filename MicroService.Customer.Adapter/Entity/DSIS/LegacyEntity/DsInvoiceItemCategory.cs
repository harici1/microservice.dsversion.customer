﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceItemCategory
    {
        public int InvoiceItemCategoryId { get; set; }
        public string Name { get; set; }
    }
}
