﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceLateFee
    {
        public int InvoiceLateFeeId { get; set; }
        public int? InvoiceId { get; set; }
        public int RelatedInvoiceId { get; set; }
        public int ContractInstallmentId { get; set; }
        public decimal LateFeeAmount { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
