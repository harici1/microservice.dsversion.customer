﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceLateFeeNew
    {
        public int InvoiceLateFeeId { get; set; }
        public int? InvoiceId { get; set; }
        public int? RelatedInvoiceId { get; set; }
        public int? ContractInstallmentId { get; set; }
        public decimal? LateFeeAmount { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? ErrorLeasingContratcId { get; set; }
        public string InstallmentCountLateınvoiceCount { get; set; }
    }
}
