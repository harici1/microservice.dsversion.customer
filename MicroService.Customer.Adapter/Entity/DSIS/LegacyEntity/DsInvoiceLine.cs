﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceLine
    {
        public DsInvoiceLine()
        {
            DsInvoiceLineToContractInstallment = new HashSet<DsInvoiceLineToContractInstallment>();
            DsInvoiceLineToOrder = new HashSet<DsInvoiceLineToOrder>();
        }

        public int InvoiceLineId { get; set; }
        public int InvoiceId { get; set; }
        public int? InvoiceItemId { get; set; }
        public string Description { get; set; }
        public decimal NetAmount { get; set; }
        public decimal TaxRate1 { get; set; }
        public decimal TaxAmount1 { get; set; }
        public decimal TaxRate2 { get; set; }
        public decimal TaxAmount2 { get; set; }
        public decimal Amount { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? DocumentId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int NumberOfProduct { get; set; }
        public int? TempId { get; set; }

        public virtual ICollection<DsInvoiceLineToContractInstallment> DsInvoiceLineToContractInstallment { get; set; }
        public virtual ICollection<DsInvoiceLineToOrder> DsInvoiceLineToOrder { get; set; }
    }
}
