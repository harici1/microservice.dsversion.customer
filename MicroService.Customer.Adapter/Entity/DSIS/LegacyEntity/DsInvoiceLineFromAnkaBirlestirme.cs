﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceLineFromAnkaBirlestirme
    {
        public int InvoiceLineFromAnkaId { get; set; }
        public int? Id { get; set; }
        public int? OpenInvoiceId { get; set; }
        public long? RowNumber { get; set; }
        public int? ContractId { get; set; }
        public int? InvoiceId { get; set; }
        public int? InvoiceDetailId { get; set; }
        public int? InvoiceAssessmentId { get; set; }
        public int? ContractDetailId { get; set; }
        public int? CatalogItemId { get; set; }
        public int? PricePlanDetailId { get; set; }
        public int? InvoiceCycleId { get; set; }
        public decimal? AmountBeforeTax { get; set; }
        public decimal? VatRate { get; set; }
        public decimal? VatAmount { get; set; }
        public decimal? OivRate { get; set; }
        public decimal? OivAmount { get; set; }
        public decimal? TotalAmount { get; set; }
        public string ItemName { get; set; }
        public string Description { get; set; }
        public decimal? NumericValue1 { get; set; }
        public string VarcharValue1 { get; set; }
        public string VarcharValue2 { get; set; }
        public DateTime? CreateDate { get; set; }
        public string SortOrder { get; set; }
        public DateTime? TransactionDate { get; set; }
        public int? PrintCycle { get; set; }
        public int? ParentContractDetailId { get; set; }
        public string ItemLevel { get; set; }
        public int? VirtualParentContractDetailId { get; set; }
        public DateTime? ContractDetailCreateDate { get; set; }
        public string Kok { get; set; }
        public decimal? KokTutar { get; set; }
        public decimal? KazancBedeli { get; set; }
        public int? SmileInvoiceId { get; set; }
    }
}
