﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceLineToContractInstallment
    {
        public int InvoiceLineToContractInstallmentId { get; set; }
        public int InvoiceLineId { get; set; }
        public int ContractInstallmentId { get; set; }
        public int? TempId { get; set; }

        public virtual DsLeasingContractInstallment ContractInstallment { get; set; }
        public virtual DsInvoiceLine InvoiceLine { get; set; }
    }
}
