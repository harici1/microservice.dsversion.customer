﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceLineToOrder
    {
        public int InvoiceLineToOrderId { get; set; }
        public int InvoiceLineId { get; set; }
        public int OrderId { get; set; }
        public bool? DoubleReplace { get; set; }
        public bool? DoubleReplaceTemp { get; set; }
        public int? DoubleSentOrderId { get; set; }

        public virtual DsInvoiceLine InvoiceLine { get; set; }
        public virtual DsOrder Order { get; set; }
    }
}
