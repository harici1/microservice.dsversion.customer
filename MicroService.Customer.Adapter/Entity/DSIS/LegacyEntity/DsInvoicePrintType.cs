﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoicePrintType
    {
        public int PrintTypeId { get; set; }
        public string Description { get; set; }
    }
}
