﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceProcessSchedulePrepaid
    {
        public int InvoiceProcessSchedulePrepaidId { get; set; }
        public string SmsDataAssemblyType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime CorporationId { get; set; }
        public DateTime CorporationName { get; set; }
        public bool IsCreditCardPayment { get; set; }
        public int PaymentTypeId { get; set; }
        public string PaymentType { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public int PartyTypeId { get; set; }
        public bool InvoiceStatus { get; set; }
    }
}
