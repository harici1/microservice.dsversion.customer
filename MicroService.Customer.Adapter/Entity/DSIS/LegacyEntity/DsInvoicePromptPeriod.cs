﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoicePromptPeriod
    {
        public int InvoicePromptPeriodId { get; set; }
        public string Name { get; set; }
        public int DayLength { get; set; }
        public int SortColumn { get; set; }
    }
}
