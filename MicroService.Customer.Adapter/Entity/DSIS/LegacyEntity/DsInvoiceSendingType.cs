﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceSendingType
    {
        public int InvoiceSendingTypeId { get; set; }
        public string Name { get; set; }
        public bool IncludeEnvelope { get; set; }
    }
}
