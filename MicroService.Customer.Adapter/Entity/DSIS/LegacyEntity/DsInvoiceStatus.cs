﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceStatus
    {
        public int InvoiceStatusId { get; set; }
        public string Name { get; set; }
    }
}
