﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceToBankChargeDetail
    {
        public int InvoiceToBankChargeDetailId { get; set; }
        public int StatusId { get; set; }
        public int BankAccountId { get; set; }
        public string InvoiceNumber { get; set; }
        public string LeasingContractNumber { get; set; }
        public decimal Amount { get; set; }
        public int InvoiceToBankFileId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ChargeDate { get; set; }
        public DateTime? DueDate { get; set; }
        public string BranchOffice { get; set; }
        public string Message { get; set; }
        public int? BankCollectionTypeId { get; set; }
        public string BankTransactionId { get; set; }
        public string ReactivateForDebitMessage { get; set; }
        public bool? ReactivateForDebit { get; set; }

        public virtual DsBankAccount BankAccount { get; set; }
        public virtual DsInvoiceToBankCollectionType BankCollectionType { get; set; }
        public virtual DsInvoiceToBankFile InvoiceToBankFile { get; set; }
        public virtual DsInvoiceToBankChargeDetailStatus Status { get; set; }
    }
}
