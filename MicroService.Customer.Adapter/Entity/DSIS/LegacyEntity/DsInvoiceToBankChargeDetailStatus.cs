﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceToBankChargeDetailStatus
    {
        public DsInvoiceToBankChargeDetailStatus()
        {
            DsInvoiceToBankChargeDetail = new HashSet<DsInvoiceToBankChargeDetail>();
        }

        public int InvoiceToBankChargeDetailStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsInvoiceToBankChargeDetail> DsInvoiceToBankChargeDetail { get; set; }
    }
}
