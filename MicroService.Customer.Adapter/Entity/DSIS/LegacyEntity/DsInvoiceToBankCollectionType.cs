﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceToBankCollectionType
    {
        public DsInvoiceToBankCollectionType()
        {
            DsInvoiceToBankChargeDetail = new HashSet<DsInvoiceToBankChargeDetail>();
            DsLeasingContractPayment = new HashSet<DsLeasingContractPayment>();
        }

        public int InvoiceToBankCollectionTypeId { get; set; }
        public int BankAccountId { get; set; }
        public string BankCollectionCode { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsInvoiceToBankChargeDetail> DsInvoiceToBankChargeDetail { get; set; }
        public virtual ICollection<DsLeasingContractPayment> DsLeasingContractPayment { get; set; }
    }
}
