﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceToBankDebitDetailHistory
    {
        public int Id { get; set; }
        public int InvoiceToBankDebitDetailId { get; set; }
        public int InvoiceId { get; set; }
        public string InvoiceNumber { get; set; }
        public int LeasingContractId { get; set; }
        public string LeasingContractNumber { get; set; }
        public DateTime DueDate { get; set; }
        public decimal Amount { get; set; }
        public string Name { get; set; }
        public string FamilyName { get; set; }
        public DateTime ToProcessDate { get; set; }
        public int InvoiceToBankFileId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime HistoryDate { get; set; }
    }
}
