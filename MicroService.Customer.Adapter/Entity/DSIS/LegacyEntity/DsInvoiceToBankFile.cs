﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceToBankFile
    {
        public DsInvoiceToBankFile()
        {
            DsInvoiceToBankChargeDetail = new HashSet<DsInvoiceToBankChargeDetail>();
            DsInvoiceToBankDebitDetail = new HashSet<DsInvoiceToBankDebitDetail>();
        }

        public int InvoiceToBankFileId { get; set; }
        public string Name { get; set; }
        public int? BankAccountId { get; set; }
        public int? ProcessedItemCount { get; set; }
        public decimal? TotalAmount { get; set; }
        public int ToProcessItemCount { get; set; }
        public DateTime ToProcessDate { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public string FileName { get; set; }
        public int? ProcessTypeId { get; set; }
        public int? LogTypeId { get; set; }
        public string ReturnMessage { get; set; }

        public virtual ICollection<DsInvoiceToBankChargeDetail> DsInvoiceToBankChargeDetail { get; set; }
        public virtual ICollection<DsInvoiceToBankDebitDetail> DsInvoiceToBankDebitDetail { get; set; }
    }
}
