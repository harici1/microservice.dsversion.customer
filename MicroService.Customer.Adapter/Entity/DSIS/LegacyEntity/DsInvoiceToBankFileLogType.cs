﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceToBankFileLogType
    {
        public int LogTypeId { get; set; }
        public string Name { get; set; }
    }
}
