﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceToBankFileProcessType
    {
        public int ProccessTypeId { get; set; }
        public string Name { get; set; }
    }
}
