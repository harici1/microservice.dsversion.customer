﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceToNumberOfInstallment
    {
        public int InvoicePaymentNumberOfInstallmentId { get; set; }
        public int InvoiceId { get; set; }
        public int NumberOfInstallment { get; set; }
    }
}
