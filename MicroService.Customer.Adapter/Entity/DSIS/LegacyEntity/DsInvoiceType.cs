﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsInvoiceType
    {
        public int InvoiceTypeId { get; set; }
        public string Name { get; set; }
    }
}
