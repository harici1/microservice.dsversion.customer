﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsIsp
    {
        public int IspId { get; set; }
        public string Name { get; set; }
        public int Code { get; set; }
    }
}
