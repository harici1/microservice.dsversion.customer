﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsIvrAgentCallTransaction
    {
        public long Id { get; set; }
        public int RouterCallKey { get; set; }
        public int RouterCallKeyDay { get; set; }
        public string AgentLoginName { get; set; }
        public long AgentId { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime CallStartDate { get; set; }
        public DateTime? CallEndDate { get; set; }
        public string Io { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public string SkillTypeName { get; set; }
    }
}
