﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsIvrRequestLog
    {
        public int IvrRequestLogId { get; set; }
        public int? IvrRequestId { get; set; }
        public int? IvrRequestStatusId { get; set; }
        public string MobilePhoneNumber { get; set; }
        public long? SmartCardShortSerialNumber { get; set; }
        public int? ActivationId { get; set; }
        public int? CustomerId { get; set; }
        public int? IvrRequestLogTypeId { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? DocumentId { get; set; }
    }
}
