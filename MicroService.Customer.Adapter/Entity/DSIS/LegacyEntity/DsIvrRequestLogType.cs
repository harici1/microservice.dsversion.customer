﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsIvrRequestLogType
    {
        public int IvrRequestLogTypeId { get; set; }
        public string Name { get; set; }
    }
}
