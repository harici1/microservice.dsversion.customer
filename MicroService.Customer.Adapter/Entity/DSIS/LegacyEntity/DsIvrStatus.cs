﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsIvrStatus
    {
        public int IvrStatusId { get; set; }
        public string Name { get; set; }
    }
}
