﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsIvrWebServiceLog
    {
        public int IvrWebServiceLogId { get; set; }
        public string IvrMethodName { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
