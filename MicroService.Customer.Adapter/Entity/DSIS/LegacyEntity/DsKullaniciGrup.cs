﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsKullaniciGrup
    {
        public int GrupId { get; set; }
        public string GrupAdi { get; set; }
        public byte? GrupTuru { get; set; }
        public byte? OrtamTuru { get; set; }
        public string CagriHavaleEdilebilir { get; set; }
        public string Aktif { get; set; }
        public int OrganizationId { get; set; }
        public int? MaxResponseTimeInMinute { get; set; }
        public string MaxResponseTimeDescription { get; set; }
        public int TeamLeaderGroupId { get; set; }
        public int WorkOrderReturnRoleId { get; set; }
    }
}
