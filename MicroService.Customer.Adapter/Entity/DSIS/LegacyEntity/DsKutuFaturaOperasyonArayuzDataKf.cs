﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsKutuFaturaOperasyonArayuzDataKf
    {
        public int Id { get; set; }
        public int? LeasingContractNumber { get; set; }
        public decimal? KutuBedeli { get; set; }
        public bool? IsOk { get; set; }
    }
}
