﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLawyer
    {
        public int LawyerId { get; set; }
        public string Name { get; set; }
        public string LawFirm { get; set; }
        public string PhoneNumber1 { get; set; }
        public string PhoneNumber2 { get; set; }
        public int? UserId { get; set; }
    }
}
