﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContract
    {
        public DsLeasingContract()
        {
            DsBonuseski = new HashSet<DsBonuseski>();
            DsInvoiceToBankDebitDetail = new HashSet<DsInvoiceToBankDebitDetail>();
            DsLeasingContractChangeLog = new HashSet<DsLeasingContractChangeLog>();
            DsLeasingContractDetail = new HashSet<DsLeasingContractDetail>();
            DsLeasingContractDetailLog = new HashSet<DsLeasingContractDetailLog>();
            DsLeasingContractInstallment = new HashSet<DsLeasingContractInstallment>();
        }

        public int LeasingContractId { get; set; }
        public int PartyId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string LeasingContractNumber { get; set; }
        public int LeasingContractStatusId { get; set; }
        public int LeasingContractTypeId { get; set; }
        public int? NumberOfInstallment { get; set; }
        public decimal? ContractAmount { get; set; }
        public decimal? PeriodPaymentAmount { get; set; }
        public decimal? ServiceCost { get; set; }
        public int? VposId { get; set; }
        public string MobilePaymentGsmNo { get; set; }
        public int DocumentTypeId { get; set; }
        public int DocumentId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? SmartCardId { get; set; }
        public int? PaymentTypeId { get; set; }
        public int? LeasingContractLocationId { get; set; }
        public int? LeasingContractDeliveryStatusId { get; set; }
        public int? FormLastUpdatedById { get; set; }
        public bool? IdentificationDocumentIsOk { get; set; }
        public bool? SignatureIsOk { get; set; }
        public bool? PaymentInstructionIsOk { get; set; }
        public bool? ContractDocumentIsOk { get; set; }
        public bool? OtherDocumentIsOk { get; set; }
        public DateTime? ContractDeliveryDate { get; set; }
        public bool? IsNewVer { get; set; }
        public bool? DirectDebit { get; set; }
        public string Barcode { get; set; }
        public string Note { get; set; }
        public DateTime? CancelDate { get; set; }
        public int? LeasingContractArchiveStatusId { get; set; }
        public bool? WaitingForCancel { get; set; }
        public bool? CancelIsOk { get; set; }
        public bool? ContractViolationPenalty { get; set; }
        public bool? ContractViolationInvoiceIsOk { get; set; }
        public int? ActivationId { get; set; }
        public byte[] CardNumberEncrypt { get; set; }
        public byte[] ExpirationMonthEncrypt { get; set; }
        public byte[] ExpirationYearEncrypt { get; set; }
        public byte[] BnameEncrypt { get; set; }
        public int? DiscountAvailable { get; set; }
        public int? NoFinalInvoiceReasonId { get; set; }
        public DateTime? CancelRequestDate { get; set; }
        public int? ParentLeasingContractId { get; set; }
        public bool? LegitimateProceeding { get; set; }
        public DateTime? WrittenCancelRequestDate { get; set; }
        public string ExternalCustomerAccountCode { get; set; }
        public int? LeasingContractChangeLogTypeId { get; set; }
        public int? LastOrderId { get; set; }
        public int? LastOrderTypeId { get; set; }

        public virtual DsLeasingContractArchiveStatus LeasingContractArchiveStatus { get; set; }
        public virtual DsLeasingContractStatus LeasingContractStatus { get; set; }
        public virtual DsLeasingContractType LeasingContractType { get; set; }
        public virtual DsParty Party { get; set; }
        public virtual DsVposServiceProvider Vpos { get; set; }
        public virtual ICollection<DsBonuseski> DsBonuseski { get; set; }
        public virtual ICollection<DsInvoiceToBankDebitDetail> DsInvoiceToBankDebitDetail { get; set; }
        public virtual ICollection<DsLeasingContractChangeLog> DsLeasingContractChangeLog { get; set; }
        public virtual ICollection<DsLeasingContractDetail> DsLeasingContractDetail { get; set; }
        public virtual ICollection<DsLeasingContractDetailLog> DsLeasingContractDetailLog { get; set; }
        public virtual ICollection<DsLeasingContractInstallment> DsLeasingContractInstallment { get; set; }
    }
}
