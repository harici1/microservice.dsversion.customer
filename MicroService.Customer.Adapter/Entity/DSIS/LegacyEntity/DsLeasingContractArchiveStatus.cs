﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractArchiveStatus
    {
        public DsLeasingContractArchiveStatus()
        {
            DsLeasingContract = new HashSet<DsLeasingContract>();
        }

        public int LeasingContractArchiveStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsLeasingContract> DsLeasingContract { get; set; }
    }
}
