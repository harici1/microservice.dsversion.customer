﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractBarcode
    {
        public int BarcodeId { get; set; }
        public string LotNumber { get; set; }
        public string BarcodeNumber { get; set; }
    }
}
