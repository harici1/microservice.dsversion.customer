﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractCancelExceptionLog
    {
        public int LeasingContractCancelExceptionLogId { get; set; }
        public int CustomerId { get; set; }
        public int? LeasingContractId { get; set; }
        public string ExternalCustomerAccountCode { get; set; }
        public int OrderId { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public string ExceptionMessage { get; set; }
        public bool Throw { get; set; }
        public int? OrderTypeId { get; set; }
    }
}
