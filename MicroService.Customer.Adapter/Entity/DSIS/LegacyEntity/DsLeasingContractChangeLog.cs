﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractChangeLog
    {
        public int LeasingContractChangeLogId { get; set; }
        public int? LeasingContractId { get; set; }
        public int? PartyId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string LeasingContractNumber { get; set; }
        public int? NumberOfInstallment { get; set; }
        public decimal? ContractAmount { get; set; }
        public decimal? PeriodPaymentAmount { get; set; }
        public int? VposId { get; set; }
        public string MobilePaymentGsmNo { get; set; }
        public int? LeasingContractDocumentTypeId { get; set; }
        public int? LeasingContractDocumentId { get; set; }
        public int? LeasingContractTypeId { get; set; }
        public int? LeasingContractStatusId { get; set; }
        public int? TypeId { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public string OldValues { get; set; }
        public string NewValues { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? CreationDate { get; set; }
        public decimal? ServiceCost { get; set; }
        public int? CampaignId { get; set; }
        public int? LeasingContractDeliveryStatusId { get; set; }
        public int? ArrangedByPartyId { get; set; }
        public int? LeasingContractCancelReasonId { get; set; }
        public int? LeasingContractLocationId { get; set; }
        public int? PaymentTypeId { get; set; }
        public byte[] CardNumberEncrypt { get; set; }
        public byte[] ExpirationMonthEncrypt { get; set; }
        public byte[] ExpirationYearEncrypt { get; set; }
        public byte[] BnameEncrypt { get; set; }
        public int? DiscountAvailable { get; set; }
        public int? ActivationId { get; set; }
        public int? ParentLeasingContractId { get; set; }
        public DateTime? CancelRequestDate { get; set; }
        public DateTime? WrittenCancelRequestDate { get; set; }
        public DateTime? CancelDate { get; set; }
        public bool? LegitimateProceeding { get; set; }
        public string ExternalCustomerAccountCode { get; set; }

        public virtual DsLeasingContract LeasingContract { get; set; }
        public virtual DsLeasingContractChangeLogType Type { get; set; }
    }
}
