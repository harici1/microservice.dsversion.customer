﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractChangeLogType
    {
        public DsLeasingContractChangeLogType()
        {
            DsLeasingContractChangeLog = new HashSet<DsLeasingContractChangeLog>();
        }

        public int LeasingContractChangeLogTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<DsLeasingContractChangeLog> DsLeasingContractChangeLog { get; set; }
    }
}
