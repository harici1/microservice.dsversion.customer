﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractDeliveryStatus
    {
        public DsLeasingContractDeliveryStatus()
        {
            DsLeasingContractLocation = new HashSet<DsLeasingContractLocation>();
        }

        public int LeasingContractDeliveryStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsLeasingContractLocation> DsLeasingContractLocation { get; set; }
    }
}
