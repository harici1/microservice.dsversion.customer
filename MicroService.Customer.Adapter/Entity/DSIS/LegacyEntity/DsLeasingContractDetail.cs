﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractDetail
    {
        public DsLeasingContractDetail()
        {
            DsLeasingContractDetailLog = new HashSet<DsLeasingContractDetailLog>();
        }

        public int LeasingContractDetailId { get; set; }
        public int LeasingContractId { get; set; }
        public int LeasingContractDetailTypeId { get; set; }
        public string Value { get; set; }
        public bool Cancelled { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public int LastUpdatedById { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? LastDocumentId { get; set; }
        public int? LastDocumentTypeId { get; set; }

        public virtual DsLeasingContract LeasingContract { get; set; }
        public virtual DsLeasingContractDetailType LeasingContractDetailType { get; set; }
        public virtual ICollection<DsLeasingContractDetailLog> DsLeasingContractDetailLog { get; set; }
    }
}
