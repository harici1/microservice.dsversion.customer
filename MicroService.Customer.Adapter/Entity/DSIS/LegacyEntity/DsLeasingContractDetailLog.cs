﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractDetailLog
    {
        public int LeasingContractDetailLogId { get; set; }
        public int LeasingContractDetailId { get; set; }
        public int LeasingContractId { get; set; }
        public int LeasingContractDetailTypeId { get; set; }
        public string Value { get; set; }
        public bool Cancelled { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }

        public virtual DsLeasingContract LeasingContract { get; set; }
        public virtual DsLeasingContractDetail LeasingContractDetail { get; set; }
        public virtual DsLeasingContractDetailType LeasingContractDetailType { get; set; }
    }
}
