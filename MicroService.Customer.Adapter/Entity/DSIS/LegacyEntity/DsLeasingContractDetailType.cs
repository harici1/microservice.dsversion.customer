﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractDetailType
    {
        public DsLeasingContractDetailType()
        {
            DsLeasingContractDetail = new HashSet<DsLeasingContractDetail>();
            DsLeasingContractDetailLog = new HashSet<DsLeasingContractDetailLog>();
        }

        public int LeasingContractDetailTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsLeasingContractDetail> DsLeasingContractDetail { get; set; }
        public virtual ICollection<DsLeasingContractDetailLog> DsLeasingContractDetailLog { get; set; }
    }
}
