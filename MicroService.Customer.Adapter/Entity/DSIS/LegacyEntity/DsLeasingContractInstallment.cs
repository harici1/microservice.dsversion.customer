﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractInstallment
    {
        public DsLeasingContractInstallment()
        {
            DsInvoiceLineToContractInstallment = new HashSet<DsInvoiceLineToContractInstallment>();
            DsLeasingContractServiceBonus = new HashSet<DsLeasingContractServiceBonus>();
            DsWorkOrderCancelStbLeasingInvoiceItem = new HashSet<DsWorkOrderCancelStbLeasingInvoiceItem>();
        }

        public int LeasingContractInstallmentId { get; set; }
        public int LeasingContractId { get; set; }
        public int LeasingContractInstallmentTypeId { get; set; }
        public int LeasingContractInstallmentStatusId { get; set; }
        public int? ProductId { get; set; }
        public int? InstallmentNo { get; set; }
        public DateTime PaymentDate { get; set; }
        public DateTime? PaymentChargeDate { get; set; }
        public DateTime? PaymentPeriodStart { get; set; }
        public DateTime? PaymentPeriodEnd { get; set; }
        public DateTime? LastProcessDate { get; set; }
        public DateTime? CancelDate { get; set; }
        public decimal Price { get; set; }
        public decimal? Balance { get; set; }
        public decimal? ServiceCost { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int FailedProcessCount { get; set; }
        public int? PaymentTypeId { get; set; }
        public int? VposId { get; set; }
        public int? BankAccountId { get; set; }
        public int? BankCollectionTypeId { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? RelatedInstallmentId { get; set; }
        public int? PaymentId { get; set; }
        public int? CampaignId { get; set; }
        public int? ReferrerInstallment { get; set; }
        public bool? IsOk { get; set; }
        public int? OldProductId { get; set; }
        public string Notes { get; set; }
        public int? Id { get; set; }
        public decimal? TempNewPrice { get; set; }
        public bool? Processed { get; set; }
        public int? LeasingContractServiceId { get; set; }
        public int? StbTypeId { get; set; }
        public int? RelatedProductId { get; set; }
        public int? SmileInvoiceDetailId { get; set; }
        public bool? IsCanceled { get; set; }
        public int? MainLeasingContractId { get; set; }
        public int? LeasingContractInstallmentGroupId { get; set; }

        public virtual DsLeasingContract LeasingContract { get; set; }
        public virtual DsLeasingContractInstallmentStatus LeasingContractInstallmentStatus { get; set; }
        public virtual ICollection<DsInvoiceLineToContractInstallment> DsInvoiceLineToContractInstallment { get; set; }
        public virtual ICollection<DsLeasingContractServiceBonus> DsLeasingContractServiceBonus { get; set; }
        public virtual ICollection<DsWorkOrderCancelStbLeasingInvoiceItem> DsWorkOrderCancelStbLeasingInvoiceItem { get; set; }
    }
}
