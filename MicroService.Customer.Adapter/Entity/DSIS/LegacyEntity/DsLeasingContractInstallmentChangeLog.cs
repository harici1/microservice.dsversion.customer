﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractInstallmentChangeLog
    {
        public int LeasingContractInstallmentChangeLogId { get; set; }
        public int LeasingContractInstallmentId { get; set; }
        public int TypeId { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }

        public virtual DsLeasingContractInstallmentChangeLogType Type { get; set; }
    }
}
