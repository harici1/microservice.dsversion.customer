﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractInstallmentChangeLogType
    {
        public DsLeasingContractInstallmentChangeLogType()
        {
            DsLeasingContractInstallmentChangeLog = new HashSet<DsLeasingContractInstallmentChangeLog>();
        }

        public int LeasingContractInstallmentChangeLogTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsLeasingContractInstallmentChangeLog> DsLeasingContractInstallmentChangeLog { get; set; }
    }
}
