﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractInstallmentStatus
    {
        public DsLeasingContractInstallmentStatus()
        {
            DsLeasingContractInstallment = new HashSet<DsLeasingContractInstallment>();
        }

        public int LeasingContractInstallmentStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsLeasingContractInstallment> DsLeasingContractInstallment { get; set; }
    }
}
