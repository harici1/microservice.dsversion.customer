﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractInstallmentType
    {
        public DsLeasingContractInstallmentType()
        {
            DsLeasingContractInstallmentTypeToProduct = new HashSet<DsLeasingContractInstallmentTypeToProduct>();
        }

        public int InstallmentTypeId { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public string AuthDebit { get; set; }
        public string Description { get; set; }
        public bool Cancelable { get; set; }

        public virtual DsLeasingContractInstallmentTypeCategory Category { get; set; }
        public virtual ICollection<DsLeasingContractInstallmentTypeToProduct> DsLeasingContractInstallmentTypeToProduct { get; set; }
    }
}
