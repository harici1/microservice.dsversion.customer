﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractInstallmentTypeCategory
    {
        public DsLeasingContractInstallmentTypeCategory()
        {
            DsLeasingContractInstallmentType = new HashSet<DsLeasingContractInstallmentType>();
        }

        public int LeasingContractInstallmentTypeCategoryId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsLeasingContractInstallmentType> DsLeasingContractInstallmentType { get; set; }
    }
}
