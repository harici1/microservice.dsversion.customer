﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractInstallmentTypeToProduct
    {
        public int LeasingContractInstallmentTypeToProductId { get; set; }
        public int ProductId { get; set; }
        public int LeasingContractInstallmentTypeId { get; set; }
        public int LeasingContractServiceTypeId { get; set; }

        public virtual DsLeasingContractInstallmentType LeasingContractInstallmentType { get; set; }
        public virtual DsProduct Product { get; set; }
    }
}
