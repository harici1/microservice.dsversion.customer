﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractInvoiceBonus
    {
        public int LeasingContractInvoiceBonusId { get; set; }
        public int LeasingContractId { get; set; }
        public decimal Amount { get; set; }
        public DateTime BonusDate { get; set; }
    }
}
