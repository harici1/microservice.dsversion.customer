﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractLocation
    {
        public int LeasingContractLocationId { get; set; }
        public string Name { get; set; }
        public int LeasingContractDeliveryStatusId { get; set; }
        public bool FieldsEnable { get; set; }

        public virtual DsLeasingContractDeliveryStatus LeasingContractDeliveryStatus { get; set; }
    }
}
