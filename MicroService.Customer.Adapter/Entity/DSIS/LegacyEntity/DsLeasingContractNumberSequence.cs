﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractNumberSequence
    {
        public int SequenceId { get; set; }
    }
}
