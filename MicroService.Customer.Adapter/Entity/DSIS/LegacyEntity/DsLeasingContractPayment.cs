﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractPayment
    {
        public int LeasingContractPaymentId { get; set; }
        public int InvoiceId { get; set; }
        public int ContractInstallmentId { get; set; }
        public decimal Amount { get; set; }
        public int? VposId { get; set; }
        public int? BankId { get; set; }
        public int? PaymentTypeId { get; set; }
        public int? PaymentId { get; set; }
        public int? BankCollectionTypeId { get; set; }
        public DateTime? PaymentDate { get; set; }
        public bool? IsCancelled { get; set; }
        public DateTime? CancelDate { get; set; }
        public int? CancelDocumentId { get; set; }
        public int? CancelDocumentTypeId { get; set; }
        public int? CancelByUserId { get; set; }
        public bool? IsInvoiceClosePayment { get; set; }
        public int? LeasingContractPaymentStatusId { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public bool? IsBalance { get; set; }

        public virtual DsInvoiceToBankCollectionType BankCollectionType { get; set; }
        public virtual DsInvoice Invoice { get; set; }
    }
}
