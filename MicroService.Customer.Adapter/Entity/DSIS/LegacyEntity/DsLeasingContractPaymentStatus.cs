﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractPaymentStatus
    {
        public int LeasingContractPaymentStatusId { get; set; }
        public string Name { get; set; }
    }
}
