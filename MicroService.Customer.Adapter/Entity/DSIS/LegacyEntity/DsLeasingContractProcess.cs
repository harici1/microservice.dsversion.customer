﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractProcess
    {
        public int LeasingContractProcessId { get; set; }
        public int LeasingContractId { get; set; }
        public int LeasingContractProcessTypeId { get; set; }
        public int LeasingContractProcessStatusId { get; set; }
        public string Note { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? LastUpdatedById { get; set; }
    }
}
