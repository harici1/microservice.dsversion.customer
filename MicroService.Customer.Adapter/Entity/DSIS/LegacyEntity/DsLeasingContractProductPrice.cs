﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractProductPrice
    {
        public DsLeasingContractProductPrice()
        {
            DsLeasingContractProductPriceToStbType = new HashSet<DsLeasingContractProductPriceToStbType>();
            DsRoleTableConstraint = new HashSet<DsRoleTableConstraint>();
        }

        public int LeasingContractProductPriceId { get; set; }
        public int LeasingContractServiceTypeId { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public int CampaignId { get; set; }
        public int ProductId { get; set; }
        public int? StbTypeId { get; set; }
        public decimal Price { get; set; }
        public bool DirectDebitIsForced { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? ActivationLenghtInMonth { get; set; }
        public int? InstallmentNumber { get; set; }
        public int? BonusNumber { get; set; }
        public decimal? BonusAmount { get; set; }
        public int? BonusProductId { get; set; }
        public int? InvoiceDelayInMonth { get; set; }
        public int? OldCampaignId { get; set; }
        public string Note { get; set; }
        public string Note2 { get; set; }
        public bool IsPrepaid { get; set; }
        public bool IsVisibleToWeb { get; set; }
        public int ActivationLenght { get; set; }
        public int ActivationLenghtTypeId { get; set; }
        public decimal? PrepaidWebPrice { get; set; }
        public int? InitialPartialPriceBonusProduct { get; set; }

        public virtual DsStbType StbType { get; set; }
        public virtual ICollection<DsLeasingContractProductPriceToStbType> DsLeasingContractProductPriceToStbType { get; set; }
        public virtual ICollection<DsRoleTableConstraint> DsRoleTableConstraint { get; set; }
    }
}
