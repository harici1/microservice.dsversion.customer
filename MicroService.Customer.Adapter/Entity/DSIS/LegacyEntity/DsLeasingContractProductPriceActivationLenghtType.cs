﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractProductPriceActivationLenghtType
    {
        public int ActivationLenghtTypeId { get; set; }
        public string Name { get; set; }
    }
}
