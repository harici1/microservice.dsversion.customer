﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractProductPriceToStbType
    {
        public int LeasingContractProductPriceToStbTypeId { get; set; }
        public int LeasingContractProductPriceId { get; set; }
        public int StbTypeId { get; set; }

        public virtual DsLeasingContractProductPrice LeasingContractProductPrice { get; set; }
    }
}
