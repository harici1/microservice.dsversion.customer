﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractService
    {
        public DsLeasingContractService()
        {
            DsWorkOrderLeasingContractServiceGeneric = new HashSet<DsWorkOrderLeasingContractServiceGeneric>();
        }

        public int LeasingContractServiceId { get; set; }
        public int LeasingContractId { get; set; }
        public int LeasingContractServiceTypeId { get; set; }
        public int ProductId { get; set; }
        public int CampaignId { get; set; }
        public decimal Price { get; set; }
        public int LeasingContractServiceStatusId { get; set; }
        public int LeasingContractServiceLogTypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Notes { get; set; }
        public int? BonusProductId { get; set; }
        public decimal? BonusAmount { get; set; }
        public DateTime? BonusStartDate { get; set; }
        public DateTime? BonusEndDate { get; set; }
        public int? ActivationServiceId { get; set; }
        public DateTime? CancelDate { get; set; }
        public DateTime? SuspendDate { get; set; }
        public int? LeasingContractServiceStatusReasonId { get; set; }
        public int? TempId { get; set; }
        public bool? IsCancelable { get; set; }
        public DateTime? ContractStartDate { get; set; }
        public DateTime? ContractEndDate { get; set; }
        public int? ActivationId { get; set; }
        public int? InstallmentNumber { get; set; }
        public decimal? TotalInstallmentAmount { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public string HdDegistirStbNotes { get; set; }
        public int? StbTypeId { get; set; }
        public int? SmsopHddegistirStbprice { get; set; }
        public int? SmsopBundleBonusUpdate { get; set; }
        public int? NoFinalInvoiceReasonId { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public int? LastOrderId { get; set; }
        public int? LastOrderTypeId { get; set; }
        public int? ParentLeasingContractServiceId { get; set; }

        public virtual DsLeasingContractServiceStatus LeasingContractServiceStatus { get; set; }
        public virtual DsProduct Product { get; set; }
        public virtual ICollection<DsWorkOrderLeasingContractServiceGeneric> DsWorkOrderLeasingContractServiceGeneric { get; set; }
    }
}
