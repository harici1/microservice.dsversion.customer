﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractServiceBonus
    {
        public DsLeasingContractServiceBonus()
        {
            DsWorkOrderCancelStbLeasingInvoiceItem = new HashSet<DsWorkOrderCancelStbLeasingInvoiceItem>();
        }

        public int LeasingContractServiceBonusId { get; set; }
        public int LeasingContractServiceId { get; set; }
        public int BonusProductId { get; set; }
        public int? ContractInstallmentId { get; set; }
        public decimal Amount { get; set; }
        public int CampaignId { get; set; }
        public DateTime PaymentDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }

        public virtual DsLeasingContractInstallment ContractInstallment { get; set; }
        public virtual ICollection<DsWorkOrderCancelStbLeasingInvoiceItem> DsWorkOrderCancelStbLeasingInvoiceItem { get; set; }
    }
}
