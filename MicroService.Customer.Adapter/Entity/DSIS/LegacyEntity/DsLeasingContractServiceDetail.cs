﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractServiceDetail
    {
        public int LeasingContractServiceDetailId { get; set; }
        public int LeasingContractServiceId { get; set; }
        public int LeasingContractServiceDetailTypeId { get; set; }
        public string Value { get; set; }
        public DateTime CreateDate { get; set; }
        public int CreatedById { get; set; }
        public bool Cancelled { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? UpdatedById { get; set; }
    }
}
