﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractServiceDetailType
    {
        public int LeasingContractServiceDetailTypeId { get; set; }
        public string Name { get; set; }
    }
}
