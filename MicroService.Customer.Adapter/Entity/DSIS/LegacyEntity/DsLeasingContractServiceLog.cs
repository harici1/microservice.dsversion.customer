﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractServiceLog
    {
        public int LeasingContractServiceLogId { get; set; }
        public int LeasingContractServiceId { get; set; }
        public int LeasingContractServiceStatusId { get; set; }
        public int LeasingContractServiceLogTypeId { get; set; }
        public int? LeasingContractServiceTypeId { get; set; }
        public int ProductId { get; set; }
        public int CampaignId { get; set; }
        public decimal Price { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? ActivationServiceId { get; set; }
        public int? BonusProductId { get; set; }
        public decimal? BonusAmount { get; set; }
        public DateTime? BonusStartDate { get; set; }
        public DateTime? BonusEndDate { get; set; }
        public DateTime? ProcessDate { get; set; }
        public int DocumentId { get; set; }
        public int DocumentTypeId { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public int? LeasingContractServiceStatusReasonId { get; set; }
        public DateTime? ContractStartDate { get; set; }
        public DateTime? ContractEndDate { get; set; }
        public int? ActivationId { get; set; }
        public int? InstallmentNumber { get; set; }
        public decimal? TotalInstallmentAmount { get; set; }
        public bool? IsInvoiced { get; set; }

        public virtual DsProduct BonusProduct { get; set; }
        public virtual DsLeasingContractServiceLogType LeasingContractServiceLogType { get; set; }
        public virtual DsLeasingContractServiceStatus LeasingContractServiceStatus { get; set; }
        public virtual DsProduct Product { get; set; }
    }
}
