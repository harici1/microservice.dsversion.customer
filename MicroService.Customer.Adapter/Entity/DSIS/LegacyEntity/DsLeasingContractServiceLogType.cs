﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractServiceLogType
    {
        public DsLeasingContractServiceLogType()
        {
            DsLeasingContractServiceLog = new HashSet<DsLeasingContractServiceLog>();
        }

        public int LeasingContractServiceLogTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsLeasingContractServiceLog> DsLeasingContractServiceLog { get; set; }
    }
}
