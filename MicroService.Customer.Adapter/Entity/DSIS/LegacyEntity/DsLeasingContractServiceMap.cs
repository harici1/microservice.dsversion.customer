﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractServiceMap
    {
        public int LeasingContractServiceMapId { get; set; }
        public int LeasingContractServiceTypeId { get; set; }
        public int CampaignId { get; set; }
        public int ProductId { get; set; }
        public int StbTypeId { get; set; }
    }
}
