﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractServiceRaiseException
    {
        public int LeasingContractServiceRaiseExceptionId { get; set; }
        public int LeasingContractId { get; set; }
        public string Message { get; set; }
        public DateTime CreationDate { get; set; }
        public int Cycle { get; set; }
    }
}
