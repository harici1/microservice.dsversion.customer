﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractServiceRaiseRule
    {
        public int LeasingContractServiceRaiseRuleId { get; set; }
        public int? RuleProfileId { get; set; }
        public int RaiseTypeId { get; set; }
        public decimal RaiseAmount { get; set; }
        public decimal PriceRangeBegining { get; set; }
        public decimal PriceRangeEnding { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int LeasingContractTypeId { get; set; }
        public int StbOwner { get; set; }
        public int StbTypeId { get; set; }

        public virtual DsLeasingContractServiceRaiseType RaiseType { get; set; }
    }
}
