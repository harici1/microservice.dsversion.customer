﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractServiceRaiseRuleProfile
    {
        public int LeasingContractServiceRaiseRuleProfileId { get; set; }
        public int ProfileId { get; set; }
        public int PackageId { get; set; }
        public string Description { get; set; }
    }
}
