﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractServiceRaiseRuleProfileOperator
    {
        public int LeasingContractServiceRaiseRuleProfileOperatorId { get; set; }
        public string Operator { get; set; }
    }
}
