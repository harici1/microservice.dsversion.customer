﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractServiceRaiseRuleProfilePackage
    {
        public int Id { get; set; }
        public int ProfilePackageId { get; set; }
        public string Value { get; set; }
        public int OperatorId { get; set; }
        public int ProfileTypeId { get; set; }

        public virtual DsLeasingContractServiceRaiseRuleProfileType ProfileType { get; set; }
    }
}
