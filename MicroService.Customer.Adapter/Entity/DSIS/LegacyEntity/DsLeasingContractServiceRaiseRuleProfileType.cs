﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractServiceRaiseRuleProfileType
    {
        public DsLeasingContractServiceRaiseRuleProfileType()
        {
            DsLeasingContractServiceRaiseRuleProfilePackage = new HashSet<DsLeasingContractServiceRaiseRuleProfilePackage>();
        }

        public int LeasingContractServiceRaiseRuleProfileTypeId { get; set; }
        public string Name { get; set; }
        public string ColumnName { get; set; }
        public bool LogExceptions { get; set; }

        public virtual ICollection<DsLeasingContractServiceRaiseRuleProfilePackage> DsLeasingContractServiceRaiseRuleProfilePackage { get; set; }
    }
}
