﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractServiceRaiseStatus
    {
        public DsLeasingContractServiceRaiseStatus()
        {
            DsLeasingContractServiceRaiseTransaction = new HashSet<DsLeasingContractServiceRaiseTransaction>();
        }

        public int LeasingContractServiceRaiseStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsLeasingContractServiceRaiseTransaction> DsLeasingContractServiceRaiseTransaction { get; set; }
    }
}
