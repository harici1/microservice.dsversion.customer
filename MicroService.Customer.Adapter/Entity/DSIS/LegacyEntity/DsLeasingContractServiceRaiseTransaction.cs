﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractServiceRaiseTransaction
    {
        public int LeasingContractServiceRaiseTransactionId { get; set; }
        public int RaiseTypeId { get; set; }
        public decimal RaiseAmount { get; set; }
        public int LeasingContractId { get; set; }
        public DateTime ContractStartDate { get; set; }
        public DateTime ContractEndDate { get; set; }
        public int LeasingContractServiceId { get; set; }
        public int RaiseStatusId { get; set; }
        public int ProductId { get; set; }
        public int CampaignId { get; set; }
        public decimal CurrentPrice { get; set; }
        public decimal? CurrentBonusPrice { get; set; }
        public decimal NewPrice { get; set; }
        public decimal? NewBonusPrice { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public int LastUpdatedById { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public int Cycle { get; set; }
        public bool Disabled { get; set; }
        public int? RaiseOrderId { get; set; }
        public decimal? TotalDsisPrice { get; set; }
        public decimal? TotalAnkaPrice { get; set; }
        public int? AnkaCatalogItemId { get; set; }
        public int? AnkaCatalogItemCampaignId { get; set; }
        public DateTime? CyclePeriod { get; set; }
        public string Snapshot { get; set; }
        public int? RaiseDocumentTypeId { get; set; }

        public virtual DsLeasingContractServiceRaiseStatus RaiseStatus { get; set; }
        public virtual DsLeasingContractServiceRaiseType RaiseType { get; set; }
    }
}
