﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractServiceRaiseType
    {
        public DsLeasingContractServiceRaiseType()
        {
            DsLeasingContractServiceRaiseRule = new HashSet<DsLeasingContractServiceRaiseRule>();
            DsLeasingContractServiceRaiseTransaction = new HashSet<DsLeasingContractServiceRaiseTransaction>();
        }

        public int LeasingContractServiceRaiseTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsLeasingContractServiceRaiseRule> DsLeasingContractServiceRaiseRule { get; set; }
        public virtual ICollection<DsLeasingContractServiceRaiseTransaction> DsLeasingContractServiceRaiseTransaction { get; set; }
    }
}
