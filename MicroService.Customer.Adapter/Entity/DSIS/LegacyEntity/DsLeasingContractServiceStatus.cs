﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractServiceStatus
    {
        public DsLeasingContractServiceStatus()
        {
            DsLeasingContractService = new HashSet<DsLeasingContractService>();
            DsLeasingContractServiceLog = new HashSet<DsLeasingContractServiceLog>();
            DsLeasingContractServiceStatusReason = new HashSet<DsLeasingContractServiceStatusReason>();
        }

        public int LeasingContractServiceStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsLeasingContractService> DsLeasingContractService { get; set; }
        public virtual ICollection<DsLeasingContractServiceLog> DsLeasingContractServiceLog { get; set; }
        public virtual ICollection<DsLeasingContractServiceStatusReason> DsLeasingContractServiceStatusReason { get; set; }
    }
}
