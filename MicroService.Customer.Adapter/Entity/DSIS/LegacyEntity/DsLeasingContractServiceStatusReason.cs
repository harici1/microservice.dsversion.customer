﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractServiceStatusReason
    {
        public int LeasingContractServiceStatusReasonId { get; set; }
        public int LeasingContractServiceStatusId { get; set; }
        public int ProductId { get; set; }
        public string Name { get; set; }

        public virtual DsLeasingContractServiceStatus LeasingContractServiceStatus { get; set; }
        public virtual DsProduct Product { get; set; }
    }
}
