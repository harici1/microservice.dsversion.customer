﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractServiceType
    {
        public int LeasingContractServiceTypeId { get; set; }
        public string Name { get; set; }
        public bool InsertPartialPrice { get; set; }
        public bool GetPriceFromPriceTable { get; set; }
    }
}
