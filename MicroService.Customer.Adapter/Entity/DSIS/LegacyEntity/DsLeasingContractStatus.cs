﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractStatus
    {
        public DsLeasingContractStatus()
        {
            DsLeasingContract = new HashSet<DsLeasingContract>();
        }

        public int LeasingContractStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsLeasingContract> DsLeasingContract { get; set; }
    }
}
