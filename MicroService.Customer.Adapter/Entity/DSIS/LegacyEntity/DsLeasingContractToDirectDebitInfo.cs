﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractToDirectDebitInfo
    {
        public int LeasingContractToDirectDebitInfoId { get; set; }
        public int LeasingContractId { get; set; }
        public int OnlineBankId { get; set; }
        public int CreatedById { get; set; }
        public int LastUpdatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public bool IsActive { get; set; }
    }
}
