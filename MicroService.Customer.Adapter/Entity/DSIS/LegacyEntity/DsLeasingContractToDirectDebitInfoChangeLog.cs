﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractToDirectDebitInfoChangeLog
    {
        public int LeasingContractToDirectDebitInfoChangeLogId { get; set; }
        public int LeasingContractId { get; set; }
        public int OnlineBankId { get; set; }
        public DateTime CreationDate { get; set; }
        public bool IsActive { get; set; }
    }
}
