﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractToInvoice
    {
        public int LeasingContractToInvoiceId { get; set; }
        public int LeasingContractId { get; set; }
        public int BatchId { get; set; }
        public bool IsOk { get; set; }
        public bool IsOkToLateFee { get; set; }
        public bool IsOktoInstallment { get; set; }
        public bool IsSuspend { get; set; }
        public bool? NotInvoiced { get; set; }
        public bool InCancelProcess { get; set; }
        public bool IsCloseOpen { get; set; }
        public bool? IsUpgrade { get; set; }
        public bool DontInvoice { get; set; }
        public DateTime? InvoicePeriod { get; set; }
        public string Note { get; set; }
        public bool IsFreeze { get; set; }
        public string FaturaIslemGrubu { get; set; }
        public string FaturaIslemSebebi { get; set; }
        public string InvoiceType { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? CreatedById { get; set; }
        public int? IsStampDuty { get; set; }
        public int? CampaignId { get; set; }
        public int? MainLeasingContractId { get; set; }
        public int? IsParent { get; set; }
    }
}
