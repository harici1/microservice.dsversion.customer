﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractToInvoiceProcess
    {
        public int LeasingContractToInvoiceProcessId { get; set; }
        public bool InvoiceIsCreated { get; set; }
        public DateTime InvoicePeriodDate { get; set; }
        public string Description { get; set; }
        public bool IsSendEmailOk { get; set; }
    }
}
