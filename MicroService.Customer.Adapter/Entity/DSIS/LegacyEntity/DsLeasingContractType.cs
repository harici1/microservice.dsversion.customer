﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLeasingContractType
    {
        public DsLeasingContractType()
        {
            DsLeasingContract = new HashSet<DsLeasingContract>();
        }

        public int LeasingContractTypeId { get; set; }
        public string Name { get; set; }
        public bool IsBundle { get; set; }

        public virtual ICollection<DsLeasingContract> DsLeasingContract { get; set; }
    }
}
