﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLoginPolicy
    {
        public long LoginPolicyId { get; set; }
        public int UserId { get; set; }
        public int LoginPolicyTypeId { get; set; }
        public string Value { get; set; }
        public bool Enabled { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public virtual DsLoginPolicyType LoginPolicyType { get; set; }
        public virtual DsUser User { get; set; }
    }
}
