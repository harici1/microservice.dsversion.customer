﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsLoginPolicyType
    {
        public DsLoginPolicyType()
        {
            DsLoginPolicy = new HashSet<DsLoginPolicy>();
        }

        public int LoginPolicyTypeId { get; set; }
        public string Name { get; set; }
        public bool AllowedMultiple { get; set; }
        public string ValidationRegex { get; set; }
        public string ValidationRegexErrorMessage { get; set; }
        public string DefaultValue { get; set; }

        public virtual ICollection<DsLoginPolicy> DsLoginPolicy { get; set; }
    }
}
