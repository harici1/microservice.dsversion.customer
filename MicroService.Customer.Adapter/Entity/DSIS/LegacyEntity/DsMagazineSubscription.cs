﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsMagazineSubscription
    {
        public DsMagazineSubscription()
        {
            DsMagazineSubscriptionLog = new HashSet<DsMagazineSubscriptionLog>();
        }

        public int MagazineSubscriptionId { get; set; }
        public int CustomerId { get; set; }
        public int? ProductId { get; set; }
        public int MagazineSubscriptionTypeId { get; set; }
        public int MagazineSubscriptionLogTypeId { get; set; }
        public DateTime SubscriptionStartDate { get; set; }
        public DateTime SubscriptionEndDate { get; set; }
        public int DistrictId { get; set; }
        public string Address { get; set; }
        public string Note { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedByDocumentId { get; set; }
        public int CreatedByDocumentTypeId { get; set; }
        public int? CancelledByDocumentId { get; set; }
        public int? CancelledByDocumentTypeId { get; set; }
        public int? LastUpdateById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? NeighbourhoodId { get; set; }
        public int? ProvinceId { get; set; }
        public string ZipCode { get; set; }

        public virtual DsDistrict District { get; set; }
        public virtual DsMagazineSubscriptionLogType MagazineSubscriptionLogType { get; set; }
        public virtual DsMessageIdentificationType MagazineSubscriptionType { get; set; }
        public virtual ICollection<DsMagazineSubscriptionLog> DsMagazineSubscriptionLog { get; set; }
    }
}
