﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsMagazineSubscriptionLog
    {
        public int MagazineSubscriptionLogId { get; set; }
        public int MagazineSubscriptionId { get; set; }
        public int MagazineSubscriptionLogTypeId { get; set; }
        public int DocumentId { get; set; }
        public int DocumentTypeId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }

        public virtual DsMagazineSubscription MagazineSubscription { get; set; }
        public virtual DsMagazineSubscriptionLogType MagazineSubscriptionLogType { get; set; }
    }
}
