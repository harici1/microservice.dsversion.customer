﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsMagazineSubscriptionLogType
    {
        public DsMagazineSubscriptionLogType()
        {
            DsMagazineSubscription = new HashSet<DsMagazineSubscription>();
            DsMagazineSubscriptionLog = new HashSet<DsMagazineSubscriptionLog>();
        }

        public int MagazineSubscriptionLogTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsMagazineSubscription> DsMagazineSubscription { get; set; }
        public virtual ICollection<DsMagazineSubscriptionLog> DsMagazineSubscriptionLog { get; set; }
    }
}
