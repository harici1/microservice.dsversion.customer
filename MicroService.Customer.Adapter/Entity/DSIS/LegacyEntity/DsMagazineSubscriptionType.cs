﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsMagazineSubscriptionType
    {
        public int MagazineSubscriptionTypeId { get; set; }
        public string Name { get; set; }
    }
}
