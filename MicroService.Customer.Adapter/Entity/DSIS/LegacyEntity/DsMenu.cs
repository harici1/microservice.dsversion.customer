﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsMenu
    {
        public DsMenu()
        {
            AppAstekMenu = new HashSet<AppAstekMenu>();
            AppAstekVisualItem = new HashSet<AppAstekVisualItem>();
            AppBettingMenu = new HashSet<AppBettingMenu>();
            AppBettingMenuVisualItem = new HashSet<AppBettingMenuVisualItem>();
            AppDsisMenu = new HashSet<AppDsisMenu>();
            AppDsisMenuVisualItem = new HashSet<AppDsisMenuVisualItem>();
            AppDsisMobileMenu = new HashSet<AppDsisMobileMenu>();
            AppDsisMobileVisualItem = new HashSet<AppDsisMobileVisualItem>();
            AppSmartOnlineVisualItem = new HashSet<AppSmartOnlineVisualItem>();
            AppWebRequest = new HashSet<AppWebRequest>();
            DsRoleAuthorization = new HashSet<DsRoleAuthorization>();
            DsRoleTableConstraint = new HashSet<DsRoleTableConstraint>();
            InverseParentMenu = new HashSet<DsMenu>();
        }

        public int MenuId { get; set; }
        public string Name { get; set; }
        public int? ParentMenuId { get; set; }
        public string AttachedObjectName { get; set; }
        public int? AttachedObjectType { get; set; }
        public string Description { get; set; }
        public bool? DisplayModal { get; set; }
        public string ExtendedProperties { get; set; }

        public virtual DsMenu ParentMenu { get; set; }
        public virtual ICollection<AppAstekMenu> AppAstekMenu { get; set; }
        public virtual ICollection<AppAstekVisualItem> AppAstekVisualItem { get; set; }
        public virtual ICollection<AppBettingMenu> AppBettingMenu { get; set; }
        public virtual ICollection<AppBettingMenuVisualItem> AppBettingMenuVisualItem { get; set; }
        public virtual ICollection<AppDsisMenu> AppDsisMenu { get; set; }
        public virtual ICollection<AppDsisMenuVisualItem> AppDsisMenuVisualItem { get; set; }
        public virtual ICollection<AppDsisMobileMenu> AppDsisMobileMenu { get; set; }
        public virtual ICollection<AppDsisMobileVisualItem> AppDsisMobileVisualItem { get; set; }
        public virtual ICollection<AppSmartOnlineVisualItem> AppSmartOnlineVisualItem { get; set; }
        public virtual ICollection<AppWebRequest> AppWebRequest { get; set; }
        public virtual ICollection<DsRoleAuthorization> DsRoleAuthorization { get; set; }
        public virtual ICollection<DsRoleTableConstraint> DsRoleTableConstraint { get; set; }
        public virtual ICollection<DsMenu> InverseParentMenu { get; set; }
    }
}
