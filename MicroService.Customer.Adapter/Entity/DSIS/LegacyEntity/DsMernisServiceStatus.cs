﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsMernisServiceStatus
    {
        public int MernisServiceStatusId { get; set; }
        public bool ServiceStatus { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int MenuId { get; set; }
    }
}
