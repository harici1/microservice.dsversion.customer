﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsMessage222
    {
        public int MessageId { get; set; }
        public string IncomingMessage { get; set; }
        public string ReplyMessage { get; set; }
        public string UnhandledErrorMessage { get; set; }
        public int? IdentificationType { get; set; }
        public string SenderCode { get; set; }
        public string ServiceCode { get; set; }
        public int? PartyId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public bool? IsOk { get; set; }
        public int? CampaignId { get; set; }
        public int? MessageStatusId { get; set; }
        public int? SmartCardId { get; set; }
        public int? StbId { get; set; }
        public string CustomCode1 { get; set; }
        public string CustomCode2 { get; set; }
        public string CustomCode3 { get; set; }
        public string CustomCode4 { get; set; }
        public int? ProductId { get; set; }
        public string GsmOperatorName { get; set; }
        public string MarjinalTransactionId { get; set; }
        public decimal? ChargeAmount { get; set; }
        public int? MessageProcessTypeId { get; set; }
        public string MarjinalSubscriptionType { get; set; }
        public string MarjinalSubscriptionService { get; set; }
        public DateTime? EntitlementDate { get; set; }
        public int? PaymentTypeId { get; set; }
        public string SpecialResponseCode { get; set; }
    }
}
