﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsMessageIdentificationType
    {
        public DsMessageIdentificationType()
        {
            DsMagazineSubscription = new HashSet<DsMagazineSubscription>();
        }

        public int MessageIdentificationTypeId { get; set; }
        public string Name { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string ClassName { get; set; }

        public virtual ICollection<DsMagazineSubscription> DsMagazineSubscription { get; set; }
    }
}
