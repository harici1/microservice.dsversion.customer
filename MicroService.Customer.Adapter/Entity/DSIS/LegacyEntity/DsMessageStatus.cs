﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsMessageStatus
    {
        public int MessageStatusId { get; set; }
        public string Name { get; set; }
    }
}
