﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsMobileAuthenticationLog
    {
        public int MobileAuthenticationLogId { get; set; }
        public int MobileAuthenticationId { get; set; }
        public int UserId { get; set; }
        public string DeviceId { get; set; }
        public string AccessCode { get; set; }
        public DateTime? AccessExpiryDate { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public string Ip { get; set; }
        public string ApiKey { get; set; }
    }
}
