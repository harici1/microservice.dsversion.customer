﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsMobilePayment
    {
        public int MobilePaymentPaymentId { get; set; }
        public decimal Amount { get; set; }
        public int TokenType2Id { get; set; }
        public decimal TokenAmount { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? SmsMessageId { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public decimal? ServiceCostAmount { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string MarjinalTransId { get; set; }
    }
}
