﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsMobilePayment3PayApproveLog
    {
        public int MobilePayment3PayApproveLogId { get; set; }
        public int LeasingContractId { get; set; }
        public bool ApproveResult { get; set; }
        public string ApproveMessage { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
