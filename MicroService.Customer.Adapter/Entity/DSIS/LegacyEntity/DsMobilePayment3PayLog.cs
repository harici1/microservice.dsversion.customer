﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsMobilePayment3PayLog
    {
        public int MobilePayment3PayLogId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Gsm { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string IpAddress { get; set; }
        public int LeasingContractId { get; set; }
        public int ProductId { get; set; }
        public int CampaignId { get; set; }
        public string Extra { get; set; }
        public bool? Result { get; set; }
        public string ResultMessage { get; set; }
        public bool? IsRenewal { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string CouponCode { get; set; }
    }
}
