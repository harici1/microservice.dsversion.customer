﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsMobilePaymentPpvSubscription
    {
        public int MobilePaymentPpvSubscriptionId { get; set; }
        public int PpvActivationId { get; set; }
        public int ActivationId { get; set; }
        public int CampaignId { get; set; }
        public decimal ChargeAmount { get; set; }
        public decimal TokenAmount { get; set; }
        public DateTime NextChargeDate { get; set; }
        public DateTime LastChargeDate { get; set; }
        public string GsmNumber { get; set; }
        public bool IsCancelled { get; set; }
        public int ChargeIntervalInDay { get; set; }
        public string MarjinalTransId { get; set; }
        public string MarjinalVariantId { get; set; }
        public string MarjinalAccountId { get; set; }
        public int DocumentId { get; set; }
        public int DocumentTypeId { get; set; }
        public int UserId { get; set; }
        public DateTime CreationDate { get; set; }
        public string Note { get; set; }
    }
}
