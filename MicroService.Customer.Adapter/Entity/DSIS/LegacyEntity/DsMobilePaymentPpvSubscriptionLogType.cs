﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsMobilePaymentPpvSubscriptionLogType
    {
        public int MobilePaymentSubscriptionLogTypeId { get; set; }
        public string Name { get; set; }
    }
}
