﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsModemModel
    {
        public int ModemModelId { get; set; }
        public string Name { get; set; }
        public bool Wireless { get; set; }
        public bool Voip { get; set; }
        public int ModemTypeId { get; set; }
        public int CompanyId { get; set; }
    }
}
