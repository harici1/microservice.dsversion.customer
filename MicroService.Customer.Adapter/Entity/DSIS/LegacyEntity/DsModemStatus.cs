﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsModemStatus
    {
        public int ModemStatusId { get; set; }
        public string Name { get; set; }
    }
}
