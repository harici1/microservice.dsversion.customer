﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsModemTestSparePartValue
    {
        public int ModemTestSparePartValueId { get; set; }
        public int ModemTestId { get; set; }
        public int ModemTestSparePartId { get; set; }
        public bool? IsActive { get; set; }
    }
}
