﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsModemThirdParty
    {
        public int ModemThirdPartyId { get; set; }
        public string SerialNumber { get; set; }
        public string UniqueIdentifierNo { get; set; }
        public int? BrandId { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? ModelId { get; set; }
        public int? OwnerId { get; set; }
        public int? VendorProductId { get; set; }
        public int? LocationPartyId { get; set; }
        public int? TransactionTypeId { get; set; }
        public int? StatusId { get; set; }
        public string Notes { get; set; }
        public DateTime? FirstActivationDate { get; set; }
        public int? LastUpdateById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
    }
}
