﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsModemThirdPartyTransaction
    {
        public int ModemThirdPartyTransactionId { get; set; }
        public int ModemThirdPartyId { get; set; }
        public int? FromPartyId { get; set; }
        public int? ToPartyId { get; set; }
        public int? TransactionTypeId { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? StatusId { get; set; }
        public int? OwnerId { get; set; }
        public int? VendorProductId { get; set; }
    }
}
