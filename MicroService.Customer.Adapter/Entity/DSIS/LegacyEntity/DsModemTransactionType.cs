﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsModemTransactionType
    {
        public int ModemTransactionTypeId { get; set; }
        public string Name { get; set; }
    }
}
