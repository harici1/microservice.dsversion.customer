﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsModemType
    {
        public int ModemTypeId { get; set; }
        public string Name { get; set; }
    }
}
