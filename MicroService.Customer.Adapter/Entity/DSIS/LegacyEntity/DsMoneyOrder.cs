﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsMoneyOrder
    {
        public int MoneyOrderId { get; set; }
        public string ReferenceNumber { get; set; }
        public int BankAccountId { get; set; }
        public decimal Amount { get; set; }
        public decimal ServiceCostAmount { get; set; }
        public int DocumentId { get; set; }
        public int DocumentTypeId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public bool? IsAccountingOk { get; set; }
        public bool? IsCancelled { get; set; }
        public int? CancelDocumentId { get; set; }
        public int? CancelDocumentTypeId { get; set; }
        public int? CancelByUserId { get; set; }
        public DateTime? CancelDate { get; set; }

        public virtual DsBankAccount BankAccount { get; set; }
    }
}
