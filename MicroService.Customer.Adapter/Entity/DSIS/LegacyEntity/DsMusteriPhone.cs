﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsMusteriPhone
    {
        public int MusteriPhoneId { get; set; }
        public int CustomerId { get; set; }
        public string Phone { get; set; }
        public int? PhoneTypeCode { get; set; }
        public bool? IsPrimary { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
