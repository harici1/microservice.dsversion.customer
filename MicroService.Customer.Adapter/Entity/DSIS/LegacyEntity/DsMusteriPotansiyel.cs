﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsMusteriPotansiyel
    {
        public int MusteriId { get; set; }
        public string Adi { get; set; }
        public string Soyadi { get; set; }
        public DateTime? DogumTarihi { get; set; }
        public string Cinsiyet { get; set; }
        public string Mahalle { get; set; }
        public string Cadde { get; set; }
        public string Sokak { get; set; }
        public string Bina { get; set; }
        public string Site { get; set; }
        public string BlokNo { get; set; }
        public string BinaNo { get; set; }
        public string Kat { get; set; }
        public string Daire { get; set; }
        public string Semt { get; set; }
        public string IlceAdi { get; set; }
        public string EvTelefon { get; set; }
        public string IsTelefon { get; set; }
        public string CepTelefon { get; set; }
        public string Faks { get; set; }
        public string Eposta { get; set; }
        public DateTime? KayitTarih { get; set; }
        public string BaskaStb { get; set; }
        public string BaskaStbmarka { get; set; }
        public string DigiTurk { get; set; }
        public string PostaKodu { get; set; }
        public string Dsmart { get; set; }
        public int? ProvinceId { get; set; }
        public int? CountryId { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string CommercialPotentialName { get; set; }
        public int? CommercialPotentialScopeId { get; set; }
        public int? NumberOfDisplays { get; set; }
        public int? NumberOfBranches { get; set; }
        public int? CommercialPotentialWorkingPeriodId { get; set; }
        public int? CommercialPotentialCapacityId { get; set; }
        public bool? IsAlcoholicBeverageServed { get; set; }
        public int? PotentialTypeId { get; set; }

        public virtual DsCommercialCustomerScope CommercialPotentialScope { get; set; }
        public virtual DsCommercialCustomerWorkingPeriod CommercialPotentialWorkingPeriod { get; set; }
        public virtual DsPotentialType PotentialType { get; set; }
        public virtual DsIl Province { get; set; }
    }
}
