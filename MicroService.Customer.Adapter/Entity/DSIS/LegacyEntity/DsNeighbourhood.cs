﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsNeighbourhood
    {
        public DsNeighbourhood()
        {
            DsDealerToNeighbourhood = new HashSet<DsDealerToNeighbourhood>();
            DsDsisneighbourhoodToAnkaDistrict = new HashSet<DsDsisneighbourhoodToAnkaDistrict>();
            DsMusteri = new HashSet<DsMusteri>();
        }

        public int NeighbourhoodId { get; set; }
        public string Name { get; set; }
        public int DistrictId { get; set; }
        public int? StsDealerId { get; set; }
        public bool IsSite { get; set; }
        public int? AnkaProvinceId { get; set; }
        public int? AnkaCityId { get; set; }
        public int? AnkaDistrictId { get; set; }
        public bool? IsActiveTemp { get; set; }
        public string Postcode { get; set; }

        public virtual ICollection<DsDealerToNeighbourhood> DsDealerToNeighbourhood { get; set; }
        public virtual ICollection<DsDsisneighbourhoodToAnkaDistrict> DsDsisneighbourhoodToAnkaDistrict { get; set; }
        public virtual ICollection<DsMusteri> DsMusteri { get; set; }
    }
}
