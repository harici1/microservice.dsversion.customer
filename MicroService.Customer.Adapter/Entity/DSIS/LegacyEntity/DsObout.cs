﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsObout
    {
        public int OboutId { get; set; }
        public string OboutName { get; set; }
        public string OboutLocationClass { get; set; }
        public string InnerHtmlSp { get; set; }
        public string SpParameters { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsActive { get; set; }
    }
}
