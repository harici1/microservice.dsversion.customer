﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsOboutDetail
    {
        public int OboutDetailId { get; set; }
        public int OboutId { get; set; }
        public int OboutTypeId { get; set; }
        public string PropertyName { get; set; }
        public string PropertyType { get; set; }
        public string PropertyValue { get; set; }
        public string AssemblyName { get; set; }
        public bool IsActive { get; set; }
    }
}
