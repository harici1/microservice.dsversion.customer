﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsOffer
    {
        public int OfferId { get; set; }
        public int OfferTypeId { get; set; }
        public int OfferStatusId { get; set; }
        public int? PartyId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int DocumentId { get; set; }
        public int DocumentTypeId { get; set; }
        public decimal? DiscountAmount { get; set; }
        public int? RatioAmount { get; set; }
        public int? AppendDayAmount { get; set; }
        public int? PromotionProductId { get; set; }
        public int? UsedById { get; set; }
        public DateTime? UsedDate { get; set; }
        public int? UsedByDocumentId { get; set; }
        public int? UsedByDocumentTypeId { get; set; }
        public int? CancelledByDocumentId { get; set; }
        public int? CancelledByDocumentTypeId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? CancelledById { get; set; }
        public DateTime? CancelDate { get; set; }
    }
}
