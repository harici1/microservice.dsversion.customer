﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsOfferClass
    {
        public int OfferClassId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
