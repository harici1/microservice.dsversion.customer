﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsOfferOrder
    {
        public int OfferOrderId { get; set; }
        public int PartyId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreatonDate { get; set; }
        public int ContactChannelId { get; set; }
        public string Description { get; set; }

        public virtual DsContactChannel ContactChannel { get; set; }
    }
}
