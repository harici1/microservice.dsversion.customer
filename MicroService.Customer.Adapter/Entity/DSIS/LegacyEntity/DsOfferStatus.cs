﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsOfferStatus
    {
        public int OfferStatusId { get; set; }
        public string Name { get; set; }
    }
}
