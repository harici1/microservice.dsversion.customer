﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsOfferType
    {
        public DsOfferType()
        {
            DsOfferTypeToProductPrice = new HashSet<DsOfferTypeToProductPrice>();
            DsWebTvActivationCustomerTypeOffer = new HashSet<DsWebTvActivationCustomerTypeOffer>();
            DsWebTvActivationToOffer = new HashSet<DsWebTvActivationToOffer>();
            DsWebTvPpvActivation = new HashSet<DsWebTvPpvActivation>();
            DsWebTvPpvEvent = new HashSet<DsWebTvPpvEvent>();
            DsWebTvProductCategoryToOfferType = new HashSet<DsWebTvProductCategoryToOfferType>();
        }

        public int OfferTypeId { get; set; }
        public string Name { get; set; }
        public int CampaignId { get; set; }
        public int ValidationLenghtInDays { get; set; }
        public bool AutoAuthorizeEnabled { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public string Description { get; set; }
        public int OfferClassId { get; set; }
        public decimal? DiscountAmount { get; set; }
        public int? RatioAmount { get; set; }
        public int? AppendDayAmount { get; set; }
        public int? PromotionProductId { get; set; }

        public virtual DsCampaign Campaign { get; set; }
        public virtual DsProduct PromotionProduct { get; set; }
        public virtual ICollection<DsOfferTypeToProductPrice> DsOfferTypeToProductPrice { get; set; }
        public virtual ICollection<DsWebTvActivationCustomerTypeOffer> DsWebTvActivationCustomerTypeOffer { get; set; }
        public virtual ICollection<DsWebTvActivationToOffer> DsWebTvActivationToOffer { get; set; }
        public virtual ICollection<DsWebTvPpvActivation> DsWebTvPpvActivation { get; set; }
        public virtual ICollection<DsWebTvPpvEvent> DsWebTvPpvEvent { get; set; }
        public virtual ICollection<DsWebTvProductCategoryToOfferType> DsWebTvProductCategoryToOfferType { get; set; }
    }
}
