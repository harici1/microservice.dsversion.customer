﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsOfferTypeToProductPrice
    {
        public int OfferTypeToProductPriceId { get; set; }
        public int OfferTypeId { get; set; }
        public int ProductPriceId { get; set; }

        public virtual DsOfferType OfferType { get; set; }
        public virtual DsProductPrice ProductPrice { get; set; }
    }
}
