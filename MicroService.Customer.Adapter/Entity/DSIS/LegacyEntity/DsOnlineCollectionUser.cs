﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsOnlineCollectionUser
    {
        public int OnlineCollectionUserId { get; set; }
        public string BankUserName { get; set; }
        public string Password { get; set; }
        public int DsisBankCode { get; set; }
        public int AnkaBankCode { get; set; }
        public bool IsActive { get; set; }
    }
}
