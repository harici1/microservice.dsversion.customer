﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsOnlinePayment
    {
        public int OnlinePaymentId { get; set; }
        public decimal Amount { get; set; }
        public string Bname { get; set; }
        public int? LeasingContractId { get; set; }
        public int? BankCode { get; set; }
        public string InvoiceNumber { get; set; }
        public string ReferenceNumber { get; set; }
        public string Stan { get; set; }
        public string BankRefNo { get; set; }
        public string BankBranchNo { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public decimal? ServiceCostAmount { get; set; }
        public int? NumberOfInstallments { get; set; }
        public decimal? InterestCost { get; set; }
        public decimal? DiscountAmount { get; set; }
        public bool? IsCancelled { get; set; }
        public int? CancelDocumentId { get; set; }
        public int? CancelDocumentTypeId { get; set; }
        public int? CancelByUserId { get; set; }
        public DateTime? CancelDate { get; set; }
        public DateTime PaymentDate { get; set; }
        public int? InvoiceToBankChargeDetailId { get; set; }
    }
}
