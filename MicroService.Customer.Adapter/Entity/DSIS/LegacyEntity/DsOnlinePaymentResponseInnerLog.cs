﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsOnlinePaymentResponseInnerLog
    {
        public int ResponseId { get; set; }
        public int RequestId { get; set; }
        public int MethodId { get; set; }
        public string Response { get; set; }
        public DateTime? CreationDate { get; set; }
    }
}
