﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsOnlineSetPaymentCancelRequestLog
    {
        public int RequestId { get; set; }
        public int BankCode { get; set; }
        public string SubscriberNo { get; set; }
        public string InvoiceNo { get; set; }
        public string CancelDate { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public DateTime? CreationDate { get; set; }
    }
}
