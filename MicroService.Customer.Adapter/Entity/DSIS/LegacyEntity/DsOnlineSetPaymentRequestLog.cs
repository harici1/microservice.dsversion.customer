﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsOnlineSetPaymentRequestLog
    {
        public int RequestId { get; set; }
        public int BankCode { get; set; }
        public string SubscriberNo { get; set; }
        public string InvoiceNo { get; set; }
        public string PaymentAmount { get; set; }
        public string PaymentDate { get; set; }
        public string BankRefNo { get; set; }
        public string BankBranchNo { get; set; }
        public string PaymentChannel { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public DateTime? CreationDate { get; set; }
        public bool? IsProcessed { get; set; }
    }
}
