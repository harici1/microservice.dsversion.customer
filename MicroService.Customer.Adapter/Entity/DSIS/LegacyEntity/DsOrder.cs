﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsOrder
    {
        public DsOrder()
        {
            DsActivation = new HashSet<DsActivation>();
            DsInvoiceLineToOrder = new HashSet<DsInvoiceLineToOrder>();
        }

        public int OrderId { get; set; }
        public int? PartyId { get; set; }
        public int? DealerId { get; set; }
        public int ProductId { get; set; }
        public int? SmartCardId { get; set; }
        public int? StbId { get; set; }
        public int? NdsserviceId { get; set; }
        public string ReferenceNumber1 { get; set; }
        public string ReferenceNumber2 { get; set; }
        public int? CampaignId { get; set; }
        public decimal Amount { get; set; }
        public int CurrencyId { get; set; }
        public decimal LocalCurrencyEquivalent { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? LastCancellationDate { get; set; }
        public bool IsCancelled { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? ProductProcessTypeId { get; set; }
        public int? PotentialId { get; set; }
        public int ContactChannelId { get; set; }
        public bool? IsOk { get; set; }
        public string Description { get; set; }
        public int? PaymentTypeId { get; set; }
        public int? VposServiceProviderId { get; set; }
        public int? ScratchCardId { get; set; }
        public int? SalesSourceId { get; set; }
        public int? BatchId { get; set; }
        public int? MultiSwitchInstallationEquipmentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? DocumentId { get; set; }
        public int? ContentId { get; set; }
        public decimal? TokenAmount { get; set; }
        public decimal? ServiceCost { get; set; }
        public int? RelatedOrderId { get; set; }
        public int? ToPartyId { get; set; }
        public int? ProductPriceId { get; set; }
        public int? RelatedProductId { get; set; }
        public int? CancelledByDocumentId { get; set; }
        public int? CancelledByDocumentTypeId { get; set; }
        public bool? IsPostEventsCreated { get; set; }
        public int? RequestedById { get; set; }
        public int? ComfirmedById { get; set; }
        public bool? IsPostEventsCreated2 { get; set; }
        public int? ExternalEquipmentId { get; set; }
        public int? RequestedBonusProductId { get; set; }

        public virtual ICollection<DsActivation> DsActivation { get; set; }
        public virtual ICollection<DsInvoiceLineToOrder> DsInvoiceLineToOrder { get; set; }
    }
}
