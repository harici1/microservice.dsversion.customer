﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsOrderChangeLog
    {
        public int OrderChangeLogId { get; set; }
        public int? OrderId { get; set; }
        public int? TypeId { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? CreatedById { get; set; }
        public string OldValues { get; set; }
        public string NewValues { get; set; }

        public virtual DsOrderChangeLogType Type { get; set; }
    }
}
