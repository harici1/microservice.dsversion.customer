﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsOrderChangeLogType
    {
        public DsOrderChangeLogType()
        {
            DsOrderChangeLog = new HashSet<DsOrderChangeLog>();
        }

        public int OrderChangeLogTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<DsOrderChangeLog> DsOrderChangeLog { get; set; }
    }
}
