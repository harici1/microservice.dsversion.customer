﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsOrganization
    {
        public int OrganizationId { get; set; }
        public int OrganizationTypeId { get; set; }
        public string Name { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? HeadCount { get; set; }
        public string Referrer { get; set; }
        public string EstablishDate { get; set; }
        public string Web { get; set; }
        public string Email { get; set; }
        public string AddressNote { get; set; }
        public string SecondaryAccessCode { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Fax { get; set; }
        public string PostalCode { get; set; }
        public string Phone1Note { get; set; }
        public string Phone2Note { get; set; }
        public string Address { get; set; }
        public string Notes { get; set; }
        public int? ProvinceId { get; set; }
        public string District { get; set; }
        public string ShortCode { get; set; }

        public virtual DsIl Province { get; set; }
    }
}
