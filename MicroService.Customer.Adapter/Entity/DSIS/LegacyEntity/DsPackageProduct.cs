﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPackageProduct
    {
        public int PackageProductId { get; set; }
        public int PackageProductTypeId { get; set; }
        public int PackageId { get; set; }
        public int ProductId { get; set; }
        public int Amount { get; set; }

        public virtual DsProduct Package { get; set; }
        public virtual DsPackageProductType PackageProductType { get; set; }
        public virtual DsProduct Product { get; set; }
    }
}
