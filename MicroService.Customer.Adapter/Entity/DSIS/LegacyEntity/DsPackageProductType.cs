﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPackageProductType
    {
        public DsPackageProductType()
        {
            DsPackageProduct = new HashSet<DsPackageProduct>();
        }

        public int PackageProductTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsPackageProduct> DsPackageProduct { get; set; }
    }
}
