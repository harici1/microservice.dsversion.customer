﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsParentalRating
    {
        public int ParentalRatingId { get; set; }
        public string ParentalRatingCode { get; set; }
        public string Name { get; set; }
    }
}
