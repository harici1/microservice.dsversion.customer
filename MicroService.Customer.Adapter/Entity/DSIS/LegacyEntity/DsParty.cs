﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsParty
    {
        public DsParty()
        {
            DsExpenseBill = new HashSet<DsExpenseBill>();
            DsInventoryTransferRuleFromPartyNavigation = new HashSet<DsInventoryTransferRule>();
            DsInventoryTransferRuleToPartyNavigation = new HashSet<DsInventoryTransferRule>();
            DsLeasingContract = new HashSet<DsLeasingContract>();
            DsPartyDetailInfo = new HashSet<DsPartyDetailInfo>();
            DsStbmodel = new HashSet<DsStbmodel>();
            DsWebSubscriber = new HashSet<DsWebSubscriber>();
            DsWorkOrder = new HashSet<DsWorkOrder>();
            DsWorkOrderCustomerGeneric = new HashSet<DsWorkOrderCustomerGeneric>();
            DsWorkOrderLog = new HashSet<DsWorkOrderLog>();
            TssisDsWorkOrder = new HashSet<TssisDsWorkOrder>();
        }

        public int PartyId { get; set; }
        public int TypeId { get; set; }
        public string Name { get; set; }
        public int? ReferenceNumber { get; set; }
        public bool? IsActive { get; set; }
        public string SystemCode { get; set; }
        public string InvoiceTitle { get; set; }
        public string InvoiceTaxNumber { get; set; }
        public string InvoiceTaxOffice { get; set; }
        public string InvoiceAddress { get; set; }
        public int? InvoiceDistrictId { get; set; }
        public int? InvoiceSendingTypeId { get; set; }
        public int? NeighbourhoodId { get; set; }
        public string ZipCode { get; set; }
        public int? CountryId { get; set; }
        public int? ProvinceId { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public DateTime? CreationDate { get; set; }
        public bool Einvoice { get; set; }
        public bool? StoreCustomerInfo { get; set; }
        public bool? ShareCustomerInfo { get; set; }
        public bool? TaxFree { get; set; }
        public bool? IsEnvelopeRequested { get; set; }

        public virtual DsEmployee DsEmployee { get; set; }
        public virtual DsExternalSystem DsExternalSystem { get; set; }
        public virtual ICollection<DsExpenseBill> DsExpenseBill { get; set; }
        public virtual ICollection<DsInventoryTransferRule> DsInventoryTransferRuleFromPartyNavigation { get; set; }
        public virtual ICollection<DsInventoryTransferRule> DsInventoryTransferRuleToPartyNavigation { get; set; }
        public virtual ICollection<DsLeasingContract> DsLeasingContract { get; set; }
        public virtual ICollection<DsPartyDetailInfo> DsPartyDetailInfo { get; set; }
        public virtual ICollection<DsStbmodel> DsStbmodel { get; set; }
        public virtual ICollection<DsWebSubscriber> DsWebSubscriber { get; set; }
        public virtual ICollection<DsWorkOrder> DsWorkOrder { get; set; }
        public virtual ICollection<DsWorkOrderCustomerGeneric> DsWorkOrderCustomerGeneric { get; set; }
        public virtual ICollection<DsWorkOrderLog> DsWorkOrderLog { get; set; }
        public virtual ICollection<TssisDsWorkOrder> TssisDsWorkOrder { get; set; }
    }
}
