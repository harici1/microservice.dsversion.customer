﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPartyAttachment
    {
        public int AttachmentId { get; set; }
        public int AttachmenTypeId { get; set; }
        public int AttachmentReferenceId { get; set; }
        public int AttachmentReferenceTypeId { get; set; }
        public int DocumentId { get; set; }
        public int DocumentTypeId { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
    }
}
