﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPartyDetailInfo
    {
        public int PartyDetailInfoId { get; set; }
        public int PartyDetailInfoTypeId { get; set; }
        public int? PartyId { get; set; }
        public int? LeasingContractId { get; set; }
        public string Value { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public bool Cancelled { get; set; }

        public virtual DsParty Party { get; set; }
        public virtual DsPartyDetailInfoType PartyDetailInfoType { get; set; }
    }
}
