﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPartyDetailInfoType
    {
        public DsPartyDetailInfoType()
        {
            DsPartyDetailInfo = new HashSet<DsPartyDetailInfo>();
        }

        public int PartyDetailInfoTypeId { get; set; }
        public string Name { get; set; }
        public string CalculationStoredProcedure { get; set; }
        public string DataType { get; set; }

        public virtual ICollection<DsPartyDetailInfo> DsPartyDetailInfo { get; set; }
    }
}
