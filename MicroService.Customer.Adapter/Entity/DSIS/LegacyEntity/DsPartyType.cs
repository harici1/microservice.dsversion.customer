﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPartyType
    {
        public int PartyTypeId { get; set; }
        public string Name { get; set; }
    }
}
