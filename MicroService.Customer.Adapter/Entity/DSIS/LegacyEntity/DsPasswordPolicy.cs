﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPasswordPolicy
    {
        public int PasswordPolicyId { get; set; }
        public int ContactChannelId { get; set; }
        public string ValidationRegex { get; set; }
        public int UpdatePeriodInDay { get; set; }
        public int FailRetryCount { get; set; }
        public string FailRetryCountType { get; set; }
        public int FailRetryCountWaitInMinute { get; set; }
        public int ForcedToUpdateForFirstUse { get; set; }
        public int OldPasswordCountToBlock { get; set; }
        public int LoginAttempCountToBlock { get; set; }
        public string AlertMessage { get; set; }

        public virtual DsContactChannel ContactChannel { get; set; }
    }
}
