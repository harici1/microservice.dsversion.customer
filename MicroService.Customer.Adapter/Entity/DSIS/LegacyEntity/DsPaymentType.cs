﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPaymentType
    {
        public DsPaymentType()
        {
            DsAccAccount = new HashSet<DsAccAccount>();
            DsAccPaymentType = new HashSet<DsAccPaymentType>();
            DsCampaignToPaymentType = new HashSet<DsCampaignToPaymentType>();
            DsProductPrice = new HashSet<DsProductPrice>();
            DsWebTvProductCategoryDefaultPrice = new HashSet<DsWebTvProductCategoryDefaultPrice>();
        }

        public int PaymentTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsAccAccount> DsAccAccount { get; set; }
        public virtual ICollection<DsAccPaymentType> DsAccPaymentType { get; set; }
        public virtual ICollection<DsCampaignToPaymentType> DsCampaignToPaymentType { get; set; }
        public virtual ICollection<DsProductPrice> DsProductPrice { get; set; }
        public virtual ICollection<DsWebTvProductCategoryDefaultPrice> DsWebTvProductCategoryDefaultPrice { get; set; }
    }
}
