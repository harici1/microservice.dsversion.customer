﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPersRequestLog
    {
        public int PersRequestLogId { get; set; }
        public string PostRequest { get; set; }
    }
}
