﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPersonalization
    {
        public int PersonalizationId { get; set; }
        public byte[] Response { get; set; }
    }
}
