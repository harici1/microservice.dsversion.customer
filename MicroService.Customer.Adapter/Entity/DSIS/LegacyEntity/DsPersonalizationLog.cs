﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPersonalizationLog
    {
        public int PersonalizationLogId { get; set; }
        public int LogTypeId { get; set; }
        public byte[] MessageInBytes { get; set; }
        public string MessageInStringBytes { get; set; }
        public string Message { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
