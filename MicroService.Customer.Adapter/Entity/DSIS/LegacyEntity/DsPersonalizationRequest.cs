﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPersonalizationRequest
    {
        public int PersonalizationRequestId { get; set; }
        public string Header { get; set; }
        public string Host { get; set; }
        public string DrmCoreVersion { get; set; }
        public string PlatformName { get; set; }
        public string PlatformVersion { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceId { get; set; }
        public string AndroidId { get; set; }
        public string BuildSerial { get; set; }
        public string WifiMac { get; set; }
        public string AppVersion { get; set; }
        public string SerializedMessage { get; set; }
        public string ReturnMessage { get; set; }
        public string Status { get; set; }
        public DateTime? CreationDate { get; set; }
    }
}
