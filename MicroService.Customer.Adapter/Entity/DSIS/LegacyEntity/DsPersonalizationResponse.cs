﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPersonalizationResponse
    {
        public int PersonalizationResponseId { get; set; }
        public int RequestId { get; set; }
        public int DataTypeId { get; set; }
        public string Data { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
