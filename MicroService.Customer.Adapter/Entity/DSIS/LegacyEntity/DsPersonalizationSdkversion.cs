﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPersonalizationSdkversion
    {
        public int PersonalizationSdkversionId { get; set; }
        public string DrmcoreVersion { get; set; }
        public string PlatformName { get; set; }
        public string PlatformVersion { get; set; }
        public string CodeHash { get; set; }
        public string SecretKey { get; set; }
        public bool? KeyStatus { get; set; }
    }
}
