﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPhysicalStatus
    {
        public int PhysicalStatusId { get; set; }
        public string Name { get; set; }
        public bool? AlllowActivation { get; set; }
        public bool? AllowStatusChange { get; set; }
        public bool? IsVisible { get; set; }
    }
}
