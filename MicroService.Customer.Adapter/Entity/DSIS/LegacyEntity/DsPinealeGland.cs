﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPinealeGland
    {
        public int PinealeGlandId { get; set; }
        public string IncomingMessage { get; set; }
        public string ReturnedMessage { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? RetryCount { get; set; }
        public DateTime? ProcessDate { get; set; }
        public int? ProcessStatus { get; set; }
        public DateTime? ProcessExpirationDate { get; set; }
        public string ErrorMessage { get; set; }
    }
}
