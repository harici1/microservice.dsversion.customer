﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPopup
    {
        public DsPopup()
        {
            DsPopupToRole = new HashSet<DsPopupToRole>();
        }

        public int PopupId { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string FormContent { get; set; }
        public string FormScript { get; set; }
        public int ContactChannelId { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int? DsisMenuVisualItemId { get; set; }

        public virtual DsContactChannel ContactChannel { get; set; }
        public virtual ICollection<DsPopupToRole> DsPopupToRole { get; set; }
    }
}
