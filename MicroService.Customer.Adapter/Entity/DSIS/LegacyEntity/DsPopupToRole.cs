﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPopupToRole
    {
        public int PopupToRoleId { get; set; }
        public int PopupId { get; set; }
        public int RoleId { get; set; }

        public virtual DsPopup Popup { get; set; }
        public virtual DsRole Role { get; set; }
    }
}
