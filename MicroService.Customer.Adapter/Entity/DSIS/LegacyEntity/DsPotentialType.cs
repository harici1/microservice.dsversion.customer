﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPotentialType
    {
        public DsPotentialType()
        {
            DsMusteriPotansiyel = new HashSet<DsMusteriPotansiyel>();
        }

        public int PotentialTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<DsMusteriPotansiyel> DsMusteriPotansiyel { get; set; }
    }
}
