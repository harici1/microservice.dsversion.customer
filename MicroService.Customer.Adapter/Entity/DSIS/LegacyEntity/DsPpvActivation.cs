﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPpvActivation
    {
        public DsPpvActivation()
        {
            DsActivationService = new HashSet<DsActivationService>();
        }

        public int PpvActivationId { get; set; }
        public int ProductId { get; set; }
        public int DocumentId { get; set; }
        public int DocumentTypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime LastCancellationDate { get; set; }
        public int? PpvActivationStatusId { get; set; }
        public int? CustomerId { get; set; }
        public decimal? TokenAmount { get; set; }
        public decimal? PaidAmount { get; set; }
        public int? TokenType2Id { get; set; }
        public int? TransactionTypeId { get; set; }
        public int? StbId { get; set; }
        public int? SmartCardId { get; set; }
        public int? LeasingContractId { get; set; }
        public int? ActivationId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? ToPpvActivationId { get; set; }
        public int? FromPpvActivationId { get; set; }
        public int? DealerId { get; set; }
        public int? PaymentTypeId { get; set; }
        public int? ContactChannelId { get; set; }
        public int? CampaignId { get; set; }
        public string Description { get; set; }
        public bool? IsOk { get; set; }
        public string TmpPayStatus { get; set; }
        public int? LeasingContractServiceId { get; set; }
        public bool? IsSuspendBeforeSeptember2010 { get; set; }
        public int? SaledById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public int? LastOrderId { get; set; }
        public int? LastOrderTypeId { get; set; }

        public virtual ICollection<DsActivationService> DsActivationService { get; set; }
    }
}
