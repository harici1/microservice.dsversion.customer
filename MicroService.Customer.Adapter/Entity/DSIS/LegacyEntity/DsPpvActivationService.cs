﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPpvActivationService
    {
        public int PpvActivationServiceId { get; set; }
        public int PpvActivationId { get; set; }
        public int ServiceId { get; set; }
        public string Note { get; set; }
    }
}
