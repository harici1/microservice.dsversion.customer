﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPpvActivationStatus
    {
        public int PpvActivationStatusId { get; set; }
        public string Name { get; set; }
    }
}
