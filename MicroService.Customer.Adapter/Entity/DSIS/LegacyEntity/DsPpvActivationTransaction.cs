﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPpvActivationTransaction
    {
        public int PpvActivationTransactionId { get; set; }
        public int PpvActivationId { get; set; }
        public int? ProductId { get; set; }
        public int? PpvActivationStatusId { get; set; }
        public int? PpvActivationDocumentId { get; set; }
        public int? PpvActivationDocumentTypeId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? LastCancellationDate { get; set; }
        public int? CustomerId { get; set; }
        public int? SmartCardId { get; set; }
        public int? LeasingContractId { get; set; }
        public int? ActivationId { get; set; }
        public int TransactionTypeId { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public DateTime CreationDate { get; set; }
        public long CreatedById { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public int? DealerId { get; set; }
        public int? PaymentTypeId { get; set; }
        public int? ContactChannelId { get; set; }
        public int? CampaignId { get; set; }
        public string Description { get; set; }
        public int? FromPpvActivationId { get; set; }
        public int? ToPpvActivationId { get; set; }
        public int? LeasingContractServiceId { get; set; }
        public int? ChangeStbOrderId { get; set; }
    }
}
