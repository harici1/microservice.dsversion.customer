﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPpvActivationTransactionType
    {
        public int PpvActivationTransactionTypeId { get; set; }
        public string Name { get; set; }
    }
}
