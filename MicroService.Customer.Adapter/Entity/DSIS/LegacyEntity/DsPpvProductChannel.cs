﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPpvProductChannel
    {
        public DsPpvProductChannel()
        {
            DsPpvProductChannelToProduct = new HashSet<DsPpvProductChannelToProduct>();
        }

        public int PpvProductChannelId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }

        public virtual ICollection<DsPpvProductChannelToProduct> DsPpvProductChannelToProduct { get; set; }
    }
}
