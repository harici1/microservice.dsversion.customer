﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPpvProductChannelToProduct
    {
        public int PpvProductChannelToProductId { get; set; }
        public int ProductId { get; set; }
        public int PpvProductChannelId { get; set; }

        public virtual DsPpvProductChannel PpvProductChannel { get; set; }
    }
}
