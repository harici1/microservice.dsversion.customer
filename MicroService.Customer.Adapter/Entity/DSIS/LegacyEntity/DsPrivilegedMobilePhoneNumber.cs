﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPrivilegedMobilePhoneNumber
    {
        public int PrivilegedMobilePhoneId { get; set; }
        public string PrivilegedMobilePhoneNumber { get; set; }
        public string Name { get; set; }
    }
}
