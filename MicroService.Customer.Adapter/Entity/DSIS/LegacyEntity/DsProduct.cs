﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProduct
    {
        public DsProduct()
        {
            DsActivationServiceLogFromProduct = new HashSet<DsActivationServiceLog>();
            DsActivationServiceLogToProduct = new HashSet<DsActivationServiceLog>();
            DsBonuseski = new HashSet<DsBonuseski>();
            DsCampaignToExitPrice = new HashSet<DsCampaignToExitPrice>();
            DsLeasingContractInstallmentTypeToProduct = new HashSet<DsLeasingContractInstallmentTypeToProduct>();
            DsLeasingContractService = new HashSet<DsLeasingContractService>();
            DsLeasingContractServiceLogBonusProduct = new HashSet<DsLeasingContractServiceLog>();
            DsLeasingContractServiceLogProduct = new HashSet<DsLeasingContractServiceLog>();
            DsLeasingContractServiceStatusReason = new HashSet<DsLeasingContractServiceStatusReason>();
            DsOfferType = new HashSet<DsOfferType>();
            DsPackageProductPackage = new HashSet<DsPackageProduct>();
            DsPackageProductProduct = new HashSet<DsPackageProduct>();
            DsProductPrice = new HashSet<DsProductPrice>();
            DsProductToBluAsset = new HashSet<DsProductToBluAsset>();
            DsProductToService = new HashSet<DsProductToService>();
            DsSaleCompensationType = new HashSet<DsSaleCompensationType>();
            DsService = new HashSet<DsService>();
            DsStbTypeToProduct = new HashSet<DsStbTypeToProduct>();
            DsWebTvProductMapping = new HashSet<DsWebTvProductMapping>();
            DsWorkOrderCancelStbLeasingInvoiceItem = new HashSet<DsWorkOrderCancelStbLeasingInvoiceItem>();
        }

        public int ProductId { get; set; }
        public string Name { get; set; }
        public int ProductTypeId { get; set; }
        public decimal? Price { get; set; }
        public string SystemCode { get; set; }
        public string ShortCode { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public int? ActivationLengthInDays { get; set; }
        public DateTime? PpvExpiryDate { get; set; }
        public DateTime? LastViewingDate { get; set; }
        public bool? ValidatePpvEvents { get; set; }
        public int? ContentCategoryId { get; set; }
        public bool? IsCopyProtectionEnabled { get; set; }
        public int? TokenTypeId { get; set; }
        public decimal? TokenAmount { get; set; }
        public decimal? CcServiceCostAmount { get; set; }
        public int? MaxLimitPerEvent { get; set; }
        public DateTime? PurchaseCutOffDate { get; set; }
        public string ClassName { get; set; }
        public int? StartWeekDay { get; set; }
        public int? EndWeekDay { get; set; }
        public string ShortCodeTemp { get; set; }
        public int? RelatedProductId { get; set; }
        public int? GenreId { get; set; }
        public decimal? TokenAmount2 { get; set; }
        public int? TokenTypeId2 { get; set; }
        public int? ReservationLenghtInDays { get; set; }
        public int? ProductCategoryId { get; set; }
        public int? WebTvProductCategoryTypeId { get; set; }
        public int? CorporationId { get; set; }
        public int? SportContentId { get; set; }
        public int? ProductDurationGroupId { get; set; }
        public int? PackageSalesModelId { get; set; }
        public int? LgigroupingId { get; set; }
        public int? SellableItemId { get; set; }
        public int? ProductOriginId { get; set; }
        public bool? IsCommercialNewSale { get; set; }
        public int? InvoiceItemId { get; set; }
        public int? ProductMainPackageId { get; set; }
        public int? ProductMainProductId { get; set; }
        public bool? AddOnHd { get; set; }
        public bool? AddOn18 { get; set; }
        public bool? AddOnChild { get; set; }
        public bool? AddOnCinema { get; set; }
        public bool? AddOnCinemaHd { get; set; }
        public bool? AddOnDocumentary { get; set; }
        public bool? AddOnDvd { get; set; }
        public bool? IsSubscriptionProduct { get; set; }
        public int? TmpProductChangeId { get; set; }
        public int? ProductCompensationCategoryId { get; set; }
        public string ImageUrl { get; set; }
        public bool? IsInstallationCostProduct { get; set; }
        public bool IsBluProduct { get; set; }
        public DateTime? WebViewStartDate { get; set; }
        public DateTime? WebViewEndDate { get; set; }
        public bool? AddOnXtra { get; set; }
        public int? ValidDayCount { get; set; }
        public int? ProductPackageTypeId { get; set; }
        public bool BluEnabler { get; set; }
        public bool IsAddOn { get; set; }
        public bool AddOnSporXtra { get; set; }
        public string OldName { get; set; }
        public int? ConvertToProductId { get; set; }
        public int? ConvertIsDone { get; set; }
        public int? NotToConvert { get; set; }

        public virtual DsInvoiceItem InvoiceItem { get; set; }
        public virtual ICollection<DsActivationServiceLog> DsActivationServiceLogFromProduct { get; set; }
        public virtual ICollection<DsActivationServiceLog> DsActivationServiceLogToProduct { get; set; }
        public virtual ICollection<DsBonuseski> DsBonuseski { get; set; }
        public virtual ICollection<DsCampaignToExitPrice> DsCampaignToExitPrice { get; set; }
        public virtual ICollection<DsLeasingContractInstallmentTypeToProduct> DsLeasingContractInstallmentTypeToProduct { get; set; }
        public virtual ICollection<DsLeasingContractService> DsLeasingContractService { get; set; }
        public virtual ICollection<DsLeasingContractServiceLog> DsLeasingContractServiceLogBonusProduct { get; set; }
        public virtual ICollection<DsLeasingContractServiceLog> DsLeasingContractServiceLogProduct { get; set; }
        public virtual ICollection<DsLeasingContractServiceStatusReason> DsLeasingContractServiceStatusReason { get; set; }
        public virtual ICollection<DsOfferType> DsOfferType { get; set; }
        public virtual ICollection<DsPackageProduct> DsPackageProductPackage { get; set; }
        public virtual ICollection<DsPackageProduct> DsPackageProductProduct { get; set; }
        public virtual ICollection<DsProductPrice> DsProductPrice { get; set; }
        public virtual ICollection<DsProductToBluAsset> DsProductToBluAsset { get; set; }
        public virtual ICollection<DsProductToService> DsProductToService { get; set; }
        public virtual ICollection<DsSaleCompensationType> DsSaleCompensationType { get; set; }
        public virtual ICollection<DsService> DsService { get; set; }
        public virtual ICollection<DsStbTypeToProduct> DsStbTypeToProduct { get; set; }
        public virtual ICollection<DsWebTvProductMapping> DsWebTvProductMapping { get; set; }
        public virtual ICollection<DsWorkOrderCancelStbLeasingInvoiceItem> DsWorkOrderCancelStbLeasingInvoiceItem { get; set; }
    }
}
