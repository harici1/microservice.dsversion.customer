﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductCategory
    {
        public int ProductCategoryId { get; set; }
        public int ProductCategoryTypeId { get; set; }
        public string Name { get; set; }
    }
}
