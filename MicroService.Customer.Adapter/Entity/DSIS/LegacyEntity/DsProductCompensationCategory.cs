﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductCompensationCategory
    {
        public int ProductCompensationCategoryId { get; set; }
        public string Name { get; set; }
    }
}
