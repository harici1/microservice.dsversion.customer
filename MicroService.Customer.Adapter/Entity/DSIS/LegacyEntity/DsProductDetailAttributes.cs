﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductDetailAttributes
    {
        public int ProductDetailAttributeId { get; set; }
        public int? ProductId { get; set; }
        public int? GenericAttributeItemCode { get; set; }
    }
}
