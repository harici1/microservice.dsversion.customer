﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductMainPackage
    {
        public int ProductMainPackageId { get; set; }
        public string TypeName { get; set; }
        public bool IsVisible { get; set; }
        public int ReferenceProductMainPackageId { get; set; }
    }
}
