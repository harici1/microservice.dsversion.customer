﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductPackageType
    {
        public int ProductPackageTypeId { get; set; }
        public string Name { get; set; }
    }
}
