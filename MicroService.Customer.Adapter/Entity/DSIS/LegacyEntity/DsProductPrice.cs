﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductPrice
    {
        public DsProductPrice()
        {
            DsCampaignToProductPrice = new HashSet<DsCampaignToProductPrice>();
            DsOfferTypeToProductPrice = new HashSet<DsOfferTypeToProductPrice>();
            DsRoleTableConstraint = new HashSet<DsRoleTableConstraint>();
            DsWebTvActivationCustomerTypeOffer = new HashSet<DsWebTvActivationCustomerTypeOffer>();
            DsWebTvActivationToOffer = new HashSet<DsWebTvActivationToOffer>();
            DsWebTvPpvActivation = new HashSet<DsWebTvPpvActivation>();
            DsWebTvPpvEvent = new HashSet<DsWebTvPpvEvent>();
        }

        public int ProductPriceId { get; set; }
        public int ProductId { get; set; }
        public int PaymentTypeId { get; set; }
        public int ContactChannelId { get; set; }
        public decimal Amount { get; set; }
        public decimal ServiceCost { get; set; }
        public int NumberOfInstallments { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? VposId { get; set; }
        public decimal InterestCost { get; set; }
        public string Description { get; set; }
        public int TokenTypeId { get; set; }
        public bool? UseDefaultProductPrice { get; set; }
        public int? TempProductPriceId { get; set; }
        public string Description2 { get; set; }

        public virtual DsPaymentType PaymentType { get; set; }
        public virtual DsProduct Product { get; set; }
        public virtual DsTokenType TokenType { get; set; }
        public virtual ICollection<DsCampaignToProductPrice> DsCampaignToProductPrice { get; set; }
        public virtual ICollection<DsOfferTypeToProductPrice> DsOfferTypeToProductPrice { get; set; }
        public virtual ICollection<DsRoleTableConstraint> DsRoleTableConstraint { get; set; }
        public virtual ICollection<DsWebTvActivationCustomerTypeOffer> DsWebTvActivationCustomerTypeOffer { get; set; }
        public virtual ICollection<DsWebTvActivationToOffer> DsWebTvActivationToOffer { get; set; }
        public virtual ICollection<DsWebTvPpvActivation> DsWebTvPpvActivation { get; set; }
        public virtual ICollection<DsWebTvPpvEvent> DsWebTvPpvEvent { get; set; }
    }
}
