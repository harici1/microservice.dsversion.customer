﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductPriceDetail
    {
        public int ProductPriceDetaiId { get; set; }
        public int ProductPriceId { get; set; }
        public int InvoiceItemId { get; set; }
        public decimal? Amount { get; set; }
        public int? ProductId { get; set; }
    }
}
