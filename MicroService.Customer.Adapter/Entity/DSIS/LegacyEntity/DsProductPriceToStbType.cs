﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductPriceToStbType
    {
        public int ProductPriceToStbTypeId { get; set; }
        public int ProductPriceId { get; set; }
        public int StbTypeId { get; set; }
    }
}
