﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductProcessType
    {
        public DsProductProcessType()
        {
            DsRoleTableConstraint = new HashSet<DsRoleTableConstraint>();
            DsWorkOrderLeasingContractServiceGeneric = new HashSet<DsWorkOrderLeasingContractServiceGeneric>();
        }

        public int ProductProcessTypeId { get; set; }
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string SystemCode { get; set; }
        public bool? IsVisible { get; set; }
        public bool? ValidateInvoiceCount { get; set; }
        public int? SuspendBouquet { get; set; }
        public int? SuspendRegionKey { get; set; }
        public int? PpvExtendDay { get; set; }
        public bool? AllowReActivate { get; set; }
        public bool? CheckAnkaSuspendStatus { get; set; }
        public bool? CheckAnkaReactivateStatus { get; set; }
        public int? ProductProcessTypeCategoryId { get; set; }
        public int? BlackListTypeId { get; set; }
        public bool? CallAnkaService { get; set; }
        public string ValidationStoredProcedure { get; set; }
        public bool ValidateReactivation { get; set; }
        public string ProcessStoredProcedure { get; set; }
        public bool VisibleToWeb { get; set; }

        public virtual ICollection<DsRoleTableConstraint> DsRoleTableConstraint { get; set; }
        public virtual ICollection<DsWorkOrderLeasingContractServiceGeneric> DsWorkOrderLeasingContractServiceGeneric { get; set; }
    }
}
