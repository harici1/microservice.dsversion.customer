﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductProcessTypeCategory
    {
        public int ProductProcessTypeCategoryId { get; set; }
        public string Name { get; set; }
        public bool VisibleToWeb { get; set; }
    }
}
