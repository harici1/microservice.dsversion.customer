﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductSalePriceType
    {
        public int ProductSalePriceTypeId { get; set; }
        public string Name { get; set; }
    }
}
