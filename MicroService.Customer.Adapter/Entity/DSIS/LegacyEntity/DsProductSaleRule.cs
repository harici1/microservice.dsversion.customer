﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductSaleRule
    {
        public int ProductSaleRuleId { get; set; }
        public int ProductSaleRuleMapId { get; set; }
        public int? FromProductSaleRulePackageGroupId { get; set; }
        public int ToProductSaleRulePackageGroupId { get; set; }
        public decimal ProductSalePrice { get; set; }
        public int ProductSalePriceTypeId { get; set; }
        public int? ProductSaleRuleDetailId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Disabled { get; set; }
        public bool Visible { get; set; }
        public string Note { get; set; }
        public int? ProductSaleRuleGroupId { get; set; }
    }
}
