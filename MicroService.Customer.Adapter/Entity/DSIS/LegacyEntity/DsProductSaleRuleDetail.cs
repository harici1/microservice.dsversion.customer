﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductSaleRuleDetail
    {
        public int Id { get; set; }
        public int ProductSaleRuleDetailId { get; set; }
        public int ProductSaleRuleDetailTypeId { get; set; }
        public string Value { get; set; }
    }
}
