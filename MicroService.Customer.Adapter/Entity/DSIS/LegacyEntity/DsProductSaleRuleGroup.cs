﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductSaleRuleGroup
    {
        public int ProductSaleRuleGroupId { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
    }
}
