﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductSaleRuleMap
    {
        public int Id { get; set; }
        public int ProductSaleRuleMapId { get; set; }
        public int ProductSaleRuleMapTypeId { get; set; }
        public string Value { get; set; }
        public string Operator { get; set; }
    }
}
