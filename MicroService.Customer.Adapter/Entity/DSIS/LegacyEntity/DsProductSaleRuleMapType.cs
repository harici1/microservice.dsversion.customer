﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductSaleRuleMapType
    {
        public int ProductSaleRuleMapTypeId { get; set; }
        public string Name { get; set; }
    }
}
