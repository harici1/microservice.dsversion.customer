﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductSaleRuleOffer
    {
        public long ProductSaleRuleOfferId { get; set; }
        public int ProductSaleRuleOfferStatusId { get; set; }
        public int BaseProductSaleRuleId { get; set; }
        public int ProductSaleRuleId { get; set; }
        public int CustomerId { get; set; }
        public int? LeasingContractId { get; set; }
        public int MenuId { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public int LastUpdateById { get; set; }
        public Guid BusinessTransactionId { get; set; }
    }
}
