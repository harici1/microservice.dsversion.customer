﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductSaleRuleOfferStatus
    {
        public int ProductSaleRuleOfferStatusId { get; set; }
        public string Name { get; set; }
    }
}
