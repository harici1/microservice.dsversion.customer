﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductSaleRulePackage
    {
        public int Id { get; set; }
        public int ProductSaleRulePackageId { get; set; }
        public int ProductSaleRulePropertyItemId { get; set; }
    }
}
