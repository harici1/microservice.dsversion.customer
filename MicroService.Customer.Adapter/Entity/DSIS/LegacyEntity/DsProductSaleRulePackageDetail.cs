﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductSaleRulePackageDetail
    {
        public int ProductSaleRulePackageDetailId { get; set; }
        public int ProductSaleRulePackageId { get; set; }
        public int ProductSaleRulePackageDetailTypeId { get; set; }
        public string Value { get; set; }

        public virtual DsProductSaleRulePackageDetailType ProductSaleRulePackageDetailType { get; set; }
    }
}
