﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductSaleRulePackageDetailType
    {
        public DsProductSaleRulePackageDetailType()
        {
            DsProductSaleRulePackageDetail = new HashSet<DsProductSaleRulePackageDetail>();
        }

        public int ProductSaleRulePackageDetailTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsProductSaleRulePackageDetail> DsProductSaleRulePackageDetail { get; set; }
    }
}
