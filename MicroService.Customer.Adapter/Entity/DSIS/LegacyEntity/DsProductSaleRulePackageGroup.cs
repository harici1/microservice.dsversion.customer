﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductSaleRulePackageGroup
    {
        public int Id { get; set; }
        public int ProductSaleRulePackageGroupId { get; set; }
        public int ProductSaleRulePackageId { get; set; }
    }
}
