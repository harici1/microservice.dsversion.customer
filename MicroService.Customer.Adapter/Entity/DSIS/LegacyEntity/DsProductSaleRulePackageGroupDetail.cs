﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductSaleRulePackageGroupDetail
    {
        public int ProductSaleRulePackageGroupDetailId { get; set; }
        public int ProductSaleRulePackageGroupId { get; set; }
        public decimal? ListPrice { get; set; }
        public decimal? MinPrice { get; set; }
        public decimal? ChurnPrice { get; set; }
        public decimal? ChurnMinprice { get; set; }
    }
}
