﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductSaleRulePackageGroupPivot
    {
        public int ProductSaleRulePackageGroupPivotId { get; set; }
        public int ProductSaleRulePackageGroupId { get; set; }
        public string Bandwidth { get; set; }
        public string Quota { get; set; }
        public string Akn { get; set; }
        public string Naked { get; set; }
        public string Tech { get; set; }
        public string PpvProduct { get; set; }
    }
}
