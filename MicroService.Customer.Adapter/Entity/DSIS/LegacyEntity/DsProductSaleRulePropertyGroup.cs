﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductSaleRulePropertyGroup
    {
        public int ProductSaleRulePropertyGroupId { get; set; }
        public string Name { get; set; }
    }
}
