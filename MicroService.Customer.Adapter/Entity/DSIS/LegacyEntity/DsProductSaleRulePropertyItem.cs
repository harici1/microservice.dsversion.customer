﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductSaleRulePropertyItem
    {
        public int ProductSaleRulePropertyItemId { get; set; }
        public int ProductSaleRulePropertyId { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}
