﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductToAddOnProduct
    {
        public int ProductToAddOnProductId { get; set; }
        public int ProductId { get; set; }
        public int? AllowedAddOnProductId { get; set; }
        public int? RequiredAddOnProductId { get; set; }
        public int? ForbiddenAddOnProductId { get; set; }
        public int? ForcedAddOnProductId { get; set; }
        public string Notes { get; set; }
    }
}
