﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductToBluAsset
    {
        public int ProductToBluAssetId { get; set; }
        public int ProductId { get; set; }
        public string AssetCode { get; set; }

        public virtual DsProduct Product { get; set; }
    }
}
