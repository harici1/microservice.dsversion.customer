﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductToBluPackage
    {
        public int ProductToBluPackageId { get; set; }
        public int ProductId { get; set; }
        public string BluPackageName { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
    }
}
