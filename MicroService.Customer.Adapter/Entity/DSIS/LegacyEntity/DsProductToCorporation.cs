﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductToCorporation
    {
        public int ProductToCorporationId { get; set; }
        public int ProductId { get; set; }
        public int CorporationId { get; set; }
    }
}
