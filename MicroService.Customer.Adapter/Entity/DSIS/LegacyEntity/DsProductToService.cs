﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductToService
    {
        public int ProductToServiceId { get; set; }
        public int ProductId { get; set; }
        public int ServiceId { get; set; }
        public int StbTypeId { get; set; }
        public string Description { get; set; }

        public virtual DsProduct Product { get; set; }
        public virtual DsService Service { get; set; }
    }
}
