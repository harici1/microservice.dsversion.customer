﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProductType
    {
        public DsProductType()
        {
            DsInventoryTransferRule = new HashSet<DsInventoryTransferRule>();
        }

        public int ProductTypeId { get; set; }
        public string TypeName { get; set; }

        public virtual ICollection<DsInventoryTransferRule> DsInventoryTransferRule { get; set; }
    }
}
