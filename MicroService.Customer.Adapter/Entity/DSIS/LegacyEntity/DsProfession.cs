﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsProfession
    {
        public DsProfession()
        {
            DsGenericCustomer = new HashSet<DsGenericCustomer>();
        }

        public int ProfessionId { get; set; }
        public string Name { get; set; }
        public int? SystemCode { get; set; }

        public virtual ICollection<DsGenericCustomer> DsGenericCustomer { get; set; }
    }
}
