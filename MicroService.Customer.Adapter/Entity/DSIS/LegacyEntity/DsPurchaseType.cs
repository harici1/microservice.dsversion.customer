﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsPurchaseType
    {
        public int PurchaseTypeId { get; set; }
        public string Name { get; set; }
    }
}
