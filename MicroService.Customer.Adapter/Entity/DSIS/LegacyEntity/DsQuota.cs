﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsQuota
    {
        public int QuotaId { get; set; }
        public int QuotaTypeId { get; set; }
        public int QuotaPeriodId { get; set; }
        public int PartyId { get; set; }
        public int QuotaLimit { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
    }
}
