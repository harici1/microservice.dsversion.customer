﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsQuotaCategory
    {
        public DsQuotaCategory()
        {
            DsQuotaType = new HashSet<DsQuotaType>();
        }

        public int QuotaCategoryId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsQuotaType> DsQuotaType { get; set; }
    }
}
