﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsQuotaType
    {
        public int QuotaTypeId { get; set; }
        public string Name { get; set; }
        public int QuotaCategoryId { get; set; }

        public virtual DsQuotaCategory QuotaCategory { get; set; }
    }
}
