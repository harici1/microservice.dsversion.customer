﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsReactivateAnkaAccount
    {
        public int ReactivateAnkaAccountId { get; set; }
        public int? LeasingContractId { get; set; }
        public bool SuspendStatus { get; set; }
        public bool ReactivateStatus { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? LastUpdateById { get; set; }
        public int? PartyId { get; set; }
        public string ExceptionMessage { get; set; }
    }
}
