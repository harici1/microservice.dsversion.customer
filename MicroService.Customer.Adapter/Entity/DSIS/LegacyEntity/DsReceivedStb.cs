﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsReceivedStb
    {
        public int OrderId { get; set; }
        public int StbBrandId { get; set; }
        public int StbTypeId { get; set; }
        public string StbModelName { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public string ReferenceNumber { get; set; }
        public string StbBrandName { get; set; }

        public virtual DsStbBrand StbBrand { get; set; }
        public virtual DsStbType StbType { get; set; }
    }
}
