﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsRegionKey
    {
        public int RegionKeyId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
