﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsRegisteredEmail
    {
        public int DsRegisteredEmailId { get; set; }
        public int UserId { get; set; }
        public int RegisteredEmailRequestId { get; set; }
        public string Email { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
