﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsRegisteredEmailRequest
    {
        public int RegisteredEmailRequestId { get; set; }
        public int UserId { get; set; }
        public string Email { get; set; }
        public string ActivationCode { get; set; }
        public int CreatedById { get; set; }
        public DateTime ExpireDate { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
