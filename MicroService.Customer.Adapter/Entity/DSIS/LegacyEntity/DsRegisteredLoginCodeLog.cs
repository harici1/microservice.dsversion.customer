﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsRegisteredLoginCodeLog
    {
        public int RegisteredLoginCodeLogId { get; set; }
        public int RegisteredLoginCodeLogTypeId { get; set; }
        public int UserId { get; set; }
        public string RegisteredLoginCode { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
