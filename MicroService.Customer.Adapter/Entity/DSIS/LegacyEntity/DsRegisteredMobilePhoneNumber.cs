﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsRegisteredMobilePhoneNumber
    {
        public int RegisteredMobilePhoneNumberId { get; set; }
        public int UserId { get; set; }
        public string RegisteredMobilePhoneNumber { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
