﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsRegistryToLogisticsInventory
    {
        public int RegistryToLogisticsInventoryId { get; set; }
        public string StbSerialNumber { get; set; }
        public string StbModel { get; set; }
        public string Name { get; set; }
        public long? ShortSerialNumber { get; set; }
        public int? PackageNumber { get; set; }
        public string DealerCode { get; set; }
        public string DealerName { get; set; }
        public string PhoneNumber { get; set; }
        public string DealerTaxNumber { get; set; }
        public int? ConsignmentNumber { get; set; }
        public int? InvoiceNumber { get; set; }
        public int? RemoteEquipmentStatusId { get; set; }
        public int? ScartCableEquipmentStatusId { get; set; }
        public int? HdmiequipmentStatusId { get; set; }
        public string ReturnReason { get; set; }
        public string Description { get; set; }
        public int? StbStatusId { get; set; }
        public DateTime? ShipmentDate { get; set; }
        public DateTime? DocumentDate { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? PaletteNo { get; set; }
        public int? GuaranteeStatusId { get; set; }
        public string StbType { get; set; }
        public int? CreatedById { get; set; }
        public int? StbId { get; set; }
    }
}
