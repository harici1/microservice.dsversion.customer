﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsRegistryToLogisticsInventoryEquipmentStatus
    {
        public int RegistryToLogisticsInventoryEquipmentStatusId { get; set; }
        public string Status { get; set; }
    }
}
