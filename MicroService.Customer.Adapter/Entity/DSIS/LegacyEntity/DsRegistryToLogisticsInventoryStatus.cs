﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsRegistryToLogisticsInventoryStatus
    {
        public int RegistryToLogisticsInventoryStatusId { get; set; }
        public string Status { get; set; }
    }
}
