﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsRemindProgramServiceLog
    {
        public int LogId { get; set; }
        public int ActivationId { get; set; }
        public int SiServiceId { get; set; }
        public string TrafficKey { get; set; }
        public int EventId { get; set; }
        public DateTime EventDate { get; set; }
        public DateTime CreationDate { get; set; }
        public string ChannelName { get; set; }
        public string ProgramName { get; set; }
        public string MobilePhoneNumber { get; set; }
        public int? ContactChannelId { get; set; }
        public int? ContactChannelSubDomainId { get; set; }
    }
}
