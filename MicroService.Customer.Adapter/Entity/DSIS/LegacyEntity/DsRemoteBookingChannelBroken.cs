﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsRemoteBookingChannelBroken
    {
        public int RemoteBookingChannelId { get; set; }
        public int SelectNumber { get; set; }
        public string ChannelName { get; set; }
        public int SiServiceId { get; set; }
        public int OrigNetworkId { get; set; }
        public int TransportId { get; set; }
        public int StbTypeId { get; set; }
        public int? RemoteBookingChannelCategoryId { get; set; }
        public string ImageUrl { get; set; }
        public bool SirecordingAvaliable { get; set; }
        public bool IsActive { get; set; }
        public string SiServiceKey { get; set; }
    }
}
