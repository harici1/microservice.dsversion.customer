﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsRemoteBookingChannelToProduct
    {
        public int RemoteBookingChannelToProductId { get; set; }
        public int ProductId { get; set; }
        public int RemoteBookingChannelId { get; set; }
    }
}
