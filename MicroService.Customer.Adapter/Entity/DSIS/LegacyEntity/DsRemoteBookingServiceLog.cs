﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsRemoteBookingServiceLog
    {
        public int LogId { get; set; }
        public int ActivationId { get; set; }
        public int RemoteBookingChannelId { get; set; }
        public int EventId { get; set; }
        public DateTime EventDate { get; set; }
        public string Duration { get; set; }
        public DateTime CreationDate { get; set; }
        public int UserId { get; set; }
        public string PostXml { get; set; }
        public string ResponseXml { get; set; }
        public string ChannelName { get; set; }
        public string ProgramName { get; set; }
        public int? ContactChannelId { get; set; }
        public int? ContactChannelSubDomainId { get; set; }
    }
}
