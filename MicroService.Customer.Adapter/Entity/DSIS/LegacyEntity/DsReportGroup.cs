﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsReportGroup
    {
        public int ReportGroupId { get; set; }
        public int ReportGroupTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual DsReportGroupType ReportGroupType { get; set; }
    }
}
