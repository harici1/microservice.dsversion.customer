﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsReportGroupType
    {
        public DsReportGroupType()
        {
            DsReportGroup = new HashSet<DsReportGroup>();
        }

        public int ReportGroupTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<DsReportGroup> DsReportGroup { get; set; }
    }
}
