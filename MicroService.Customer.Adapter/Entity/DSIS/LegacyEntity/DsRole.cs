﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsRole
    {
        public DsRole()
        {
            Crmv2ProductOfferCatalogToRole = new HashSet<Crmv2ProductOfferCatalogToRole>();
            DsPopupToRole = new HashSet<DsPopupToRole>();
            DsRoleAuthorization = new HashSet<DsRoleAuthorization>();
            DsRoleTableConstraintRole = new HashSet<DsRoleTableConstraint>();
            DsRoleTableConstraintRole2 = new HashSet<DsRoleTableConstraint>();
            DsUserRole = new HashSet<DsUserRole>();
            DsWorkOrderLogTypeRuleFromRole = new HashSet<DsWorkOrderLogTypeRule>();
            DsWorkOrderLogTypeRuleToRole = new HashSet<DsWorkOrderLogTypeRule>();
        }

        public int RoleId { get; set; }
        public string Name { get; set; }
        public bool BaseRole { get; set; }
        public int PartyTypeId { get; set; }
        public string SystemCode { get; set; }
        public bool VisibleToSubDealer { get; set; }

        public virtual DsBonusUserRight DsBonusUserRight { get; set; }
        public virtual ICollection<Crmv2ProductOfferCatalogToRole> Crmv2ProductOfferCatalogToRole { get; set; }
        public virtual ICollection<DsPopupToRole> DsPopupToRole { get; set; }
        public virtual ICollection<DsRoleAuthorization> DsRoleAuthorization { get; set; }
        public virtual ICollection<DsRoleTableConstraint> DsRoleTableConstraintRole { get; set; }
        public virtual ICollection<DsRoleTableConstraint> DsRoleTableConstraintRole2 { get; set; }
        public virtual ICollection<DsUserRole> DsUserRole { get; set; }
        public virtual ICollection<DsWorkOrderLogTypeRule> DsWorkOrderLogTypeRuleFromRole { get; set; }
        public virtual ICollection<DsWorkOrderLogTypeRule> DsWorkOrderLogTypeRuleToRole { get; set; }
    }
}
