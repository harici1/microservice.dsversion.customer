﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsRoleAuthorization
    {
        public int RoleId { get; set; }
        public int MenuId { get; set; }

        public virtual DsMenu Menu { get; set; }
        public virtual DsRole Role { get; set; }
    }
}
