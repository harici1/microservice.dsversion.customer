﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsRoleTableConstraint
    {
        public int RoleTableConstraintId { get; set; }
        public int RoleId { get; set; }
        public int MenuId { get; set; }
        public int? ActivationSalesModelId { get; set; }
        public int? CustomerTypeId { get; set; }
        public int? CampaignId { get; set; }
        public int? ProductPriceId { get; set; }
        public int? LeasingContractProductPriceId { get; set; }
        public int? OfferTypeId { get; set; }
        public int? BankAccountId { get; set; }
        public int? ProductProcessTypeId { get; set; }
        public int? StbUsageTypeId { get; set; }
        public int? SmartCardUsageTypeId { get; set; }
        public int? BouquetId { get; set; }
        public int? ContentProductId { get; set; }
        public int? GenericCustomerTypeId { get; set; }
        public int? Role2Id { get; set; }
        public int? FromCustomerTypeId { get; set; }
        public int? DealerDocumentId { get; set; }
        public int? SmartCardReservationTypeId { get; set; }
        public int? StsTypeId { get; set; }
        public int? DsisDealerMenuId { get; set; }
        public int? SurveyTypeId { get; set; }
        public int? DealerDocumentCategoryId { get; set; }
        public int? LeasingContractInstallmentTypeToProductId { get; set; }
        public int? StbStatusId { get; set; }
        public int? NumberOfInstallment { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string Notes { get; set; }
        public int? CustomerInfoSmstextId { get; set; }

        public virtual DsActivationSalesModel ActivationSalesModel { get; set; }
        public virtual DsBankAccount BankAccount { get; set; }
        public virtual DsBouquet Bouquet { get; set; }
        public virtual DsCampaign Campaign { get; set; }
        public virtual DsCustomerType CustomerType { get; set; }
        public virtual DsLeasingContractProductPrice LeasingContractProductPrice { get; set; }
        public virtual DsMenu Menu { get; set; }
        public virtual DsProductPrice ProductPrice { get; set; }
        public virtual DsProductProcessType ProductProcessType { get; set; }
        public virtual DsRole Role { get; set; }
        public virtual DsRole Role2 { get; set; }
        public virtual DsSmartCardUsageType SmartCardUsageType { get; set; }
        public virtual DsStbUsageType StbUsageType { get; set; }
    }
}
