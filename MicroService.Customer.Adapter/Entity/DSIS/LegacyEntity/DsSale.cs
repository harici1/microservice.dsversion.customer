﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSale
    {
        public int SaleId { get; set; }
        public int PartyId { get; set; }
        public int? ActivationId { get; set; }
        public int ProductId { get; set; }
        public decimal ChargeAmount { get; set; }
        public decimal Price { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal ListPrice { get; set; }
        public int PaymentTypeId { get; set; }
        public int ContactChannelId { get; set; }
        public int? CampaignId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int DocumentId { get; set; }
        public int DocumentTypeId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public bool IsCancelled { get; set; }
        public int? RelatedSaleId { get; set; }
        public int? DealerId { get; set; }
        public int? CancelledByDocumentId { get; set; }
        public int? CancelledByDocumentTypeId { get; set; }
        public int? CancelledByUserId { get; set; }
        public DateTime? CancelDate { get; set; }
    }
}
