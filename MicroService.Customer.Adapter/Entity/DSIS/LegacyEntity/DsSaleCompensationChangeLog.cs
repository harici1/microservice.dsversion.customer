﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSaleCompensationChangeLog
    {
        public int SaleCompensationChangeLogId { get; set; }
        public int SaleCompensationId { get; set; }
        public int SaleCompensationChangeLogTypeId { get; set; }
        public int SaleCompensationPeriodId { get; set; }
        public int SaleCompensationStatusId { get; set; }
        public int? SaleCompensationStatusReasonId { get; set; }
        public int CorporationId { get; set; }
        public int DealerId { get; set; }
        public int BatchId { get; set; }
        public int? InvoiceId { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public decimal Amount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public int? ConfirmedByUserId { get; set; }
        public string Note { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public DateTime? InvoiceProcessDate { get; set; }
        public DateTime? SapSendDate { get; set; }
        public int? TempId { get; set; }

        public virtual DsSaleCompensation SaleCompensation { get; set; }
        public virtual DsSaleCompensationChangeLogType SaleCompensationChangeLogType { get; set; }
    }
}
