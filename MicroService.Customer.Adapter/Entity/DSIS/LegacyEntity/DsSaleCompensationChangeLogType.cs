﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSaleCompensationChangeLogType
    {
        public DsSaleCompensationChangeLogType()
        {
            DsSaleCompensationChangeLog = new HashSet<DsSaleCompensationChangeLog>();
        }

        public int SaleCompensationChangeLogTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsSaleCompensationChangeLog> DsSaleCompensationChangeLog { get; set; }
    }
}
