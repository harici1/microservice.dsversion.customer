﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSaleCompensationLineInformal
    {
        public int SaleCompensationLineInformalId { get; set; }
        public int SaleCompensationInformalId { get; set; }
        public int SaleCompensationTypeId { get; set; }
        public decimal Amount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal UnitPrice { get; set; }
        public int ItemCount { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
    }
}
