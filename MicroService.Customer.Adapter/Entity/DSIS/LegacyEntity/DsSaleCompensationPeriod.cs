﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSaleCompensationPeriod
    {
        public DsSaleCompensationPeriod()
        {
            DsSaleCompensation = new HashSet<DsSaleCompensation>();
        }

        public int SaleCompensationPeriodId { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime ProcessDate { get; set; }
        public bool SapPeriodIsClosed { get; set; }
        public bool? IsVisible { get; set; }
        public bool IsFormal { get; set; }

        public virtual ICollection<DsSaleCompensation> DsSaleCompensation { get; set; }
    }
}
