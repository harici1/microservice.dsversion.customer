﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSaleCompensationStatus
    {
        public DsSaleCompensationStatus()
        {
            DsSaleCompensation = new HashSet<DsSaleCompensation>();
            DsSaleCompensationStatusHistory = new HashSet<DsSaleCompensationStatusHistory>();
            DsSaleCompensationStatusReason = new HashSet<DsSaleCompensationStatusReason>();
        }

        public int SaleCompensationStatusId { get; set; }
        public string Name { get; set; }
        public int? SystemCode { get; set; }
        public string Description { get; set; }
        public bool IsVisible { get; set; }

        public virtual ICollection<DsSaleCompensation> DsSaleCompensation { get; set; }
        public virtual ICollection<DsSaleCompensationStatusHistory> DsSaleCompensationStatusHistory { get; set; }
        public virtual ICollection<DsSaleCompensationStatusReason> DsSaleCompensationStatusReason { get; set; }
    }
}
