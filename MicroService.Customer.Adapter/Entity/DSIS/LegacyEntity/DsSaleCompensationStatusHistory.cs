﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSaleCompensationStatusHistory
    {
        public int SaleCompensationStatusHistoryId { get; set; }
        public int SaleCompensationId { get; set; }
        public int SaleCompensationStatusId { get; set; }
        public int? SaleCompensationStatusReasonId { get; set; }
        public int UpdatedById { get; set; }
        public DateTime UpdateDate { get; set; }

        public virtual DsSaleCompensation SaleCompensation { get; set; }
        public virtual DsSaleCompensationStatus SaleCompensationStatus { get; set; }
        public virtual DsSaleCompensationStatusReason SaleCompensationStatusReason { get; set; }
    }
}
