﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSaleCompensationStatusReason
    {
        public DsSaleCompensationStatusReason()
        {
            DsSaleCompensation = new HashSet<DsSaleCompensation>();
            DsSaleCompensationStatusHistory = new HashSet<DsSaleCompensationStatusHistory>();
        }

        public int SaleCompensationStatusReasonId { get; set; }
        public int SaleCompensationStatusId { get; set; }
        public string Name { get; set; }

        public virtual DsSaleCompensationStatus SaleCompensationStatus { get; set; }
        public virtual ICollection<DsSaleCompensation> DsSaleCompensation { get; set; }
        public virtual ICollection<DsSaleCompensationStatusHistory> DsSaleCompensationStatusHistory { get; set; }
    }
}
