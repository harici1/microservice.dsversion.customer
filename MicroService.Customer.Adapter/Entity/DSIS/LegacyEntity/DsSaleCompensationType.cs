﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSaleCompensationType
    {
        public int SaleCompensationTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ProductId { get; set; }
        public int CorporationId { get; set; }
        public string ClassName { get; set; }
        public bool ContractDeliverIsForced { get; set; }

        public virtual DsProduct Product { get; set; }
    }
}
