﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSaleCompensationTypeRule
    {
        public int SaleCompensationTypeRuleId { get; set; }
        public int SaleCompensationTypeId { get; set; }
        public int? CampaignId { get; set; }
        public int? ProductId { get; set; }
        public bool? IsActive { get; set; }
    }
}
