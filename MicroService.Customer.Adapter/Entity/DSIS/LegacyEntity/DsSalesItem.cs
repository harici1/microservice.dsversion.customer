﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSalesItem
    {
        public int SalesItemId { get; set; }
        public string ExternalSystemProductName { get; set; }
        public string ExternalSystemProductCode { get; set; }
        public decimal ExternalSystemAmount { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? ProductId { get; set; }
        public int? OfferTypeId { get; set; }
    }
}
