﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSalesOrderProduct
    {
        public int SaleOrderProductId { get; set; }
        public int DocumentId { get; set; }
        public int DocumentTypeId { get; set; }
        public int ProductId { get; set; }
        public int Amount { get; set; }
    }
}
