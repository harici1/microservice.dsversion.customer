﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSalesSource
    {
        public int SalesSourceId { get; set; }
        public string Name { get; set; }
        public int SalesSourceCategoryId { get; set; }
        public bool IsVisible { get; set; }

        public virtual DsSalesSourceCategory SalesSourceCategory { get; set; }
    }
}
