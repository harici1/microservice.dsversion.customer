﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSalesSourceCategory
    {
        public DsSalesSourceCategory()
        {
            DsSalesSource = new HashSet<DsSalesSource>();
        }

        public int SalesSourceCategoryId { get; set; }
        public string Name { get; set; }
        public bool IsVisible { get; set; }

        public virtual ICollection<DsSalesSource> DsSalesSource { get; set; }
    }
}
