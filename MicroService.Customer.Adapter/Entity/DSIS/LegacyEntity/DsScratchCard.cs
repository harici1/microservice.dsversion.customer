﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsScratchCard
    {
        public DsScratchCard()
        {
            DsScratchCardTransaction = new HashSet<DsScratchCardTransaction>();
        }

        public int ScratchCardId { get; set; }
        public int ScratchCardBatchId { get; set; }
        public string PinCode { get; set; }
        public string SerialNumber { get; set; }
        public bool IsAuthorized { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public string DealerPinCode { get; set; }
        public int? DealerId { get; set; }
        public decimal? Amount { get; set; }
        public int? CurrencyId { get; set; }
        public decimal? LocalCurrencyEquivalent { get; set; }
        public decimal? CcServiceCostAmount { get; set; }
        public int? TypeId { get; set; }
        public int LocationPartyId { get; set; }
        public bool? IsCancelled { get; set; }
        public int? TokenType2Id { get; set; }
        public decimal? TokenAmount { get; set; }
        public decimal? UnitPaidAmount { get; set; }
        public string Note { get; set; }

        public virtual ICollection<DsScratchCardTransaction> DsScratchCardTransaction { get; set; }
    }
}
