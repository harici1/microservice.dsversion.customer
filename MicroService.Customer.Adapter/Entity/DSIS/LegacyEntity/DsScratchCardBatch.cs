﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsScratchCardBatch
    {
        public int ScratchCardBatchId { get; set; }
        public string ScratchCardBatchCode { get; set; }
        public int ProductId { get; set; }
        public int DistributorId { get; set; }
        public int? CampaignId { get; set; }
        public bool? CheckActivationCampaign { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int? ScratchCardCount { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public string Note { get; set; }
    }
}
