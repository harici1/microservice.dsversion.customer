﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsScratchCardTransaction
    {
        public int ScratchCardTransactionId { get; set; }
        public int ScratchCardId { get; set; }
        public int TransactionTypeId { get; set; }
        public int? FromParty { get; set; }
        public int ToParty { get; set; }
        public string ReferenceNumber { get; set; }
        public int DocumentTypeId { get; set; }
        public int DocumentId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }

        public virtual DsScratchCard ScratchCard { get; set; }
        public virtual DsScratchCardTransactionType TransactionType { get; set; }
    }
}
