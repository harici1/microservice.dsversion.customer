﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsScratchCardTransactionType
    {
        public DsScratchCardTransactionType()
        {
            DsScratchCardTransaction = new HashSet<DsScratchCardTransaction>();
        }

        public int ScratchCardTransactionTypeId { get; set; }
        public string TypeName { get; set; }

        public virtual ICollection<DsScratchCardTransaction> DsScratchCardTransaction { get; set; }
    }
}
