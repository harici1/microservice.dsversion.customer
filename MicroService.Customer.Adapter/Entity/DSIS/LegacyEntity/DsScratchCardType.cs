﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsScratchCardType
    {
        public int ScratchCardTypeId { get; set; }
        public string TypeName { get; set; }
    }
}
