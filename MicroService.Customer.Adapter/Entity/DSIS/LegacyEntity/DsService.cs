﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsService
    {
        public DsService()
        {
            DsContentProduct = new HashSet<DsContentProduct>();
            DsProductToService = new HashSet<DsProductToService>();
        }

        public int ServiceId { get; set; }
        public int? ServiceTypeId { get; set; }
        public int? NdsserviceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? ProductId { get; set; }
        public string NdsproductId { get; set; }
        public string NdsproductType { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public string ServiceParameter { get; set; }

        public virtual DsProduct Product { get; set; }
        public virtual ICollection<DsContentProduct> DsContentProduct { get; set; }
        public virtual ICollection<DsProductToService> DsProductToService { get; set; }
    }
}
