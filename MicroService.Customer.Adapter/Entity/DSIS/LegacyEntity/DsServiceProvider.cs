﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsServiceProvider
    {
        public int ServiceProviderId { get; set; }
        public string Name { get; set; }
        public int? SystemCode { get; set; }
    }
}
