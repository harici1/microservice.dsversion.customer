﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsServiceType
    {
        public int ServiceTypeId { get; set; }
        public string Name { get; set; }
    }
}
