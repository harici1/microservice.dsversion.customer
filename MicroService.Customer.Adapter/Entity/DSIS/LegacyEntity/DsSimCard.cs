﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSimCard
    {
        public DsSimCard()
        {
            DsSimCardTransaction = new HashSet<DsSimCardTransaction>();
        }

        public int SimCardId { get; set; }
        public int SimCardTypeId { get; set; }
        public int SimCardTransactionTypeId { get; set; }
        public string SerialNumber { get; set; }
        public string PhoneNumber { get; set; }
        public int LocationPartyId { get; set; }
        public int LastDocumentId { get; set; }
        public int LastDocumentTypeId { get; set; }

        public virtual DsSimCardTransactionType SimCardTransactionType { get; set; }
        public virtual DsSimCardType SimCardType { get; set; }
        public virtual ICollection<DsSimCardTransaction> DsSimCardTransaction { get; set; }
    }
}
