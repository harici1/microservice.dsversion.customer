﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSimCardTransaction
    {
        public int SimCardTransactionId { get; set; }
        public int SimCardId { get; set; }
        public int SimCardTransactionTypeId { get; set; }
        public int FromPartyId { get; set; }
        public int ToPartyId { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public int DocumentId { get; set; }
        public int DocumentTypeId { get; set; }

        public virtual DsSimCard SimCard { get; set; }
        public virtual DsSimCardTransactionType SimCardTransactionType { get; set; }
    }
}
