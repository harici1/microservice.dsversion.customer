﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSimCardTransactionType
    {
        public DsSimCardTransactionType()
        {
            DsSimCard = new HashSet<DsSimCard>();
            DsSimCardTransaction = new HashSet<DsSimCardTransaction>();
        }

        public int SimCardTransactionTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsSimCard> DsSimCard { get; set; }
        public virtual ICollection<DsSimCardTransaction> DsSimCardTransaction { get; set; }
    }
}
