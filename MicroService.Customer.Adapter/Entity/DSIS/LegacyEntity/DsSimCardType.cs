﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSimCardType
    {
        public DsSimCardType()
        {
            DsSimCard = new HashSet<DsSimCard>();
        }

        public int SimCardTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsSimCard> DsSimCard { get; set; }
    }
}
