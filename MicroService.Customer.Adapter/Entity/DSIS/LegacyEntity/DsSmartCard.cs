﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSmartCard
    {
        public DsSmartCard()
        {
            DsBettingCustomer = new HashSet<DsBettingCustomer>();
            DsCampaignToSmartCard = new HashSet<DsCampaignToSmartCard>();
            DsSmartCardChangeLog = new HashSet<DsSmartCardChangeLog>();
        }

        public int SmartCardId { get; set; }
        public string SerialNumber { get; set; }
        public long ShortSerialNumber { get; set; }
        public int ProductId { get; set; }
        public string NdsbatchNo { get; set; }
        public string NdsboxNo { get; set; }
        public int? LastTransactionId { get; set; }
        public int? Stbid { get; set; }
        public int? PartyId { get; set; }
        public int? NdssubscriberId { get; set; }
        public int? RegionKeyId { get; set; }
        public bool IsPaired { get; set; }
        public bool IsActivated { get; set; }
        public bool? IsFreeToAirEnableSet { get; set; }
        public int PhysicalStatusId { get; set; }
        public int? NdsbouquetId { get; set; }
        public int? ParentalRatingId { get; set; }
        public string PersonelBits { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? UsageTypeId { get; set; }
        public DateTime? FirstActivationDate { get; set; }
        public string RegisteredMobilePhoneNumber { get; set; }
        public int? MultiSwitchInstallationEquipmentId { get; set; }
        public int? AdditionalDisplayCount { get; set; }
        public DateTime? LastActivationDate { get; set; }
        public int CompatibleStbUsageTypeId { get; set; }
        public int? ActivationOrderId { get; set; }
        public int ReservationTypeId { get; set; }
        public string Note { get; set; }
        public int? SmartCardNdspriceId { get; set; }
        public bool? IsLocked { get; set; }

        public virtual DsSmartCardReservationType ReservationType { get; set; }
        public virtual ICollection<DsBettingCustomer> DsBettingCustomer { get; set; }
        public virtual ICollection<DsCampaignToSmartCard> DsCampaignToSmartCard { get; set; }
        public virtual ICollection<DsSmartCardChangeLog> DsSmartCardChangeLog { get; set; }
    }
}
