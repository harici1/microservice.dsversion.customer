﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSmartCardChangeLog
    {
        public int SmartCardChangeLogId { get; set; }
        public int? SmartCardId { get; set; }
        public int? TypeId { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? CreatedById { get; set; }
        public string OldValues { get; set; }
        public string NewValues { get; set; }

        public virtual DsSmartCard SmartCard { get; set; }
        public virtual DsSmartCardChangeLogType Type { get; set; }
    }
}
