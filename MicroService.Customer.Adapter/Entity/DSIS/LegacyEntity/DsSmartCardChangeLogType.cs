﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSmartCardChangeLogType
    {
        public DsSmartCardChangeLogType()
        {
            DsSmartCardChangeLog = new HashSet<DsSmartCardChangeLog>();
        }

        public int SmartCardChangeLogTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<DsSmartCardChangeLog> DsSmartCardChangeLog { get; set; }
    }
}
