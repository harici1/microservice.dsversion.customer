﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSmartCardNdsprice
    {
        public int SmartCardNdspriceId { get; set; }
        public string Name { get; set; }
    }
}
