﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSmartCardReservationType
    {
        public DsSmartCardReservationType()
        {
            DsSmartCard = new HashSet<DsSmartCard>();
            DsStbUsageTypeToCustomerType = new HashSet<DsStbUsageTypeToCustomerType>();
        }

        public int SmartCardReservationTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<DsSmartCard> DsSmartCard { get; set; }
        public virtual ICollection<DsStbUsageTypeToCustomerType> DsStbUsageTypeToCustomerType { get; set; }
    }
}
