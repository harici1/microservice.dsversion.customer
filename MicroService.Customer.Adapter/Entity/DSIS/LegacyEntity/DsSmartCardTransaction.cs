﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSmartCardTransaction
    {
        public int SmartCardTransactionId { get; set; }
        public int SmartCardId { get; set; }
        public int TransactionTypeId { get; set; }
        public int FromParty { get; set; }
        public int ToParty { get; set; }
        public DateTime TransactionDate { get; set; }
        public string ReferenceNumber { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? DocumentId { get; set; }
        public int? EntitlementTypeId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
