﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSmartCardUsageType
    {
        public DsSmartCardUsageType()
        {
            DsRoleTableConstraint = new HashSet<DsRoleTableConstraint>();
        }

        public int SmartCardUsageTypeId { get; set; }
        public string Name { get; set; }
        public int PersonelBitNumber { get; set; }

        public virtual ICollection<DsRoleTableConstraint> DsRoleTableConstraint { get; set; }
    }
}
