﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSmsType
    {
        public int Id { get; set; }
        public string SmsTypeName { get; set; }
    }
}
