﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSocialIdentificationRegistrationOrder
    {
        public int SocialIdentificationRegistrationOrderId { get; set; }
        public int? CustomerId { get; set; }
        public int StatusId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreatonDate { get; set; }
        public string SocialIdentificationNumber { get; set; }
        public int ContactChannelId { get; set; }
        public string Description { get; set; }
        public string MernisReturnResult { get; set; }
        public string MernisReturnObject { get; set; }

        public virtual DsContactChannel ContactChannel { get; set; }
        public virtual DsSocialIdentificationRegistrationOrderStatus Status { get; set; }
    }
}
