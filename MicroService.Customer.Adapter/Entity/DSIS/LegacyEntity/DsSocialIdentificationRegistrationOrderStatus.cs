﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSocialIdentificationRegistrationOrderStatus
    {
        public DsSocialIdentificationRegistrationOrderStatus()
        {
            DsSocialIdentificationRegistrationOrder = new HashSet<DsSocialIdentificationRegistrationOrder>();
        }

        public int SocialIdentificationRegistrationOrderStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsSocialIdentificationRegistrationOrder> DsSocialIdentificationRegistrationOrder { get; set; }
    }
}
