﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSorunTanimi
    {
        public DsSorunTanimi()
        {
            DsCozumTanimi = new HashSet<DsCozumTanimi>();
        }

        public int KonuId { get; set; }
        public int SorunId { get; set; }
        public string Tanimi { get; set; }
        public string Aciklama { get; set; }
        public int? OlusturanKullanici { get; set; }
        public DateTime? OlusturmaTarihi { get; set; }
        public int? DuzeltenKullanici { get; set; }
        public DateTime? DuzeltmeTarihi { get; set; }
        public byte? Durumu { get; set; }
        public string BayiMust { get; set; }
        public string KutuMust { get; set; }

        public virtual DsDestekKonusu Konu { get; set; }
        public virtual ICollection<DsCozumTanimi> DsCozumTanimi { get; set; }
    }
}
