﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSpCallLog
    {
        public long SpCallLogId { get; set; }
        public string SpName { get; set; }
        public string SpParameter { get; set; }
        public string HostName { get; set; }
        public string LoginName { get; set; }
        public DateTime CreationDate { get; set; }
        public string ExceptionMessage { get; set; }
    }
}
