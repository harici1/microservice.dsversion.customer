﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStb
    {
        public DsStb()
        {
            DsCampaignToSmartCard = new HashSet<DsCampaignToSmartCard>();
            DsStbchangeLog = new HashSet<DsStbchangeLog>();
        }

        public int StbId { get; set; }
        public string SerialNumber { get; set; }
        public long ShortSerialNumber { get; set; }
        public string BootLoaderVersion { get; set; }
        public string LotNumber { get; set; }
        public int BrandId { get; set; }
        public string SourceFileName { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public int ModelId { get; set; }
        public int StbUsageTypeId { get; set; }
        public int? DealerId { get; set; }
        public int? OwnerId { get; set; }
        public string Note { get; set; }
        public int StbSkuId { get; set; }
        public string SerialNumberLegacy { get; set; }
        public long? ShortSerialNumberLegacy { get; set; }
        public string StbCaNumber { get; set; }
        public long? ShortStbCaNumber { get; set; }
        public bool? SaledToCustomer { get; set; }
        public bool? IsLocked { get; set; }
        public int? StbLocationId { get; set; }
        public int? LocationPartyId { get; set; }
        public int? TransactionTypeId { get; set; }
        public DateTime? HdChangeStbDate { get; set; }
        public bool? TransactionIsOk { get; set; }
        public int? ReturnVendorOrderId { get; set; }
        public int? ReturnVendorOrderLineId { get; set; }
        public DateTime? FirstActivationDate { get; set; }
        public int? HdChangeVendorOrderLineId { get; set; }
        public bool TempDoubleStbId { get; set; }
        public int? StbPhysicalStatusId { get; set; }
        public int? StbStatusId { get; set; }
        public bool? IsAlreadyMozaik { get; set; }
        public int? LastUpdateById { get; set; }
        public DateTime? LastUpdateDate { get; set; }

        public virtual ICollection<DsCampaignToSmartCard> DsCampaignToSmartCard { get; set; }
        public virtual ICollection<DsStbchangeLog> DsStbchangeLog { get; set; }
    }
}
