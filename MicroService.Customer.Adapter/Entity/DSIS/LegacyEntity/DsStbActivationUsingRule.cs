﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStbActivationUsingRule
    {
        public int StbActivationUsingRuleId { get; set; }
        public int ActivationSalesModelId { get; set; }
        public int CustomerTypeId { get; set; }
        public int CampaignId { get; set; }
        public int? OwnerId { get; set; }
        public int? PostActivationOwnerId { get; set; }

        public virtual DsCustomerType CustomerType { get; set; }
    }
}
