﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStbBrand
    {
        public DsStbBrand()
        {
            DsReceivedStb = new HashSet<DsReceivedStb>();
            DsStbSku = new HashSet<DsStbSku>();
        }

        public int StbBrandId { get; set; }
        public string Name { get; set; }
        public int SortColumn { get; set; }

        public virtual ICollection<DsReceivedStb> DsReceivedStb { get; set; }
        public virtual ICollection<DsStbSku> DsStbSku { get; set; }
    }
}
