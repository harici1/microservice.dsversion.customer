﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStbLocation
    {
        public int StbLocationId { get; set; }
        public string Name { get; set; }
    }
}
