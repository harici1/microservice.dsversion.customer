﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStbLocationExceptionLog
    {
        public int StbLocationExceptionLogId { get; set; }
        public int StbId { get; set; }
        public int UserId { get; set; }
        public int DealerId { get; set; }
        public int CurrentLocationPartyId { get; set; }
        public DateTime CreationDate { get; set; }
        public int? MenuId { get; set; }
        public Guid MsreplTranVersion { get; set; }
    }
}
