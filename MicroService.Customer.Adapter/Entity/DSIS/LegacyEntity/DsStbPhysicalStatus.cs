﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStbPhysicalStatus
    {
        public int StbPhysicalStatusId { get; set; }
        public string Name { get; set; }
        public bool AllowActivation { get; set; }
        public bool Visible { get; set; }
        public bool VisibleForReturnStb { get; set; }
    }
}
