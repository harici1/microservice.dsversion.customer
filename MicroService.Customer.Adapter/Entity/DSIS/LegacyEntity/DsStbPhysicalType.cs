﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStbPhysicalType
    {
        public int StbPhysicalTypeId { get; set; }
        public string Name { get; set; }
    }
}
