﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStbSku
    {
        public DsStbSku()
        {
            DsActivation = new HashSet<DsActivation>();
            VendorOrderConstraintSkuRuleFromStbSku = new HashSet<VendorOrderConstraintSkuRule>();
            VendorOrderConstraintSkuRuleToStbSku = new HashSet<VendorOrderConstraintSkuRule>();
        }

        public int StbSkuId { get; set; }
        public string Name { get; set; }
        public int StbBrandId { get; set; }
        public int? StbTypeId { get; set; }
        public string Description { get; set; }
        public bool IsSendForActivation { get; set; }
        public int? InvoiceItemId { get; set; }
        public bool InventoryNewInsert { get; set; }
        public bool SmsnewInsert { get; set; }
        public DateTime? ActivationStartDate { get; set; }
        public DateTime? ActivationEndDate { get; set; }
        public bool IsHardDisk { get; set; }
        public int? StbSkuSendingGroupId { get; set; }
        public int? SendingOrder { get; set; }
        public bool UpdateStbSku { get; set; }
        public int? Temp { get; set; }
        public bool SapStsreturnStbmapFlag { get; set; }

        public virtual DsStbBrand StbBrand { get; set; }
        public virtual DsStbSkuSendingGroup StbSkuSendingGroup { get; set; }
        public virtual DsStbType StbType { get; set; }
        public virtual ICollection<DsActivation> DsActivation { get; set; }
        public virtual ICollection<VendorOrderConstraintSkuRule> VendorOrderConstraintSkuRuleFromStbSku { get; set; }
        public virtual ICollection<VendorOrderConstraintSkuRule> VendorOrderConstraintSkuRuleToStbSku { get; set; }
    }
}
