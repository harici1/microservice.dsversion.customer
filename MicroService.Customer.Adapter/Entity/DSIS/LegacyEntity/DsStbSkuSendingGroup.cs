﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStbSkuSendingGroup
    {
        public DsStbSkuSendingGroup()
        {
            DsStbSku = new HashSet<DsStbSku>();
        }

        public int StbSkuSendingGroupId { get; set; }
        public string Name { get; set; }
        public int ReferenceGroupId { get; set; }

        public virtual ICollection<DsStbSku> DsStbSku { get; set; }
    }
}
