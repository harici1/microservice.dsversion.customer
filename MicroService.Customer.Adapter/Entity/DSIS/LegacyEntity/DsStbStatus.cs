﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStbStatus
    {
        public int StbStatusId { get; set; }
        public string Name { get; set; }
        public bool Visible { get; set; }
        public bool AllowActivation { get; set; }
    }
}
