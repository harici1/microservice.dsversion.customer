﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStbTransaction
    {
        public int StbTransactionId { get; set; }
        public int StbId { get; set; }
        public int StbLocationId { get; set; }
        public int? FromPartyId { get; set; }
        public int? ToPartyId { get; set; }
        public int TransactionTypeId { get; set; }
        public int? ActivationId { get; set; }
        public int? CampaignId { get; set; }
        public DateTime CreationDate { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? CreatedById { get; set; }
        public string Note { get; set; }
        public int? StbPhysicalStatusId { get; set; }
        public int? StbStatusId { get; set; }
        public int? OwnerId { get; set; }
        public int? StbSkuId { get; set; }
        public bool? IsAlreadyMozaik { get; set; }
    }
}
