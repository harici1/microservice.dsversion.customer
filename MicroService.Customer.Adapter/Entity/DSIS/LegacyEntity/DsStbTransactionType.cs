﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStbTransactionType
    {
        public int StbTransactionTypeId { get; set; }
        public string Name { get; set; }
        public Guid MsreplTranVersion { get; set; }
    }
}
