﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStbType
    {
        public DsStbType()
        {
            DsCampaignToExitPrice = new HashSet<DsCampaignToExitPrice>();
            DsLeasingContractProductPrice = new HashSet<DsLeasingContractProductPrice>();
            DsReceivedStb = new HashSet<DsReceivedStb>();
            DsStbSku = new HashSet<DsStbSku>();
            DsStbTypeToProduct = new HashSet<DsStbTypeToProduct>();
        }

        public int StbTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public bool IsSmartCity { get; set; }
        public bool ForceExternalEquipment { get; set; }
        public bool SupportHdd { get; set; }
        public bool? IsVisibleToStock { get; set; }

        public virtual ICollection<DsCampaignToExitPrice> DsCampaignToExitPrice { get; set; }
        public virtual ICollection<DsLeasingContractProductPrice> DsLeasingContractProductPrice { get; set; }
        public virtual ICollection<DsReceivedStb> DsReceivedStb { get; set; }
        public virtual ICollection<DsStbSku> DsStbSku { get; set; }
        public virtual ICollection<DsStbTypeToProduct> DsStbTypeToProduct { get; set; }
    }
}
