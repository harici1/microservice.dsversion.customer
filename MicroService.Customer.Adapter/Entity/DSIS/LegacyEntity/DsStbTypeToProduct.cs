﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStbTypeToProduct
    {
        public int StbTypeToProductId { get; set; }
        public int CustomerTypeId { get; set; }
        public int StbTypeId { get; set; }
        public int ProductId { get; set; }

        public virtual DsCustomerType CustomerType { get; set; }
        public virtual DsProduct Product { get; set; }
        public virtual DsStbType StbType { get; set; }
    }
}
