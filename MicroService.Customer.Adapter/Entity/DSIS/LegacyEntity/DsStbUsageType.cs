﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStbUsageType
    {
        public DsStbUsageType()
        {
            DsRoleTableConstraint = new HashSet<DsRoleTableConstraint>();
            DsStbUsageTypeToCustomerType = new HashSet<DsStbUsageTypeToCustomerType>();
        }

        public int StbUsageTypeId { get; set; }
        public string Name { get; set; }
        public bool IsForced { get; set; }
        public string Description { get; set; }

        public virtual ICollection<DsRoleTableConstraint> DsRoleTableConstraint { get; set; }
        public virtual ICollection<DsStbUsageTypeToCustomerType> DsStbUsageTypeToCustomerType { get; set; }
    }
}
