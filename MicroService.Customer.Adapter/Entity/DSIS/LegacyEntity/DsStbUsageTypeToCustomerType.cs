﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStbUsageTypeToCustomerType
    {
        public int StbUsageTypeToCustomerTypeId { get; set; }
        public int CustomerTypeId { get; set; }
        public int StbUsageTypeId { get; set; }
        public int SmartCardReservationTypeId { get; set; }
        public string Description { get; set; }
        public int? PostActivationStbUsageTypeId { get; set; }
        public int? PostActivationSmartCardReservationTypeId { get; set; }

        public virtual DsCustomerType CustomerType { get; set; }
        public virtual DsSmartCardReservationType SmartCardReservationType { get; set; }
        public virtual DsStbUsageType StbUsageType { get; set; }
    }
}
