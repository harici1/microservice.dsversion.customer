﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStbchangeLog
    {
        public int StbchangeLogId { get; set; }
        public int? Stbid { get; set; }
        public int? TypeId { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? CreatedById { get; set; }
        public string OldValues { get; set; }
        public string NewValues { get; set; }

        public virtual DsStb Stb { get; set; }
        public virtual DsStbchangeLogType Type { get; set; }
    }
}
