﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStbchangeLogType
    {
        public DsStbchangeLogType()
        {
            DsStbchangeLog = new HashSet<DsStbchangeLog>();
        }

        public int StbchangeLogTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<DsStbchangeLog> DsStbchangeLog { get; set; }
    }
}
