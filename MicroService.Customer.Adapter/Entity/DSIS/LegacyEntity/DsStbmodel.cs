﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStbmodel
    {
        public DsStbmodel()
        {
            DsContentProductToStbModel = new HashSet<DsContentProductToStbModel>();
        }

        public int StbmodelId { get; set; }
        public string Name { get; set; }
        public int ManufacturerId { get; set; }
        public string Code { get; set; }
        public string Caprefix { get; set; }
        public int StbTypeId { get; set; }
        public string Description { get; set; }
        public int DefaultUsageType { get; set; }
        public int DefaultParentalRatingId { get; set; }
        public int DefaultReportbackAvailabilityId { get; set; }
        public bool HddserialNumber { get; set; }
        public bool IsSmartCity { get; set; }
        public bool ForceExternalEquipment { get; set; }
        public bool SupportHdd { get; set; }

        public virtual DsParty Manufacturer { get; set; }
        public virtual ICollection<DsContentProductToStbModel> DsContentProductToStbModel { get; set; }
    }
}
