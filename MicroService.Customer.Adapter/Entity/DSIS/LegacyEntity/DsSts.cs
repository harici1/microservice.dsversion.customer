﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSts
    {
        public int StsId { get; set; }
        public int WorkOrderId { get; set; }
        public int StsTypeId { get; set; }
        public int DealerId { get; set; }
        public int? ActivationId { get; set; }
        public int CustomerId { get; set; }
        public int? SmartCardId { get; set; }
        public int? StbId { get; set; }
        public int StsProcessTypeId { get; set; }
        public int StbStatusId { get; set; }
        public int SmartCardStatusId { get; set; }
        public int RemoteControlStatusId { get; set; }
        public int ElectricalCableStatusId { get; set; }
        public int HdmicableStatusId { get; set; }
        public int ModulStatusId { get; set; }
        public int ScartCableStatusId { get; set; }
        public int SplitterStatusId { get; set; }
        public int LnbstatusId { get; set; }
        public int DishStatusId { get; set; }
        public int DishPlaceStatusId { get; set; }
        public int OtherStatusId { get; set; }
        public string OtherExplanation { get; set; }
        public int? OldStbId { get; set; }
        public int? OldSmartCardId { get; set; }
        public string ProcessEmployeeName { get; set; }
        public string ServiceReason { get; set; }
        public string ProcessAction { get; set; }
        public string ProcessActionExplanation { get; set; }
        public int StsLocationId { get; set; }
        public decimal CustomerPaidAmount { get; set; }
        public decimal DsmartPaidAmount { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? StsServiceId { get; set; }
    }
}
