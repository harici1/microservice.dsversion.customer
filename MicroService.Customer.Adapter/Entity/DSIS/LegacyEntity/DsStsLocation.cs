﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStsLocation
    {
        public DsStsLocation()
        {
            DsStsPrice = new HashSet<DsStsPrice>();
        }

        public int StsLocationId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsStsPrice> DsStsPrice { get; set; }
    }
}
