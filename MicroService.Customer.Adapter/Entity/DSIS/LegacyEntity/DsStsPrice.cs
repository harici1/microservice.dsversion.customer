﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStsPrice
    {
        public int StsPriceId { get; set; }
        public int StsTypeId { get; set; }
        public int StsLocationId { get; set; }
        public decimal DsmartPaidAmount { get; set; }
        public decimal CustomerPaidAmount { get; set; }
        public decimal TotalPaidAmount { get; set; }

        public virtual DsStsLocation StsLocation { get; set; }
        public virtual DsStsType StsType { get; set; }
    }
}
