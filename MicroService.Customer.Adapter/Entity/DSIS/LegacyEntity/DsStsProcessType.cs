﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStsProcessType
    {
        public int StsProcessTypeId { get; set; }
        public string Name { get; set; }
    }
}
