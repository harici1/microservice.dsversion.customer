﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStsService
    {
        public int StsServiceId { get; set; }
        public int StsServiceStatusId { get; set; }
        public int ActivationId { get; set; }
        public int CustomerId { get; set; }
        public int SmartCardId { get; set; }
        public int StbId { get; set; }
        public int OrderId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? LastUpdatedById { get; set; }
    }
}
