﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStsSupportSubject
    {
        public int StsSupportSubjectId { get; set; }
        public int StsSupportTypeId { get; set; }
        public string Name { get; set; }
        public bool? IsActive { get; set; }

        public virtual DsStsSupportType StsSupportType { get; set; }
    }
}
