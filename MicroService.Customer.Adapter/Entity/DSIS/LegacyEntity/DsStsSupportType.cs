﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStsSupportType
    {
        public DsStsSupportType()
        {
            DsStsSupportSubject = new HashSet<DsStsSupportSubject>();
        }

        public int StsSupportTypeId { get; set; }
        public int StsTypeId { get; set; }
        public string Name { get; set; }
        public bool? IsActive { get; set; }
        public bool OnlyMozaik { get; set; }

        public virtual DsStsType StsType { get; set; }
        public virtual ICollection<DsStsSupportSubject> DsStsSupportSubject { get; set; }
    }
}
