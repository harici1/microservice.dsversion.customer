﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStsType
    {
        public DsStsType()
        {
            DsStsPrice = new HashSet<DsStsPrice>();
            DsStsSupportType = new HashSet<DsStsSupportType>();
        }

        public int StsTypeId { get; set; }
        public string StsTypeName { get; set; }
        public bool? IsActive { get; set; }
        public int WorkOrderSubjectId { get; set; }

        public virtual ICollection<DsStsPrice> DsStsPrice { get; set; }
        public virtual ICollection<DsStsSupportType> DsStsSupportType { get; set; }
    }
}
