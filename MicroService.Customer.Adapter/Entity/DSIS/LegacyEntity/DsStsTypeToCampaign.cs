﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsStsTypeToCampaign
    {
        public int StsTypeToCampaignId { get; set; }
        public int StsTypeId { get; set; }
        public int CampaingId { get; set; }
        public int ActivationLengthInDays { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}
