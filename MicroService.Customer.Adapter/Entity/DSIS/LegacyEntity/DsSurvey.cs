﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSurvey
    {
        public int SurveyId { get; set; }
        public int SurveyTypeId { get; set; }
        public int PartyId { get; set; }
        public int ContactChannelId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public string Note { get; set; }
    }
}
