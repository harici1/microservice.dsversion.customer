﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSurveyAnswer
    {
        public int SurveyAnswerId { get; set; }
        public int SurveyId { get; set; }
        public int AnswerId { get; set; }
        public string AnswerNote { get; set; }
        public int? AnswerDetailId { get; set; }
    }
}
