﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSurveyQuestion
    {
        public DsSurveyQuestion()
        {
            DsSurveyQuestionAnswer = new HashSet<DsSurveyQuestionAnswer>();
        }

        public int SurveyQuestionId { get; set; }
        public int SurveySectionId { get; set; }
        public string Name { get; set; }
        public int OrderNo { get; set; }
        public bool ReplyMandatory { get; set; }

        public virtual DsSurveySection SurveySection { get; set; }
        public virtual ICollection<DsSurveyQuestionAnswer> DsSurveyQuestionAnswer { get; set; }
    }
}
