﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSurveyQuestionAnswer
    {
        public DsSurveyQuestionAnswer()
        {
            DsSurveyQuestionAnswerDetail = new HashSet<DsSurveyQuestionAnswerDetail>();
        }

        public int SurveyQuestionAnswerId { get; set; }
        public int SurveyQuestionId { get; set; }
        public string Name { get; set; }
        public int AnswerScore { get; set; }
        public int OrderNo { get; set; }

        public virtual DsSurveyQuestion SurveyQuestion { get; set; }
        public virtual ICollection<DsSurveyQuestionAnswerDetail> DsSurveyQuestionAnswerDetail { get; set; }
    }
}
