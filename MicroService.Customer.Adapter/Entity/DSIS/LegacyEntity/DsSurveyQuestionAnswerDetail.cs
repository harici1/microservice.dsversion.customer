﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSurveyQuestionAnswerDetail
    {
        public int SurveyQuestionAnswerDetailId { get; set; }
        public int SurveyQuestionAnswerId { get; set; }
        public string Name { get; set; }
        public int AnswerScore { get; set; }
        public int OrderNo { get; set; }

        public virtual DsSurveyQuestionAnswer SurveyQuestionAnswer { get; set; }
    }
}
