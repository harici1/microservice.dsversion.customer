﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSurveySection
    {
        public DsSurveySection()
        {
            DsSurveyQuestion = new HashSet<DsSurveyQuestion>();
        }

        public int SurveySectionId { get; set; }
        public int SurveyTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int OrderNo { get; set; }

        public virtual ICollection<DsSurveyQuestion> DsSurveyQuestion { get; set; }
    }
}
