﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSurveyType
    {
        public int SurveyTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime Enddate { get; set; }
        public DateTime CreationDate { get; set; }
        public int? OrderNo { get; set; }
    }
}
