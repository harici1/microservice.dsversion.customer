﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsSystemParameter
    {
        public int ParameterId { get; set; }
        public string Description { get; set; }
        public byte ParameterType { get; set; }
        public int? IntegerValue { get; set; }
        public string CodeValue { get; set; }
        public string TextValue { get; set; }
        public DateTime? DateValue { get; set; }
    }
}
