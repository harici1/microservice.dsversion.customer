﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsTestActivation
    {
        public int TestActivationId { get; set; }
        public int StbId { get; set; }
        public int SmartCardId { get; set; }
        public int? ExternalEquipmentId { get; set; }
        public int BouquetId { get; set; }
        public bool? IsActive { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public string Notes { get; set; }
        public bool IsWorking { get; set; }
        public string PersonalBits { get; set; }
        public bool IsFrontPanelChanged { get; set; }
        public bool IsTopPanelChanged { get; set; }
        public bool IsBottomChanged { get; set; }
        public bool IsSmartcardChanged { get; set; }
        public bool IsAdaptorChanged { get; set; }
        public bool IsCableChanged { get; set; }
        public bool IsStbFixed { get; set; }
        public bool IsPainted { get; set; }
        public bool? IsPanelLensChanged { get; set; }
        public bool? IsSmartCardKapakLensChanged { get; set; }
        public bool? IsHdmimaintenanceDone { get; set; }
        public bool? IsRemoteMaintenanceDone { get; set; }
    }
}
