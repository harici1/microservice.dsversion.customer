﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsTestActivationModemSparePartValue
    {
        public int TestActivationModemSparePartValueId { get; set; }
        public int TestActivationModemId { get; set; }
        public int TestActivationModemSparePartId { get; set; }
    }
}
