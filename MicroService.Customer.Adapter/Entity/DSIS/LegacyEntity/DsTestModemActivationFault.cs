﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsTestModemActivationFault
    {
        public int TestModemActivationFaultId { get; set; }
        public int ModemId { get; set; }
        public bool? IsActive { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public string Notes { get; set; }
        public bool IsWorking { get; set; }
    }
}
