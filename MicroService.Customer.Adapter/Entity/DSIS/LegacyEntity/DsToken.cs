﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsToken
    {
        public DsToken()
        {
            DsTokenUsage = new HashSet<DsTokenUsage>();
        }

        public int TokenId { get; set; }
        public int CustomerId { get; set; }
        public int? TokenTypeId { get; set; }
        public decimal Amount { get; set; }
        public decimal Balance { get; set; }
        public int PaymentTypeId { get; set; }
        public decimal PaidAmount { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int? BatchId { get; set; }
        public string Description { get; set; }
        public int ContactChannelId { get; set; }
        public int? CampaignId { get; set; }
        public bool IsCancelled { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public decimal? CcServiceCostAmount { get; set; }
        public int? MaxLimitPerEvent { get; set; }
        public int? ProductId { get; set; }
        public int? CancelledByDocumentId { get; set; }
        public int? CancelledByDocumentTypeId { get; set; }
        public int? TokenTypeId2 { get; set; }
        public decimal? UnitPaidAmount { get; set; }
        public decimal? TokenAmount { get; set; }
        public decimal? DevredenTutarOrjinal { get; set; }
        public decimal? DevredenTutarGuncel { get; set; }
        public decimal? AssetAmount { get; set; }
        public decimal? AssetAmountAccounted { get; set; }

        public virtual DsTokenType TokenType { get; set; }
        public virtual DsTokenType TokenTypeId2Navigation { get; set; }
        public virtual ICollection<DsTokenUsage> DsTokenUsage { get; set; }
    }
}
