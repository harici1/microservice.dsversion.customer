﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsTokenType
    {
        public DsTokenType()
        {
            DsProductPrice = new HashSet<DsProductPrice>();
            DsTokenTokenType = new HashSet<DsToken>();
            DsTokenTokenTypeId2Navigation = new HashSet<DsToken>();
            DsWebTvGeographicalLocation = new HashSet<DsWebTvGeographicalLocation>();
        }

        public int TokenTypeId { get; set; }
        public string Name { get; set; }
        public int? Priority { get; set; }

        public virtual ICollection<DsProductPrice> DsProductPrice { get; set; }
        public virtual ICollection<DsToken> DsTokenTokenType { get; set; }
        public virtual ICollection<DsToken> DsTokenTokenTypeId2Navigation { get; set; }
        public virtual ICollection<DsWebTvGeographicalLocation> DsWebTvGeographicalLocation { get; set; }
    }
}
