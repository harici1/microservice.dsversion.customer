﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsTokenUsage
    {
        public int TokenUsageId { get; set; }
        public int TokenId { get; set; }
        public decimal Amount { get; set; }
        public int? BatchId { get; set; }
        public string Description { get; set; }
        public int ContactChannelId { get; set; }
        public int? CampaignId { get; set; }
        public int DocumentId { get; set; }
        public int DocumentTypeId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? ProductId { get; set; }
        public decimal? PaidAmount { get; set; }
        public decimal? TokenAmount { get; set; }
        public int? TokenType2Id { get; set; }
        public decimal? UnitPaidAmount { get; set; }
        public bool? IsCancelled { get; set; }
        public int? CancelledByDocumentId { get; set; }
        public int? CancelledByDocumentTypeId { get; set; }
        public decimal? DiscountAmount { get; set; }

        public virtual DsToken Token { get; set; }
    }
}
