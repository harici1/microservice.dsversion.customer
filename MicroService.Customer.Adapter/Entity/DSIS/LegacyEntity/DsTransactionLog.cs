﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsTransactionLog
    {
        public int TransactionLog { get; set; }
        public int TransactionLogTypeId { get; set; }
        public int ContactChannelId { get; set; }
        public int? Count { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }

        public virtual DsContactChannel ContactChannel { get; set; }
        public virtual DsTransactionLogType TransactionLogType { get; set; }
    }
}
