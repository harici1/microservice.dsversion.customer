﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsTransactionLogType
    {
        public DsTransactionLogType()
        {
            DsTransactionLog = new HashSet<DsTransactionLog>();
        }

        public int TransactionLogTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<DsTransactionLog> DsTransactionLog { get; set; }
    }
}
