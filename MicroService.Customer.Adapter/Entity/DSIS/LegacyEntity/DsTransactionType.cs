﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsTransactionType
    {
        public DsTransactionType()
        {
            DsInventoryTransferRule = new HashSet<DsInventoryTransferRule>();
        }

        public int TransactionTypeId { get; set; }
        public string TypeName { get; set; }

        public virtual ICollection<DsInventoryTransferRule> DsInventoryTransferRule { get; set; }
    }
}
