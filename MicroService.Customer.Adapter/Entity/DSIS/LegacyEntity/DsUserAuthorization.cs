﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsUserAuthorization
    {
        public int UserId { get; set; }
        public int MenuId { get; set; }
    }
}
