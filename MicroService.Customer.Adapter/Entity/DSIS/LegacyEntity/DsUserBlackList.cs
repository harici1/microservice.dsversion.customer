﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsUserBlackList
    {
        public int UserBlackListId { get; set; }
        public string UserCode { get; set; }
        public bool IsActive { get; set; }
        public string Note { get; set; }
    }
}
