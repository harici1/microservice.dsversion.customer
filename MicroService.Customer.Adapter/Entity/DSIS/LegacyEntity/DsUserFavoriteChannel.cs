﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsUserFavoriteChannel
    {
        public int UserFavoriteChannelId { get; set; }
        public int UserId { get; set; }
        public int BouquetId { get; set; }
        public int SiServiceId { get; set; }
        public int ContactChannelId { get; set; }
        public int ContactChannelSubDomainId { get; set; }
    }
}
