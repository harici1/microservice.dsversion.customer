﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsUserInformationChangeLog
    {
        public int UserInformationChangeLogId { get; set; }
        public int UserInformationChangeLogTypeId { get; set; }
        public int UserId { get; set; }
        public int CreatedById { get; set; }
        public string ChangedInfo { get; set; }
        public string NewInfo { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
