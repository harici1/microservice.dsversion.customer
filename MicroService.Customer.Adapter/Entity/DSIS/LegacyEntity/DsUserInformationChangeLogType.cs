﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsUserInformationChangeLogType
    {
        public int UserInformationChangeLogTypeId { get; set; }
        public string Type { get; set; }
    }
}
