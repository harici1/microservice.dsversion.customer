﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsUserKopya111
    {
        public int UserId { get; set; }
        public string UserCode { get; set; }
        public string Name { get; set; }
        public string FamilyName { get; set; }
        public int PartyId { get; set; }
        public int PartyTypeId { get; set; }
        public bool? IsAdministrator { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public DateTime? LastConnectionDate { get; set; }
        public string Password { get; set; }
        public DateTime? PasswordExpireDate { get; set; }
        public int? GroupId { get; set; }
        public bool? IsActive { get; set; }
        public int? UserType { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public string AllowedIpAddress { get; set; }
        public string SecurityCode { get; set; }
        public string RegisteredEmail { get; set; }
        public string RegisteredMobilePhone { get; set; }
        public string ActivationCode { get; set; }
        public string PasswordResetCode { get; set; }
        public DateTime? PasswordResetExpiryDate { get; set; }
        public byte[] PasswordEncrypt { get; set; }
        public byte[] SecurityCodeEncrypt { get; set; }
    }
}
