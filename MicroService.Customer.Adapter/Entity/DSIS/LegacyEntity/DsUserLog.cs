﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsUserLog
    {
        public int UserLogId { get; set; }
        public int UserId { get; set; }
        public string Password { get; set; }
        public DateTime? PasswordExpireDate { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? CreationDate { get; set; }
        public int UserLogTypeId { get; set; }
        public string Ip { get; set; }
        public byte[] PasswordEncrypt { get; set; }

        public virtual DsUser User { get; set; }
    }
}
