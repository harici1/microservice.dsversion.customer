﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsUserRole
    {
        public int UserRoleId { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public int? ProcessUserId { get; set; }
        public DateTime? ProcessDate { get; set; }

        public virtual DsRole Role { get; set; }
        public virtual DsUser User { get; set; }
    }
}
