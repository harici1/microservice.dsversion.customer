﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsUserRoleLog
    {
        public int UserRoleLogId { get; set; }
        public int UserRoleId { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public DateTime TriggerDate { get; set; }
        public string TriggerUserName { get; set; }
        public string TriggerHostName { get; set; }
        public string TriggerType { get; set; }
        public int? ProcessUserId { get; set; }
    }
}
