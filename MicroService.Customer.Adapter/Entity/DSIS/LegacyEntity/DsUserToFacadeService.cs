﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsUserToFacadeService
    {
        public int FacadeServiceId { get; set; }
        public int UserId { get; set; }

        public virtual DsFacadeService FacadeService { get; set; }
    }
}
