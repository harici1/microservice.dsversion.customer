﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsUserType
    {
        public int UserTypeId { get; set; }
        public string Name { get; set; }
    }
}
