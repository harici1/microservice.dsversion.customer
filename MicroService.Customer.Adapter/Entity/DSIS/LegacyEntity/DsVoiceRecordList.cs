﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsVoiceRecordList
    {
        public int VoiceRecordListId { get; set; }
        public int WorkOrderId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public string Period { get; set; }
        public int BatchId { get; set; }
        public string BatchName { get; set; }
        public int Status { get; set; }
        public int OwnerUserId { get; set; }
    }
}
