﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsVposServiceProvider
    {
        public DsVposServiceProvider()
        {
            DsAccAccount = new HashSet<DsAccAccount>();
            DsExpenseBillReturnPayment = new HashSet<DsExpenseBillReturnPayment>();
            DsLeasingContract = new HashSet<DsLeasingContract>();
            DsWebTvProductCategoryDefaultPrice = new HashSet<DsWebTvProductCategoryDefaultPrice>();
        }

        public int VposServiceProviderId { get; set; }
        public string Name { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? ProcessModeId { get; set; }
        public bool AccountingIsForced { get; set; }
        public int ReferenceVposServiceProviderId { get; set; }
        public int BankId { get; set; }
        public string Description { get; set; }
        public bool IbanIsForced { get; set; }
        public int OrganizationId { get; set; }
        public int? CorporationId { get; set; }
        public string TsvposCode { get; set; }
        public string ShortName { get; set; }

        public virtual DsCorporation Corporation { get; set; }
        public virtual ICollection<DsAccAccount> DsAccAccount { get; set; }
        public virtual ICollection<DsExpenseBillReturnPayment> DsExpenseBillReturnPayment { get; set; }
        public virtual ICollection<DsLeasingContract> DsLeasingContract { get; set; }
        public virtual ICollection<DsWebTvProductCategoryDefaultPrice> DsWebTvProductCategoryDefaultPrice { get; set; }
    }
}
