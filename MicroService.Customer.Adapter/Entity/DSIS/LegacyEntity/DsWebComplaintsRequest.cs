﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebComplaintsRequest
    {
        public int WebComplaintsRequestId { get; set; }
        public int SubscriberUserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int WebComplaintsRequestTypeId { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public string RefrenceNumber { get; set; }
    }
}
