﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebComplaintsRequestType
    {
        public int WebComplaintsRequestTypeId { get; set; }
        public string TypeName { get; set; }
    }
}
