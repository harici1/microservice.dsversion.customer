﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebSubcriberChangeLog
    {
        public int WebSubcriberChangeLogId { get; set; }
        public int SubscriberUserId { get; set; }
        public int PartyId { get; set; }
        public int WebSubcriberChangeLogTypeId { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public string ExtraValue { get; set; }
    }
}
