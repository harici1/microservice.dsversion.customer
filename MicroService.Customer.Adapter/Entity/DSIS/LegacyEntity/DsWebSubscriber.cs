﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebSubscriber
    {
        public int WebSubscriberId { get; set; }
        public int SubscriberUserId { get; set; }
        public int PartyId { get; set; }
        public bool? IsActive { get; set; }

        public virtual DsParty Party { get; set; }
    }
}
