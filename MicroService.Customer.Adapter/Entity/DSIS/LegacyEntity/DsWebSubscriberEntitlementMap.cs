﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebSubscriberEntitlementMap
    {
        public int WebSubscriberEntitlementMapId { get; set; }
        public int WebSubscriberEntitlementProductId { get; set; }
        public int ProductMainPackageId { get; set; }

        public virtual DsWebSubscriberEntitlementProduct WebSubscriberEntitlementProduct { get; set; }
    }
}
