﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebSubscriberEntitlementProduct
    {
        public DsWebSubscriberEntitlementProduct()
        {
            DsWebSubscriberEntitlementMap = new HashSet<DsWebSubscriberEntitlementMap>();
        }

        public int WebSubscriberEntitlementProductId { get; set; }
        public string ResourceId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsWebSubscriberEntitlementMap> DsWebSubscriberEntitlementMap { get; set; }
    }
}
