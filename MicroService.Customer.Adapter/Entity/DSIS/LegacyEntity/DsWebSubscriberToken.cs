﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebSubscriberToken
    {
        public int WebSubscriberTokenId { get; set; }
        public int SubscriberUserId { get; set; }
        public string Token { get; set; }
        public int ExpiresIn { get; set; }
        public bool Active { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public int LastUpdatedById { get; set; }
    }
}
