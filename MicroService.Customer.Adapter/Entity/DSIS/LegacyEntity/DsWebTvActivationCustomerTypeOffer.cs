﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebTvActivationCustomerTypeOffer
    {
        public int WebTvActivationCustomerTypeOfferId { get; set; }
        public int WebTvGeographicalLocationId { get; set; }
        public int WebTvCustomerTypeId { get; set; }
        public int OfferTypeId { get; set; }
        public int ProductPriceId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public bool? IsShowGlobal { get; set; }

        public virtual DsOfferType OfferType { get; set; }
        public virtual DsProductPrice ProductPrice { get; set; }
        public virtual DsWebTvCustomerType WebTvCustomerType { get; set; }
        public virtual DsWebTvGeographicalLocation WebTvGeographicalLocation { get; set; }
    }
}
