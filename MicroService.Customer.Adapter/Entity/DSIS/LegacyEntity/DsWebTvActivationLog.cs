﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebTvActivationLog
    {
        public int WebTvActivationLogId { get; set; }
        public int TypeId { get; set; }
        public int WebTvActivationId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? CustomerId { get; set; }
        public string EmailAddress { get; set; }
        public DateTime? SubscriptionDate { get; set; }
        public int? WebTvActivationStatusId { get; set; }
        public string WebTvSubscriptionCode { get; set; }
        public int? WebTvGeographicalLocationId { get; set; }
        public int? InternetServiceProviderId { get; set; }
        public int? InternetConnectionSpeedId { get; set; }
        public int? InternetQuotaLimitId { get; set; }

        public virtual DsWebTvActivationLogType Type { get; set; }
        public virtual DsWebTvActivation WebTvActivation { get; set; }
        public virtual DsWebTvActivationStatus WebTvActivationStatus { get; set; }
    }
}
