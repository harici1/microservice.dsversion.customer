﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebTvActivationLogType
    {
        public DsWebTvActivationLogType()
        {
            DsWebTvActivationLog = new HashSet<DsWebTvActivationLog>();
        }

        public int WebTvActivationLogTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<DsWebTvActivationLog> DsWebTvActivationLog { get; set; }
    }
}
