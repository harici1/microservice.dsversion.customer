﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebTvActivationStatus
    {
        public DsWebTvActivationStatus()
        {
            DsWebTvActivation = new HashSet<DsWebTvActivation>();
            DsWebTvActivationLog = new HashSet<DsWebTvActivationLog>();
        }

        public int WebTvActivationStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsWebTvActivation> DsWebTvActivation { get; set; }
        public virtual ICollection<DsWebTvActivationLog> DsWebTvActivationLog { get; set; }
    }
}
