﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebTvActivationToCustomerType
    {
        public int WebTvActivationToCustomerTypeId { get; set; }
        public int CustomerId { get; set; }
        public int WebTvCustomerTypeId { get; set; }
        public string SystemCode { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? CreationDate { get; set; }

        public virtual DsWebTvCustomerType WebTvCustomerType { get; set; }
    }
}
