﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebTvActivationToOffer
    {
        public int WebTvActivationToOfferId { get; set; }
        public int? CustomerId { get; set; }
        public int ProductPriceId { get; set; }
        public int OfferTypeId { get; set; }
        public int CampaignId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsOfferUsed { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? DocumentId { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? WebTvActivationId { get; set; }
        public int? DiscountCouponId { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? CreationDate { get; set; }

        public virtual DsOfferType OfferType { get; set; }
        public virtual DsProductPrice ProductPrice { get; set; }
    }
}
