﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebTvAuthenticationLogType
    {
        public int WebTvAuthenticationLogTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
