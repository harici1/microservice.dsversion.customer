﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebTvCustomerType
    {
        public DsWebTvCustomerType()
        {
            DsWebTvActivationCustomerTypeOffer = new HashSet<DsWebTvActivationCustomerTypeOffer>();
            DsWebTvActivationToCustomerType = new HashSet<DsWebTvActivationToCustomerType>();
            DsWebTvProductCategoryToOfferType = new HashSet<DsWebTvProductCategoryToOfferType>();
        }

        public int WebTvCustomerTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsWebTvActivationCustomerTypeOffer> DsWebTvActivationCustomerTypeOffer { get; set; }
        public virtual ICollection<DsWebTvActivationToCustomerType> DsWebTvActivationToCustomerType { get; set; }
        public virtual ICollection<DsWebTvProductCategoryToOfferType> DsWebTvProductCategoryToOfferType { get; set; }
    }
}
