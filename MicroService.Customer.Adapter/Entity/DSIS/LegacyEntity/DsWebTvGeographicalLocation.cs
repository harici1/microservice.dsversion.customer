﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebTvGeographicalLocation
    {
        public DsWebTvGeographicalLocation()
        {
            DsWebTvActivationCustomerTypeOffer = new HashSet<DsWebTvActivationCustomerTypeOffer>();
            DsWebTvProductCategoryDefaultPrice = new HashSet<DsWebTvProductCategoryDefaultPrice>();
        }

        public int WebTvGeographicalLocationId { get; set; }
        public string Name { get; set; }
        public int TokenTypeId { get; set; }
        public int RoleId { get; set; }
        public decimal? CurrencyAmount { get; set; }

        public virtual DsTokenType TokenType { get; set; }
        public virtual ICollection<DsWebTvActivationCustomerTypeOffer> DsWebTvActivationCustomerTypeOffer { get; set; }
        public virtual ICollection<DsWebTvProductCategoryDefaultPrice> DsWebTvProductCategoryDefaultPrice { get; set; }
    }
}
