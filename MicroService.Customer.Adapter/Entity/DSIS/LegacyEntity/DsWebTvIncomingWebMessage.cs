﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebTvIncomingWebMessage
    {
        public int WebTvIncomingWebMessageId { get; set; }
        public int RequestTypeId { get; set; }
        public int StatusId { get; set; }
        public string Name { get; set; }
        public string FamilyName { get; set; }
        public int? DistrictId { get; set; }
        public int? NeighbourhoodId { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Phone3 { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public string Address { get; set; }
        public string XmlMessage { get; set; }
        public string XmlMessage2 { get; set; }
        public int? PartyId { get; set; }
    }
}
