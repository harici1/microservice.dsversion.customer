﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebTvIncomingWebMessageStatus
    {
        public int WebTvIncomingWebMessageStatusId { get; set; }
        public string Name { get; set; }
    }
}
