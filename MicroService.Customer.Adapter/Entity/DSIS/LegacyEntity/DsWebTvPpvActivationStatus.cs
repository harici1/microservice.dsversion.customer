﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebTvPpvActivationStatus
    {
        public int WebTvPpvActivationStatusId { get; set; }
        public string Name { get; set; }
    }
}
