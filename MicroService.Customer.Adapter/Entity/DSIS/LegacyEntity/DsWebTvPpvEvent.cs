﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebTvPpvEvent
    {
        public int WebTvPpvEventId { get; set; }
        public int ProductPriceId { get; set; }
        public int? OfferTypeId { get; set; }
        public int WebTvProductCode { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? WebTvPpvEventStatusId { get; set; }
        public int? CustomerId { get; set; }

        public virtual DsOfferType OfferType { get; set; }
        public virtual DsProductPrice ProductPrice { get; set; }
        public virtual DsWebTvPpvEventStatus WebTvPpvEventStatus { get; set; }
    }
}
