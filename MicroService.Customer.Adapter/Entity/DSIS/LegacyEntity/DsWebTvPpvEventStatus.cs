﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebTvPpvEventStatus
    {
        public DsWebTvPpvEventStatus()
        {
            DsWebTvPpvEvent = new HashSet<DsWebTvPpvEvent>();
        }

        public int WebTvPpvEventStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsWebTvPpvEvent> DsWebTvPpvEvent { get; set; }
    }
}
