﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebTvProductCategoryDefaultPrice
    {
        public int WebTvProductCategoryDefaultPriceId { get; set; }
        public int WebTvProductCategoryTypeId { get; set; }
        public decimal Amount { get; set; }
        public decimal ServiceCost { get; set; }
        public int NumberOfInstallments { get; set; }
        public int VposId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int PaymentTypeId { get; set; }
        public string Description { get; set; }
        public int WebTvGeographicalLocationId { get; set; }

        public virtual DsPaymentType PaymentType { get; set; }
        public virtual DsVposServiceProvider Vpos { get; set; }
        public virtual DsWebTvGeographicalLocation WebTvGeographicalLocation { get; set; }
        public virtual DsWebTvProductCategoryType WebTvProductCategoryType { get; set; }
    }
}
