﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebTvProductCategoryToOfferType
    {
        public int WebTvProductCategoryToOfferTypeId { get; set; }
        public int OfferTypeId { get; set; }
        public int WebTvCustomerTypeId { get; set; }
        public int WebTvProductCategoryDefaultPriceId { get; set; }
        public bool? IsShowGlobal { get; set; }

        public virtual DsOfferType OfferType { get; set; }
        public virtual DsWebTvCustomerType WebTvCustomerType { get; set; }
    }
}
