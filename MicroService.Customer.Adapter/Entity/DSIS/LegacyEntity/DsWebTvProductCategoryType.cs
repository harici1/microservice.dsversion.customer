﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebTvProductCategoryType
    {
        public DsWebTvProductCategoryType()
        {
            DsWebTvProductCategoryDefaultPrice = new HashSet<DsWebTvProductCategoryDefaultPrice>();
        }

        public int WebTvProductCategoryTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsWebTvProductCategoryDefaultPrice> DsWebTvProductCategoryDefaultPrice { get; set; }
    }
}
