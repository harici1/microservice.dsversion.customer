﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebTvProductMapping
    {
        public int WebTvProductMappingId { get; set; }
        public int WebTvProductCode { get; set; }
        public int ProductId { get; set; }
        public bool IsForceWebTvPpvEvent { get; set; }
        public int? WebTvProductCategoryTypeId { get; set; }

        public virtual DsProduct Product { get; set; }
    }
}
