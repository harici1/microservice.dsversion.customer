﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebTvUser
    {
        public int WebTvUserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int StatusId { get; set; }
        public string UserType { get; set; }
        public string SubscriberNumber { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
    }
}
