﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWebTvUserStatus
    {
        public int WebTvUserStatusId { get; set; }
        public string Name { get; set; }
    }
}
