﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWindows
    {
        public int PropertyId { get; set; }
        public int WinId { get; set; }
        public int WinTypeId { get; set; }
        public string WinName { get; set; }
        public string PropertyName { get; set; }
        public string PropertyType { get; set; }
        public string PropValue { get; set; }
        public string AssemblyName { get; set; }
        public bool IsActive { get; set; }
        public int? AuthorizationId { get; set; }
        public string ClassName { get; set; }
    }
}
