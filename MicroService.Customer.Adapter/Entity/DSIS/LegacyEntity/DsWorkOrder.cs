﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrder
    {
        public DsWorkOrder()
        {
            DsWorkOrderActivationGeneric = new HashSet<DsWorkOrderActivationGeneric>();
            DsWorkOrderCancelStbLeasingInvoiceItem = new HashSet<DsWorkOrderCancelStbLeasingInvoiceItem>();
            DsWorkOrderCustomerGeneric = new HashSet<DsWorkOrderCustomerGeneric>();
            DsWorkOrderDealer = new HashSet<DsWorkOrderDealer>();
            DsWorkOrderDetailInfo = new HashSet<DsWorkOrderDetailInfo>();
            DsWorkOrderLog = new HashSet<DsWorkOrderLog>();
            DsWorkOrderStbReturn = new HashSet<DsWorkOrderStbReturn>();
            DsWorkOrderSts = new HashSet<DsWorkOrderSts>();
            DsWorkOrderSystemLog = new HashSet<DsWorkOrderSystemLog>();
        }

        public int WorkOrderId { get; set; }
        public int WorkOrderLogTypeId { get; set; }
        public int WorkOrderSubjectId { get; set; }
        public int? OwnerId { get; set; }
        public int? RoleId { get; set; }
        public int? SubjectPartyId { get; set; }
        public int WorkOrderStatusId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public int LastUpdatedById { get; set; }
        public string Note { get; set; }
        public int? CampaignId { get; set; }
        public DateTime? ReservationDate { get; set; }
        public int? RedirectedDealerId { get; set; }
        public DateTime? Startdate { get; set; }
        public DateTime? Enddate { get; set; }
        public string SmsopNote { get; set; }
        public bool IsPushed { get; set; }
        public bool IsInvoiced { get; set; }
        public bool? TemIsOk { get; set; }
        public int? IsBlu { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int ContactChannelId { get; set; }
        public Guid UniqueId { get; set; }
        public int WorkOrderRetryCount { get; set; }

        public virtual DsParty SubjectParty { get; set; }
        public virtual DsWorkOrderLogType WorkOrderLogType { get; set; }
        public virtual ICollection<DsWorkOrderActivationGeneric> DsWorkOrderActivationGeneric { get; set; }
        public virtual ICollection<DsWorkOrderCancelStbLeasingInvoiceItem> DsWorkOrderCancelStbLeasingInvoiceItem { get; set; }
        public virtual ICollection<DsWorkOrderCustomerGeneric> DsWorkOrderCustomerGeneric { get; set; }
        public virtual ICollection<DsWorkOrderDealer> DsWorkOrderDealer { get; set; }
        public virtual ICollection<DsWorkOrderDetailInfo> DsWorkOrderDetailInfo { get; set; }
        public virtual ICollection<DsWorkOrderLog> DsWorkOrderLog { get; set; }
        public virtual ICollection<DsWorkOrderStbReturn> DsWorkOrderStbReturn { get; set; }
        public virtual ICollection<DsWorkOrderSts> DsWorkOrderSts { get; set; }
        public virtual ICollection<DsWorkOrderSystemLog> DsWorkOrderSystemLog { get; set; }
    }
}
