﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderActivationGeneric
    {
        public int WorkOrderLogId { get; set; }
        public int WorkOrderId { get; set; }
        public int WorkOrderLogTypeId { get; set; }
        public int WorkOrderSubjectId { get; set; }
        public int WorkOrderStatusId { get; set; }
        public int? OwnerId { get; set; }
        public int? RoleId { get; set; }
        public int SubjectPartyId { get; set; }
        public int ActivationId { get; set; }
        public string Note { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public int? RedirectedDealerId { get; set; }
        public int? ChangeSmartCardAndStbReasonId { get; set; }
        public int? FromWorkOrderLogId { get; set; }
        public int? ProductId { get; set; }
        public int? StsActivationId { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? PhysicalStatusId { get; set; }
        public int? CampaignId { get; set; }
        public int? StbStatusId { get; set; }
        public int? SmartCardStatusId { get; set; }
        public int? RemoteControlStatusId { get; set; }
        public int? ScartCableStatusId { get; set; }
        public int? HdmicableStatusId { get; set; }
        public int? ActivationCostPriceId { get; set; }
        public int? SataliteCostPriceId { get; set; }
        public int? IsBlu { get; set; }
        public int? CampaignCode1 { get; set; }
        public int? CampaignCode2 { get; set; }
        public string NameField1 { get; set; }
        public string NameField2 { get; set; }
        public string NameField3 { get; set; }
        public string ExtraCampaignInfo { get; set; }
        public string SmileCampaignName { get; set; }
        public string SmileProductName { get; set; }
        public bool DirectDebit { get; set; }
        public int? SaledByUserId { get; set; }
        public int? StbTypeId { get; set; }
        public int? WorkOrderLogTypeReasonId { get; set; }
        public byte[] CardNumberEncrypt { get; set; }
        public byte[] ExpirationMonthEncrypt { get; set; }
        public byte[] ExpirationYearEncrypt { get; set; }
        public byte[] BnameEncrypt { get; set; }
        public int? DiscountAvailable { get; set; }
        public int? RequestedBonusProductId { get; set; }

        public virtual DsActivation Activation { get; set; }
        public virtual DsWorkOrder WorkOrder { get; set; }
        public virtual DsWorkOrderLogType WorkOrderLogType { get; set; }
        public virtual DsWorkOrderStatus WorkOrderStatus { get; set; }
        public virtual DsWorkOrderSubject WorkOrderSubject { get; set; }
    }
}
