﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderActivationSmile
    {
        public int WorkOrderLogId { get; set; }
        public int WorkOrderId { get; set; }
        public int WorkOrderLogTypeId { get; set; }
        public int WorkOrderSubjectId { get; set; }
        public int WorkOrderStatusId { get; set; }
        public int? OwnerId { get; set; }
        public int? RoleId { get; set; }
        public int SubjectPartyId { get; set; }
        public int CampaignId { get; set; }
        public int ProductId { get; set; }
        public int SmileProductId { get; set; }
        public int DeliveryProvinceId { get; set; }
        public int InvoiceProvinceId { get; set; }
        public int DeliveryDistrictId { get; set; }
        public int InvoiceDistrictId { get; set; }
        public int? SecurityQuestId { get; set; }
        public string SecurityAnswer { get; set; }
        public string AdslPhoneNumber { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string MotherFirstName { get; set; }
        public int? PaymentTypeId { get; set; }
        public int? InstalmentTypeId { get; set; }
        public int? PreviousAdslOperatorId { get; set; }
        public string InvoiceAddress { get; set; }
        public string DeliveryAddress { get; set; }
        public string AdslPortRegistrationWebServiceResult { get; set; }
        public string AnkaPhoneRegistrationWebServiceResult { get; set; }
        public string UserNameRegistrationWebServiceResult { get; set; }
        public int? SmartCardId { get; set; }
        public bool? InvoiceAddressIsSameDeliveryAddress { get; set; }
        public string Note { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? ReservationDate { get; set; }
        public int? StbTypeId { get; set; }
        public int? RedirectedDealerId { get; set; }
        public int? FromWorkOrderLogId { get; set; }
    }
}
