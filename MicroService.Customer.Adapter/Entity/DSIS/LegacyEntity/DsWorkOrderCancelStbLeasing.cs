﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderCancelStbLeasing
    {
        public int WorkOrderLogId { get; set; }
        public int WorkOrderId { get; set; }
        public int WorkOrderLogTypeId { get; set; }
        public int WorkOrderSubjectId { get; set; }
        public int WorkOrderStatusId { get; set; }
        public int? OwnerId { get; set; }
        public int? RoleId { get; set; }
        public int SubjectPartyId { get; set; }
        public int ActivationId { get; set; }
        public int LeasingContractId { get; set; }
        public string Note { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public int? RedirectedDealerId { get; set; }
        public int? StbStatusId { get; set; }
        public int? SmartCardStatusId { get; set; }
        public int? RemoteControlStatusId { get; set; }
        public int? ScartCableStatusId { get; set; }
        public int? ProductProcessTypeId { get; set; }
        public int? FromWorkOrderLogId { get; set; }
        public int? HdmicableStatusId { get; set; }
        public decimal? InvoiceAmount { get; set; }
        public int? WorkOrderLogTypeReasonId { get; set; }
        public int? SmartCardId { get; set; }
        public int? StbId { get; set; }
        public bool? Documental { get; set; }
        public int? SecondWorkOrderLogTypeReasonId { get; set; }
        public int? AdaptorStatusId { get; set; }
        public int? ModemStatusId { get; set; }
        public DateTime? DocumentReceivingDate { get; set; }
        public DateTime? ReservationDate { get; set; }

        public virtual DsWorkOrderLogTypeReason WorkOrderLogTypeReason { get; set; }
    }
}
