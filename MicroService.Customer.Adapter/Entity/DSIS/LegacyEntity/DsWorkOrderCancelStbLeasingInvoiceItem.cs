﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderCancelStbLeasingInvoiceItem
    {
        public int WorkOrderCancelStbLeasingInvoiceItemId { get; set; }
        public int WorkOrderId { get; set; }
        public int ProductId { get; set; }
        public decimal Amount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? LeasingContractInstallmentId { get; set; }
        public int? LeasingContractServiceBonusId { get; set; }
        public bool IsInvoiced { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LeasingContractId { get; set; }
        public bool Cancelled { get; set; }

        public virtual DsLeasingContractInstallment LeasingContractInstallment { get; set; }
        public virtual DsLeasingContractServiceBonus LeasingContractServiceBonus { get; set; }
        public virtual DsProduct Product { get; set; }
        public virtual DsWorkOrder WorkOrder { get; set; }
    }
}
