﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderContractActivation
    {
        public int WorkOrderLogId { get; set; }
        public int WorkOrderId { get; set; }
        public int WorkOrderLogTypeId { get; set; }
        public int WorkOrderSubjectId { get; set; }
        public int WorkOrderStatusId { get; set; }
        public int? OwnerId { get; set; }
        public int? RoleId { get; set; }
        public int SubjectPartyId { get; set; }
        public int CampaignId { get; set; }
        public int ProductId { get; set; }
        public int? VposId { get; set; }
        public string InvoiceAddress { get; set; }
        public int InvoiceDistrictId { get; set; }
        public int? CampaignCode1 { get; set; }
        public int? CampaignCode2 { get; set; }
        public string NameField1 { get; set; }
        public string NameField2 { get; set; }
        public string NameField3 { get; set; }
        public bool DirectDebit { get; set; }
        public string Note { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public DateTime? ReservationDate { get; set; }
        public int? StbTypeId { get; set; }
        public string ActivationCostDescription { get; set; }
        public string SataliteCostDescription { get; set; }
        public int? SaledByUserId { get; set; }
        public int? RedirectedDealerId { get; set; }
        public string DiscountCouponCode { get; set; }
        public int? ActivationId { get; set; }
        public int? FromWorkOrderLogId { get; set; }
        public string ExtraCampaignInfo { get; set; }
        public int? WorkOrderLogTypeReasonId { get; set; }
        public string SmileCampaignName { get; set; }
        public string SmileProductName { get; set; }
        public int? ActivationCostPriceId { get; set; }
        public int? SataliteCostPriceId { get; set; }
        public int? IsBlu { get; set; }
        public string IspName { get; set; }
        public string XdslNo { get; set; }
        public byte[] CardNumberEncrypt { get; set; }
        public byte[] ExpirationMonthEncrypt { get; set; }
        public byte[] ExpirationYearEncrypt { get; set; }
        public byte[] BnameEncrypt { get; set; }
        public int? RequestedBonusProductId { get; set; }
        public int? DiscountAvailable { get; set; }
    }
}
