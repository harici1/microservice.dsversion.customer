﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderCustomerGeneric
    {
        public int WorkOrderLogId { get; set; }
        public int WorkOrderId { get; set; }
        public int WorkOrderLogTypeId { get; set; }
        public int WorkOrderSubjectId { get; set; }
        public int WorkOrderStatusId { get; set; }
        public int CustomerId { get; set; }
        public int? OwnerId { get; set; }
        public int? RoleId { get; set; }
        public int SubjectPartyId { get; set; }
        public string Note { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public int? RedirectedDealerId { get; set; }
        public int? FromWorkOrderLogId { get; set; }
        public int? LawyerId { get; set; }
        public int? CampaignId { get; set; }
        public int? CampaignCode1 { get; set; }
        public int? CampaignCode2 { get; set; }
        public string NameField1 { get; set; }
        public string NameField2 { get; set; }
        public string NameField3 { get; set; }
        public string ExtraCampaignInfo { get; set; }
        public string SmileCampaignName { get; set; }
        public string SmileProductName { get; set; }
        public string IspName { get; set; }
        public string XdslNo { get; set; }
        public DateTime? ReservationDate { get; set; }
        public int? WorkOrderLogTypeReasonId { get; set; }
        public DateTime? DocumentReceivingDate { get; set; }

        public virtual DsParty Customer { get; set; }
        public virtual DsWorkOrder WorkOrder { get; set; }
        public virtual DsWorkOrderLogType WorkOrderLogType { get; set; }
        public virtual DsWorkOrderStatus WorkOrderStatus { get; set; }
        public virtual DsWorkOrderSubject WorkOrderSubject { get; set; }
    }
}
