﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderDetailInfo
    {
        public int WorkOrderDetailInfoId { get; set; }
        public int WorkOrderId { get; set; }
        public int WorkOrderDetailInfoTypeId { get; set; }
        public string Value { get; set; }
        public bool Cancelled { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? LastUpdateById { get; set; }
        public string Note { get; set; }
        public string ValueBig { get; set; }

        public virtual DsWorkOrder WorkOrder { get; set; }
        public virtual DsWorkOrderDetailInfoType WorkOrderDetailInfoType { get; set; }
    }
}
