﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderDetailInfoType
    {
        public DsWorkOrderDetailInfoType()
        {
            DsWorkOrderDetailInfo = new HashSet<DsWorkOrderDetailInfo>();
        }

        public int WorkOrderDetailInfoTypeId { get; set; }
        public string Name { get; set; }
        public string DataType { get; set; }
        public bool Editable { get; set; }
        public string CamundaName { get; set; }

        public virtual ICollection<DsWorkOrderDetailInfo> DsWorkOrderDetailInfo { get; set; }
    }
}
