﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderDetailScreenControlRelation
    {
        public int WorkOrderDetailScreenControlRelationId { get; set; }
        public int DetailScreenId { get; set; }
        public int DetailScreenControlId { get; set; }
        public int OrderLevel { get; set; }
    }
}
