﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderDetailScreenControls
    {
        public int DetailScreenControlId { get; set; }
        public string ControlLabelText { get; set; }
        public bool ControlLabelTextIsDataField { get; set; }
        public string ControlLabelTooltip { get; set; }
        public string DataFieldName { get; set; }
        public string DataFormatString { get; set; }
        public int ControlType { get; set; }
    }
}
