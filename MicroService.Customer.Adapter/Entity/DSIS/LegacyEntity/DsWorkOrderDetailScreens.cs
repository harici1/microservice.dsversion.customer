﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderDetailScreens
    {
        public int WorkOrderDetailScreenId { get; set; }
        public int WorkOrderSubjectId { get; set; }
        public string DetailScreenName { get; set; }
        public string DetailScreenDescription { get; set; }
        public string DatabaseConnectionKey { get; set; }
        public string DatabaseQuery { get; set; }
        public int QueryType { get; set; }
        public string DatabaseQueryNewPurchaseFlow { get; set; }
    }
}
