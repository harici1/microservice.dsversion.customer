﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderDeviceReplacementReason
    {
        public int DealerReplaceDeviceReasonId { get; set; }
        public string Reason { get; set; }
        public bool? Status { get; set; }
    }
}
