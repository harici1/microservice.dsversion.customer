﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderDeviceReplacementToReason
    {
        public int WorkOrderDeviceReplacementToReasonId { get; set; }
        public int WorkOrderDeviceReplacementId { get; set; }
        public int WorkOrderDeviceReplacementReasonId { get; set; }
    }
}
