﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderEquipmentReplacementDetail
    {
        public int WorkOrderEquipmentReplacementDetailId { get; set; }
        public int WorkOrderEquipmentReplacementId { get; set; }
        public int WorkOrderEquipmentId { get; set; }
        public int? WorkOrderDeviceReplacementEquipmentDetailDirectionId { get; set; }
        public int? WorkOrderDeviceReplacementEquipmentStatusId { get; set; }
        public string DeviceShortSerial { get; set; }
    }
}
