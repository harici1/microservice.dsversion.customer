﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderExceptionLog
    {
        public int WorkOrderExceptionLogId { get; set; }
        public int WorkOrderId { get; set; }
        public string ExceptionMessage { get; set; }
        public DateTime CreationDate { get; set; }
        public int? CreatedById { get; set; }
        public int? WorkOrderLogTypeId { get; set; }
    }
}
