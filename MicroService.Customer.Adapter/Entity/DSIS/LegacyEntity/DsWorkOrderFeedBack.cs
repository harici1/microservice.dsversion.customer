﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderFeedBack
    {
        public int WorkOrderFeedBackId { get; set; }
        public int WorkOrderId { get; set; }
        public int WorkOrderLogId { get; set; }
        public int WorkOrderFeedBackReasonId { get; set; }
        public string Note { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
