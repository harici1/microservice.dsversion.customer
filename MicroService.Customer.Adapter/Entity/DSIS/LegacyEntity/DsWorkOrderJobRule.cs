﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderJobRule
    {
        public int WorkOrderJobRuleId { get; set; }
        public int WorkOrderSubjectId { get; set; }
        public int WorkOrderLogTypeId { get; set; }
        public int? WaitTimeInMinuteForCreation { get; set; }
        public int? WaitTimeInMinuteForLastUpdate { get; set; }
        public bool Disabled { get; set; }
        public int Priority { get; set; }
        public int ContactChannelId { get; set; }
        public int PreferedSuccessProcessWorkOrderLogTypeId { get; set; }
        public int PreferedFailureProcessWorkOrderLogTypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? WorkOrderLogTypeReasonId { get; set; }
        public int MaxRetryCount { get; set; }

        public virtual DsWorkOrderLogType WorkOrderLogType { get; set; }
        public virtual DsWorkOrderSubject WorkOrderSubject { get; set; }
    }
}
