﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderJobRuleForExecutor
    {
        public int WorkOrderJobRuleId { get; set; }
        public int? WaitTimeInMinuteForCreation { get; set; }
        public int? WaitTimeInMinuteForLastUpdate { get; set; }
        public int PreferedSuccessProcessWorkOrderLogTypeId { get; set; }
        public int PreferedFailureProcessWorkOrderLogTypeId { get; set; }
        public int? WorkOrderLogTypeReasonId { get; set; }
        public string GetPendingJobSp { get; set; }
        public bool Disabled { get; set; }
        public int Priority { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int WorkOrderJobRuleForExecutorRetryCount { get; set; }
        public int WorkOrderJobRuleForExecutorRetryTimeoutByMinute { get; set; }
    }
}
