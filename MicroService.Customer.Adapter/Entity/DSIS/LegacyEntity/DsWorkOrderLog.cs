﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderLog
    {
        public int WorkOrderLogId { get; set; }
        public int WorkOrderId { get; set; }
        public int WorkOrderLogTypeId { get; set; }
        public int WorkOrderSubjectId { get; set; }
        public int? OwnerId { get; set; }
        public int? RoleId { get; set; }
        public int? SubjectPartyId { get; set; }
        public int WorkOrderStatusId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public string Note { get; set; }
        public int? CampaignId { get; set; }
        public DateTime? ReservationDate { get; set; }
        public int? RedirectedDealerId { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public Guid UniqueId { get; set; }

        public virtual DsParty SubjectParty { get; set; }
        public virtual DsWorkOrder WorkOrder { get; set; }
        public virtual DsWorkOrderLogType WorkOrderLogType { get; set; }
    }
}
