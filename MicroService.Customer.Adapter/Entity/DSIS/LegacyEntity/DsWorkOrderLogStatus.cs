﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderLogStatus
    {
        public long WorkOrderLogStatusId { get; set; }
        public long? WorkOrderId { get; set; }
        public int? WorkOrderLogType { get; set; }
        public long? OrderId { get; set; }
        public int? OrderProductId { get; set; }
        public int DocumentProcessStatusId { get; set; }
        public DateTime? CreateDate { get; set; }
        public string TriggerUserName { get; set; }
        public string TriggerHostName { get; set; }
        public DateTime? TriggerDate { get; set; }
        public string TriggerType { get; set; }
        public int? PrimeProcessStatusId { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? UserId { get; set; }
        public int? SendingCbrmstatusId { get; set; }

        public virtual DsDocumentProcessStatus DocumentProcessStatus { get; set; }
    }
}
