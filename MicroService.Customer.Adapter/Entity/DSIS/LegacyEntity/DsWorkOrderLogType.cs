﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderLogType
    {
        public DsWorkOrderLogType()
        {
            DsWorkOrder = new HashSet<DsWorkOrder>();
            DsWorkOrderActivationGeneric = new HashSet<DsWorkOrderActivationGeneric>();
            DsWorkOrderCustomerGeneric = new HashSet<DsWorkOrderCustomerGeneric>();
            DsWorkOrderJobRule = new HashSet<DsWorkOrderJobRule>();
            DsWorkOrderLeasingContractServiceGeneric = new HashSet<DsWorkOrderLeasingContractServiceGeneric>();
            DsWorkOrderLog = new HashSet<DsWorkOrderLog>();
            DsWorkOrderLogTypeRuleFromWorkOrderLogType = new HashSet<DsWorkOrderLogTypeRule>();
            DsWorkOrderLogTypeRuleToWorkOrderLogType = new HashSet<DsWorkOrderLogTypeRule>();
            DsWorkOrderStbReturn = new HashSet<DsWorkOrderStbReturn>();
            DsWorkOrderSts = new HashSet<DsWorkOrderSts>();
            TssisDsWorkOrder = new HashSet<TssisDsWorkOrder>();
        }

        public int WorkOrderLogTypeId { get; set; }
        public string Name { get; set; }
        public int WorkOrderSubjectId { get; set; }
        public bool StartAction { get; set; }
        public bool FinishAction { get; set; }
        public bool SubjectIsOk { get; set; }
        public bool RedirectToDealer { get; set; }
        public bool ReservationEnabled { get; set; }
        public bool HasForm { get; set; }
        public string FormName { get; set; }
        public int? FormHeight { get; set; }
        public int? FormWidht { get; set; }
        public bool SecondStartAction { get; set; }
        public int WorkOrderLogTypeGroupId { get; set; }
        public string ProcessFunctionName { get; set; }
        public string Message { get; set; }
        public bool SuccessProcessLogType { get; set; }
        public bool FailureProcessLogType { get; set; }
        public string DynamicMessage { get; set; }
        public bool ForceUpdateCustomerContactInfo { get; set; }
        public string ValidationStoredProcedure { get; set; }
        public string ProcessStoredProcedure { get; set; }
        public bool DisableProcessButton { get; set; }
        public bool AttachmentRequired { get; set; }
        public bool FatalErrorType { get; set; }
        public bool DocumentReceivingDateEnabled { get; set; }
        public int? JobEmailTypeId { get; set; }
        public int? SystemCode { get; set; }

        public virtual ICollection<DsWorkOrder> DsWorkOrder { get; set; }
        public virtual ICollection<DsWorkOrderActivationGeneric> DsWorkOrderActivationGeneric { get; set; }
        public virtual ICollection<DsWorkOrderCustomerGeneric> DsWorkOrderCustomerGeneric { get; set; }
        public virtual ICollection<DsWorkOrderJobRule> DsWorkOrderJobRule { get; set; }
        public virtual ICollection<DsWorkOrderLeasingContractServiceGeneric> DsWorkOrderLeasingContractServiceGeneric { get; set; }
        public virtual ICollection<DsWorkOrderLog> DsWorkOrderLog { get; set; }
        public virtual ICollection<DsWorkOrderLogTypeRule> DsWorkOrderLogTypeRuleFromWorkOrderLogType { get; set; }
        public virtual ICollection<DsWorkOrderLogTypeRule> DsWorkOrderLogTypeRuleToWorkOrderLogType { get; set; }
        public virtual ICollection<DsWorkOrderStbReturn> DsWorkOrderStbReturn { get; set; }
        public virtual ICollection<DsWorkOrderSts> DsWorkOrderSts { get; set; }
        public virtual ICollection<TssisDsWorkOrder> TssisDsWorkOrder { get; set; }
    }
}
