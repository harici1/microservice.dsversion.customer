﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderLogTypeDetail
    {
        public int WorkOrderLogTypeDetailId { get; set; }
        public int WorkOrderLogTypeDetailTypeId { get; set; }
        public int WorkOrderLogTypeId { get; set; }
        public string Value { get; set; }
        public bool Disabled { get; set; }

        public virtual DsWorkOrderLogTypeDetailType WorkOrderLogTypeDetailType { get; set; }
    }
}
