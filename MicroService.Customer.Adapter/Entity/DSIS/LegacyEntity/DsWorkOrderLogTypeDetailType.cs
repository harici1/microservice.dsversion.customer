﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderLogTypeDetailType
    {
        public DsWorkOrderLogTypeDetailType()
        {
            DsWorkOrderLogTypeDetail = new HashSet<DsWorkOrderLogTypeDetail>();
        }

        public int WorkOrderLogTypeDetailTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsWorkOrderLogTypeDetail> DsWorkOrderLogTypeDetail { get; set; }
    }
}
