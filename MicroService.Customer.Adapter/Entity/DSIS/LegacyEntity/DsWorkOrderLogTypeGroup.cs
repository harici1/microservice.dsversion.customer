﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderLogTypeGroup
    {
        public int WorkOrderLogTypeGroupId { get; set; }
        public string Name { get; set; }
        public bool PendingState { get; set; }
    }
}
