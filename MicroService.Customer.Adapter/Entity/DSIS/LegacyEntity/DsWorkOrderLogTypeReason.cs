﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderLogTypeReason
    {
        public DsWorkOrderLogTypeReason()
        {
            DsWorkOrderCancelStbLeasing = new HashSet<DsWorkOrderCancelStbLeasing>();
        }

        public int WorkOrderLogTypeReasonId { get; set; }
        public int WorkOrderLogTypeId { get; set; }
        public string Name { get; set; }
        public int? WorkOrderLogTypeReasonSubjectId { get; set; }
        public bool IsActive { get; set; }
        public bool? VisibleToWeb { get; set; }
        public string WebDescription { get; set; }
        public string SystemCode { get; set; }

        public virtual ICollection<DsWorkOrderCancelStbLeasing> DsWorkOrderCancelStbLeasing { get; set; }
    }
}
