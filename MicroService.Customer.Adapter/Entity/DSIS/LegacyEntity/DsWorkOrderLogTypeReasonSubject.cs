﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderLogTypeReasonSubject
    {
        public int WorkOrderLogTypeReasonSubjectId { get; set; }
        public string Name { get; set; }
        public int WorkOrderSubjectId { get; set; }
        public bool IsActive { get; set; }
        public bool? VisibleToWeb { get; set; }
        public string WebDescription { get; set; }
    }
}
