﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderLogTypeRule
    {
        public int WorkOrderLogTypeRuleId { get; set; }
        public int FromWorkOrderLogTypeId { get; set; }
        public int ToWorkOrderLogTypeId { get; set; }
        public int FromRoleId { get; set; }
        public int? ToRoleId { get; set; }
        public string SetOwnerRoleAndLogTypeStoredProcedure { get; set; }

        public virtual DsRole FromRole { get; set; }
        public virtual DsWorkOrderLogType FromWorkOrderLogType { get; set; }
        public virtual DsRole ToRole { get; set; }
        public virtual DsWorkOrderLogType ToWorkOrderLogType { get; set; }
    }
}
