﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderModemReplacement
    {
        public int WorkOrderModemReplacementId { get; set; }
        public int WorkOrderId { get; set; }
        public int UserId { get; set; }
        public int DealerId { get; set; }
        public int CustomerId { get; set; }
        public int? ActivationId { get; set; }
        public string Note { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
