﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderModemReplacementDetail
    {
        public int WorkOrderModemReplacementDetailId { get; set; }
        public int WorkOrderModemReplacementId { get; set; }
        public int WorkOrderModemId { get; set; }
        public int? WorkOrderModemReplacementEquipmentDetailDirectionId { get; set; }
        public int? WorkOrderModemReplacementEquipmentStatusId { get; set; }
        public string DeviceShortSerial { get; set; }
    }
}
