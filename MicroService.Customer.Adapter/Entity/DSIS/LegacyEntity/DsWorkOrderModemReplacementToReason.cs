﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderModemReplacementToReason
    {
        public int WorkOrderModemReplacementToReasonId { get; set; }
        public int WorkOrderModemReplacementId { get; set; }
        public int WorkOrderModemReplacementReasonId { get; set; }
    }
}
