﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderPotentialCustomer
    {
        public int WorkOrderLogId { get; set; }
        public int WorkOrderId { get; set; }
        public int WorkOrderLogTypeId { get; set; }
        public int WorkOrderSubjectId { get; set; }
        public int WorkOrderStatusId { get; set; }
        public int? OwnerId { get; set; }
        public int? RoleId { get; set; }
        public int? SubjectPartyId { get; set; }
        public string Name { get; set; }
        public string FamilyName { get; set; }
        public int? DistrictId { get; set; }
        public int? NeighbourhoodId { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string PhoneNumber2 { get; set; }
        public string PhoneNumber3 { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public string Note { get; set; }
        public int? RedirectedDealerId { get; set; }
        public DateTime? BirthDate { get; set; }
        public bool? IsMale { get; set; }
        public int? NumberOfSibling { get; set; }
        public int? FootballTeamId { get; set; }
        public string Hobbies { get; set; }
        public string SchoolName { get; set; }
        public string ParentPhone { get; set; }
        public string Address { get; set; }
        public string ParentAddress { get; set; }
        public string ParentEmail { get; set; }
        public string SmartJuniorUserName { get; set; }
        public string SmartJuniorPassword { get; set; }
        public string SmartCardSerialNumber { get; set; }
        public string ResponseMessage { get; set; }
        public int? FromWorkOrderLogId { get; set; }
        public int? PotentialOrderTypeId { get; set; }
        public string TckimlikNo { get; set; }
        public int? WorkOrderLogTypeReasonId { get; set; }
        public int? WebSiteId { get; set; }
        public string WebSource { get; set; }
    }
}
