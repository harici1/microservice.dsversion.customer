﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderPotentialOrderType
    {
        public int PotentialOrderTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? CampaignId { get; set; }
        public int? ContactChannelId { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public int? WorkOrderSubjectId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
