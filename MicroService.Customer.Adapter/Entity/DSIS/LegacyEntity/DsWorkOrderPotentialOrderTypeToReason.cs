﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderPotentialOrderTypeToReason
    {
        public int WorkOrderPotentialOrderTypeToReasonId { get; set; }
        public int PotentialOrderTypeId { get; set; }
        public int WorkOrderLogTypeReasonId { get; set; }
    }
}
