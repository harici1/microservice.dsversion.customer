﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderPpvSale
    {
        public int WorkOrderLogId { get; set; }
        public int WorkOrderId { get; set; }
        public int WorkOrderLogTypeId { get; set; }
        public int WorkOrderSubjectId { get; set; }
        public int WorkOrderStatusId { get; set; }
        public int? OwnerId { get; set; }
        public int? RoleId { get; set; }
        public int SubjectPartyId { get; set; }
        public int ActivationId { get; set; }
        public int ProductId { get; set; }
        public int CampaignId { get; set; }
        public decimal Amount { get; set; }
        public bool DirectDebit { get; set; }
        public int? VposId { get; set; }
        public string InvoiceAddress { get; set; }
        public int InvoiceDistrictId { get; set; }
        public string Note { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public DateTime ReservationDate { get; set; }
        public int? RedirectedDealerId { get; set; }
        public int? FromWorkOrderLogId { get; set; }
        public byte[] CardNumberEncrypt { get; set; }
        public byte[] ExpirationMonthEncrypt { get; set; }
        public byte[] ExpirationYearEncrypt { get; set; }
        public byte[] BnameEncrypt { get; set; }
        public int? DiscountAvailable { get; set; }
    }
}
