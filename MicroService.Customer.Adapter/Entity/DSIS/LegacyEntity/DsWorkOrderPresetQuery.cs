﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderPresetQuery
    {
        public int WorkOrderPresetQueryId { get; set; }
        public string Name { get; set; }
        public string PresetQuerySpName { get; set; }
        public bool IsActive { get; set; }
    }
}
