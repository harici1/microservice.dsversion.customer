﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderPresetQueryToRole
    {
        public int WorkOrderPresetQueryToRoleId { get; set; }
        public int WorkOrderPresetQueryId { get; set; }
        public int RoleId { get; set; }
    }
}
