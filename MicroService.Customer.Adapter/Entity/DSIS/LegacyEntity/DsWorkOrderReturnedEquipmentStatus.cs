﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderReturnedEquipmentStatus
    {
        public int WorkOrderReturnedEquipmentStatusId { get; set; }
        public string Name { get; set; }
    }
}
