﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderStatus
    {
        public DsWorkOrderStatus()
        {
            DsWorkOrderActivationGeneric = new HashSet<DsWorkOrderActivationGeneric>();
            DsWorkOrderCustomerGeneric = new HashSet<DsWorkOrderCustomerGeneric>();
            DsWorkOrderLeasingContractServiceGeneric = new HashSet<DsWorkOrderLeasingContractServiceGeneric>();
            DsWorkOrderStbReturn = new HashSet<DsWorkOrderStbReturn>();
            DsWorkOrderSts = new HashSet<DsWorkOrderSts>();
        }

        public int WorkOrderStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DsWorkOrderActivationGeneric> DsWorkOrderActivationGeneric { get; set; }
        public virtual ICollection<DsWorkOrderCustomerGeneric> DsWorkOrderCustomerGeneric { get; set; }
        public virtual ICollection<DsWorkOrderLeasingContractServiceGeneric> DsWorkOrderLeasingContractServiceGeneric { get; set; }
        public virtual ICollection<DsWorkOrderStbReturn> DsWorkOrderStbReturn { get; set; }
        public virtual ICollection<DsWorkOrderSts> DsWorkOrderSts { get; set; }
    }
}
