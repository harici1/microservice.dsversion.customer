﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderStbReturn
    {
        public int WorkOrderLogId { get; set; }
        public int WorkOrderId { get; set; }
        public int WorkOrderLogTypeId { get; set; }
        public int WorkOrderSubjectId { get; set; }
        public int WorkOrderStatusId { get; set; }
        public int? OwnerId { get; set; }
        public int? RoleId { get; set; }
        public int SubjectPartyId { get; set; }
        public int ActivationId { get; set; }
        public string Note { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime CreatedById { get; set; }
        public DateTime AssigmentDate { get; set; }
        public int? RedirectedDealerId { get; set; }
        public int? FromWorkOrderLogId { get; set; }

        public virtual DsActivation Activation { get; set; }
        public virtual DsWorkOrder WorkOrder { get; set; }
        public virtual DsWorkOrderLogType WorkOrderLogType { get; set; }
        public virtual DsWorkOrderStatus WorkOrderStatus { get; set; }
        public virtual DsWorkOrderSubject WorkOrderSubject { get; set; }
    }
}
