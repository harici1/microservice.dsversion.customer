﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderSubject
    {
        public DsWorkOrderSubject()
        {
            DsWorkOrderActivationGeneric = new HashSet<DsWorkOrderActivationGeneric>();
            DsWorkOrderCustomerGeneric = new HashSet<DsWorkOrderCustomerGeneric>();
            DsWorkOrderJobRule = new HashSet<DsWorkOrderJobRule>();
            DsWorkOrderLeasingContractServiceGeneric = new HashSet<DsWorkOrderLeasingContractServiceGeneric>();
            DsWorkOrderStbReturn = new HashSet<DsWorkOrderStbReturn>();
            DsWorkOrderSts = new HashSet<DsWorkOrderSts>();
        }

        public int WorkOrderSubjectId { get; set; }
        public int WorkOrderTypeId { get; set; }
        public string Name { get; set; }
        public int WorkOrderSubjectClassId { get; set; }
        public bool IsVisible { get; set; }
        public int? SystemCode { get; set; }
        public bool? CloseableWhenContractCancel { get; set; }

        public virtual DsWorkOrderSubjectClass WorkOrderSubjectClass { get; set; }
        public virtual DsWorkOrderType WorkOrderType { get; set; }
        public virtual ICollection<DsWorkOrderActivationGeneric> DsWorkOrderActivationGeneric { get; set; }
        public virtual ICollection<DsWorkOrderCustomerGeneric> DsWorkOrderCustomerGeneric { get; set; }
        public virtual ICollection<DsWorkOrderJobRule> DsWorkOrderJobRule { get; set; }
        public virtual ICollection<DsWorkOrderLeasingContractServiceGeneric> DsWorkOrderLeasingContractServiceGeneric { get; set; }
        public virtual ICollection<DsWorkOrderStbReturn> DsWorkOrderStbReturn { get; set; }
        public virtual ICollection<DsWorkOrderSts> DsWorkOrderSts { get; set; }
    }
}
