﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderSubjectClass
    {
        public DsWorkOrderSubjectClass()
        {
            DsWorkOrderSubject = new HashSet<DsWorkOrderSubject>();
        }

        public int WorkOrderSubjectClassId { get; set; }
        public int SystemCode { get; set; }
        public string ClassName { get; set; }
        public int CreationFormHeight { get; set; }
        public int CreationFormWidht { get; set; }
        public string CreationAspxFormName { get; set; }
        public int ProcessFormHeight { get; set; }
        public int ProcessFormWidht { get; set; }
        public string ProcessAspxFormName { get; set; }
        public string Description { get; set; }
        public string WorkOrderSubTable { get; set; }

        public virtual ICollection<DsWorkOrderSubject> DsWorkOrderSubject { get; set; }
    }
}
