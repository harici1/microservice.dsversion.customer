﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderSystemLog
    {
        public int WorkOrderSystemLogId { get; set; }
        public int WorkOrderId { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedById { get; set; }
        public int DocumentId { get; set; }
        public int DocumentTypeId { get; set; }

        public virtual DsWorkOrder WorkOrder { get; set; }
    }
}
