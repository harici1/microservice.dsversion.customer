﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class DsWorkOrderType
    {
        public DsWorkOrderType()
        {
            DsWorkOrderSubject = new HashSet<DsWorkOrderSubject>();
        }

        public int WorkOrderTypeId { get; set; }
        public string Name { get; set; }
        public bool IsPotantial { get; set; }
        public bool IsDealer { get; set; }
        public bool IsPotantialSale { get; set; }
        public int? MaxResponseTimeInMinute { get; set; }
        public string MaxResponseTimeDescription { get; set; }

        public virtual ICollection<DsWorkOrderSubject> DsWorkOrderSubject { get; set; }
    }
}
