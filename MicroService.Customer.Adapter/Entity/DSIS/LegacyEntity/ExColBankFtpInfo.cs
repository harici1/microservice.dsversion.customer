﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class ExColBankFtpInfo
    {
        public int Id { get; set; }
        public int CollectionBankId { get; set; }
        public string DebtFileCreationAddress { get; set; }
        public string DebtFileUploadAddress { get; set; }
        public string DebtFileArchiveAddress { get; set; }
        public string CollectionFileCreationAddress { get; set; }
        public string CollectionFileDownloadAddress { get; set; }
        public string CollectionFileArchiveAddress { get; set; }
        public string FtpUserName { get; set; }
        public string FtpPassword { get; set; }
        public string ClientId { get; set; }
        public string DebtFileCreationClassName { get; set; }
        public string CollectionFileReadClassName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string DebtFilePrefix { get; set; }
        public string CollectionFilePrefix { get; set; }
        public string BankShortName { get; set; }
        public bool IsSecureFtp { get; set; }
        public string SFftCollectionFolder { get; set; }
        public string SFftDebtFolder { get; set; }
        public string CollectionFileReadClassName2 { get; set; }
    }
}
