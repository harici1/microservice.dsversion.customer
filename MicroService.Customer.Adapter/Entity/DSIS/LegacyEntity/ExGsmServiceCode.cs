﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class ExGsmServiceCode
    {
        public string GsmServiceCode { get; set; }
        public string GsmServiceName { get; set; }
    }
}
