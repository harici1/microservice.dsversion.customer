﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class ExMobilePhoneAreaCode
    {
        public string MobilePhoneAreaCode { get; set; }
        public string MobilePhoneOperator { get; set; }
    }
}
