﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class ExSiService
    {
        public int Id { get; set; }
        public int SiService { get; set; }
        public string SiServiceName { get; set; }
        public string ShortCode { get; set; }
        public int ViewerChannelNum { get; set; }
    }
}
