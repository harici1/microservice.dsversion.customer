﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class ExSmileInvoice
    {
        public int InvoiceId { get; set; }
        public string AnkaAccountNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public int PartyId { get; set; }
        public string PartyName { get; set; }
        public string Address { get; set; }
        public string TaxNumber { get; set; }
        public int BatchId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int LeasingContractId { get; set; }
    }
}
