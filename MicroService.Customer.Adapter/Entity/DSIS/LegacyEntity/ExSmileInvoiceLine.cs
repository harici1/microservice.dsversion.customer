﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class ExSmileInvoiceLine
    {
        public int InvoiceLineId { get; set; }
        public int InvoiceId { get; set; }
        public int InvoiceItemId { get; set; }
        public string Description { get; set; }
        public decimal NetAmount { get; set; }
        public decimal TaxRate1 { get; set; }
        public decimal TaxAmount1 { get; set; }
        public decimal TaxRate2 { get; set; }
        public decimal TaxAmount2 { get; set; }
        public decimal Amount { get; set; }
        public int ContractInstallmentId { get; set; }
        public DateTime ServiceStartDate { get; set; }
        public DateTime ServiceEndDate { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
    }
}
