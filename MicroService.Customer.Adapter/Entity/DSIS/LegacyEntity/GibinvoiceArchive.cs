﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class GibinvoiceArchive
    {
        public string CustInvId { get; set; }
        public Guid Uuid { get; set; }
        public string InvoiceId { get; set; }
        public Guid EnvelopeUuid { get; set; }
        public DateTime ProcessDateTime { get; set; }
        public int ProcessResultCode { get; set; }
        public string ProcessResultMessage { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public int? BatchId { get; set; }
    }
}
