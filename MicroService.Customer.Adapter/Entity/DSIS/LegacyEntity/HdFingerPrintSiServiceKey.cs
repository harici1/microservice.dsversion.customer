﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class HdFingerPrintSiServiceKey
    {
        public int FingerPrintSiServiceKeyId { get; set; }
        public int ChannelNumber { get; set; }
        public string SiServiceName { get; set; }
        public int SiServiceKey { get; set; }
        public bool AllowFingerPrint { get; set; }
        public int Duration { get; set; }
    }
}
