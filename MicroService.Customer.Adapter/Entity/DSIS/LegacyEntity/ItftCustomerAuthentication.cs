﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class ItftCustomerAuthentication
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SurName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool? SysAdmin { get; set; }
        public int? Status { get; set; }
        public DateTime? RegisteredDate { get; set; }
        public bool? IsOnline { get; set; }
        public int? DsisUserId { get; set; }
        public string Email { get; set; }
    }
}
