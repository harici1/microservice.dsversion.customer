﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class ItftCustomerAuthenticationStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
