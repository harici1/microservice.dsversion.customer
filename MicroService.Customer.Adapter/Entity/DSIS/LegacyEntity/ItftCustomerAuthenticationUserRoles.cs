﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class ItftCustomerAuthenticationUserRoles
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? PageId { get; set; }
    }
}
