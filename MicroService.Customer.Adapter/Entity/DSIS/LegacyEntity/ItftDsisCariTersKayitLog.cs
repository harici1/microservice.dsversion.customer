﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class ItftDsisCariTersKayitLog
    {
        public int Id { get; set; }
        public string LeasingContractNumber { get; set; }
        public decimal? Price { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
    }
}
