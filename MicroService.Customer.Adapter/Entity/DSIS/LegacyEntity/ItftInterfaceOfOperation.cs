﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class ItftInterfaceOfOperation
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public int? System { get; set; }
        public int? Sira { get; set; }
        public int? ParentId { get; set; }
    }
}
