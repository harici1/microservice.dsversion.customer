﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class ItftInterfaceOfOperationSystem
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
