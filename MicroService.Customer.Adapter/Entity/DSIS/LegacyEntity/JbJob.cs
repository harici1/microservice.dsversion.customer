﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJob
    {
        public int JobId { get; set; }
        public int JobTypeId { get; set; }
        public int JobStatusId { get; set; }
        public DateTime JobStartDate { get; set; }
        public DateTime JobExpiryDate { get; set; }
        public int PriorityNumber { get; set; }
        public int DocumentTypeId { get; set; }
        public int DocumentId { get; set; }
        public string ReturnMessage { get; set; }
        public string Description { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? RetryCount { get; set; }
        public int? JobExecutorId { get; set; }
        public int? EmmgExecutorId { get; set; }
        public int? PartyId { get; set; }
        public int? ContactChannelId { get; set; }
    }
}
