﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobActivateSmsCaProduct
    {
        public int JobActivateSmsCaProductId { get; set; }
        public int ActivationId { get; set; }
        public int ProductId { get; set; }
        public int? PinCode { get; set; }
    }
}
