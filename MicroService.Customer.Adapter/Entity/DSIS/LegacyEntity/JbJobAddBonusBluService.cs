﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobAddBonusBluService
    {
        public int JobAddBonusBluServiceId { get; set; }
        public string LeasingContractNumber { get; set; }
        public string ProductSystemCode { get; set; }
        public int CampaignId { get; set; }
    }
}
