﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobAuthorizeOppv
    {
        public int JobAuthorizeOppvId { get; set; }
        public int NdsSubscriberId { get; set; }
        public int CaProductId { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int TapingStatus { get; set; }
    }
}
