﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobBatch
    {
        public int JobBatchId { get; set; }
        public string Name { get; set; }
        public int JobTypeId { get; set; }
        public int JobBatchTypeId { get; set; }
        public int? CampaignId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
