﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobBatchType
    {
        public int JobBatchTypeId { get; set; }
        public string Name { get; set; }
    }
}
