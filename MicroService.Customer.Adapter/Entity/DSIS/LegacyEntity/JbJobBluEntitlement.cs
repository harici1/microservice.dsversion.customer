﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobBluEntitlement
    {
        public int JobBluEntitlementId { get; set; }
        public int LeasingContractId { get; set; }
    }
}
