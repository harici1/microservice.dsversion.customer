﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobBluEntitlementWithSubscriberUser
    {
        public int JobBluEntitlementWithSubscriberUserId { get; set; }
        public int SubscriberUserId { get; set; }
    }
}
