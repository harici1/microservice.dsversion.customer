﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobBmail
    {
        public int JobBmailId { get; set; }
        public int NdsSubscriberId { get; set; }
        public string Header { get; set; }
        public string Body { get; set; }
        public int Slot { get; set; }
        public DateTime EmmgDeleteDate { get; set; }
        public string AddressType { get; set; }
        public string Address { get; set; }
        public int? ActivationId { get; set; }
    }
}
