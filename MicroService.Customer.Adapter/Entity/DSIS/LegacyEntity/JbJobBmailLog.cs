﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobBmailLog
    {
        public int JobBmailLogId { get; set; }
        public int JobBmailId { get; set; }
        public string Packet { get; set; }
        public string Response { get; set; }
        public string ReturnCode { get; set; }
    }
}
