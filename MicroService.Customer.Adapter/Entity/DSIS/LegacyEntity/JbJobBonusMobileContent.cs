﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobBonusMobileContent
    {
        public int JobBonusMobileContentId { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string PromotionCode { get; set; }
    }
}
