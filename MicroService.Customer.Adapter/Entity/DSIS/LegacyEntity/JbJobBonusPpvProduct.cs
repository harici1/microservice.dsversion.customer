﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobBonusPpvProduct
    {
        public int JobBonusPpvProductId { get; set; }
        public int PartyId { get; set; }
        public int SmartCardId { get; set; }
        public DateTime EntitlementDate { get; set; }
        public int ProductPriceId { get; set; }
        public int? CampaignId { get; set; }
        public int? ProductProcessTypeId { get; set; }
    }
}
