﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobBonusPpvProductToCustomerWithNoActivation
    {
        public int JobBonusPpvProductToCustomerWithNoActivationId { get; set; }
        public int PartyId { get; set; }
        public DateTime EntitlementDate { get; set; }
        public int ProductPriceId { get; set; }
        public int? CampaignId { get; set; }
        public int? ProductProcessTypeId { get; set; }
    }
}
