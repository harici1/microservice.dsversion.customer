﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobBonusTokenToCustomerWithActivation
    {
        public int JobBonusTokenId { get; set; }
        public int PartyId { get; set; }
        public int ProductPriceId { get; set; }
        public int? CampaignId { get; set; }
        public int? ProductProcessTypeId { get; set; }
    }
}
