﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobBonusTokenToDealer
    {
        public int JobBonusTokenToDealerId { get; set; }
        public int DealerId { get; set; }
        public decimal TokenAmount { get; set; }
        public int CampaignId { get; set; }
        public int? ProductPriceId { get; set; }
        public int? ProductProcessTypeId { get; set; }
    }
}
