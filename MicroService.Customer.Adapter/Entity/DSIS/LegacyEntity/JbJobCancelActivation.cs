﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobCancelActivation
    {
        public int JobCancelActivationId { get; set; }
        public int ActivationId { get; set; }
        public int ProductProcessTypeId { get; set; }
    }
}
