﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobCancelActivationService
    {
        public int JobCancelActivationServiceId { get; set; }
        public int ActivationServiceId { get; set; }
        public int ProductProcessTypeId { get; set; }
    }
}
