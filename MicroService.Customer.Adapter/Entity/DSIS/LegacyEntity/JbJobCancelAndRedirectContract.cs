﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobCancelAndRedirectContract
    {
        public int JobCancelAndRedirectContractId { get; set; }
        public int LeasingContractId { get; set; }
        public int WorkOrderSubjectId { get; set; }
        public string WorkOrderNote { get; set; }
        public int LogTypeReasonId { get; set; }
        public bool IsDocumentalProcess { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int ProductProcessTypeId { get; set; }
    }
}
