﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobCancelBonusProduct
    {
        public int JobCancelBonusProductId { get; set; }
        public int OrderId { get; set; }
    }
}
