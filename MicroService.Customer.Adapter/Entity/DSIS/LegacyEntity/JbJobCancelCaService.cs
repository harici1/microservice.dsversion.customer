﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobCancelCaService
    {
        public int JobCancelCaServiceId { get; set; }
        public int NdsSubscriberId { get; set; }
        public int CaProductId { get; set; }
    }
}
