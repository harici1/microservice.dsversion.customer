﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobCancelLeasingContract
    {
        public int JobCancelLeasingContractId { get; set; }
        public int ActivationServiceId { get; set; }
        public int DealerId { get; set; }
        public int ProductProcessTypeId { get; set; }
        public int PhysicalStatusId { get; set; }
    }
}
