﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobCancelMobileSubscription
    {
        public int JobCancelMobileSubscriptionId { get; set; }
        public int SubscriptionId { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string Header { get; set; }
        public string Body { get; set; }
    }
}
