﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobCancelSaleWithLeasingContract
    {
        public int JobCancelSaleWithLeasingContractId { get; set; }
        public int ActivationServiceId { get; set; }
        public int ProductProcessTypeId { get; set; }
    }
}
