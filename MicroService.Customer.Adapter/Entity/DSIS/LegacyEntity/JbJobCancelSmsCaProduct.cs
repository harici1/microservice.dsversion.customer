﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobCancelSmsCaProduct
    {
        public int JobCancelSmsCaProductId { get; set; }
        public int ActivationId { get; set; }
        public int ProductId { get; set; }
    }
}
