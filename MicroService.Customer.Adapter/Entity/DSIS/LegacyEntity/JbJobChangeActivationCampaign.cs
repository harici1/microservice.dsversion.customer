﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobChangeActivationCampaign
    {
        public int JobChangeActivationCampaignId { get; set; }
        public int LeasingContractServiceId { get; set; }
        public string Description { get; set; }
        public int ProductPriceId { get; set; }
        public int ToCampaignId { get; set; }
    }
}
