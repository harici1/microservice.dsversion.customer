﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobChargeMobileSubscription
    {
        public int JobChargeMobileSubscriptionId { get; set; }
        public int SubscriptionId { get; set; }
        public string MarjinalVariantId { get; set; }
        public string MarjinalTransId { get; set; }
        public string MarjinalAccountId { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string Header { get; set; }
        public string Body { get; set; }
    }
}
