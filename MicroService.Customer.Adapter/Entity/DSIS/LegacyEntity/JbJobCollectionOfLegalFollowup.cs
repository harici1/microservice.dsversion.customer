﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobCollectionOfLegalFollowup
    {
        public int JobCollectionOfLegalFollowupId { get; set; }
        public string LeasingContractNumber { get; set; }
        public DateTime ActivityDate { get; set; }
        public decimal Amount { get; set; }
        public int LawOfficeId { get; set; }
    }
}
