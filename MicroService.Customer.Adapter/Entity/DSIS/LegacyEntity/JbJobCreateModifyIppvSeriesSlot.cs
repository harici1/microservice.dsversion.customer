﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobCreateModifyIppvSeriesSlot
    {
        public int JobCreateModifyIppvSeriesSlotId { get; set; }
        public int SubscriberId { get; set; }
        public int SlotNumber { get; set; }
        public int SeriesId { get; set; }
        public int Balance { get; set; }
        public int Limit { get; set; }
        public int Threshold { get; set; }
        public int MaxIppvRecords { get; set; }
        public int BalanceReadjusted { get; set; }
    }
}
