﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobDefaultPriorityNumber
    {
        public int JobDefaultPriorityNumberId { get; set; }
        public string Name { get; set; }
        public int PriorityNumber { get; set; }
    }
}
