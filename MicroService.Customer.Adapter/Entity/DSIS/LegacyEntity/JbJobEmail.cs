﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobEmail
    {
        public int JobEmailId { get; set; }
        public string EmailFrom { get; set; }
        public string EmailFromName { get; set; }
        public string EmailTo { get; set; }
        public string Header { get; set; }
        public string Body { get; set; }
        public string AttachmentFileName { get; set; }
        public int? EmailTypeId { get; set; }
        public int JobEmailOperatorId { get; set; }
    }
}
