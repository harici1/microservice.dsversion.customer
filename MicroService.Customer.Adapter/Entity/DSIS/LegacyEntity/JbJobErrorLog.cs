﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobErrorLog
    {
        public int JobErrorLogId { get; set; }
        public int? JobTypeId { get; set; }
        public string ErrorText { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? DocumentId { get; set; }
        public DateTime? CreationDate { get; set; }
    }
}
