﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobExecutorIsimIcerikKarisikligiVarSil
    {
        public int JobExecutorId { get; set; }
        public string Name { get; set; }
    }
}
