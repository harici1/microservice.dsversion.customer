﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobFingerPrint
    {
        public int JobFingerPrintId { get; set; }
        public string AddressType { get; set; }
        public int? SubscriberId { get; set; }
        public string Address { get; set; }
        public int CharacterSize { get; set; }
        public int CharacterColor { get; set; }
        public int RegionHeight { get; set; }
        public int RegionWidth { get; set; }
        public int RegionColor { get; set; }
        public int XstartOffsetOfRegion { get; set; }
        public int YstartOffsetOfRegion { get; set; }
        public int XstartOffsetOfText { get; set; }
        public int YstartOffsetOfText { get; set; }
        public int Duration { get; set; }
        public int ChannelNumber { get; set; }
    }
}
