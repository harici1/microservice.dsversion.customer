﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobIphonePushNotification
    {
        public int JobIphonePushNotificationId { get; set; }
        public string DeviceToken { get; set; }
        public string PushMessage { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string Udid { get; set; }
    }
}
