﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobLegitimateProceeding
    {
        public int JobLegitimateProceedingId { get; set; }
        public int CustomerId { get; set; }
        public int LawyerId { get; set; }
        public int? LeasingContractId { get; set; }
    }
}
