﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobOffer
    {
        public int JobOfferId { get; set; }
        public int OfferTypeId { get; set; }
        public DateTime StartDate { get; set; }
        public int PartyId { get; set; }
    }
}
