﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobOsd
    {
        public int JobOsdid { get; set; }
        public string AddressType { get; set; }
        public int? SubscriberId { get; set; }
        public string Address { get; set; }
        public int DurationOfDisplay { get; set; }
        public string Message { get; set; }
    }
}
