﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobPersonalBit
    {
        public int JobPersonalBitId { get; set; }
        public int ActivationId { get; set; }
        public int PersonalBitId { get; set; }
        public bool IsSet { get; set; }
    }
}
