﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobPostpaidInvoicePayment
    {
        public int JobPostpaidInvoicePaymentId { get; set; }
        public int InvoiceId { get; set; }
    }
}
