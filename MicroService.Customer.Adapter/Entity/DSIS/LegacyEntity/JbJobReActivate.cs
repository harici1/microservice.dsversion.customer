﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobReActivate
    {
        public int JobReActivateId { get; set; }
        public int ActivationId { get; set; }
        public int? DocumentId { get; set; }
        public int? ProductProcessTypeId { get; set; }
    }
}
