﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobReactivationForDebit
    {
        public int JobReactivationForDebitId { get; set; }
        public int ActivationServiceId { get; set; }
        public int ProductProcessTypeId { get; set; }
        public string Note { get; set; }
    }
}
