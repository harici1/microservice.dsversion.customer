﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobRefreshPpvsignal
    {
        public int JobRefreshPpvsignalId { get; set; }
        public int PpvActivationId { get; set; }
    }
}
