﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobResendAllPackages
    {
        public int JobResendAllPackagesId { get; set; }
        public int ActivationId { get; set; }
    }
}
