﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobResetCustomerContract
    {
        public int JobResetCustomerContractId { get; set; }
        public int ActivationId { get; set; }
        public int ActivationServiceId { get; set; }
        public int CampaignId { get; set; }
        public int MenuId { get; set; }
        public int ProductId { get; set; }
    }
}
