﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobResetServiceBlocking
    {
        public int JobResetServiceBlockingId { get; set; }
        public int NdsSubscriberId { get; set; }
        public DateTime DeleteDate { get; set; }
    }
}
