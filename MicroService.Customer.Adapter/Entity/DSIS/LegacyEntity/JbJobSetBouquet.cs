﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobSetBouquet
    {
        public int JobSetBouquetId { get; set; }
        public int ActivationId { get; set; }
        public int BouquetId { get; set; }
    }
}
