﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobShipmentOrder
    {
        public int JobShipmentOrderId { get; set; }
        public int ActivationId { get; set; }
    }
}
