﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobSms
    {
        public int JobSmsId { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string Header { get; set; }
        public string Body { get; set; }
        public int? JobSmsTypeId { get; set; }
        public int? JobSmsOperatorId { get; set; }
        public string Tckno { get; set; }
    }
}
