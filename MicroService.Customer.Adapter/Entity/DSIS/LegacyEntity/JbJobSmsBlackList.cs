﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobSmsBlackList
    {
        public int JobSmsBlackListId { get; set; }
        public string MobilePhoneNumber { get; set; }
    }
}
