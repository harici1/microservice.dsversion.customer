﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobStatus
    {
        public int JobStatusId { get; set; }
        public string Name { get; set; }
    }
}
