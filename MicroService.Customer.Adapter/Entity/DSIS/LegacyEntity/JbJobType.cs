﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobType
    {
        public int JobTypeId { get; set; }
        public string Name { get; set; }
        public int JobExecutorId { get; set; }
        public string DefaultBatchName { get; set; }
        public string JobSubTable { get; set; }
    }
}
