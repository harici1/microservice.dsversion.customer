﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobUpdateExpiredDirectDebitInfo
    {
        public int JbJobUpdateExpiredDirectDebitInfoId { get; set; }
        public int LeasingContractId { get; set; }
        public int ProductProcessTypeId { get; set; }
    }
}
