﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class JbJobWorkOrderRedirect
    {
        public int JobWorkOrderRedirectId { get; set; }
        public int WorkOrderId { get; set; }
        public int WorkOrderLogTypeRuleId { get; set; }
        public string Note { get; set; }
        public int WorkOrderSubjectCode { get; set; }
        public int? DealerId { get; set; }
        public int? LogTypeReasonId { get; set; }
        public int? FromWorkOrderLogTypeId { get; set; }
        public int? ToWorkOrderLogTypeId { get; set; }
    }
}
