﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class OccParameter
    {
        public OccParameter()
        {
            OccRequestParameter = new HashSet<OccRequestParameter>();
        }

        public int ParameterId { get; set; }
        public string Description { get; set; }

        public virtual ICollection<OccRequestParameter> OccRequestParameter { get; set; }
    }
}
