﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class OccPrice
    {
        public OccPrice()
        {
            OccReasonPrice = new HashSet<OccReasonPrice>();
        }

        public int PriceId { get; set; }
        public int? ParentPriceId { get; set; }
        public string Title { get; set; }
        public decimal Amount { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateUser { get; set; }
        public DateTime? ValidFor { get; set; }

        public virtual ICollection<OccReasonPrice> OccReasonPrice { get; set; }
    }
}
