﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class OccPriceDetailTypeValue
    {
        public int PriceDetailTypeValueId { get; set; }
        public int PriceId { get; set; }
        public string TypeName { get; set; }
        public string TypeValue { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateUser { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public DateTime? LastUpdateUser { get; set; }
        public DateTime? ValidFor { get; set; }
    }
}
