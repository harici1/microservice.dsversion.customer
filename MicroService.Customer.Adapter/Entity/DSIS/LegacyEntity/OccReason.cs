﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class OccReason
    {
        public OccReason()
        {
            OccReasonPrice = new HashSet<OccReasonPrice>();
            OccRequest = new HashSet<OccRequest>();
        }

        public int ReasonId { get; set; }
        public string Title { get; set; }

        public virtual ICollection<OccReasonPrice> OccReasonPrice { get; set; }
        public virtual ICollection<OccRequest> OccRequest { get; set; }
    }
}
