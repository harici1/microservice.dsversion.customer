﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class OccReasonPrice
    {
        public int ReasonPriceId { get; set; }
        public int ReasonId { get; set; }
        public int PriceId { get; set; }

        public virtual OccPrice Price { get; set; }
        public virtual OccReason Reason { get; set; }
    }
}
