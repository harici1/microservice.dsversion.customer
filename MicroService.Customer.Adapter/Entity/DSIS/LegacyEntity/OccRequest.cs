﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class OccRequest
    {
        public OccRequest()
        {
            OccRequestParameter = new HashSet<OccRequestParameter>();
            OccService = new HashSet<OccService>();
        }

        public int RequestId { get; set; }
        public int ReasonId { get; set; }
        public DateTime RequestDate { get; set; }
        public DateTime? ProcessDate { get; set; }
        public string BusinessId { get; set; }

        public virtual OccReason Reason { get; set; }
        public virtual ICollection<OccRequestParameter> OccRequestParameter { get; set; }
        public virtual ICollection<OccService> OccService { get; set; }
    }
}
