﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class OccRequestParameter
    {
        public int RequestParameterId { get; set; }
        public int RequestId { get; set; }
        public int ParameterId { get; set; }
        public string ParameterValue { get; set; }

        public virtual OccParameter Parameter { get; set; }
        public virtual OccRequest Request { get; set; }
    }
}
