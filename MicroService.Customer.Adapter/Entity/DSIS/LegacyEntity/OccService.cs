﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class OccService
    {
        public int ServiceId { get; set; }
        public int RequestId { get; set; }
        public string Title { get; set; }
        public decimal? Amount { get; set; }

        public virtual OccRequest Request { get; set; }
    }
}
