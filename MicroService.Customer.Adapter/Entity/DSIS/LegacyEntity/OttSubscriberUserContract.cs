﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class OttSubscriberUserContract
    {
        public int OttSubscriberUserContractId { get; set; }
        public int OttSubscriberUserId { get; set; }
        public int? LeasingContractId { get; set; }
        public bool Active { get; set; }
        public DateTime CreationDate { get; set; }
        public int? SmartCardId { get; set; }
        public string ExternalCustomerAccountCode { get; set; }
    }
}
