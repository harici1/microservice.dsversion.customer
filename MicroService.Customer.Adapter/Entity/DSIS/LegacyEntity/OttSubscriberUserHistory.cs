﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class OttSubscriberUserHistory
    {
        public int OttSubscriberUserHistoryId { get; set; }
        public int OttSubscriberUserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int Status { get; set; }
        public DateTime CreationDate { get; set; }
        public string OttSubscriberUserClientId { get; set; }
        public string TriggerUserName { get; set; }
        public string TriggerHostName { get; set; }
        public DateTime TriggerDate { get; set; }
        public string TriggerType { get; set; }
    }
}
