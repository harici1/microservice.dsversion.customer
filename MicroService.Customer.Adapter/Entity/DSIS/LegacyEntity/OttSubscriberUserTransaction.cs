﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class OttSubscriberUserTransaction
    {
        public string OttSubscriberUserTransactionId { get; set; }
        public int? OttSubscriberUserTransactionOttSubscriberUserId { get; set; }
        public string OttSubscriberUserTransactionRequestJson { get; set; }
        public string OttSubscriberUserTransactionResponseJson { get; set; }
        public string OttSubscriberUserTransactionType { get; set; }
        public DateTime OttSubscriberUserTransactionRequestDate { get; set; }
        public DateTime? OttSubscriberUserTransactionResponseDate { get; set; }
        public string OttSubscriberUserTransactionClientTransactionId { get; set; }
    }
}
