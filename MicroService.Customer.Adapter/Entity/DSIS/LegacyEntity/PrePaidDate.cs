﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class PrePaidDate
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int BatchId { get; set; }
    }
}
