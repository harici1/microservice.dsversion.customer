﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class SapDailyTransferSayim
    {
        public int AktarimId { get; set; }
        public int StbTransactionId { get; set; }
        public string SirketKodu { get; set; }
        public string UrunDurumu { get; set; }
        public string IslemTipi { get; set; }
        public string Malzeme { get; set; }
        public string SerialNumber { get; set; }
        public string Adet { get; set; }
        public string OlcuBirimi { get; set; }
        public DateTime IslemTarihi { get; set; }
        public int PartySira { get; set; }
        public string SmartKart { get; set; }
        public string AboneNo { get; set; }
        public string Musteri { get; set; }
        public DateTime CreationDate { get; set; }
        public bool TopluAktarim { get; set; }
        public int? DealerId { get; set; }
        public int? StbId { get; set; }
        public int? StbskuId { get; set; }
    }
}
