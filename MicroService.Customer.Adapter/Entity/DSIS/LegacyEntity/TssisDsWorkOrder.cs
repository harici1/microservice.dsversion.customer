﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class TssisDsWorkOrder
    {
        public int WorkOrderId { get; set; }
        public int WorkOrderLogTypeId { get; set; }
        public int WorkOrderSubjectId { get; set; }
        public int? OwnerId { get; set; }
        public int? RoleId { get; set; }
        public int? SubjectPartyId { get; set; }
        public int WorkOrderStatusId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public int LastUpdatedById { get; set; }
        public string Note { get; set; }
        public int? CampaignId { get; set; }
        public DateTime? ReservationDate { get; set; }
        public int? RedirectedDealerId { get; set; }
        public DateTime? Startdate { get; set; }
        public DateTime? Enddate { get; set; }
        public string SmsopNote { get; set; }
        public bool IsPushed { get; set; }
        public bool IsInvoiced { get; set; }
        public bool? TemIsOk { get; set; }
        public int? IsBlu { get; set; }
        public int? DocumentId { get; set; }
        public int? DocumentTypeId { get; set; }
        public int ContactChannelId { get; set; }
        public Guid UniqueId { get; set; }

        public virtual DsParty SubjectParty { get; set; }
        public virtual DsWorkOrderLogType WorkOrderLogType { get; set; }
    }
}
