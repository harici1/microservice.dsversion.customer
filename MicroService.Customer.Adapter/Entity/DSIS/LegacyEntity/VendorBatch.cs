﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorBatch
    {
        public int VendorBatchId { get; set; }
        public string Name { get; set; }
        public int VendorBatchTypeId { get; set; }
        public int? ProcessedItemCount { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }

        public virtual VendorBatchType VendorBatchType { get; set; }
    }
}
