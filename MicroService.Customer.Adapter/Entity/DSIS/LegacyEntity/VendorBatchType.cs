﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorBatchType
    {
        public VendorBatchType()
        {
            VendorBatch = new HashSet<VendorBatch>();
        }

        public int VendorBatchTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<VendorBatch> VendorBatch { get; set; }
    }
}
