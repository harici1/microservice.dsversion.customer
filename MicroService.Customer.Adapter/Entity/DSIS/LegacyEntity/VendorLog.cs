﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorLog
    {
        public int VendorLogId { get; set; }
        public int VendorLogTypeId { get; set; }
        public string XmlMessage { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
