﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorLogType
    {
        public int VendorLogTypeId { get; set; }
        public string Name { get; set; }
    }
}
