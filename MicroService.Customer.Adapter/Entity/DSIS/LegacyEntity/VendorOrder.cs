﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorOrder
    {
        public int VendorOrderId { get; set; }
        public int? SourceId { get; set; }
        public int VendorId { get; set; }
        public int VendorInvoicePartyId { get; set; }
        public int VendorShipPartyId { get; set; }
        public DateTime OrderDate { get; set; }
        public string ShipClientCode { get; set; }
        public string InvoiceClientCode { get; set; }
        public string ShipClientName { get; set; }
        public string ShipClientAddress { get; set; }
        public string ShipClientDistrict { get; set; }
        public string ShipClientProvince { get; set; }
        public string InvoiceAddress { get; set; }
        public string ShipClientPlateCode { get; set; }
        public DateTime CreationDate { get; set; }
        public int VendorOrderStatusId { get; set; }
        public int? VendorBatchId { get; set; }
        public int VendorOrderTypeId { get; set; }
        public int? VendorOrderCommercialTypeId { get; set; }
        public bool? ShipAtWareHouse { get; set; }
        public int? SentByVendorBatchId { get; set; }
        public int? TempOrderId { get; set; }
        public bool? SerialNumberIsOk { get; set; }
        public int? SendingPriority { get; set; }
        public int? SerialNumberByVendorBatchId { get; set; }
        public string WayBillSerialNumber { get; set; }
        public DateTime? WayBillDate { get; set; }
        public int InvoicePromptDay { get; set; }
        public bool IsSentNotMarked { get; set; }
        public string VendorPrintTypeCode { get; set; }
        public int? CommercialTypeReasonId { get; set; }
        public string Note { get; set; }
        public bool SerialNumberIsOkTemp { get; set; }
        public int? CreatedById { get; set; }
        public int? ReturnStbDocumentTypeId { get; set; }
        public string ReturnStbDocumentNo { get; set; }
        public int? WarehousePartyId { get; set; }
        public string ShelfNo { get; set; }
        public string PaletteNo { get; set; }
        public string PackageNo { get; set; }
        public string ReferenceCode { get; set; }
    }
}
