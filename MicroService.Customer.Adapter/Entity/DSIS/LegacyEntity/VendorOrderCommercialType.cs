﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorOrderCommercialType
    {
        public VendorOrderCommercialType()
        {
            VendorOrderCommercialTypeReasonRule = new HashSet<VendorOrderCommercialTypeReasonRule>();
        }

        public int VendorOrderCommercialTypeId { get; set; }
        public int? VendorId { get; set; }
        public string Name { get; set; }
        public bool? IsUserSale { get; set; }
        public bool IncludeInvoice { get; set; }
        public int? DefaultStbUsageTypeId { get; set; }
        public bool WillInvoice { get; set; }
        public bool GetSerialNumber { get; set; }
        public bool GetWayBilllNumber { get; set; }
        public bool? IsSendProductOrder { get; set; }
        public bool IsSendProcess { get; set; }
        public int? VendorOrderTypeId { get; set; }

        public virtual ICollection<VendorOrderCommercialTypeReasonRule> VendorOrderCommercialTypeReasonRule { get; set; }
    }
}
