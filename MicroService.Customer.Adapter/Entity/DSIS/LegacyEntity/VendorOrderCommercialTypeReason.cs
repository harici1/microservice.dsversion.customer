﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorOrderCommercialTypeReason
    {
        public VendorOrderCommercialTypeReason()
        {
            VendorOrderCommercialTypeReasonRule = new HashSet<VendorOrderCommercialTypeReasonRule>();
        }

        public int VendorOrderCommercialTypeReasonId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<VendorOrderCommercialTypeReasonRule> VendorOrderCommercialTypeReasonRule { get; set; }
    }
}
