﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorOrderCommercialTypeReasonRule
    {
        public int VendorOrderCommercialTypeReasonRuleId { get; set; }
        public int VendorOrderCommercialTypeId { get; set; }
        public int VendorOrderCommercialTypeReasonId { get; set; }

        public virtual VendorOrderCommercialType VendorOrderCommercialType { get; set; }
        public virtual VendorOrderCommercialTypeReason VendorOrderCommercialTypeReason { get; set; }
    }
}
