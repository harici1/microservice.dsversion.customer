﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorOrderCommercialTypeToHdd
    {
        public int VendorOrderCommercialTypeToHddId { get; set; }
        public int VendorOrderCommercialTypeId { get; set; }
        public int VendorProductId { get; set; }
        public int? DefaultOwnerId { get; set; }
    }
}
