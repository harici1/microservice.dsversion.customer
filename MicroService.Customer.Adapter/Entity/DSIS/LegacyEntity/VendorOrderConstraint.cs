﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorOrderConstraint
    {
        public int VendorOrderConstraintId { get; set; }
        public int ActivationSalesModelId { get; set; }
        public int CustomerTypeId { get; set; }
        public int? CampaignId { get; set; }
        public int InvoicePartyId { get; set; }
        public bool IsActive { get; set; }
        public DateTime ActivationStartDate { get; set; }
        public DateTime ActivationEndDate { get; set; }

        public virtual DsCustomerType CustomerType { get; set; }
    }
}
