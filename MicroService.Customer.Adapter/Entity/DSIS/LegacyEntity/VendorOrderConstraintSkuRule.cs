﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorOrderConstraintSkuRule
    {
        public int VendorOrderConstraintSkuRuleId { get; set; }
        public int FromStbSkuId { get; set; }
        public int ToStbSkuId { get; set; }

        public virtual DsStbSku FromStbSku { get; set; }
        public virtual DsStbSku ToStbSku { get; set; }
    }
}
