﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorOrderExtraInfoDetail
    {
        public int VendorOrderExtraInfoDetailId { get; set; }
        public int VendorProductId { get; set; }
        public int VendorOrderCommercialTypeId { get; set; }
        public string ExtraInfoDetail { get; set; }
    }
}
