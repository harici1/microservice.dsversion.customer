﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorOrderLine
    {
        public VendorOrderLine()
        {
            VendorOrderLineToOrder = new HashSet<VendorOrderLineToOrder>();
        }

        public int VendorOrderLineId { get; set; }
        public int VendorOrderId { get; set; }
        public int VendorProductId { get; set; }
        public int NumberOfProduct { get; set; }
        public int? InvoicedProductCount { get; set; }
        public int? ReturnedProductCount { get; set; }
        public decimal? InvoiceNetAmount { get; set; }
        public decimal? InvoiceAmount { get; set; }
        public decimal? TaxRate { get; set; }
        public decimal? TaxAmount { get; set; }
        public bool IsOk { get; set; }
        public string ExtraInfoDetail { get; set; }
        public string ReferenceCode { get; set; }
        public string ReferenceCodeDetail { get; set; }

        public virtual ICollection<VendorOrderLineToOrder> VendorOrderLineToOrder { get; set; }
    }
}
