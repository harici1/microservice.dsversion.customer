﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorOrderLineRelation
    {
        public int VendorOrderLineRelationId { get; set; }
        public int VendorOrderLineId { get; set; }
        public int RelatedVendorOrderLineId { get; set; }
        public int RelationTypeId { get; set; }
    }
}
