﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorOrderLineToOrder
    {
        public int VendorOrderLineToOrderId { get; set; }
        public int VendorOrderLineId { get; set; }
        public int OrderId { get; set; }
        public string Temp { get; set; }
        public bool? Stbprocess { get; set; }
        public bool? ExternalEquipmentProcess { get; set; }
        public bool? ModemProcess { get; set; }

        public virtual VendorOrderLine VendorOrderLine { get; set; }
    }
}
