﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorOrderLineToSerialNumberForInput
    {
        public int VendorOrderLineToSerialNumberForInputId { get; set; }
        public int VendorOrderLineId { get; set; }
        public string VendorProductCode { get; set; }
        public int VendorProductId { get; set; }
        public string SerialNumber { get; set; }
        public int? StbId { get; set; }
        public int? HdPvrHddId { get; set; }
        public string Message { get; set; }
        public DateTime CreationDate { get; set; }
        public int? ModemId { get; set; }
    }
}
