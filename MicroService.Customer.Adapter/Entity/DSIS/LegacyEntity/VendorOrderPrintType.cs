﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorOrderPrintType
    {
        public int VendorOrderPrintTypeId { get; set; }
        public string Name { get; set; }
        public string VendorPrintTypeCode { get; set; }
        public bool VisibleForReturnStb { get; set; }
    }
}
