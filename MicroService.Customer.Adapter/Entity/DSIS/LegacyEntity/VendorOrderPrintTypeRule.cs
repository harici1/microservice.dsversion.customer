﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorOrderPrintTypeRule
    {
        public int VendorOrderPrintTypeRuleId { get; set; }
        public int VendorOrderCommercialTypeId { get; set; }
        public int VendorOrderPrintTypeId { get; set; }
    }
}
