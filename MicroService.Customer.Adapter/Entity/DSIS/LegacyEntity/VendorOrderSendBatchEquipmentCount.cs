﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorOrderSendBatchEquipmentCount
    {
        public int VendorOrderSendBatchEquipmentId { get; set; }
        public int StbTypeId { get; set; }
        public int VendorOrderSendBatchEquipmentCount1 { get; set; }
    }
}
