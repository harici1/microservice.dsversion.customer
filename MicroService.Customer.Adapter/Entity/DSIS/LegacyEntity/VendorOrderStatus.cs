﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorOrderStatus
    {
        public int VendorOrderStatusId { get; set; }
        public string Name { get; set; }
    }
}
