﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorOrderType
    {
        public int VendorOrderTypeId { get; set; }
        public string Name { get; set; }
    }
}
