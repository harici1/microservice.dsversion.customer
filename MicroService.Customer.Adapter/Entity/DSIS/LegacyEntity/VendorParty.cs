﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorParty
    {
        public VendorParty()
        {
            VendorProduct = new HashSet<VendorProduct>();
        }

        public int VendorPartyId { get; set; }
        public int VendorPartyTypeId { get; set; }
        public int VendorId { get; set; }
        public int PartyId { get; set; }
        public string VendorPartyCode { get; set; }
        public int VendorPartyStatusId { get; set; }
        public DateTime CreationDate { get; set; }
        public int? SystemCode { get; set; }
        public int? TempId { get; set; }

        public virtual DsBayi Party { get; set; }
        public virtual VendorVendor Vendor { get; set; }
        public virtual VendorPartyStatus VendorPartyStatus { get; set; }
        public virtual VendorPartyType VendorPartyType { get; set; }
        public virtual ICollection<VendorProduct> VendorProduct { get; set; }
    }
}
