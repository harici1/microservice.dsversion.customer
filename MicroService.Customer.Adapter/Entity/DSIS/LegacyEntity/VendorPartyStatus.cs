﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorPartyStatus
    {
        public VendorPartyStatus()
        {
            VendorParty = new HashSet<VendorParty>();
        }

        public int VendorPartyStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<VendorParty> VendorParty { get; set; }
    }
}
