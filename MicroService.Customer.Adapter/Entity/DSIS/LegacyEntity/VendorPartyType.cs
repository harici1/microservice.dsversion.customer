﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorPartyType
    {
        public VendorPartyType()
        {
            VendorParty = new HashSet<VendorParty>();
        }

        public int VendorPartyTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<VendorParty> VendorParty { get; set; }
    }
}
