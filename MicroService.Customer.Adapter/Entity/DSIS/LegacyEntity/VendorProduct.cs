﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorProduct
    {
        public VendorProduct()
        {
            VendorProductStock = new HashSet<VendorProductStock>();
        }

        public int VendorProductId { get; set; }
        public string Name { get; set; }
        public string ExtraInfoDetail { get; set; }
        public string VendorProductCode { get; set; }
        public int? InvoiceItemId { get; set; }
        public string BarcodeNumber { get; set; }
        public int VendorProductStatusId { get; set; }
        public int SortColumn { get; set; }
        public bool IsVirtualProduct { get; set; }
        public int? VendorPartyId { get; set; }
        public bool IsUsedForReplace { get; set; }
        public int VendorProductTypeId { get; set; }
        public int? RelatedVendorProductId { get; set; }
        public bool IsNewProduct { get; set; }
        public int DefaultStbUsageTypeId { get; set; }
        public int? StbBrandId { get; set; }
        public int? DefaultStbSkuId { get; set; }
        public int? SendingPriority { get; set; }
        public bool IsRefurbishment { get; set; }
        public bool IsExtraInfoDetail { get; set; }
        public int SerialReadType { get; set; }
        public int? SecondVendorProductId { get; set; }
        public bool? IsSecondHand { get; set; }

        public virtual DsInvoiceItem InvoiceItem { get; set; }
        public virtual VendorParty VendorParty { get; set; }
        public virtual VendorProductStatus VendorProductStatus { get; set; }
        public virtual VendorProductType VendorProductType { get; set; }
        public virtual ICollection<VendorProductStock> VendorProductStock { get; set; }
    }
}
