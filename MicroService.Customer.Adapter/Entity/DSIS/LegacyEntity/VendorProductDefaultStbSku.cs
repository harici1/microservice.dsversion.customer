﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorProductDefaultStbSku
    {
        public int Id { get; set; }
        public int StbBrandId { get; set; }
        public int StbModelId { get; set; }
        public int StbSkuId { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
    }
}
