﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorProductStatus
    {
        public VendorProductStatus()
        {
            VendorProduct = new HashSet<VendorProduct>();
        }

        public int VendorProductStatusId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<VendorProduct> VendorProduct { get; set; }
    }
}
