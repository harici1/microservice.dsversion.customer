﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorProductStock
    {
        public int VendorProductStockId { get; set; }
        public int VendorId { get; set; }
        public int VendorProductId { get; set; }
        public int ProductBalance { get; set; }
        public int ProductActivationCount { get; set; }
        public int ProductBufferCount { get; set; }

        public virtual VendorVendor Vendor { get; set; }
        public virtual VendorProduct VendorProduct { get; set; }
    }
}
