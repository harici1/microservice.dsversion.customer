﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorProductType
    {
        public VendorProductType()
        {
            VendorProduct = new HashSet<VendorProduct>();
        }

        public int VendorProductTypeId { get; set; }
        public string Name { get; set; }
        public bool VisibleToReturnScreen { get; set; }

        public virtual ICollection<VendorProduct> VendorProduct { get; set; }
    }
}
