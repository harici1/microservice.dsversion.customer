﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class VendorVendor
    {
        public VendorVendor()
        {
            VendorParty = new HashSet<VendorParty>();
            VendorProductStock = new HashSet<VendorProductStock>();
        }

        public int VendorVendorId { get; set; }
        public string Name { get; set; }
        public int CorporationId { get; set; }
        public int PartyId { get; set; }
        public string VendorUserCode { get; set; }
        public short Status { get; set; }

        public virtual ICollection<VendorParty> VendorParty { get; set; }
        public virtual ICollection<VendorProductStock> VendorProductStock { get; set; }
    }
}
