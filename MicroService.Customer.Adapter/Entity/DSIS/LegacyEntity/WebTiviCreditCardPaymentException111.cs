﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class WebTiviCreditCardPaymentException111
    {
        public int CreditCardPaymentExceptionId { get; set; }
        public int StatusId { get; set; }
        public int? VposId { get; set; }
        public int PartyId { get; set; }
        public int? OrderTypeId { get; set; }
        public int ProductId { get; set; }
        public decimal Amount { get; set; }
        public int ContactChannelId { get; set; }
        public string CardNumber { get; set; }
        public string Bname { get; set; }
        public string OrderNumber { get; set; }
        public string ReferenceNumber { get; set; }
        public string ExceptionMessage { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
