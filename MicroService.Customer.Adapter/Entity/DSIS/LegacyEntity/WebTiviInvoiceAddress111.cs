﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class WebTiviInvoiceAddress111
    {
        public int AddressId { get; set; }
        public int UserId { get; set; }
        public string InvoiceAddress { get; set; }
        public int NeighbourhoodId { get; set; }
        public string FullName { get; set; }
        public string Ssn { get; set; }
    }
}
