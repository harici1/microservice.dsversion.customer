﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class WebTiviOrder111
    {
        public int OrderId { get; set; }
        public int UserId { get; set; }
        public int ProductId { get; set; }
        public decimal Amount { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public string ProductName { get; set; }
        public string UserCode { get; set; }
        public string Password { get; set; }
    }
}
