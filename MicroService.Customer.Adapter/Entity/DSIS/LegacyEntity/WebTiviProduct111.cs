﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class WebTiviProduct111
    {
        public int ProductId { get; set; }
        public int? PoolId { get; set; }
        public int VposId { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string ImageUrl { get; set; }
        public DateTime ExpirationDate { get; set; }
        public DateTime? EventDate { get; set; }
    }
}
