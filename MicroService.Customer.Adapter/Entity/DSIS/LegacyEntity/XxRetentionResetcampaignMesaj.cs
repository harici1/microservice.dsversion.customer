﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class XxRetentionResetcampaignMesaj
    {
        public int Id { get; set; }
        public string TestGrubu { get; set; }
        public double? SmartKart { get; set; }
        public string SonPaket { get; set; }
        public string TaahhütlüTeklifKampanya { get; set; }
        public double? TaahhütlüTeklif { get; set; }
        public double? TaahhütsüzFiyat { get; set; }
        public string Mesaj { get; set; }
        public double? MüşteriId { get; set; }
        public string ÜyelikNumarası { get; set; }
        public int? ActivationId { get; set; }
        public DateTime? CreationDate { get; set; }
    }
}
