﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class ZzRegularAccountAgreement
    {
        public string AbonelikTipi { get; set; }
        public int? Dsisid { get; set; }
        public int? DsishesapNo { get; set; }
        public int? AnkaHesapNo { get; set; }
        public DateTime ContractStartDate { get; set; }
        public DateTime? ContractEndDate { get; set; }
        public DateTime CommitmentStartDate { get; set; }
        public DateTime? CommitmentEndDate { get; set; }
        public int? Commitment { get; set; }
        public string HesapStatu { get; set; }
        public string ServisStatu { get; set; }
        public string ServiceType { get; set; }
        public int? CampaignId { get; set; }
        public string Name { get; set; }
        public decimal? NetPrice { get; set; }
        public decimal? Price { get; set; }
        public decimal? Kdvorani { get; set; }
        public decimal? Oİvorani { get; set; }
        public decimal? NetBonus { get; set; }
        public decimal? Bonus { get; set; }
        public DateTime? BonusStartDate { get; set; }
        public DateTime? BonusEndDate { get; set; }
        public string Paket { get; set; }
        public int Id { get; set; }
    }
}
