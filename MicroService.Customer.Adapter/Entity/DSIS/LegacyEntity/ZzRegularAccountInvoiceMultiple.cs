﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class ZzRegularAccountInvoiceMultiple
    {
        public int? LeasingContractId { get; set; }
        public int? CorporationId { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string Description { get; set; }
        public decimal NetAmount { get; set; }
        public decimal TaxAmount1 { get; set; }
        public decimal TaxAmount2 { get; set; }
        public decimal Amount { get; set; }
        public DateTime? PaymentPeriodStart { get; set; }
        public DateTime? PaymentPeriodEnd { get; set; }
        public int Id { get; set; }
    }
}
