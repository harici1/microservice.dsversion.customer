﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class ZzRegularAccountPoolLogs
    {
        public int Id { get; set; }
        public int LeasingContractId { get; set; }
        public string Description { get; set; }
    }
}
