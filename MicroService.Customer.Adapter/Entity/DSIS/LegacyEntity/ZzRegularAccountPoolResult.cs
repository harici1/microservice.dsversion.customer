﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.LegacyEntity
{
    public partial class ZzRegularAccountPoolResult
    {
        public int Id { get; set; }
        public int HesapNo { get; set; }
        public int? FirmaId { get; set; }
        public string Aciklama { get; set; }
        public string ServiseTipi { get; set; }
        public DateTime? GelirDonemi { get; set; }
        public decimal? NetFaturaTutari { get; set; }
        public decimal? KdvTutari { get; set; }
        public decimal? OivTutari { get; set; }
        public decimal? ToplamTutarFatura { get; set; }
        public decimal? NetIndirimTutari { get; set; }
        public decimal? IndirimKdvTutari { get; set; }
        public decimal? IndirimOivTutari { get; set; }
        public decimal? ToplamIndirimTutariFatura { get; set; }
        public decimal? SozlesmeTutari { get; set; }
        public decimal? NetSozlesmeTutari { get; set; }
        public decimal? SozlesmeTutariIndirimi { get; set; }
        public decimal? NetSozlesmeTutariIndirimi { get; set; }
        public decimal? VergiliFiyatOrani { get; set; }
        public decimal? NetFiyatOrani { get; set; }
        public decimal? UfrsGelir { get; set; }
        public decimal? NetUfrsGelir { get; set; }
    }
}
