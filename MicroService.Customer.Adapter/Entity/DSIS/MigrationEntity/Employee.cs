﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.MigrationEntity
{
    public partial class Employee
    {
        public int LogId { get; set; }
        public DateTime LogDate { get; set; }
        public string LogUser { get; set; }
        public string LogHostName { get; set; }
        public string LogType { get; set; }
        public int? MigrationStatus { get; set; }
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string FamilyName { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreationDate { get; set; }
        public int? LastUpdatedById { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public int? EmployeeTypeId { get; set; }
    }
}
