﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.MigrationEntity
{
    public partial class Musteri
    {
        public int LogId { get; set; }
        public DateTime LogDate { get; set; }
        public string LogUser { get; set; }
        public string LogHostName { get; set; }
        public string LogType { get; set; }
        public int? MigrationStatus { get; set; }
        public int MusteriId { get; set; }
        public string Adi { get; set; }
        public string Soyadi { get; set; }
        public string KimlikNo { get; set; }
        public string KimlikTur { get; set; }
        public string TckimlikNo { get; set; }
        public string AnaAdi { get; set; }
        public string BabaAdi { get; set; }
        public string DogumYeri { get; set; }
        public DateTime? DogumTarihi { get; set; }
        public string Cinsiyet { get; set; }
        public string Mahalle { get; set; }
        public string Cadde { get; set; }
        public string Sokak { get; set; }
        public string Bina { get; set; }
        public string Site { get; set; }
        public string BlokNo { get; set; }
        public string BinaNo { get; set; }
        public string Kat { get; set; }
        public string Daire { get; set; }
        public string Semt { get; set; }
        public string IlceAdi { get; set; }
        public string EvTelefon { get; set; }
        public string IsTelefon { get; set; }
        public string CepTelefon { get; set; }
        public string Faks { get; set; }
        public string Eposta { get; set; }
        public string MedeniHali { get; set; }
        public int? CocukSayisi { get; set; }
        public string BaskaSmartCard { get; set; }
        public int? OlusturanId { get; set; }
        public DateTime? OlusturmaTarihi { get; set; }
        public int? DuzeltenId { get; set; }
        public DateTime? DuzeltmeTarih { get; set; }
        public string PostaKodu { get; set; }
        public int CustomerTypeId { get; set; }
        public int? FootballTeamId { get; set; }
        public int? DistrictId { get; set; }
        public int? ProvinceId { get; set; }
        public int? ProfessionId { get; set; }
        public int? EducationLevelId { get; set; }
        public bool? IsAuthorized { get; set; }
        public int? RegisteredProvinceId { get; set; }
        public string RegisteredDistrictName { get; set; }
        public string Address { get; set; }
        public string CustomerCode { get; set; }
        public int? CommercialCustomerScopeId { get; set; }
        public string CommercialCustomerName { get; set; }
        public int? AdditionalDisplayCount { get; set; }
        public string Description { get; set; }
        public int? GsmserviceCodeId { get; set; }
        public int? GsmsubscriptionTypeId { get; set; }
        public int? NumberOfBranches { get; set; }
        public int? CommercialWorkingPeriodId { get; set; }
        public int? CommercialCapacityId { get; set; }
        public bool? IsAlcoholicBeverageServed { get; set; }
        public int? NeighbourhoodId { get; set; }
        public bool? CreatedAsPotantial { get; set; }
        public int? HomeAdditionalInfoId { get; set; }
        public int? PreviousProviderAdditionalInfoId { get; set; }
        public int? TvCount { get; set; }
        public bool? HdTv { get; set; }
        public int? IssAdditionalInfoId { get; set; }
        public int? InternetAdditionalInfoId { get; set; }
        public int? HouseholdCount { get; set; }
        public bool? Temp { get; set; }
        public string ExternalCustomerCode { get; set; }
        public string ExternalCustomerAccountCode { get; set; }
        public bool? GsmNoIsInvalid { get; set; }
        public bool? EmailIsInvalid { get; set; }
        public bool? LegitimateProceeding { get; set; }
        public int? AnkaValuesChecked { get; set; }
        public int? PotentialWorkOrderId { get; set; }
        public int? IdentificationTypeId { get; set; }
        public bool? CheckedFromMernis { get; set; }
        public string DoorNumber { get; set; }
        public string RegionName { get; set; }
        public Guid CustomerUid { get; set; }
        public bool CustomerType { get; set; }
        public bool Einvoicer { get; set; }
        public bool EinvoicerOk { get; set; }
        public int? NumberOfRoom { get; set; }
        public int? InternetConnectingDeviceId { get; set; }
    }
}
