﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.MigrationEntity
{
    public partial class MusteriPhone
    {
        public int LogId { get; set; }
        public DateTime LogDate { get; set; }
        public string LogUser { get; set; }
        public string LogHostName { get; set; }
        public string LogType { get; set; }
        public int? MigrationStatus { get; set; }
        public int MusteriPhoneId { get; set; }
        public int CustomerId { get; set; }
        public string Phone { get; set; }
        public int? PhoneTypeCode { get; set; }
        public bool? IsPrimary { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
