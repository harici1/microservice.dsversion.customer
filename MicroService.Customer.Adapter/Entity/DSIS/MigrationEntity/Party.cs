﻿using System;
using System.Collections.Generic;

namespace Microservice.Customer.Entity.DSIS.MigrationEntity
{
    public partial class Party
    {
        public int LogId { get; set; }
        public DateTime LogDate { get; set; }
        public string LogUser { get; set; }
        public string LogHostName { get; set; }
        public string LogType { get; set; }
        public int? MigrationStatus { get; set; }
        public int PartyId { get; set; }
        public int TypeId { get; set; }
        public string Name { get; set; }
        public int? ReferenceNumber { get; set; }
        public bool? IsActive { get; set; }
        public string SystemCode { get; set; }
        public string InvoiceTitle { get; set; }
        public string InvoiceTaxNumber { get; set; }
        public string InvoiceTaxOffice { get; set; }
        public string InvoiceAddress { get; set; }
        public int? InvoiceDistrictId { get; set; }
        public int? InvoiceSendingTypeId { get; set; }
        public int? NeighbourhoodId { get; set; }
        public string ZipCode { get; set; }
        public int? CountryId { get; set; }
        public int? ProvinceId { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public DateTime? CreationDate { get; set; }
        public bool Einvoice { get; set; }
        public bool? StoreCustomerInfo { get; set; }
        public bool? ShareCustomerInfo { get; set; }
        public bool? TaxFree { get; set; }
        public bool? IsEnvelopeRequested { get; set; }
    }
}
