﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Adapter.Entity.DSIS.MigrationEntity
{
    public partial class imParty
    {
        public int Id { get; set; }
        public string CitizenShipNumber { get; set; }
        public Guid StreamId { get; set; }
    }

}
