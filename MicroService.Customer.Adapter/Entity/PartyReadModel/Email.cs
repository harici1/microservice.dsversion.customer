﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microservice.Customer.Entity.PartyReadModel
{
    public class Email
    {
        //public Guid PartyId { get; set; }
        public Guid PartyRoleId { get; set; }
        public string EmailAddress { get; set; }
        public bool IsPrimary { get; set; }   //
        public DateTime? CreationDate { get; set; }
        public DateTime? LastUpdateDate { get; set; }
    }
}
