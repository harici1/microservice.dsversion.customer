﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microservice.Customer.Entity.PartyReadModel
{
    public class Individual
    {
        public Guid PartyId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string CitizenshipNumber { get; set; }
        public string Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Reason { get; set; }  //
        public DateTime? CreationDate { get; set; }
        public DateTime? LastUpdateDate { get; set; }

    }
}
