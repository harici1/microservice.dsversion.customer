﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microservice.Customer.Entity.PartyReadModel
{
    public class Organization
    {
        // public PartyRoleType PartyRoleType { get; set; }
        public Guid PartyId { get; set; }
        public string ShortName { get; set; }
        public string FullName { get; set; }
        public string TaxNumber { get; set; }
        public string TaxOffice { get; set; }
        public string Reason { get; set; }  //
        public DateTime? CreationDate { get; set; }
        public DateTime? LastUpdateDate { get; set; }
    }
}
