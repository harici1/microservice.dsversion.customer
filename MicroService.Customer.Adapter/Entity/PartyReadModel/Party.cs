﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microservice.Customer.Entity.PartyReadModel
{
    public class Party : AggregateRoot
    {
        public DateTime PartyUpdatedDate = DateTime.Now;
        public Individual Individual { get; set; }
        public Organization Organization { get; set; }

        public List<PartyRole> PartyRoles = new List<PartyRole>();
        private Guid _id;

        public override Guid Id
        {
            get { return _id; }
        }

        public Party()
        {
            
        }

        

        public Party(Individual individual)
        {
            //ApplyChange(new IndividualCreatedEvent(individual));
            Individual = individual;
        }

        public Party(Organization organization)
        {
            //ApplyChange(new OrganizationCreatedEvent(organization));
            Organization = organization;
        }

        
    }
}
