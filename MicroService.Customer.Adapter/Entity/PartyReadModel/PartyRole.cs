﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microservice.Customer.Entity.PartyReadModel
{
    public class PartyRole
    {
        public PartyRole()      //added 08.07.2019
        {

        }
        public Guid PartyId;    //added 27.06.2019
        public int RoleTypeId;
        public string RoleTypeName;
        public Guid PartyRoleId;

        public List<Address> Addresses = new List<Address>();
        public List<Phone> Phones = new List<Phone>();
        public List<Email> Emails = new List<Email>();

        public PartyRole(int roleTypeId, string roleTypeName)
        {
            PartyRoleId = Guid.NewGuid();
            RoleTypeId = roleTypeId;
            RoleTypeName = roleTypeName;
        }

        public PartyRole(Guid partyId, Guid partyRoleId, int roleTypeId, string roleTypeName)
        {
            PartyId = partyId;
            PartyRoleId = partyRoleId;
            RoleTypeId = roleTypeId;
            RoleTypeName = roleTypeName;
        }

       
    }
}
