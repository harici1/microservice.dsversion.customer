﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microservice.Customer.Entity.PartyReadModel
{
    public class PartyRoleType
    {
        public string Name { get; set; }
        public int Id { get; set; }    // was Guid
    }
}
