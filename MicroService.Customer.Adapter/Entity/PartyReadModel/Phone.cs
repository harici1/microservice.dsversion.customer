﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microservice.Customer.Entity.PartyReadModel
{
    public class Phone
    {
        //public Guid PartyId { get; set; }
        public Guid PartyRoleId { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneType { get; set; }  // mobile / home / fax etc.
        public bool IsPrimary { get; set; }   //
        public DateTime? CreationDate { get; set; }
        public DateTime? LastUpdateDate { get; set; }
    }
}
