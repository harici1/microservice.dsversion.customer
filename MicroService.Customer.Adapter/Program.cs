﻿using MediatR;
using MicroService.Customer.Model.CommandModel;
using MicroServices.Customer.Domain.Implemantation;
using Microsoft.Extensions.DependencyInjection;
using MediatR.Pipeline;
using MicroService.Customer.Service.Handler;
using MicroService.Customer.Data.ReadModel.Repository;
using MicroService.Customer.Data.WriteModel.Repository;
using System;
using System.Data.SqlClient;
using Dapper;
using Microservice.Customer.Entity.ANKA.MigrationEntity;
using System.Collections.Generic;
using System.Linq;
using Microservice.Customer.Entity.DSIS.MigrationEntity;
using Microservice.Customer.Entity.DSIS.LegacyEntity;
using MicroService.Customer.Adapter.ElasticAdapterModel;
using System.Reflection;
using System.Timers;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Threading;
using Newtonsoft.Json;

namespace MicroService.Customer.Adapter
{
    class Program
    {
        static ServiceCollection services = new ServiceCollection();

        private static void InternetServicesTask(IMediator _mediator, IESRepository<AdapterProduct> _elasticAdapterProduct, IESRepository<AdapterProductBundle> _elasticAdapterProductBundle, IESRepository<AdapterCustomerAccount> _elasticAdapterCustomerAccount)
        {
            while (true)
            {
                var ProductId = Guid.NewGuid();
                var migrationStatus = 1;

                ContractDetail contractDetail = GetNextNotMigratedInternetService();

                // product bundle kaydı atılmış olmalı

                AdapterProductBundle adapterProductBundle = ProductBundleExistsInAdapterElastic("",contractDetail.ContractId.ToString(), _elasticAdapterProductBundle).FirstOrDefault();

                AdapterCustomerAccount adapterCustomerAccount = CustomerAccountExistsInAdapterElastic("",contractDetail.ContractId.ToString(), _elasticAdapterCustomerAccount).FirstOrDefault();

                Microservice.Customer.Entity.ANKA.LegacyEntity.CatalogItem catalogItem = GetAnkaProduct(contractDetail.CatalogItemId);

                if (catalogItem is null)
                {
                    migrationStatus = 99;
                }

                if (adapterProductBundle is null || adapterCustomerAccount is null)
                {
                    migrationStatus = 10;
                    // migration status 10 yapıp ötele

                }

                if (migrationStatus != 1)
                {
                    using (var connection = new SqlConnection(ConnectionStringHelper.GetAnkaMigrationString()))
                    {
                        string sqlQueryUpdate = $"update ContractDetail set MigrationStatus = {migrationStatus.ToString()} where ContractId= {contractDetail.ContractId}";


                        connection.Execute(sqlQueryUpdate);

                    }

                    continue;
                }

                Console.WriteLine("product buldum" + ProductId.ToString());

                AdapterProduct adapterProduct = ProductExistsInAdapterElastic("", contractDetail.ContractDetailId.ToString(), _elasticAdapterProduct).FirstOrDefault();

                if (adapterProduct == null)
                {
                    adapterProduct = new AdapterProduct
                    {
                        ProductID = ProductId.ToString(),
                        LegacyAnkaContractDetailID = contractDetail.ContractDetailId.ToString(),
                        LegacyDsisLeasingContractServiceID = ""
                    };

                    _elasticAdapterProduct.Save(adapterProduct);

                    // eventstore için command gönder

                    var legacyData = GetLegacyData(contractDetail);

                    AddProductCommand command = new AddProductCommand()
                    {
                        ProductId = ProductId,
                        ProductBundleId = Guid.Parse(adapterProductBundle.ProductBundleID),
                        CustomerAccountId = Guid.Parse(adapterCustomerAccount.CustomerAccountID),
                        CreatedBy = contractDetail.CreateBy.ToString(),
                        CreationDate = contractDetail.CreateDate,
                        LastUpdateDate = contractDetail.LastModifyDate is null ? null : contractDetail.LastModifyDate,
                        LastUpdatedBy = contractDetail.ModifyBy is null ? string.Empty : contractDetail.ModifyBy,
                        ProductName = catalogItem.CatalogItemName,
                        LegacyData = legacyData
                    };

                    var commandResult = _mediator.Send(command).Result;
                }
                else
                {
                    // productChanged event


                    // eventstore için command gönder

                    var legacyData = GetLegacyData(contractDetail);

                    ChangeProductCommand command = new ChangeProductCommand()
                    {
                        ProductId = ProductId,
                        ProductBundleId = Guid.Parse(adapterProductBundle.ProductBundleID),
                        CustomerAccountId = Guid.Parse(adapterCustomerAccount.CustomerAccountID),
                        CreatedBy = contractDetail.CreateBy.ToString(),
                        CreationDate = contractDetail.CreateDate,
                        LastUpdateDate = contractDetail.LastModifyDate,
                        LastUpdatedBy = contractDetail.ModifyBy.ToString(),
                        ProductName = catalogItem.CatalogItemName,
                        LegacyData = legacyData
                    };

                    var commandResult = _mediator.Send(command).Result;

                }


                using (var connection = new SqlConnection(ConnectionStringHelper.GetAnkaMigrationString()))
                {
                    string sqlQueryUpdate = "update ContractDetail set MigrationStatus = 1 where LogId=" + contractDetail.LogId;


                    connection.Execute(sqlQueryUpdate);
                }
            }
        }

        private static void PayTvServiceTask(IMediator _mediator, IESRepository<AdapterProduct> _elasticAdapterProduct, IESRepository<AdapterProductBundle> _elasticAdapterProductBundle, IESRepository<AdapterCustomerAccount> _elasticAdapterCustomerAccount)
        {
            while (true)
            {
                var ProductId = Guid.NewGuid();
                var migrationStatus = 1;

                LeasingContractService leasingContractService = GetNextNotMigratedPayTvLeasingContractService();

                // product bundle kaydı atılmış olmalı

                AdapterProductBundle adapterProductBundle = ProductBundleExistsInAdapterElastic(leasingContractService.LeasingContractId.ToString(), "", _elasticAdapterProductBundle).FirstOrDefault();

                AdapterCustomerAccount adapterCustomerAccount = CustomerAccountExistsInAdapterElastic(leasingContractService.LeasingContractId.ToString(), "", _elasticAdapterCustomerAccount).FirstOrDefault();

                DsProduct dsProduct = GetDsisProduct(leasingContractService.ProductId);

                if (dsProduct is null)
                {
                    migrationStatus = 99;
                }

                if (adapterProductBundle is null || adapterCustomerAccount is null)
                {
                    migrationStatus = 10;
                    // migration status 10 yapıp ötele

                }

                if (migrationStatus != 1)
                {
                    using (var connection = new SqlConnection(ConnectionStringHelper.GetDsisMigrationString()))
                    {
                        string sqlQueryUpdate = $"update LeasingContractService set MigrationStatus = {migrationStatus.ToString()} where LeasingContractId= {leasingContractService.LeasingContractId}";


                        connection.Execute(sqlQueryUpdate);

                    }

                    continue;
                }


                AdapterProduct adapterProduct = ProductExistsInAdapterElastic(leasingContractService.LeasingContractServiceId.ToString(), "", _elasticAdapterProduct).FirstOrDefault();

                if (adapterProduct == null)
                {
                    adapterProduct = new AdapterProduct
                    {
                        ProductID = ProductId.ToString(),
                        LegacyAnkaContractDetailID = "",
                        LegacyDsisLeasingContractServiceID = leasingContractService.LeasingContractServiceId.ToString()
                    };

                    _elasticAdapterProduct.Save(adapterProduct);

                    // eventstore için command gönder

                    var legacyData = GetLegacyData(leasingContractService);

                    AddProductCommand command = new AddProductCommand()
                    {
                        ProductId = ProductId,
                        ProductBundleId = Guid.Parse(adapterProductBundle.ProductBundleID),
                        CustomerAccountId = Guid.Parse(adapterCustomerAccount.CustomerAccountID),
                        CreatedBy = leasingContractService.CreatedById.ToString(),
                        CreationDate = leasingContractService.CreationDate,
                        LastUpdateDate = leasingContractService.LastUpdateDate,
                        LastUpdatedBy = leasingContractService.LastUpdatedById.ToString(),
                        ProductName = dsProduct.Name,
                        LegacyData = legacyData
                    };

                    var commandResult = _mediator.Send(command).Result;
                }
                else
                {
                    // productChanged event


                    // eventstore için command gönder

                    var legacyData = GetLegacyData(leasingContractService);

                    ChangeProductCommand command = new ChangeProductCommand()
                    {
                        ProductId = ProductId,
                        ProductBundleId = Guid.Parse(adapterProductBundle.ProductBundleID),
                        CustomerAccountId = Guid.Parse(adapterCustomerAccount.CustomerAccountID),
                        CreatedBy = leasingContractService.CreatedById.ToString(),
                        CreationDate = leasingContractService.CreationDate,
                        LastUpdateDate = leasingContractService.LastUpdateDate,
                        LastUpdatedBy = leasingContractService.LastUpdatedById.ToString(),
                        ProductName = dsProduct.Name,
                        LegacyData = legacyData
                    };

                    var commandResult = _mediator.Send(command).Result;

                }


                using (var connection = new SqlConnection(ConnectionStringHelper.GetDsisMigrationString()))
                {
                    string sqlQueryUpdate = "update LeasingContractService set MigrationStatus = 1 where LogId=" + leasingContractService.LogId;


                    connection.Execute(sqlQueryUpdate);
                }
            }
        }


        private static void SingleInternetAccountTask(IMediator _mediator, IESRepository<AdapterCustomer> _elasticAdapterCustomer, IESRepository<AdapterCustomerAccount> _elasticAdapterCustomerAccount, IESRepository<AdapterBillingAccount> _elasticAdapterBillingAccount, IESRepository<AdapterProductBundle> _elasticAdapterProductBundle, IESRepository<Microservice.Customer.Entity.PartyReadModel.Party> _elasticPartyRepo)
        {
            while (true)
            {
                Contract contract = GetNextNotMigratedSingleInternetContract();

                Microservice.Customer.Entity.ANKA.MigrationEntity.Customer ankaCustomer = GetAnkaCustomer(contract.CustomerId);

                Microservice.Customer.Entity.PartyReadModel.Party elasticParty = new Microservice.Customer.Entity.PartyReadModel.Party();

                // bu yoksa migstatus 10 yapalım tüm bu party Id ile olan kayıtları ve skip edelim.            Microservice.Customer.Entity.PartyReadModel.Party elasticParty = ;

                Console.WriteLine(contract.LogId + "anka single");


                int migrationStatusToUpdate = 1;

                if (ankaCustomer.CitizienshipNo is null || ankaCustomer.CitizienshipNo.Trim().Equals(String.Empty))
                {
                    migrationStatusToUpdate = 99;
                }
                else
                {
                    elasticParty = PartyExistsInElastic(ankaCustomer.CitizienshipNo.Trim(), _elasticPartyRepo).FirstOrDefault();
                }

                if (elasticParty is null)
                {
                    migrationStatusToUpdate = 10;
                }


                if (migrationStatusToUpdate != 1)
                {
                    using (var connection = new SqlConnection(ConnectionStringHelper.GetAnkaMigrationString()))
                    {
                        string sqlQueryUpdate = "update Contract set MigrationStatus = " + migrationStatusToUpdate + " where CustomerId=" + contract.CustomerId;


                        connection.Execute(sqlQueryUpdate);

                    }

                    continue;
                }

                if (true)
                {
                    var customerID = elasticParty.PartyRoles[0].PartyRoleId;
                    var customerAccountId = Guid.NewGuid();
                    var productBundleId = Guid.NewGuid();
                    var billingAccountId = Guid.NewGuid();
                    // yeni bir customeraccount ve productbundle söz konusu
                    AdapterCustomer adapterCustomer = CustomerExistsInAdapterElastic("", contract.CustomerId.ToString(), _elasticAdapterCustomer).FirstOrDefault();
                    AdapterCustomerAccount adapterCustomerAccount = CustomerAccountExistsInAdapterElastic("", contract.ContractId.ToString(), _elasticAdapterCustomerAccount).FirstOrDefault();
                    AdapterBillingAccount adapterBillingAccount = BillingAccountExistsInAdapterElastic("", contract.ContractId.ToString(), _elasticAdapterBillingAccount).FirstOrDefault();
                    AdapterProductBundle adapterProductBundle = ProductBundleExistsInAdapterElastic("", contract.ContractId.ToString(), _elasticAdapterProductBundle).FirstOrDefault();



                    if (adapterCustomer == null)
                    {
                        // elastic içinde yok, oluştur

                        adapterCustomer = new AdapterCustomer
                        {
                            CustomerID = customerID.ToString(),
                            LegacyAnkaCustomerID = contract.CustomerId.ToString(),
                            LegacyDsisPartyID = ""
                        };

                        _elasticAdapterCustomer.Save(adapterCustomer);

                        // eventstore için command gönder

                        var legacyData = GetLegacyData(ankaCustomer);

                        CreateCustomerCommand command = new CreateCustomerCommand()
                        {
                            CustomerId = customerID,
                            BirthDate = ankaCustomer.Birthdate,
                            FirstName = ankaCustomer.CustomerName,
                            LastName = ankaCustomer.CustomerSurname,
                            Gender = ankaCustomer.Gender,
                            MiddleName = string.Empty,
                            CreatedBy = ankaCustomer.CreateBy.ToString(),
                            CreationDate = ankaCustomer.CreateDate,
                            LastUpdateDate = ankaCustomer.LastModifyDate,
                            LastUpdatedBy = ankaCustomer.ModifyBy,
                            LegacyData = legacyData
                        };

                        var commandResult = _mediator.Send(command).Result;
                    }

                    if (adapterCustomerAccount == null)
                    {
                        // elastic içinde yok, oluştur

                        adapterCustomerAccount = new AdapterCustomerAccount()
                        {
                            CustomerAccountID = customerAccountId.ToString(),
                            LegacyDsisLEasingContractID = "",
                            LegacyDsisLEasingContractNumber = "",
                            LegacyAnkaContractID = contract.ContractId.ToString(),
                            LegacyAnkaContractNumber = contract.ContractNo is null ? string.Empty : contract.ContractNo.ToString()
                        };

                        _elasticAdapterCustomerAccount.Save(adapterCustomerAccount);

                        CreateCustomerAccountCommand command = new CreateCustomerAccountCommand()
                        {
                            AccountContactId = Guid.Parse(adapterCustomer.CustomerID),
                            CustomerAccountId = customerAccountId,
                            CreationDate = contract.CreateDate,
                            LastUpdateDate = contract.LastModifyDate,
                            ContractNumber = "AN" + contract.ContractNo,
                            CreatedBy = contract.CreateBy.ToString(),
                            CustomerId = Guid.Parse(adapterCustomer.CustomerID)
                        };

                        var commandResult = _mediator.Send(command).Result;
                    }

                    if (adapterBillingAccount == null)
                    {
                        adapterBillingAccount = new AdapterBillingAccount()
                        {
                            BillingAccountId = billingAccountId.ToString(),
                            LegacyDsisLEasingContractID = "",
                            LegacyDsisLEasingContractNumber = "",
                            LegacyAnkaContractID = contract.ContractId.ToString(),
                            LegacyAnkaContractNumber = contract.ContractNo is null ? string.Empty : contract.ContractNo.ToString()
                        };

                        _elasticAdapterBillingAccount.Save(adapterBillingAccount);

                        //Party party = GetParty(LeasingContract.PartyId);

                        Microservice.Customer.Entity.ANKA.LegacyEntity.Address ankaAdress = GetAnkaAddress(contract.CustomerId);

                        AddCustomerBillingAccountProductCommand command = new AddCustomerBillingAccountProductCommand()
                        {
                            BillingAccountId = billingAccountId,
                            CreationDate = contract.CreateDate,
                            LastUpdateDate = contract.LastModifyDate,
                            CreatedBy = contract.CreateBy.ToString(),
                            CustomerId = Guid.Parse(adapterCustomer.CustomerID),
                            IsTaxFree = null,
                            IsEInvoiceRequested = ankaCustomer.EinvoicerOk,
                            IsEnvelopeRequested = null,
                            Email = ankaCustomer.Email1,
                            MobilePhone = ankaCustomer.MobilePhone,
                            PhoneNumber = ankaCustomer.HomePhone,
                            Street = ankaAdress.StreetAddress,
                            InvoiceTaxNumber = ankaCustomer.TaxNo,
                            InvoiceTaxOffice = ankaCustomer.TaxOffice,
                            InvoiceTitle = ankaCustomer.Title,
                            LastUpdatedBy = contract.ModifyBy
                        };

                        var commandResult = _mediator.Send(command).Result;
                    }

                    var legacyDataPP = GetLegacyData(contract);
                    if (adapterProductBundle == null)
                    {
                        // elastic içinde yok, oluştur

                        adapterProductBundle = new AdapterProductBundle()
                        {
                            ProductBundleID = productBundleId.ToString(),
                            LegacyDsisLEasingContractID = "",
                            LegacyDsisLEasingContractNumber = "",
                            LegacyAnkaContractID = contract.ContractId.ToString(),
                            LegacyAnkaContractNumber = contract.ContractNo is null ? string.Empty : contract.ContractNo.ToString()
                        };



                        _elasticAdapterProductBundle.Save(adapterProductBundle);



                        AddProductBundleCommand command = new AddProductBundleCommand()
                        {

                            ProductBundleId = productBundleId,
                            CustomerAccountId = Guid.Parse(adapterCustomerAccount.CustomerAccountID),
                            CreationDate = contract.CreateDate,
                            LastUpdateDate = contract.LastModifyDate,
                            CreatedBy = contract.CreateBy.ToString(),
                            BillingAccountId = Guid.Parse(adapterBillingAccount.BillingAccountId),
                            LegacyData = legacyDataPP
                        };


                        var commandResult = _mediator.Send(command).Result;



                    }
                    else
                    {
                        // product bundle changed
                        //_elasticAdapterProductBundle.Save(adapterProductBundle);
                        ChangeProductBundleCommand command = new ChangeProductBundleCommand()
                        {

                            ProductBundleId = Guid.Parse(adapterProductBundle.ProductBundleID),
                            CustomerAccountId = Guid.Parse(adapterCustomerAccount.CustomerAccountID),
                            CreationDate = contract.CreateDate,
                            LastUpdateDate = contract.LastModifyDate,
                            CreatedBy = contract.CreateBy.ToString(),
                            BillingAccountId = Guid.Parse(adapterBillingAccount.BillingAccountId),
                            LegacyData = legacyDataPP
                        };

                        var commandResult = _mediator.Send(command).Result;

                    }

                }
                using (var connection = new SqlConnection(ConnectionStringHelper.GetAnkaMigrationString()))
                {
                    string sqlQueryUpdate = "update Contract set MigrationStatus = 1 where LogId=" + contract.LogId;


                    connection.Execute(sqlQueryUpdate);

                }







            }

        }


        private static void BundleInternetAccountTask(IMediator _mediator, IESRepository<AdapterCustomer> _elasticAdapterCustomer, IESRepository<AdapterCustomerAccount> _elasticAdapterCustomerAccount, IESRepository<AdapterBillingAccount> _elasticAdapterBillingAccount, IESRepository<AdapterProductBundle> _elasticAdapterProductBundle, IESRepository<Microservice.Customer.Entity.PartyReadModel.Party> _elasticPartyRepo)
        {
            while (true)
            {
                Contract contract = GetNextNotMigratedBundleInternetContract();

                var dsLeasingContract = GetBundleDsisLeasingContract(contract.ContractId);


                Microservice.Customer.Entity.ANKA.MigrationEntity.Customer ankaCustomer = GetAnkaCustomer(contract.CustomerId);

                Microservice.Customer.Entity.PartyReadModel.Party elasticParty = new Microservice.Customer.Entity.PartyReadModel.Party();

                // bu yoksa migstatus 10 yapalım tüm bu party Id ile olan kayıtları ve skip edelim.            Microservice.Customer.Entity.PartyReadModel.Party elasticParty = ;

                Console.WriteLine(contract.LogId + " bundle");


                int migrationStatusToUpdate = 1;

                if (ankaCustomer.CitizienshipNo is null || ankaCustomer.CitizienshipNo.Trim().Equals(String.Empty))
                {
                    migrationStatusToUpdate = 99;
                }
                else
                {
                    elasticParty = PartyExistsInElastic(ankaCustomer.CitizienshipNo.Trim(), _elasticPartyRepo).FirstOrDefault();
                }

                if (elasticParty is null)
                {
                    migrationStatusToUpdate = 10;
                }

                if (dsLeasingContract is null)
                {
                    Console.WriteLine(contract.ContractId + " BULAMADIM BANDILINI");
                    migrationStatusToUpdate = 98;
                }

                if (migrationStatusToUpdate != 1)
                {
                    using (var connection = new SqlConnection(ConnectionStringHelper.GetAnkaMigrationString()))
                    {
                        string sqlQueryUpdate = "update Contract set MigrationStatus = " + migrationStatusToUpdate + " where CustomerId=" + contract.CustomerId;

                        if (migrationStatusToUpdate == 10)
                        {
                            sqlQueryUpdate = "update Contract set MigrationStatus = MigrationStatus + 1 where CustomerId=" + contract.CustomerId;
                        }

                        connection.Execute(sqlQueryUpdate);

                    }

                    continue;
                }

                if (true)
                {
                    var customerID = elasticParty.PartyRoles[0].PartyRoleId;
                    var customerAccountId = Guid.NewGuid();
                    var productBundleId = Guid.NewGuid();
                    var billingAccountId = Guid.NewGuid();
                    // yeni bir customeraccount ve productbundle söz konusu
                    AdapterCustomer adapterCustomer = CustomerExistsInAdapterElastic("", contract.CustomerId.ToString(), _elasticAdapterCustomer).FirstOrDefault();
                    AdapterCustomerAccount adapterCustomerAccount = CustomerAccountExistsInAdapterElastic("", contract.ContractId.ToString(), _elasticAdapterCustomerAccount).FirstOrDefault();
                    AdapterBillingAccount adapterBillingAccount = BillingAccountExistsInAdapterElastic("", contract.ContractId.ToString(), _elasticAdapterBillingAccount).FirstOrDefault();
                    AdapterProductBundle adapterProductBundle = ProductBundleExistsInAdapterElastic("", contract.ContractId.ToString(), _elasticAdapterProductBundle).FirstOrDefault();



                    if (adapterCustomer == null)
                    {
                        // elastic içinde yok, oluştur

                        adapterCustomer = new AdapterCustomer
                        {
                            CustomerID = customerID.ToString(),
                            LegacyAnkaCustomerID = contract.CustomerId.ToString(),
                            LegacyDsisPartyID = dsLeasingContract.PartyId.ToString()
                        };

                        _elasticAdapterCustomer.Save(adapterCustomer);

                        // eventstore için command gönder

                        var legacyData = GetLegacyData(ankaCustomer);
                        var legacyDataDsis = GetLegacyData(dsLeasingContract);

                        foreach(var leg in legacyDataDsis)
                        {
                            legacyData.Add(leg.Key, leg.Value);
                        }

                        CreateCustomerCommand command = new CreateCustomerCommand()
                        {
                            CustomerId = customerID,
                            BirthDate = ankaCustomer.Birthdate,
                            FirstName = ankaCustomer.CustomerName,
                            LastName = ankaCustomer.CustomerSurname,
                            Gender = ankaCustomer.Gender,
                            MiddleName = string.Empty,
                            CreatedBy = ankaCustomer.CreateBy.ToString(),
                            CreationDate = ankaCustomer.CreateDate,
                            LastUpdateDate = ankaCustomer.LastModifyDate,
                            LastUpdatedBy = ankaCustomer.ModifyBy,
                            LegacyData = legacyData
                        };

                        var commandResult = _mediator.Send(command).Result;
                    }

                    if (adapterCustomerAccount == null)
                    {
                        // elastic içinde yok, oluştur

                        adapterCustomerAccount = new AdapterCustomerAccount()
                        {
                            CustomerAccountID = customerAccountId.ToString(),
                            LegacyDsisLEasingContractID = dsLeasingContract.LeasingContractId.ToString(),
                            LegacyDsisLEasingContractNumber = dsLeasingContract.LeasingContractNumber.ToString(),
                            LegacyAnkaContractID = contract.ContractId.ToString(),
                            LegacyAnkaContractNumber = contract.ContractNo is null ? string.Empty : contract.ContractNo.ToString()
                        };

                        _elasticAdapterCustomerAccount.Save(adapterCustomerAccount);

                        CreateCustomerAccountCommand command = new CreateCustomerAccountCommand()
                        {
                            AccountContactId = Guid.Parse(adapterCustomer.CustomerID),
                            CustomerAccountId = customerAccountId,
                            CreationDate = dsLeasingContract.CreationDate,
                            LastUpdateDate = dsLeasingContract.LastUpdateDate,
                            ContractNumber = "DS" + dsLeasingContract.LeasingContractNumber,
                            CreatedBy = dsLeasingContract.CreatedById.ToString(),
                            CustomerId = Guid.Parse(adapterCustomer.CustomerID)
                        };

                        var commandResult = _mediator.Send(command).Result;
                    }

                    if (adapterBillingAccount == null)
                    {
                        adapterBillingAccount = new AdapterBillingAccount()
                        {
                            BillingAccountId = billingAccountId.ToString(),
                            LegacyDsisLEasingContractID = dsLeasingContract.LeasingContractId.ToString(),
                            LegacyDsisLEasingContractNumber = dsLeasingContract.LeasingContractNumber,
                            LegacyAnkaContractID = contract.ContractId.ToString(),
                            LegacyAnkaContractNumber = contract.ContractNo
                        };

                        _elasticAdapterBillingAccount.Save(adapterBillingAccount);

                        //Party party = GetParty(LeasingContract.PartyId);

                        Microservice.Customer.Entity.ANKA.LegacyEntity.Address ankaAdress = GetAnkaAddress(contract.CustomerId);

                        AddCustomerBillingAccountProductCommand command = new AddCustomerBillingAccountProductCommand()
                        {
                            BillingAccountId = billingAccountId,
                            CreationDate = contract.CreateDate,
                            LastUpdateDate = contract.LastModifyDate,
                            CreatedBy = contract.CreateBy.ToString(),
                            CustomerId = Guid.Parse(adapterCustomer.CustomerID),
                            IsTaxFree = null,
                            IsEInvoiceRequested = ankaCustomer.EinvoicerOk,
                            IsEnvelopeRequested = null,
                            Email = ankaCustomer.Email1,
                            MobilePhone = ankaCustomer.MobilePhone,
                            PhoneNumber = ankaCustomer.HomePhone,
                            Street = ankaAdress.StreetAddress,
                            InvoiceTaxNumber = ankaCustomer.TaxNo,
                            InvoiceTaxOffice = ankaCustomer.TaxOffice,
                            InvoiceTitle = ankaCustomer.Title,
                            LastUpdatedBy = contract.ModifyBy
                        };

                        var commandResult = _mediator.Send(command).Result;
                    }

                    var legacyDataPP = GetLegacyData(contract);
                    if (adapterProductBundle == null)
                    {
                        // elastic içinde yok, oluştur

                        adapterProductBundle = new AdapterProductBundle()
                        {
                            ProductBundleID = productBundleId.ToString(),
                            LegacyDsisLEasingContractID = dsLeasingContract.LeasingContractId.ToString(),
                            LegacyDsisLEasingContractNumber = dsLeasingContract.LeasingContractNumber,
                            LegacyAnkaContractID = contract.ContractId.ToString(),
                            LegacyAnkaContractNumber = contract.ContractNo
                        };



                        _elasticAdapterProductBundle.Save(adapterProductBundle);



                        AddProductBundleCommand command = new AddProductBundleCommand()
                        {

                            ProductBundleId = productBundleId,
                            CustomerAccountId = Guid.Parse(adapterCustomerAccount.CustomerAccountID),
                            CreationDate = contract.CreateDate,
                            LastUpdateDate = contract.LastModifyDate,
                            CreatedBy = contract.CreateBy.ToString(),
                            BillingAccountId = Guid.Parse(adapterBillingAccount.BillingAccountId),
                            LegacyData = legacyDataPP
                        };


                        var commandResult = _mediator.Send(command).Result;



                    }
                    else
                    {
                        // product bundle changed
                        //_elasticAdapterProductBundle.Save(adapterProductBundle);
                        ChangeProductBundleCommand command = new ChangeProductBundleCommand()
                        {

                            ProductBundleId = Guid.Parse(adapterProductBundle.ProductBundleID),
                            CustomerAccountId = Guid.Parse(adapterCustomerAccount.CustomerAccountID),
                            CreationDate = contract.CreateDate,
                            LastUpdateDate = contract.LastModifyDate,
                            CreatedBy = contract.CreateBy.ToString(),
                            BillingAccountId = Guid.Parse(adapterBillingAccount.BillingAccountId),
                            LegacyData = legacyDataPP
                        };

                        var commandResult = _mediator.Send(command).Result;

                    }

                }
                using (var connection = new SqlConnection(ConnectionStringHelper.GetAnkaMigrationString()))
                {
                    string sqlQueryUpdate = "update Contract set MigrationStatus = 1 where LogId=" + contract.LogId;


                    connection.Execute(sqlQueryUpdate);

                }







            }

        }



        private static Microservice.Customer.Entity.ANKA.LegacyEntity.Address GetAnkaAddress(int customerId)
        {
            Microservice.Customer.Entity.ANKA.LegacyEntity.Address ankaAddress = new Microservice.Customer.Entity.ANKA.LegacyEntity.Address();
            string sqlQueryParty = $"select top 1 * from ANKA.dbo.Address where RefType = 'Customer' And RefParameter = '{customerId}' order by DefaultAdress desc, active desc";

            using (var connection = new SqlConnection(ConnectionStringHelper.GetAnkaLegacyString()))
            {
                ankaAddress = connection.QueryFirstOrDefault<Microservice.Customer.Entity.ANKA.LegacyEntity.Address>(sqlQueryParty);
            }

            return ankaAddress;
        }

        private static Microservice.Customer.Entity.ANKA.MigrationEntity.Customer GetAnkaCustomer(int customerId)
        {
            Microservice.Customer.Entity.ANKA.MigrationEntity.Customer ankaCustomer = new Microservice.Customer.Entity.ANKA.MigrationEntity.Customer();
            string sqlQueryParty = "select * from Customer where CustomerId = " + customerId;

            using (var connection = new SqlConnection(ConnectionStringHelper.GetAnkaLegacyString()))
            {
                ankaCustomer = connection.QueryFirstOrDefault<Microservice.Customer.Entity.ANKA.MigrationEntity.Customer>(sqlQueryParty);
            }

            return ankaCustomer;
        }

        private static void SinglePayTvAccountTask(IMediator _mediator, IESRepository<AdapterCustomer> _elasticAdapterCustomer, IESRepository<AdapterCustomerAccount> _elasticAdapterCustomerAccount, IESRepository<AdapterBillingAccount> _elasticAdapterBillingAccount, IESRepository<AdapterProductBundle> _elasticAdapterProductBundle, IESRepository<Microservice.Customer.Entity.PartyReadModel.Party> _elasticPartyRepo)
        {
            while (true)
            {
                LeasingContract LeasingContract = GetNextNotMigratedSinglePayTvLeasingContract();
                DsMusteri dsMusteri = GetDsisMusteri(LeasingContract.PartyId);

                Microservice.Customer.Entity.PartyReadModel.Party elasticParty = new Microservice.Customer.Entity.PartyReadModel.Party();

                // bu yoksa migstatus 10 yapalım tüm bu party Id ile olan kayıtları ve skip edelim.            Microservice.Customer.Entity.PartyReadModel.Party elasticParty = ;

                Console.WriteLine(LeasingContract.LogId);


                int migrationStatusToUpdate = 1;

                if (dsMusteri.TckimlikNo is null || dsMusteri.TckimlikNo.Trim().Equals(String.Empty))
                {
                    migrationStatusToUpdate = 99;
                }
                else
                {
                    elasticParty = PartyExistsInElastic(dsMusteri.TckimlikNo.Trim(), _elasticPartyRepo).FirstOrDefault();
                }

                if (elasticParty is null)
                {
                    migrationStatusToUpdate = 10;
                }


                if (migrationStatusToUpdate != 1)
                {
                    using (var connection = new SqlConnection(ConnectionStringHelper.GetDsisMigrationString()))
                    {
                        string sqlQueryUpdate = "update LeasingContract set MigrationStatus = " + migrationStatusToUpdate + " where PartyID=" + LeasingContract.PartyId;


                        connection.Execute(sqlQueryUpdate);

                    }

                    continue;
                }


                if (LeasingContract.LogType == "Update" || LeasingContract.LogType == "Insert")
                {
                    var customerID = elasticParty.PartyRoles[0].PartyRoleId;
                    var customerAccountId = Guid.NewGuid();
                    var productBundleId = Guid.NewGuid();
                    var billingAccountId = Guid.NewGuid();
                    // yeni bir customeraccount ve productbundle söz konusu
                    AdapterCustomer adapterCustomer = CustomerExistsInAdapterElastic(LeasingContract.PartyId.ToString(), "", _elasticAdapterCustomer).FirstOrDefault();
                    AdapterCustomerAccount adapterCustomerAccount = CustomerAccountExistsInAdapterElastic(LeasingContract.LeasingContractId.ToString(), "", _elasticAdapterCustomerAccount).FirstOrDefault();
                    AdapterBillingAccount adapterBillingAccount = BillingAccountExistsInAdapterElastic(LeasingContract.LeasingContractId.ToString(), "", _elasticAdapterBillingAccount).FirstOrDefault();
                    AdapterProductBundle adapterProductBundle = ProductBundleExistsInAdapterElastic(LeasingContract.LeasingContractId.ToString(), "", _elasticAdapterProductBundle).FirstOrDefault();



                    if (adapterCustomer == null)
                    {
                        // elastic içinde yok, oluştur

                        adapterCustomer = new AdapterCustomer
                        {
                            CustomerID = customerID.ToString(),
                            LegacyAnkaCustomerID = "0",
                            LegacyDsisPartyID = dsMusteri.MusteriId.ToString()
                        };

                        _elasticAdapterCustomer.Save(adapterCustomer);

                        // eventstore için command gönder

                        //var legacyData = GetLegacyData(dsMusteri);

                        CreateCustomerCommand command = new CreateCustomerCommand()
                        {
                            CustomerId = customerID,
                            BirthDate = dsMusteri.DogumTarihi,
                            FirstName = dsMusteri.Adi,
                            LastName = dsMusteri.Soyadi,
                            Gender = dsMusteri.Cinsiyet,
                            MiddleName = string.Empty,
                            CreatedBy = dsMusteri.OlusturanId.ToString(),
                            CreationDate = dsMusteri.OlusturmaTarihi,
                            LastUpdateDate = dsMusteri.DuzeltmeTarih,
                            LastUpdatedBy = dsMusteri.DuzeltenId.ToString()
                            //LegacyData = legacyData
                        };

                        var commandResult = _mediator.Send(command).Result;
                    }

                    if (adapterCustomerAccount == null)
                    {
                        // elastic içinde yok, oluştur

                        adapterCustomerAccount = new AdapterCustomerAccount()
                        {
                            CustomerAccountID = customerAccountId.ToString(),
                            LegacyDsisLEasingContractID = LeasingContract.LeasingContractId.ToString(),
                            LegacyDsisLEasingContractNumber = LeasingContract.LeasingContractNumber,
                            LegacyAnkaContractID = "",
                            LegacyAnkaContractNumber = ""
                        };

                        _elasticAdapterCustomerAccount.Save(adapterCustomerAccount);

                        CreateCustomerAccountCommand command = new CreateCustomerAccountCommand()
                        {
                            AccountContactId = Guid.Parse(adapterCustomer.CustomerID),
                            CustomerAccountId = customerAccountId,
                            CreationDate = LeasingContract.CreationDate,
                            LastUpdateDate = LeasingContract.LastUpdateDate,
                            ContractNumber = "DS" + LeasingContract.LeasingContractNumber,
                            CreatedBy = LeasingContract.CreatedById.ToString(),
                            CustomerId = Guid.Parse(adapterCustomer.CustomerID)
                        };

                        var commandResult = _mediator.Send(command).Result;
                    }

                    if (adapterBillingAccount == null)
                    {
                        adapterBillingAccount = new AdapterBillingAccount()
                        {
                            BillingAccountId = billingAccountId.ToString(),
                            LegacyDsisLEasingContractID = LeasingContract.LeasingContractId.ToString(),
                            LegacyDsisLEasingContractNumber = LeasingContract.LeasingContractNumber,
                            LegacyAnkaContractID = "",
                            LegacyAnkaContractNumber = ""
                        };

                        _elasticAdapterBillingAccount.Save(adapterBillingAccount);

                        Party party = GetParty(LeasingContract.PartyId);

                        AddCustomerBillingAccountProductCommand command = new AddCustomerBillingAccountProductCommand()
                        {
                            BillingAccountId = billingAccountId,
                            CreationDate = LeasingContract.CreationDate,
                            LastUpdateDate = LeasingContract.LastUpdateDate,
                            CreatedBy = LeasingContract.CreatedById.ToString(),
                            CustomerId = Guid.Parse(adapterCustomer.CustomerID),
                            IsTaxFree = party.TaxFree,
                            IsEInvoiceRequested = party.Einvoice,
                            IsEnvelopeRequested = party.IsEnvelopeRequested,
                            Email = dsMusteri.Eposta,
                            MobilePhone = dsMusteri.CepTelefon,
                            PhoneNumber = dsMusteri.EvTelefon,
                            Street = party.InvoiceAddress,
                            InvoiceTaxNumber = party.InvoiceTaxNumber,
                            InvoiceTaxOffice = party.InvoiceTaxOffice,
                            InvoiceTitle = party.InvoiceTitle,
                            LastUpdatedBy = LeasingContract.LastUpdatedById.ToString()
                        };

                        var commandResult = _mediator.Send(command).Result;
                    }

                    var legacyDataPP = GetLegacyData(LeasingContract);
                    if (adapterProductBundle == null)
                    {
                        // elastic içinde yok, oluştur

                        adapterProductBundle = new AdapterProductBundle()
                        {
                            ProductBundleID = productBundleId.ToString(),
                            LegacyDsisLEasingContractID = LeasingContract.LeasingContractId.ToString(),
                            LegacyDsisLEasingContractNumber = LeasingContract.LeasingContractNumber,
                            LegacyAnkaContractID = "",
                            LegacyAnkaContractNumber = ""
                        };



                        _elasticAdapterProductBundle.Save(adapterProductBundle);



                        AddProductBundleCommand command = new AddProductBundleCommand()
                        {

                            ProductBundleId = productBundleId,
                            CustomerAccountId = Guid.Parse(adapterCustomerAccount.CustomerAccountID),
                            CreationDate = LeasingContract.CreationDate,
                            LastUpdateDate = LeasingContract.LastUpdateDate,
                            CreatedBy = LeasingContract.CreatedById.ToString(),
                            BillingAccountId = Guid.Parse(adapterBillingAccount.BillingAccountId),
                            LegacyData = legacyDataPP
                        };


                        var commandResult = _mediator.Send(command).Result;



                    }
                    else
                    {
                        // product bundle changed
                        //_elasticAdapterProductBundle.Save(adapterProductBundle);
                        ChangeProductBundleCommand command = new ChangeProductBundleCommand()
                        {

                            ProductBundleId = Guid.Parse(adapterProductBundle.ProductBundleID),
                            CustomerAccountId = Guid.Parse(adapterCustomerAccount.CustomerAccountID),
                            CreationDate = LeasingContract.CreationDate,
                            LastUpdateDate = LeasingContract.LastUpdateDate,
                            CreatedBy = LeasingContract.CreatedById.ToString(),
                            BillingAccountId = Guid.Parse(adapterBillingAccount.BillingAccountId),
                            LegacyData = legacyDataPP
                        };

                        var commandResult = _mediator.Send(command).Result;

                    }

                }
                using (var connection = new SqlConnection(ConnectionStringHelper.GetDsisMigrationString()))
                {
                    string sqlQueryUpdate = "update LeasingContract set MigrationStatus = 1 where LogId=" + LeasingContract.LogId;


                    connection.Execute(sqlQueryUpdate);

                }
            }

        }




        static void Main(string[] args)
        {
            Build();
            var provider = services.BuildServiceProvider();

            var _mediator = provider.GetRequiredService<IMediator>();


            var _elasticLeasingContract = provider.GetRequiredService<IESRepository<LeasingContract>>();
            var _elasticAdapterCustomer = provider.GetRequiredService<IESRepository<AdapterCustomer>>();
            var _elasticAdapterCustomerAccount = provider.GetRequiredService<IESRepository<AdapterCustomerAccount>>();
            var _elasticAdapterBillingAccount = provider.GetRequiredService<IESRepository<AdapterBillingAccount>>();
            var _elasticAdapterProductBundle = provider.GetRequiredService<IESRepository<AdapterProductBundle>>();
            var _elasticAdapterProduct = provider.GetRequiredService<IESRepository<AdapterProduct>>();
            var _elasticParty = provider.GetRequiredService<IESRepository<Microservice.Customer.Entity.PartyReadModel.Party>>();


            //Stopwatch t = new Stopwatch();
            //t.Start();

            Task.Run(() => BundleInternetAccountTask(_mediator, _elasticAdapterCustomer, _elasticAdapterCustomerAccount, _elasticAdapterBillingAccount, _elasticAdapterProductBundle, _elasticParty));

            Task.Run(() => SingleInternetAccountTask(_mediator, _elasticAdapterCustomer, _elasticAdapterCustomerAccount, _elasticAdapterBillingAccount, _elasticAdapterProductBundle, _elasticParty));

            Task.Run(() => SinglePayTvAccountTask(_mediator, _elasticAdapterCustomer, _elasticAdapterCustomerAccount, _elasticAdapterBillingAccount, _elasticAdapterProductBundle, _elasticParty));


            Task.Run(() => PayTvServiceTask(_mediator, _elasticAdapterProduct, _elasticAdapterProductBundle, _elasticAdapterCustomerAccount));

            Task.Run(() => InternetServicesTask(_mediator, _elasticAdapterProduct, _elasticAdapterProductBundle, _elasticAdapterCustomerAccount));

            //t.Stop();

            //IEnumerable<Microservice.Customer.Entity.PartyReadModel.Party> parties = _elasticParty.SelectBySize(100, 1000);
            //List<Microservice.Customer.Entity.PartyReadModel.Party> securedParties = new List<Microservice.Customer.Entity.PartyReadModel.Party>();
            //foreach (var p in parties)
            //{
            //    //Microservice.Customer.Entity.PartyReadModel.Party sp = new Microservice.Customer.Entity.PartyReadModel.Party();
            //    //sp.Individual = p.Individual;
            //    //sp.Organization = p.Organization;
            //    //sp.PartyRoles = p.PartyRoles;
            //    //sp.Id = p.Id;



            //    p.Individual.CitizenshipNumber = "12345678912";
            //    p.Individual.Surname = "Borax";
            //}


            //var jsonString = JsonConvert.SerializeObject(parties);
            //Console.WriteLine("100 ok - " + t.ElapsedMilliseconds / 1000M);
            Console.ReadLine();
        }


        private static Dictionary<string, string> GetLegacyData(object obj)
        {
            Type type = obj.GetType();
            var legacyData = new Dictionary<string, string>();

            IList<PropertyInfo> props = new List<PropertyInfo>(type.GetProperties());
            foreach (var prop in props)
            {
                var val = prop.GetValue(obj);
                string strval = "";
                if (val == null)
                    strval = "null";
                else
                    strval = val.ToString();
                legacyData.Add(prop.Name, strval);
            }


            return legacyData;
        }

        private static IEnumerable<AdapterProductBundle> ProductBundleExistsInAdapterElastic(string dsisLeasingContractId, string ankaContractId, IESRepository<AdapterProductBundle> elasticAdapterProductBundle)
        {
            var searchModel = new Model.ReadModelEntity.BaseSearchModel();
            searchModel.Fields = new Dictionary<string, string>();

            if (dsisLeasingContractId != "")
            {
                searchModel.Fields.Add("legacyDsisLEasingContractID", dsisLeasingContractId);
            }
            else
            {
                searchModel.Fields.Add("legacyAnkaContractID", ankaContractId);
            }

            var result = elasticAdapterProductBundle.Search(searchModel);


            //Console.WriteLine("buldum");


            return result;
        }


        private static IEnumerable<Microservice.Customer.Entity.PartyReadModel.Party> PartyExistsInElastic(string tckimlik, IESRepository<Microservice.Customer.Entity.PartyReadModel.Party> elasticPartyClient)
        {
            var searchModel = new Model.ReadModelEntity.BaseSearchModel();
            searchModel.Fields = new Dictionary<string, string>();

            searchModel.Fields.Add("individual.citizenshipNumber", tckimlik);

            var result = elasticPartyClient.Search(searchModel);

            if (result.Count() > 0)
                Console.WriteLine("buldum");


            return result;


            // şimdilik imParty tablosuna bakacağım....

            //List<Microservice.Customer.Entity.PartyReadModel.Party> parties = new List<Microservice.Customer.Entity.PartyReadModel.Party>();
            //var imparties = new List<Entity.DSIS.MigrationEntity.imParty>();

            //using (var connection = new SqlConnection(ConnectionStringHelper.GetDsisMigrationString()))
            //{
            //    string sqlQuery = $"select * FROM imParty where CitizenShipNumber = '{tckimlik.Trim()}'";

            //    //string sqlQuery = "WITH ct AS(select ROW_NUMBER() OVER(PARTITION BY LeasingContractID ORDER BY LogType ASC, LogId ASC) rn, *from LeasingContract where LeasingContractID = 1445074) select Top 1 * from ct WHERE MigrationStatus <> 1 Order By rn ASC";

            //    imparties = connection.Query<Entity.DSIS.MigrationEntity.imParty>(sqlQuery).ToList();

            //}

            //foreach(var iparty in imparties)
            //{
            //    Microservice.Customer.Entity.PartyReadModel.Individual ind = new Microservice.Customer.Entity.PartyReadModel.Individual();
            //    ind.PartyId = iparty.StreamId;
            //    Microservice.Customer.Entity.PartyReadModel.Party party = new Microservice.Customer.Entity.PartyReadModel.Party(ind);

            //    parties.Add(party);

            //}

            //if (parties.Count() > 0)
            //    Console.WriteLine("buldum");

            //return parties;

        }

        private static LeasingContract GetNextNotMigratedSinglePayTvLeasingContract()
        {
            LeasingContract LeasingContract = new LeasingContract();
            using (var connection = new SqlConnection(ConnectionStringHelper.GetDsisMigrationString()))
            {
                string sqlQuery = "WITH ct AS(select ROW_NUMBER() OVER(PARTITION BY LeasingContractID ORDER BY LogType ASC, LogId ASC) rn, *from LeasingContract where LeasingContractID = (select top 1 LeasingContractID FROM LeasingContract where LeasingContractTypeID = 1 And MigrationStatus <> 1 and Datediff(MINUTE, LogDate, getdate()) > 1 order by MigrationStatus ASC,  logId ASC)) select Top 1 * from ct WHERE MigrationStatus <> 1 Order By rn ASC";

                //string sqlQuery = "WITH ct AS(select ROW_NUMBER() OVER(PARTITION BY LeasingContractID ORDER BY LogType ASC, LogId ASC) rn, *from LeasingContract where LeasingContractID = 1445074) select Top 1 * from ct WHERE MigrationStatus <> 1 Order By rn ASC";

                LeasingContract = connection.QueryFirstOrDefault<LeasingContract>(sqlQuery);

            }

            return LeasingContract;
        }


        private static Contract GetNextNotMigratedSingleInternetContract()
        {
            Contract contract = new Contract();
            using (var connection = new SqlConnection(ConnectionStringHelper.GetAnkaMigrationString()))
            {
                string sqlQuery = "WITH ct AS(select ROW_NUMBER() OVER(PARTITION BY ContractId ORDER BY LogType ASC, LogId ASC) rn, *from Contract where ContractID = (select top 1 ContractID FROM Contract c where NOT EXISTS(select 1 from ANKA.dbo.ContractDetail cd where cd.ContractId = c.ContractID AND cd.CatalogItemId = 5106)  And MigrationStatus<> 1 and Datediff(MINUTE, LogDate, getdate()) > 1 order by MigrationStatus ASC, logId ASC)) select Top 1 * from ct WHERE MigrationStatus <> 1 Order By rn ASC";

                //string sqlQuery = "WITH ct AS(select ROW_NUMBER() OVER(PARTITION BY LeasingContractID ORDER BY LogType ASC, LogId ASC) rn, *from LeasingContract where LeasingContractID = 1445074) select Top 1 * from ct WHERE MigrationStatus <> 1 Order By rn ASC";

                contract = connection.QueryFirstOrDefault<Contract>(sqlQuery);

            }

            return contract;
        }

        private static Contract GetNextNotMigratedBundleInternetContract()
        {
            Contract contract = new Contract();
            using (var connection = new SqlConnection(ConnectionStringHelper.GetAnkaMigrationString()))
            {
                string sqlQuery = "WITH ct AS(select ROW_NUMBER() OVER(PARTITION BY ContractId ORDER BY LogType ASC, LogId ASC) rn, *from Contract where ContractID = (select top 1 ContractID FROM Contract c where EXISTS(select 1 from ANKA.dbo.ContractDetail cd inner join ANKA.dbo.ContractDetailStatus cds on cds.ContractDetailStatusId = cd.ContractDetailStatusId and cds.SelectPriority = 1 where cd.ContractId = c.ContractID AND cd.CatalogItemId = 5106 and cd.ContractDetailStatusId NOT IN(80, 81)) And MigrationStatus<> 1 and Datediff(MINUTE, LogDate, getdate()) > 1 order by MigrationStatus ASC, logId ASC)) select Top 1 * from ct WHERE MigrationStatus <> 1 Order By rn ASC";

                //string sqlQuery = "WITH ct AS(select ROW_NUMBER() OVER(PARTITION BY LeasingContractID ORDER BY LogType ASC, LogId ASC) rn, *from LeasingContract where LeasingContractID = 1445074) select Top 1 * from ct WHERE MigrationStatus <> 1 Order By rn ASC";

                contract = connection.QueryFirstOrDefault<Contract>(sqlQuery);

            }

            return contract;
        }

        private static Microservice.Customer.Entity.DSIS.LegacyEntity.DsLeasingContract GetBundleDsisLeasingContract(int ankaContractId)
        {
            string leasingContractNumber = "";
            string sqlQuery = $"select cdsp.Value from anka.dbo.contractdetail cd inner join anka.dbo.ContractDetailServiceParam cdsp ON cdsp.ContractDetailId = cd.ContractDetailId where ServiceParamDetailId = 382 and contractId = {ankaContractId} and CatalogItemId = 5106";
            using (var connection = new SqlConnection(ConnectionStringHelper.GetAnkaLegacyString()))
            {
                leasingContractNumber = connection.QueryFirstOrDefault<string>(sqlQuery);
            }

            if(leasingContractNumber == "")
            {
                return null;
            }

            var result = new Microservice.Customer.Entity.DSIS.LegacyEntity.DsLeasingContract();

            string sqlQuery2 = $"select* from sms.dbo.dsLeasingContract where LeasingContractNumber = {leasingContractNumber}";
            using (var connection = new SqlConnection(ConnectionStringHelper.GetDsisLegacyString()))
            {
                result = connection.QueryFirstOrDefault<DsLeasingContract>(sqlQuery2);
            }

            return result;

        }


        private static LeasingContractService GetNextNotMigratedPayTvLeasingContractService()
        {
            LeasingContractService leasingContractService = new LeasingContractService();
            using (var connection = new SqlConnection(ConnectionStringHelper.GetDsisMigrationString()))
            {
                string sqlQuery = "sp_GetNextNotMigratedService;";

                leasingContractService = connection.QueryFirstOrDefault<LeasingContractService>(sqlQuery);
            }

            return leasingContractService;
        }

        private static ContractDetail GetNextNotMigratedInternetService()
        {
            ContractDetail contractDetail = new ContractDetail();
            using (var connection = new SqlConnection(ConnectionStringHelper.GetAnkaMigrationString()))
            {
                string sqlQuery = "sp_GetNextNotMigratedInternetService;";

                contractDetail = connection.QueryFirstOrDefault<ContractDetail>(sqlQuery);
            }

            return contractDetail;
        }

        

        private static Party GetParty(int partyId)
        {
            Party party = new Party();
            using (var connection = new SqlConnection(ConnectionStringHelper.GetDsisLegacyString()))
            {
                string sqlQuery = $"Select * FROM dsParty Where PartyId = {partyId.ToString()} ";
                party = connection.QueryFirstOrDefault<Party>(sqlQuery);
            }

            return party;
        }

        public static DsMusteri GetDsisMusteri(int partyID)
        {
            DsMusteri dsMusteri = new DsMusteri();
            string sqlQueryParty = "select * from dsMusteri where MusteriId = " + partyID;

            using (var connection = new SqlConnection(ConnectionStringHelper.GetDsisLegacyString()))
            {
                dsMusteri = connection.QueryFirstOrDefault<DsMusteri>(sqlQueryParty);
            }

            return dsMusteri;
        }


        public static DsProduct GetDsisProduct(int productId)
        {
            DsProduct dsProduct = new DsProduct();
            string sqlQueryParty = "select * from SMS.dbo.dsProduct where ProductID = " + productId;

            using (var connection = new SqlConnection(ConnectionStringHelper.GetDsisLegacyString()))
            {
                dsProduct = connection.QueryFirstOrDefault<DsProduct>(sqlQueryParty);
            }

            return dsProduct;
        }

        public static Microservice.Customer.Entity.ANKA.LegacyEntity.CatalogItem GetAnkaProduct(int catalogItemId)
        {
            Microservice.Customer.Entity.ANKA.LegacyEntity.CatalogItem dsProduct = new Microservice.Customer.Entity.ANKA.LegacyEntity.CatalogItem();
            string sqlQueryParty = "select * from ANKA.dbo.CatalogItem where CatalogItemId = " + catalogItemId;

            using (var connection = new SqlConnection(ConnectionStringHelper.GetAnkaLegacyString()))
            {
                dsProduct = connection.QueryFirstOrDefault<Microservice.Customer.Entity.ANKA.LegacyEntity.CatalogItem>(sqlQueryParty);
            }

            return dsProduct;
        }



        public static IEnumerable<AdapterCustomer> CustomerExistsInAdapterElastic(string dsisPartyId, string ankaCustomerId, IESRepository<AdapterCustomer> _elasticAdapterCustomer)
        {
            var searchModel = new Model.ReadModelEntity.BaseSearchModel();
            searchModel.Fields = new Dictionary<string, string>();

            if (dsisPartyId != "")
            {
                searchModel.Fields.Add("legacyDsisPartyID", dsisPartyId);
            }
            else
            {
                searchModel.Fields.Add("LegacyAnkaCustomerID", ankaCustomerId);
            }

            IEnumerable<AdapterCustomer> res = _elasticAdapterCustomer.Search(searchModel);

            return res;
        }


        public static IEnumerable<AdapterProduct> ProductExistsInAdapterElastic(string dsisLeasingContractServiceId, string ankaContractDetailId, IESRepository<AdapterProduct> _elasticAdapterProduct)
        {
            var searchModel = new Model.ReadModelEntity.BaseSearchModel();
            searchModel.Fields = new Dictionary<string, string>();

            if (dsisLeasingContractServiceId != "")
            {
                searchModel.Fields.Add("legacyDsisLeasingContractServiceID", dsisLeasingContractServiceId);
            }
            else
            {
                searchModel.Fields.Add("legacyAnkaContractDetailID", ankaContractDetailId);
            }

            IEnumerable<AdapterProduct> res = _elasticAdapterProduct.Search(searchModel);

            return res;
        }

        public static IEnumerable<AdapterCustomerAccount> CustomerAccountExistsInAdapterElastic(string dsisLeasingContractId, string ankaContractId, IESRepository<AdapterCustomerAccount> _elasticAdapterCustomerAccount)
        {
            var searchModel = new Model.ReadModelEntity.BaseSearchModel();
            searchModel.Fields = new Dictionary<string, string>();

            if (dsisLeasingContractId != "")
            {
                searchModel.Fields.Add("legacyDsisLEasingContractID", dsisLeasingContractId);
            }
            else
            {
                searchModel.Fields.Add("legacyAnkaContractID", ankaContractId);
            }

            var result = _elasticAdapterCustomerAccount.Search(searchModel);

            return result;
        }

        public static IEnumerable<AdapterBillingAccount> BillingAccountExistsInAdapterElastic(string dsisLeasingContractId, string ankaContractId, IESRepository<AdapterBillingAccount> _elasticAdapterBillingAccount)
        {
            var searchModel = new Model.ReadModelEntity.BaseSearchModel();
            searchModel.Fields = new Dictionary<string, string>();

            if (dsisLeasingContractId != "")
            {
                searchModel.Fields.Add("legacyDsisLEasingContractID", dsisLeasingContractId);
            }
            else
            {
                searchModel.Fields.Add("legacyAnkaContractID", ankaContractId);
            }

            var result = _elasticAdapterBillingAccount.Search(searchModel);

            return result;
        }


        private static void Build()
        {
            services.AddScoped<ServiceFactory>(p => p.GetService);
            services.AddScoped(typeof(ICustomer), typeof(MicroServices.Customer.Domain.Customer));
            services.AddScoped(typeof(ICustomerAccount), typeof(MicroServices.Customer.Domain.CustomerAccount));
            services.AddScoped(typeof(IESRepository<>), typeof(ReadModelRepository<>));
            services.AddScoped(typeof(IEventStoreRepository), typeof(WriteModelRepository));
            services.AddMediatR(typeof(CustomerCommandHandler));
            services.Scan(scan => scan
                .FromAssembliesOf(typeof(IMediator))
                .AddClasses()
               .AsImplementedInterfaces());


        }

    }
}
