﻿using Camunda.Api.Client;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MicroService.Customer.BPM
{
    public class CamundaWorkerTask : BackgroundService
    {
        private readonly CamundaClient _camundaClient;
        public CamundaWorkerTask(CamundaClient camundaClient)
        {
            _camundaClient = camundaClient;
        }

        private IList<ExternalTaskWorker> _workers = new List<ExternalTaskWorker>();
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                var externalTaskWorkers = RetrieveExternalTaskWorkerInfo();

                foreach (var taskWorkerInfo in externalTaskWorkers)
                {
                    ExternalTaskWorker worker = new ExternalTaskWorker(_camundaClient, taskWorkerInfo);
                    _workers.Add(worker);
                    worker.StartWork();
                }

                await Task.Delay(-1, stoppingToken);
            }
            catch (OperationCanceledException)
            {
                foreach (ExternalTaskWorker worker in _workers)
                {
                    worker.StopWork();
                }
            }
        }

        private IEnumerable<ExternalTaskWorkerInfo> RetrieveExternalTaskWorkerInfo()
        {
            // find all classes with CustomAttribute [ExternalTask("name")]
            var externalTaskWorkers =
                from t in GetLoadableTypes(Assembly.GetExecutingAssembly())
                let externalTaskTopicAttribute = t.GetCustomAttributes(typeof(ExternalTaskTopicAttribute), true).FirstOrDefault() as ExternalTaskTopicAttribute
                let externalTaskVariableRequirements = t.GetCustomAttributes(typeof(ExternalTaskVariableRequirementsAttribute), true).FirstOrDefault() as ExternalTaskVariableRequirementsAttribute
                where externalTaskTopicAttribute != null
                select new ExternalTaskWorkerInfo
                {
                    Type = t,
                    TopicName = externalTaskTopicAttribute.TopicName,
                    Retries = externalTaskTopicAttribute.Retries,
                    RetryTimeout = externalTaskTopicAttribute.RetryTimeout,
                    LockDuration = externalTaskTopicAttribute.LockDuration,
                    MaxTasks = externalTaskTopicAttribute.MaxTasks,
                    MaxDegreeOfParallelism = externalTaskTopicAttribute.MaxDegreeOfParallelism,
                    VariablesToFetch = externalTaskVariableRequirements?.VariablesToFetch,
                    TaskAdapter = t.GetConstructor(new[] { _camundaClient.GetType() })?.Invoke(new[] { _camundaClient }) as IExternalTaskAdapter
                };

            return externalTaskWorkers;
        }

        private IEnumerable<Type> GetLoadableTypes(Assembly assembly)
        {
            try
            {
                return assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException e)
            {
                return e.Types.Where(t => t != null);
            }
        }
    }
}
