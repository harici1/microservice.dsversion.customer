﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.BusinessInteraction.BPM.Dto
{
    public class ExternalTaskWorkerInfo
    {
        public int Retries { get; set; }
        public long RetryTimeout { get; set; }
        public long LockDuration { get; set; }
        public int MaxTasks { get; set; }
        public int MaxDegreeOfParallelism { get; set; }
        public Type Type { get; set; }
        public string TopicName { get; set; }
        public List<string> VariablesToFetch { get; set; }
        public IExternalTaskAdapter TaskAdapter { get; set; }
    }
}
