﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.BPM
{
    [System.AttributeUsage(System.AttributeTargets.Class |
                              System.AttributeTargets.Struct |
                               System.AttributeTargets.Method)
       ]
    public sealed class ExternalTaskTopicAttribute : System.Attribute
    {
        public string TopicName { get; }
        public int Retries { get; } = 3; // default: 5 times
        public long RetryTimeout { get; } = 60 * 1000; // default: 10 seconds
        public long LockDuration { get; } = 60 * 1000; // 1 minute
        public int MaxTasks { get; } = 30;
        public int MaxDegreeOfParallelism { get; } = 5;

        public ExternalTaskTopicAttribute(string topicName)
        {
            TopicName = topicName;
        }

        public ExternalTaskTopicAttribute(string topicName, long lockDuration)
        {
            TopicName = topicName;
            LockDuration = lockDuration;
        }

        public ExternalTaskTopicAttribute(string topicName, int retries, long retryTimeout, long lockDuration, int maxTasks, int maxDegreeOfParallelism)
        {
            TopicName = topicName;
            Retries = retries;
            RetryTimeout = retryTimeout;
            LockDuration = lockDuration;
            MaxTasks = maxTasks;
            MaxDegreeOfParallelism = maxDegreeOfParallelism;
        }
    }
}
