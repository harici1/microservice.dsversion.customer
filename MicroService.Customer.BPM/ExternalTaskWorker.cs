﻿using Camunda.Api.Client;
using Camunda.Api.Client.ExternalTask;
using MicroService.Customer.BPM.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MicroService.Customer.BPM
{
    class ExternalTaskWorker
    {
        private Timer taskQueryTimer;
        private long pollingIntervalInMilliseconds = 1 * 5000; // every second
        private ExternalTaskWorkerInfo _taskWorkerInfo;
        private CamundaClient _camundaClient;

        public ExternalTaskWorker(CamundaClient camundaClient, ExternalTaskWorkerInfo taskWorkerInfo)
        {
            _camundaClient = camundaClient;
            _taskWorkerInfo = taskWorkerInfo;
        }

        public async void DoPolling()
        {
            // Query External Tasks
            try
            {
                var topic = new FetchExternalTaskTopic(_taskWorkerInfo.TopicName, _taskWorkerInfo.LockDuration);
                topic.Variables = _taskWorkerInfo.VariablesToFetch;

                var tasks = await _camundaClient.ExternalTasks.FetchAndLock(new FetchExternalTasks { MaxTasks = _taskWorkerInfo.MaxTasks, WorkerId = "1", Topics = new List<FetchExternalTaskTopic> { topic } });

                Parallel.ForEach(
                    tasks,
                    new ParallelOptions { MaxDegreeOfParallelism = _taskWorkerInfo.MaxDegreeOfParallelism },
                    async externalTask => await Execute(externalTask)
                );
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
            }

            if (taskQueryTimer != null)
                taskQueryTimer.Change(pollingIntervalInMilliseconds, Timeout.Infinite);
        }

        private async Task Execute(LockedExternalTask externalTask)
        {
            //var logger = LogManager.GetLogger("Camunda client logger");

            Dictionary<string, object> resultVariables = new Dictionary<string, object>();

            //logger.Info($"Execute External Task from topic '{_taskWorkerInfo.TopicName}': {externalTask}...");
            try
            {
                await _taskWorkerInfo.TaskAdapter.Execute(externalTask);
                //logger.Info($"...finished External Task {externalTask.Id}");
                //externalTaskService.Complete(workerId, externalTask.Id, resultVariables);
            }
            //catch (UnrecoverableException ex)
            //{
            //    logger.Error($"...failed unrecoverable External Task  {externalTask.Id}", ex);
            //    externalTaskService.Error(workerId, externalTask.Id, ex.BusinessErrorCode);
            //}
            catch (Exception ex)
            {
                //logger.Error($"...failed External Task  {externalTask.Id}", ex);
                //var retriesLeft = taskWorkerInfo.Retries; // start with default
                //if (externalTask.Retries.HasValue) // or decrement if retries are already set
                //{
                //    retriesLeft = externalTask.Retries.Value - 1;
                //}
                //externalTaskService.Failure(workerId, externalTask.Id, ex.Message, retriesLeft, taskWorkerInfo.RetryTimeout);
            }
        }

        public void StartWork()
        {
            this.taskQueryTimer = new Timer(_ => DoPolling(), null, pollingIntervalInMilliseconds, Timeout.Infinite);
        }

        public void StopWork()
        {
            this.taskQueryTimer.Dispose();
        }
    }
}
