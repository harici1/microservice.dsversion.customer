﻿using Camunda.Api.Client.ExternalTask;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Customer.BPM
{
    public interface IExternalTaskAdapter
    {
        Task Execute(LockedExternalTask externalTask);

    }
}
