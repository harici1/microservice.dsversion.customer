﻿using Camunda.Api.Client;
using MediatR;
using MicroService.Customer.Data.ReadModel.Repository;
using MicroService.Customer.Data.WriteModel.Repository;
using MicroService.Customer.Service.Handler;
using MicroServices.Customer.Domain.Implemantation;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace MicroService.Customer.BPM
{
    class Program
    {
        static ServiceCollection services = new ServiceCollection();
        static async void Main(string[] args)
        {
            services.AddScoped<ServiceFactory>(p => p.GetService);
            services.AddScoped(typeof(ICustomer), typeof(MicroServices.Customer.Domain.Customer));
            services.AddScoped(typeof(IESRepository<>), typeof(ReadModelRepository<>));
            services.AddScoped(typeof(IEventStoreRepository), typeof(WriteModelRepository));
            services.AddMediatR(typeof(CustomerCommandHandler));
            services.Scan(scan => scan
                .FromAssembliesOf(typeof(IMediator))
                .AddClasses()
               .AsImplementedInterfaces());
            var client = CamundaClient.Create("http://172.21.1.83:8080/engine-rest");

            services.AddSingleton(client);

            services.AddHostedService<CamundaWorkerTask>();
           

            var host = new HostBuilder()
                            .UseConsoleLifetime()
                           .Build();

            using (host)
            {
                // Start the host
                await host.StartAsync();

                // Wait for the host to shutdown
                await host.WaitForShutdownAsync();
            }

        }
    }
}

