﻿using Camunda.Api.Client;
using Camunda.Api.Client.ExternalTask;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Customer.BPM.Tasks
{
    [ExternalTaskTopic("ActiveService", 86400000)]
    public class ActivateServiceTask : IExternalTaskAdapter
    {
        private readonly CamundaClient _camundaClient;
        private readonly IMediator mediator;
        public ActivateServiceTask(CamundaClient camundaClient, IMediator mediator)
        {
            _camundaClient = camundaClient;
        }

        public async Task Execute(LockedExternalTask externalTask)
        {
            List<IRequest<bool>> events = new List<IRequest<bool>>();

            //Service create event'i oluştur.
            CreatePayTvServiceCommand createPayTvCommand = new CreatePayTvServiceCommand();

            createPayTvCommand.ServiceId = Guid.NewGuid();
            if (externalTask.Variables.ContainsKey("ServiceCompositeId"))
                createPayTvCommand.ServiceCompositeId = Convert.ToInt32(externalTask.Variables["ServiceCompositeId"].Value);
            else
                createPayTvCommand.ServiceCompositeId = 1000000;

            createPayTvCommand.LegacySystemParameters = new System.Collections.Generic.Dictionary<string, string>();
            createPayTvCommand.LegacySystemParameters.Add("OriginSystem", "Camunda");

            //external task property'lerini extra variables içine bas.
            foreach (var item in externalTask.GetType().GetProperties())
            {
                if (item.GetValue(externalTask, null) != null)
                {
                    createPayTvCommand.ExtraVariables.Add(item.Name, item.GetValue(externalTask, null).ToString());
                }
            }
            //create event'ini ekle.
            events.Add(createPayTvCommand);

            //service statüsünü aktife çekmek için event oluştur.
            ChangeStatusPayTvServiceCommand changeStatusPayTvCommand = new ChangeStatusPayTvServiceCommand();

            changeStatusPayTvCommand.ServiceId = createPayTvCommand.ServiceId;
            changeStatusPayTvCommand.ServiceStatusId = 1; //Select Model Handler'da 1 Aktif 
            changeStatusPayTvCommand.StatusChangeDate = DateTime.Now;

            //status change event'ini ekle.
            events.Add(changeStatusPayTvCommand);

            //Tüm event'leri mediator'a gönder.
            foreach (var eventt in events)
            {
                await mediator.Send(eventt);
            }

            await _camundaClient.ExternalTasks[externalTask.Id].Complete(new CompleteExternalTask { WorkerId = externalTask.WorkerId });
        }
    }
}
