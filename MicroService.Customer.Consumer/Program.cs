﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using MicroService.Customer.Consumer.Models;
using MicroService.Customer.Consumer.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Nest;
using System;
using System.Threading.Tasks;

namespace MicroService.Customer.Consumer
{
    // Read from Eventstore - Copying event data into created persistent subscription - Transfering data into read model (elasticsearch).
    public class Program
    {
        static async Task Main(string[] args)
        {
            var host = new HostBuilder()
               .ConfigureLogging((hostContext, config) =>
               {
                   config.AddConsole();
                   config.AddDebug();
               })
               .ConfigureHostConfiguration(config =>
               {
                   config.AddEnvironmentVariables();
               })
               .ConfigureAppConfiguration((hostContext, config) =>
               {
                   config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                   config.AddJsonFile($"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json", optional: true, reloadOnChange: true);
                   config.AddCommandLine(args);
               })
               .UseServiceProviderFactory(new AutofacServiceProviderFactory())
               .ConfigureContainer<ContainerBuilder>((hostContext, autofactProvider) =>
               {
                   var services = new ServiceCollection();

                   services.AddElasticsearch(hostContext.Configuration);

                   services.AddLogging();

                   services.AddHostedService<CustomerConsumerTask>();

                   autofactProvider.Populate(services);
               })
               .UseConsoleLifetime()
               .Build();


            using (host)
            {
                // Start the host
                await host.StartAsync();

                // Wait for the host to shutdown
                await host.WaitForShutdownAsync();
            }
        }
    }

    static class CustomExtensionsMethods
    {
        public static void AddElasticsearch(
        this IServiceCollection services, IConfiguration configuration)
        {
            var url = configuration["elasticsearch:url"];
            var defaultIndex = configuration["elasticsearch:index"];

            var settings = new ConnectionSettings(new Uri(url))
            .DefaultIndex(defaultIndex)
            .DefaultMappingFor<MicroServices.Customer.Domain.Customer>(m => m
            .IndexName("test_customer")
                .IdProperty(p => p.Id))
            .DefaultMappingFor<EventQueueModel>(m => m
                .IndexName("test_customer_eventqueue")
                .IdProperty(p => p.EventId)
            );

            var client = new ElasticClient(settings);

            services.AddSingleton<IElasticClient>(client);
        }
    }
}

