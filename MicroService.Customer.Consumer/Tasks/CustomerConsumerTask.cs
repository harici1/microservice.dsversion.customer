﻿using Elasticsearch.Net;
using MicroService.Customer.Consumer.Models;
using MicroService.Customer.Model.CommandModel;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace MicroService.Customer.Consumer.Tasks
{
    internal class CustomerConsumerTask : BackgroundService
    {
        //private readonly IMediator _mediator;
        private readonly ILogger _logger;
        private readonly IElasticClient _elasticClient;
        public CustomerConsumerTask(ILogger<CustomerConsumerTask> logger, IElasticClient elasticClient)
        {
            //_mediator = mediator;
            _logger = logger;
            _elasticClient = elasticClient;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {

                    var events = await _elasticClient.SearchAsync<EventQueueModel>(s => s
                                .Query(q => q
                                .MatchPhrase(mp => mp
                                .Field(new Field("statusList.SelectModelConsumer"))
                                .Query(((int)EventQueueIndexStatus.Waiting).ToString())
                                ) || !q
                                .Exists(e => e.Field(new Field("statusList.SelectModelConsumer")))));

                    if (events.Documents.Any())
                    {
                        events.Documents.ToList().ForEach(x =>
                        {
                            if (!x.StatusList.ContainsKey("SelectModelConsumer"))
                                x.StatusList.Add("SelectModelConsumer", (int)EventQueueIndexStatus.InProgress);
                            else
                                x.StatusList["SelectModelConsumer"] = (int)EventQueueIndexStatus.InProgress;
                        });


                        await _elasticClient.BulkAsync(x => x
                                .UpdateMany(events.Documents, (t, p) => t.Doc(p))
                        );

                        foreach (var item in events.Documents)
                        {
                            try
                            {
                                ConsumeEvent(item);

                                item.StatusList["SelectModelConsumer"] = (int)EventQueueIndexStatus.Success;
                                item.LastProcessedDate = DateTime.Now; //DateTime.Now.ToString(CultureInfo.InvariantCulture);
                            }
                            catch (Exception ex)
                            {
                                item.StatusList["SelectModelConsumer"] = (int)EventQueueIndexStatus.Error;
                                item.LastProcessedDate = DateTime.Now; //DateTime.Now.ToString(CultureInfo.InvariantCulture);
                            }
                            await _elasticClient.UpdateAsync<EventQueueModel>(item, u => u.Doc(item));
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                }
            }
        }

        private void ConsumeEvent(EventQueueModel eventQueue)
        {
            try
            {
                var customer = new MicroServices.Customer.Domain.Customer();
                string data = eventQueue.EventData;
                switch (eventQueue.Type)
                {
                    case "CustomerCreated":
                        CreateCustomerCommand ic = JsonConvert.DeserializeObject<CreateCustomerCommand>(data);
                        customer.CreateCustomer(ic);
                        break;
                    case "BillingAccountAdded":
                        AddCustomerBillingAccountProductCommand oc = JsonConvert.DeserializeObject<AddCustomerBillingAccountProductCommand>(data);
                        customer.AddBillingAccount(oc);
                        break;
                }

                var indexResponse = _elasticClient.IndexDocument<MicroServices.Customer.Domain.Customer>(customer);
                
                _logger.LogInformation("EventType {0}, Data {1}", eventQueue.Type, eventQueue.EventData);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }
        }
    }
}
