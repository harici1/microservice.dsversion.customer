﻿using Elasticsearch.Net;
using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Data.ReadModel.WriteModelHelper
{
    public class ElasticSearchClientHelper
    {
        private readonly string _connectionString;
        public ElasticSearchClientHelper(string connectionString)
        {
            _connectionString = connectionString;
        }
        public ElasticClient CreateElasticClient()
        {
            var node = new SingleNodeConnectionPool(new Uri(_connectionString));
            var settings = new ConnectionSettings(node);
            return new ElasticClient(settings);
        }

        public void CheckIndex<T>(ElasticClient client, string indexName) where T : class
        {
            var response = client.Indices.Exists(indexName);
            if (!response.Exists)
            {
                var responsse = client.Indices.Create(indexName, index =>
                   index.Map<T>(x => x.AutoMap()));
            }
        }
    }
}
