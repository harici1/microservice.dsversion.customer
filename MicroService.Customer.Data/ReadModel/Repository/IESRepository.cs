﻿using MicroService.Customer.Model.ReadModelEntity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Customer.Data.ReadModel.Repository
{
    public interface IESRepository<T> where T : class
    {
        string Save(T entity);
        T Get(Guid id);
        IEnumerable<T> Search(BaseSearchModel request);
        bool Delete(Guid id);
        void Update(T entity);

        IReadOnlyCollection<T> SelectBySize(int from, int size);
    }
}
