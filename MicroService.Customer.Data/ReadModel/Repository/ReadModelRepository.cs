﻿using MicroService.Customer.Data.ReadModel.WriteModelHelper;
using MicroService.Customer.Model.ReadModelEntity;
using Nest;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MicroService.Customer.Data.ReadModel.Repository
{
    public class ReadModelRepository<T> : IESRepository<T> where T : class
    {
        private readonly ElasticClient _elasticClient;
        private readonly ElasticSearchClientHelper _elasticClientHelper;
        private readonly string _indexName;

        public ReadModelRepository()
        {
            _elasticClient = new ElasticSearchClientHelper(AppSettings.ESConnectionString).CreateElasticClient();
            _elasticClientHelper = new ElasticSearchClientHelper(AppSettings.ESConnectionString);
            _indexName = typeof(T).Name.ToLower();
        }

        public T Get(Guid id)
        {
            var result = _elasticClient.Get<T>(id.ToString(), idx => idx.Index(_indexName));
            if (!result.IsValid)
            {
                throw new Exception(result.OriginalException.Message);
            }
            return result.Source;
        }

        public IReadOnlyCollection<T> SelectBySize(int from, int size)
        {
            var searchResponse = _elasticClient.Search<T>(s => s.Query(q => q.MatchAll()).From(from).Size(size).Index(_indexName));
            return searchResponse.Documents;
        }


        public string Save(T entity)
        {
            _elasticClientHelper.CheckIndex<T>(_elasticClient, _indexName);

            //entity.Id = Guid.NewGuid();
            var result = _elasticClient.Index(entity, idx => idx.Index(_indexName).Refresh(Elasticsearch.Net.Refresh.True));
            
            
            if (!result.IsValid)
            {
                throw new Exception(result.OriginalException.Message);
            }
            return result.Id;
        }

        public IEnumerable<T> Search(BaseSearchModel request)
        {
            var dynamicQuery = new List<QueryContainer>();
            foreach (var item in request.Fields)
            {
                dynamicQuery.Add(Query<T>.Term(new Field(item.Key), item.Value.ToString()));
            }
            var result = _elasticClient.Search<T>(s => s
                                       .From(request.From)
                                       .Size(request.Size == 0 ? 10 : request.Size)
                                       .Index(_indexName)
                                       .Query(q => q.Bool(b => b.Must(dynamicQuery.ToArray()))));

            if (!result.IsValid)
            {
                throw new Exception(result.OriginalException.Message);
            }
            return result.Documents;
            
        }
        public bool Delete(Guid id)
        {
            var result = _elasticClient.Delete<T>(id.ToString(), idx => idx.Index(_indexName));
            if (!result.IsValid)
            {
                throw new Exception(result.OriginalException.Message);
            }
            return result.IsValid;
        }
        public void Update(T entity)
        {
            var result = _elasticClient.Update(new DocumentPath<T>(entity), u => u.Doc(entity).Index(_indexName));
            if (!result.IsValid)
            {
                throw new Exception(result.OriginalException.Message);
            }
        }

    }
}
