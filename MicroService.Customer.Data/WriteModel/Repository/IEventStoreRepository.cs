﻿using EventStore.ClientAPI;
using MicroServices.Customer.Model.EventModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Data.WriteModel.Repository
{
    public interface IEventStoreRepository
    {
        List<WriteResult> Commit(IEnumerable<Event> events);
    }
}
