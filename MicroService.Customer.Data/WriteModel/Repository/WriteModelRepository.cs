﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using EventStore.ClientAPI;
using MicroService.Customer.Data.WriteModel.WriteModelHelper;
using MicroService.Customer.Model.EventModel;
using MicroServices.Customer.Model.EventModel;
using Newtonsoft.Json;

namespace MicroService.Customer.Data.WriteModel.Repository
{
    public class WriteModelRepository : IEventStoreRepository
    {
        public List<WriteResult> Commit(IEnumerable<Event> events)
        {
            var writeResult = new List<WriteResult>();
            foreach (var eventItem in events)
            {
                var eventName = eventItem.GetType().Name;
                var eventType = eventItem.GetType().GetCustomAttributes(true)[0];

                var eventDataString = JsonConvert.SerializeObject(eventItem);

                if (eventType is CustomerEventAttribute)
                {
                    var CustomerIdProperty = eventItem.GetType().GetProperty("CustomerId");
                    var CustomerId = CustomerIdProperty.GetValue(eventItem);

                    var result = WriteCustomerEvent(CustomerId.ToString(), eventName, eventDataString);
                    writeResult.Add(result);
                }

                if (eventType is CustomerAccountEventAttribute)
                {
                    var CustomerAccountIdProperty = eventItem.GetType().GetProperty("CustomerAccountId");
                    var CustomerAccountId = CustomerAccountIdProperty.GetValue(eventItem);

                    var result = WriteCustomerAccountEvent(CustomerAccountId.ToString(), eventName, eventDataString);
                    writeResult.Add(result);
                }

            }

            return writeResult;
        }
        private WriteResult WriteCustomerEvent(string id, string eventType, string eventData)
        {
            var conn = EventStoreConnectionHelper.Instance;

            //var list = conn.ReadStreamEventsForwardAsync("$streams", 0, 100, false).Result;
            //foreach (var item in list.Events)
            //{
            //    Console.WriteLine(Encoding.UTF8.GetString(item.Event.Data));
            //}
            var streamName = "Customer_Test13-" + id;
          
            var myEvent = new EventData(Guid.NewGuid(), eventType, false, Encoding.UTF8.GetBytes(eventData), Encoding.UTF8.GetBytes(eventData));

            var result = conn.AppendToStreamAsync(streamName, ExpectedVersion.Any, myEvent).Result;

            return result;
        }

        private WriteResult WriteCustomerAccountEvent(string id, string eventType, string eventData)
        {
            var conn = EventStoreConnectionHelper.Instance;

            //var list = conn.ReadStreamEventsForwardAsync("$streams", 0, 100, false).Result;
            //foreach (var item in list.Events)
            //{
            //    Console.WriteLine(Encoding.UTF8.GetString(item.Event.Data));
            //}
            var streamName = "CustomerAccount_Test13-" + id;

            var myEvent = new EventData(Guid.NewGuid(), eventType, false, Encoding.UTF8.GetBytes(eventData), Encoding.UTF8.GetBytes(eventData));

            var result = conn.AppendToStreamAsync(streamName, ExpectedVersion.Any, myEvent).Result;

            return result;
        }
    }
}
