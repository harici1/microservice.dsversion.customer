﻿using EventStore.ClientAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Data.WriteModel.WriteModelHelper
{
    public sealed class EventStoreConnectionHelper
    {
        private static readonly Lazy<IEventStoreConnection> lazy = new Lazy<IEventStoreConnection>(() =>
        {
            var conn = EventStoreConnection.Create(new Uri("tcp://altar:4rfv5TGB6yhn7UJM@172.21.1.83:1113"));
            conn.ConnectAsync().Wait();
            return conn;
        });

        public static IEventStoreConnection Instance { get { return lazy.Value; } }

        public EventStoreConnectionHelper()
        {

        }

    }
}
