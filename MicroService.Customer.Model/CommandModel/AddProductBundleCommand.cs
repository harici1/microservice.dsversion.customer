﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Model.CommandModel
{
    public class AddProductBundleCommand : BaseCommand, IRequest<BaseCommandResult>
    {
        public Guid ProductBundleId { get; set; }

        public Guid CustomerAccountId { get; set; }

        public Guid BillingAccountId { get; set; }

        public Guid? ParentProductBundleId { get; set; }

        public Guid ProductOfferId { get; set; }

    }
}
