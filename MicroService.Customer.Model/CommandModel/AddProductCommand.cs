﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Model.CommandModel
{
    public class AddProductCommand : BaseCommand, IRequest<BaseCommandResult>
    {
        public Guid ProductId { get; set; }
        public Guid ProductBundleId { get; set; }

        public Guid CustomerAccountId { get; set; }

        public string ProductName { get; set; }
    }
}
