﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Model.CommandModel
{
    public class BaseCommand
    {
        public DateTime? CreationDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public string LastUpdatedBy { get; set; }

        public Dictionary<string, string> LegacyData { get; set; }
    }
}
