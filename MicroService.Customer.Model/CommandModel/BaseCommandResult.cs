﻿using MicroServices.Customer.Model.EventModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Model.CommandModel
{
    public class BaseCommandResult
    {
        public string Id { get; set; }
        public string Status { get; set; }
    }
}
