﻿using MediatR;
using System;

namespace MicroService.Customer.Model.CommandModel
{
    public class ChangeProductBundleCommand : BaseCommand, IRequest<BaseCommandResult>
    {
        public Guid ProductBundleId { get; set; }

        public Guid CustomerAccountId { get; set; }

        public Guid BillingAccountId { get; set; }

        public Guid? ParentProductBundleId { get; set; }

        public Guid ProductOfferId { get; set; }

    }
}
