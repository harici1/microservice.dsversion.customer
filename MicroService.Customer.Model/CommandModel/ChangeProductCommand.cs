﻿using MediatR;
using System;

namespace MicroService.Customer.Model.CommandModel
{
    public class ChangeProductCommand : BaseCommand, IRequest<BaseCommandResult>
    {
        public Guid ProductBundleId { get; set; }

        public Guid CustomerAccountId { get; set; }

        public Guid ProductId { get; set; }

        public string ProductName { get; set; }
    }
}
