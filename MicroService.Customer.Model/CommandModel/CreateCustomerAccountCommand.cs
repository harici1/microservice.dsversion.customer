﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Model.CommandModel
{
    public class CreateCustomerAccountCommand : BaseCommand, IRequest<BaseCommandResult>
    {
        public Guid CustomerId { get; set; }
        public Guid CustomerAccountId { get; set; }
        public string ContractNumber { get; set; } // string?
        public Guid AccountContactId { get; set; } // partyRoleId

    }
}
