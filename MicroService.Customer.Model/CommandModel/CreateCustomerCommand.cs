﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Model.CommandModel
{
    public class CreateCustomerCommand : BaseCommand, IRequest<BaseCommandResult>
    {
        public Guid CustomerId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime? BirthDate { get; set; }
    }
}
