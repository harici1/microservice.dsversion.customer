﻿using MicroServices.Customer.Model.EventModel;
using System;

namespace MicroService.Customer.Model.EventModel
{
    [CustomerEvent]
    public class BillingAccountAdded : Event
    {
        public Guid BillingAccountId { get; set; }
        public string DistrictName { get; set; }
        public string ProvinceName { get; set; }
        public string CountryName { get; set; }
        public string PostCode { get; set; }
        public string InvoiceTitle { get; set; }
        public string InvoiceTaxOffice { get; set; }
        public string InvoiceTaxNumber { get; set; }
        public string Street { get; set; }

        public string MobilePhone { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public bool? IsEInvoiceRequested { get; set; }
        public bool? IsEnvelopeRequested { get; set; }
        public bool? IsTaxFree { get; set; }

        public Guid CustomerId { get; set; }

    }
}
