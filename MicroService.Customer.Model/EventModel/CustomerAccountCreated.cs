﻿using MicroServices.Customer.Model.EventModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Model.EventModel
{
    [CustomerAccountEvent]
    public class CustomerAccountCreated : Event
    {
        public Guid CustomerId { get; set; }
        public Guid CustomerAccountId { get; set; }
        public string ContractNumber { get; set; }
        public Guid AccountContactId { get; set; }

    }
}
