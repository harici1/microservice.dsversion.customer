﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroServices.Customer.Model.EventModel
{
    public class Message
    {

    }

    public class Event : Message
    {
        public int Version;
        public DateTime? CreationDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public string LastUpdatedBy { get; set; }

        public Dictionary<string, string> LegacyData { get; set; }
    }


}
