﻿using MicroServices.Customer.Model.EventModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Model.EventModel
{
    [CustomerAccountEvent]
    public class ProductAdded : Event
    {
        public Guid ProductId { get; set; }
        public Guid ProductBundleId { get; set; }
        public Guid CustomerAccountId { get; set; }

        public string ProductName { get; set; }
    }
}
