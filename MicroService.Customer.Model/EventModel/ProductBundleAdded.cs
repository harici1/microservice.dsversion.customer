﻿using MicroServices.Customer.Model.EventModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Model.EventModel
{
    [CustomerAccountEvent]
    public class ProductBundleAdded : Event
    {
        public Guid ProductBundleId { get; set; }
        public Guid CustomerAccountId { get; set; }
        public Guid BillingAccountId { get; set; }

        public Guid? ParentProductBundleId { get; set; }
    }
}
