﻿using MicroServices.Customer.Model.EventModel;
using System;

namespace MicroService.Customer.Model.EventModel
{
    [CustomerAccountEvent]
    public class ProductBundleChanged : Event
    {
        public Guid ProductBundleId { get; set; }
        public Guid CustomerAccountId { get; set; }
        public Guid BillingAccountId { get; set; }

        public Guid? ParentProductBundleId { get; set; }
    }
}
