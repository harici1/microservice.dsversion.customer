﻿using MicroServices.Customer.Model.EventModel;
using System;

namespace MicroService.Customer.Model.EventModel
{
    [CustomerAccountEvent]
    public class ProductChanged : Event
    {
        public Guid ProductId { get; set; }
        public Guid ProductBundleId { get; set; }
        public Guid CustomerAccountId { get; set; }

        public string ProductName { get; set; }
    }
}
