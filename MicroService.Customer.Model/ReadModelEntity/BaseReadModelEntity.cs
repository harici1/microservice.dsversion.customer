﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Model.ReadModelEntity
{
    public class BaseReadModelEntity
    {
        public Guid Id { get; set; }
    }
}
