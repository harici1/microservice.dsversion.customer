﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Customer.Model.WriteModelEntity
{
    public class BaseWriteModelEntity
    {
        public Guid EventId { get; set; }
        public string EventName { get; set; }
        public string StreamName { get; set; }
        public string Data { get; set; }
        public string MetaData { get; set; }
    }
}
