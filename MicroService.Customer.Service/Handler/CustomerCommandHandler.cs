﻿using MediatR;
using MicroService.Customer.Data.ReadModel.Repository;
using MicroService.Customer.Data.WriteModel.Repository;
using MicroService.Customer.Model.CommandModel;
using MicroService.Customer.Model.EventModel;
using MicroService.Customer.Model.ReadModelEntity;
using MicroServices.Customer.Domain;
using MicroServices.Customer.Domain.Implemantation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MicroService.Customer.Service.Handler
{
    public class CustomerCommandHandler : IRequestHandler<CreateCustomerCommand, BaseCommandResult>, IRequestHandler<AddCustomerBillingAccountProductCommand, BaseCommandResult>, IRequestHandler<CancelProductBundleCommand, BaseCommandResult>, IRequestHandler<CancelProductCommand, BaseCommandResult>, IRequestHandler<CreateAccountContactCommand, BaseCommandResult>, IRequestHandler<CreateCustomerAccountCommand, BaseCommandResult>, IRequestHandler<CreateCustomerBillingAccountCommand, BaseCommandResult>, IRequestHandler<AddProductBundleCommand, BaseCommandResult>, IRequestHandler<ChangeProductBundleCommand, BaseCommandResult>, IRequestHandler<RemoveCustomerBillingAccountProductCommand, BaseCommandResult>, IRequestHandler<AddProductCommand, BaseCommandResult>, IRequestHandler<ChangeProductCommand, BaseCommandResult>
    {
        private readonly IEventStoreRepository _writeModelRepository;
        private readonly IESRepository<MicroServices.Customer.Domain.Customer> _readModelRepository;
        private readonly ICustomer _customerDomain;
        private readonly ICustomerAccount _customerAccountDomain;
        public CustomerCommandHandler(IEventStoreRepository writeModelRepository, ICustomer customerDomain, IESRepository<MicroServices.Customer.Domain.Customer> readModelRepository)
        {
            _writeModelRepository = writeModelRepository;
            _customerDomain = new MicroServices.Customer.Domain.Customer();
            _readModelRepository = readModelRepository;
            _customerAccountDomain = new MicroServices.Customer.Domain.CustomerAccount();
        }
        public Task<BaseCommandResult> Handle(CreateCustomerCommand request, CancellationToken cancellationToken)
        {
            _customerDomain.CreateCustomer(request);

            MicroServices.Customer.Domain.Customer customer = (MicroServices.Customer.Domain.Customer)_customerDomain;
           
           //_readModelRepository.Save(customer); //elasticsearch

            var writeResult = _writeModelRepository.Commit(customer.GetUncommittedChanges());//eventstore


            // writeresult ok ise
            customer.MarkChangesAsCommitted();

            //var commandResult = new BaseCommandResult()
            //{
            //    Status = writeResult[0].
            //};
            return Task.FromResult(new BaseCommandResult());
        }

        public Task<BaseCommandResult> Handle(RemoveCustomerBillingAccountProductCommand request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        //public Task<BaseCommandResult> Handle(CreateProductCommand request, CancellationToken cancellationToken)
        //{
        //    throw new NotImplementedException();
        //}

        public Task<BaseCommandResult> Handle(AddProductBundleCommand request, CancellationToken cancellationToken)
        {
            _customerAccountDomain.AddProductBundle(request);

            CustomerAccount customerAccount = (CustomerAccount)_customerAccountDomain;

            var writeResult = _writeModelRepository.Commit(customerAccount.GetUncommittedChanges());

            // writeresult ok ise
            customerAccount.MarkChangesAsCommitted();

            return Task.FromResult(new BaseCommandResult());
        }

        public Task<BaseCommandResult> Handle(CreateCustomerBillingAccountCommand request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<BaseCommandResult> Handle(CreateCustomerAccountCommand request, CancellationToken cancellationToken)
        {
            _customerAccountDomain.CreateCustomerAccount(request);
            
            CustomerAccount customerAccount = (CustomerAccount)_customerAccountDomain;

            var writeResult = _writeModelRepository.Commit(customerAccount.GetUncommittedChanges());

            // writeresult ok ise
            customerAccount.MarkChangesAsCommitted();

            return Task.FromResult(new BaseCommandResult());
        }

        public Task<BaseCommandResult> Handle(CreateAccountContactCommand request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<BaseCommandResult> Handle(CancelProductCommand request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<BaseCommandResult> Handle(CancelProductBundleCommand request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<BaseCommandResult> Handle(ChangeProductBundleCommand request, CancellationToken cancellationToken)
        {
            _customerAccountDomain.ChangeProductBundle(request);

            CustomerAccount customerAccount = (CustomerAccount)_customerAccountDomain;

            var writeResult = _writeModelRepository.Commit(customerAccount.GetUncommittedChanges());

            // writeresult ok ise
            customerAccount.MarkChangesAsCommitted();

            return Task.FromResult(new BaseCommandResult());
        }

        public Task<BaseCommandResult> Handle(AddCustomerBillingAccountProductCommand request, CancellationToken cancellationToken)
        {
            _customerDomain.AddBillingAccount(request);

            MicroServices.Customer.Domain.Customer customer = (MicroServices.Customer.Domain.Customer)_customerDomain;

            var writeResult = _writeModelRepository.Commit(customer.GetUncommittedChanges());

            // writeresult ok ise
            customer.MarkChangesAsCommitted();

            return Task.FromResult(new BaseCommandResult());
        }

        public Task<BaseCommandResult> Handle(AddProductCommand request, CancellationToken cancellationToken)
        {
            _customerAccountDomain.AddProduct(request);

            CustomerAccount customerAccount = (CustomerAccount)_customerAccountDomain;

            var writeResult = _writeModelRepository.Commit(customerAccount.GetUncommittedChanges());

            // writeresult ok ise
            customerAccount.MarkChangesAsCommitted();

            return Task.FromResult(new BaseCommandResult());
        }

        public Task<BaseCommandResult> Handle(ChangeProductCommand command, CancellationToken cancellationToken)
        {
            _customerAccountDomain.ChangeProduct(command);

            CustomerAccount customerAccount = (CustomerAccount)_customerAccountDomain;

            var writeResult = _writeModelRepository.Commit(customerAccount.GetUncommittedChanges());

            // writeresult ok ise
            customerAccount.MarkChangesAsCommitted();

            return Task.FromResult(new BaseCommandResult());
        }
    }
}
