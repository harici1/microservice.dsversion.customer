﻿namespace MicroServices.Customer.Domain
{
    public class Address
    {
            //public string NeighbourhoodId{get; set;} Legacy????
            public string DistrictName {get; set;}
            public string ProvinceName {get; set;}
            public string CountryName  {get; set;}
            public string PostCode { get; set; }

            public string Street { get; set; }

    }
}