﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroServices.Customer.Domain
{
    public class BillingAccount : Entity
    {
        private Guid _id;        // 

        public override Guid Id
        {
            get { return _id; }
        }

        public BillingAccount(Guid billingAccountId)
        {
            _id = billingAccountId;
        }


        public IList<ProductBundle> ProductBundles { get; set; }

        public Address InvoiceAddress { get; set; }

        public string MobilePhone { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public bool? IsEInvoiceRequested { get; set; }
        public bool? IsEnvelopeRequested { get; set; }
        public bool? IsTaxFree { get; set; }

        public Guid CustomerId { get; set; }


        // OpenInvoices
        // UnPaidInvoices

        // 

    }
}
