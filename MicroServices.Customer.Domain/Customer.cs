﻿using MicroService.Customer.Model.CommandModel;
using MicroService.Customer.Model.EventModel;
using MicroServices.Customer.Domain.Implemantation;
using MicroServices.Customer.Model.EventModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroServices.Customer.Domain
{
    public class Customer : AggregateRoot, ICustomer
    {
        private Guid _id;
        public override Guid Id
        {
            get { return _id; }
        }


        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime? BirthDate { get; set; }

        public List<BillingAccount> BillingAccounts { get; set; }

        public void CreateCustomer(CreateCustomerCommand command)
        {
            _id = command.CustomerId;
            FirstName = command.FirstName;
            MiddleName = command.MiddleName;
            LastName = command.LastName;
            Gender = command.Gender;
            BirthDate = command.BirthDate;
            CreatedBy = command.CreatedBy;
            CreationDate = command.CreationDate;
            LastUpdateDate = command.LastUpdateDate;
            LastUpdatedBy = command.LastUpdatedBy;

            CustomerCreated customerCreatedEvent = new CustomerCreated()
            {
                CustomerId = this.Id,
                FirstName = this.FirstName,
                MiddleName = this.MiddleName,
                LastName = this.LastName,
                Gender = this.Gender,
                BirthDate = this.BirthDate,
                CreatedBy = this.CreatedBy,
                CreationDate = this.CreationDate,
                LastUpdateDate = this.LastUpdateDate,
                LastUpdatedBy = this.LastUpdatedBy,
            };

            ApplyChange(customerCreatedEvent);
        }

        public void AddBillingAccount(AddCustomerBillingAccountProductCommand command)
        {
            if (BillingAccounts == null)
                BillingAccounts = new List<BillingAccount>();

            Address InvoiceAddress = new Address()
            {
                Street = command.Street,
                CountryName = command.CountryName,
                DistrictName = command.DistrictName,
                ProvinceName = command.ProvinceName,
                PostCode = command.PostCode
            };

            BillingAccount billingAccount = new BillingAccount(command.BillingAccountId)
            {
                Email = command.Email,
                InvoiceAddress = InvoiceAddress,
                PhoneNumber = command.PhoneNumber,
                MobilePhone = command.MobilePhone,
                CreatedBy = command.CreatedBy,
                CreationDate = command.CreationDate,
                LastUpdatedBy = command.LastUpdatedBy,
                LastUpdateDate = command.LastUpdateDate,
                IsTaxFree = command.IsTaxFree,
                IsEInvoiceRequested = command.IsEInvoiceRequested,
                IsEnvelopeRequested = command.IsEnvelopeRequested,
                CustomerId = command.CustomerId
                
            };

            if(BillingAccounts is null)
            {
                BillingAccounts = new List<BillingAccount>();
            }
            BillingAccounts.Add(billingAccount);


            BillingAccountAdded billingAccountAddedEvent = new BillingAccountAdded()
            {
                Email = command.Email,
                Street = command.Street,
                CountryName = command.CountryName,
                DistrictName = command.DistrictName,
                ProvinceName = command.ProvinceName,
                PostCode = command.PostCode,
                PhoneNumber = command.PhoneNumber,
                MobilePhone = command.MobilePhone,
                CreatedBy = command.CreatedBy,
                CreationDate = command.CreationDate,
                LastUpdatedBy = command.LastUpdatedBy,
                LastUpdateDate = command.LastUpdateDate,
                IsTaxFree = command.IsTaxFree,
                IsEInvoiceRequested = command.IsEInvoiceRequested,
                IsEnvelopeRequested = command.IsEnvelopeRequested,
                CustomerId = command.CustomerId,
                BillingAccountId = command.BillingAccountId,
                InvoiceTaxNumber = command.InvoiceTaxNumber,
                InvoiceTaxOffice = command.InvoiceTaxOffice,
                InvoiceTitle = command.InvoiceTitle
            };

            ApplyChange(billingAccountAddedEvent);

        }

        public Customer()
        {
            // constructor
        }


    }
}
