﻿using MicroService.Customer.Model.CommandModel;
using MicroService.Customer.Model.EventModel;
using MicroService.Customer.Model.ReadModelEntity;
using MicroServices.Customer.Domain.Implemantation;
using MicroServices.Customer.Model.EventModel;
using System;
using System.Collections.Generic;

namespace MicroServices.Customer.Domain
{
    public class CustomerAccount : AggregateRoot, ICustomerAccount
    {
        private Guid _id;

        public override Guid Id
        {
            get { return _id; }
        }

        public Guid CustomerId { get; set; }
        public string ContractNumber { get; set; } // string?
        public Guid AccountContactId { get; set; } // partyRoleId

        public List<ProductBundle> ProductBundles { get; set;  }


        public void CreateCustomerAccount(CreateCustomerAccountCommand command)
        {
            _id = command.CustomerAccountId;
            CustomerId = command.CustomerId;
            ContractNumber = command.ContractNumber;
            AccountContactId = command.AccountContactId;

            CustomerAccountCreated customerAccountCreatedEvent = new CustomerAccountCreated()
            {
                CustomerAccountId = this.Id,
                CustomerId = this.CustomerId,
                ContractNumber = this.ContractNumber,
                AccountContactId = this.AccountContactId,
                CreatedBy = this.CreatedBy,
                CreationDate = this.CreationDate,
                LastUpdateDate = this.LastUpdateDate,
                LastUpdatedBy = this.LastUpdatedBy
            };

            ApplyChange(customerAccountCreatedEvent);

        }

        public void AddProductBundle(AddProductBundleCommand command)
        {
            if (command.ParentProductBundleId == null)
                command.ParentProductBundleId = Guid.Empty;

            if(ProductBundles == null)
            {
                ProductBundles = new List<ProductBundle>();
            }

            ProductBundles.Add(new ProductBundle(command.ProductBundleId, command.CustomerAccountId, command.ProductOfferId, command.BillingAccountId, command.ParentProductBundleId) { CreatedBy = command.CreatedBy, CreationDate = command.CreationDate });

            ProductBundleAdded productBundleAddedEvent = new ProductBundleAdded()
            {
                ProductBundleId = command.ProductBundleId,
                BillingAccountId = command.BillingAccountId,
                CustomerAccountId = command.CustomerAccountId,
                LegacyData = command.LegacyData,
                ParentProductBundleId = command.ParentProductBundleId,
                CreatedBy = command.CreatedBy,
                CreationDate = command.CreationDate,
                LastUpdateDate = command.LastUpdateDate,
                LastUpdatedBy = command.LastUpdatedBy
            };

            ApplyChange(productBundleAddedEvent);

        }

        public void ChangeProductBundle(ChangeProductBundleCommand command)
        {
            if (command.ParentProductBundleId == null)
                command.ParentProductBundleId = Guid.Empty;

            ProductBundle productBundle = new ProductBundle(command.ProductBundleId, command.CustomerAccountId, command.ProductOfferId, command.BillingAccountId, command.ParentProductBundleId) { CreatedBy = command.CreatedBy, CreationDate = command.CreationDate };

            if (ProductBundles == null)
            {
                ProductBundles = new List<ProductBundle>();
                ProductBundles.Add(productBundle);
            }
            else
            {
                var remove = ProductBundles.Find(s => s.Id == productBundle.Id);
                if(remove != null)
                {
                    ProductBundles.Remove(remove);
                    ProductBundles.Add(productBundle);
                }
            }

            

            

            ProductBundleChanged productBundleChangedEvent = new ProductBundleChanged()
            {
                ProductBundleId = command.ProductBundleId,
                BillingAccountId = command.BillingAccountId,
                CustomerAccountId = command.CustomerAccountId,
                LegacyData = command.LegacyData,
                ParentProductBundleId = command.ParentProductBundleId,
                CreatedBy = command.CreatedBy,
                CreationDate = command.CreationDate,
                LastUpdateDate = command.LastUpdateDate,
                LastUpdatedBy = command.LastUpdatedBy
            };

            ApplyChange(productBundleChangedEvent);

        }


        public void CreateAccountContact()
        {
            // burada mı parti üzerinde mi olmalı?
        }

        public void AddProduct(AddProductCommand command)
        {

            Product product = new Product(command.ProductId)
            {
                
                ProductBundleId = command.ProductBundleId,
                CreatedBy = command.CreatedBy,
                CreationDate = command.CreationDate,
                LastUpdateDate = command.LastUpdateDate,
                LastUpdatedBy = command.LastUpdatedBy,
                ProductName = command.ProductName
            };

            if (ProductBundles != null)
            {
                var bundle = ProductBundles.Find(s => s.Id == command.ProductBundleId);
                if (bundle != null)
                {
                    if(bundle.Products is null)
                    {
                        bundle.Products = new List<Product>();
                    }
                    bundle.Products.Add(product);
                }
            }


            ProductAdded producAddedEvent = new ProductAdded()
            {
                ProductBundleId = command.ProductBundleId,
                ProductId = command.ProductId,
                CustomerAccountId = command.CustomerAccountId,
                LegacyData = command.LegacyData,
                CreatedBy = command.CreatedBy,
                CreationDate = command.CreationDate,
                LastUpdateDate = command.LastUpdateDate,
                LastUpdatedBy = command.LastUpdatedBy,
                ProductName = command.ProductName
            };

            ApplyChange(producAddedEvent);
        }

        public void ChangeProduct(ChangeProductCommand command)
        {
            Product product = new Product(command.ProductId)
            {

                ProductBundleId = command.ProductBundleId,
                CreatedBy = command.CreatedBy,
                CreationDate = command.CreationDate,
                LastUpdateDate = command.LastUpdateDate,
                LastUpdatedBy = command.LastUpdatedBy,
                ProductName = command.ProductName
            };

            if (ProductBundles != null)
            {
                var bundle = ProductBundles.Find(s => s.Id == command.ProductBundleId);
                if (bundle != null)
                {
                    if (bundle.Products is null)
                    {
                        bundle.Products = new List<Product>();
                    }
                    else
                    {
                        var remove = bundle.Products.Find(s => s.Id == command.ProductId);
                        if (remove != null)
                        {
                            bundle.Products.Remove(remove);
                            bundle.Products.Add(product);
                        }
                    }
                }
            }

            ProductChanged producChangedEvent = new ProductChanged()
            {
                ProductBundleId = command.ProductBundleId,
                ProductId = command.ProductId,
                CustomerAccountId = command.CustomerAccountId,
                LegacyData = command.LegacyData,
                CreatedBy = command.CreatedBy,
                CreationDate = command.CreationDate,
                LastUpdateDate = command.LastUpdateDate,
                LastUpdatedBy = command.LastUpdatedBy,
                ProductName = command.ProductName
            };

            ApplyChange(producChangedEvent);

        }
    }
}
