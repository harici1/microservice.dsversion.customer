﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroServices.Customer.Domain
{
    public abstract class Entity
    {
        public abstract Guid Id { get; }

        public DateTime? CreationDate { get; set; }
        public string CreatedBy { get; set; }

        public DateTime? LastUpdateDate { get; set; }
        public string LastUpdatedBy { get; set; }
    }
}
