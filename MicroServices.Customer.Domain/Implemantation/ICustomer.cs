﻿using MicroService.Customer.Model.CommandModel;


namespace MicroServices.Customer.Domain.Implemantation
{
    public interface ICustomer
    {
        // with or without validation (pre/post adapter) implementations
        void CreateCustomer(CreateCustomerCommand command);
        void AddBillingAccount(AddCustomerBillingAccountProductCommand command);
    }
}
