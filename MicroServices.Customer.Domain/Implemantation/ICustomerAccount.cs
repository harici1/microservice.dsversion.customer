﻿using MicroService.Customer.Model.CommandModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroServices.Customer.Domain.Implemantation
{
    public interface ICustomerAccount
    {
        void CreateCustomerAccount(CreateCustomerAccountCommand command);
        void CreateAccountContact();
        void AddProductBundle(AddProductBundleCommand request);

        void ChangeProductBundle(ChangeProductBundleCommand request);
        void AddProduct(AddProductCommand request);
        void ChangeProduct(ChangeProductCommand command);
    }
}
