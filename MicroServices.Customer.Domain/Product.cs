﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroServices.Customer.Domain
{
    public class Product : Entity
    {
        private Guid _id;        // 

        public override Guid Id
        {
            get { return _id; }
        }

        public Product(Guid productId)
        {
            _id = productId;
        }

        public string ProductName { get; set; }
        public List<Service> Services { get; set; }

        public Guid ProductBundleId { get; set; }


        public ServiceStatus ProductServiceStatus { get; set; }
        public BillingStatus ProductBillingStatus { get; set; }



    }
}
