﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroServices.Customer.Domain
{
    public class ProductBundle : Entity
    {
        private Guid _id;

        public override Guid Id
        {
            get { return _id; }
        }

        public Guid? ParentProductBundleId { get; set; }

        public Guid CustomerAccountId { get; set; }
        public Guid ProductOfferId { get; set; }

        public List<Product> Products { get; set; }

        // OfferDetails

        public Guid BillingAccountId { get; set; }

        public Dictionary<string, string> LegacyData { get; set; }

        public ProductBundle(Guid productBundleId, Guid customerAccountId, Guid productOfferId, Guid billingAccountId, Guid? parentProductBundleId)
        {
            _id =  productBundleId;
            CustomerAccountId = customerAccountId;
            ProductOfferId = productOfferId;
            BillingAccountId = billingAccountId;
            ParentProductBundleId = parentProductBundleId;

        }
    }
}
