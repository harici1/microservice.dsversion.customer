﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroServices.Customer.Domain
{
    public class Service : Entity
    {
        private Guid _id;
        public override Guid Id
        {
            get { return _id; }
        }

        public string ServiceName  { get; set; }
    }
}
