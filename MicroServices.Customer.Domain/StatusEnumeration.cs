﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroServices.Customer.Domain
{
    public enum ServiceStatus
    {
        Active,
        Suspend,
        Cancelled
    }

    public enum BillingStatus
    {
        Active,
        Suspend,
        Cancelled
    }
}
