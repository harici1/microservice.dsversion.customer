﻿namespace Microservice.CustomerAccount.Consumer.Models
{
    public class CustomerAccountEventQueueSettingModel
    {
        public bool IsStop { get; set; }
    }
}
