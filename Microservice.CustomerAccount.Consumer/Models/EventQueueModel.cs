﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Microservice.CustomerAccount.Consumer.Models
{
    public enum EventQueueIndexStatus
    {
        Waiting = 0,
        InProgress = 1,
        Success = 2,
        Error = 3,
        CommandNotFound = 4,
        UnMigratableData = 5,
        SelectModelNotFound = 6,
        Test = 7
    }
    public class EventQueueModel
    {
        public Guid EventId { get; set; }

        // Event'in stream id'si
        public string StreamId { get; set; }

        // Event'in type (Exam: IndividualCreated)
        public string Type { get; set; }

        // Event'in eventstore içinde ki seq'sı (Exam: 0 or 1 or 2 etc)
        public long EventNo { get; set; }

        // Consumer Queue daima 0 olacak
        public int Status { get; set; }
        public Dictionary<string, int> StatusList { get; set; }

        // Event'in created date'i
        public string EventCreatedDate { get; set; }

        // Event'e ait json data
        public string EventData { get; set; }

        // Event'e ait metadata
        public string Metadata { get; set; }

        // Consumer Queue daima null olacak
        public DateTime? LastProcessedDate { get; set; }

        // Consumer Queue daima DateTime.Now olacak
        public string QueueCreatedDate { get; set; }

        // Bu App'in çalıtığı pc'nin ip adresi
        public string CreatedIp { get; set; }

        // Event'in Json datasının içinde bulunan tarih
        public string EventOccurrenceDate { get; set; }

    }
}
