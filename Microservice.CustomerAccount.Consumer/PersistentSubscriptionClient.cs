﻿using EventStore.ClientAPI;
using EventStore.ClientAPI.SystemData;
using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.CustomerAccount.Consumer
{
    public class PersistentSubscriptionClient
    {
        private IEventStoreConnection _conn;
        private const string STREAM = "$ce-CustomerAccount_Test13";
        private const string GROUP = "CustomerAccount_Test13";
        private const int DEFAULTPORT = 1113;
        private static readonly UserCredentials User = new UserCredentials("admin", "changeit");//"altar", "4rfv5TGB6yhn7UJM"
        private EventStorePersistentSubscriptionBase _subscription;

        public event EventHandler<ResolvedEvent> OnEventAppeared;

        public void Start()
        {
            using (_conn = EventStoreConnectionFactory.Instance)
            {
                ConnectToSubscription();

                Console.WriteLine("waiting for events. press enter to exit");
                Console.ReadLine();
            }
        }


        private void ConnectToSubscription()
        {
            var bufferSize = 1;
            var autoAck = true;

            _subscription = _conn.ConnectToPersistentSubscription(STREAM, GROUP, EventAppeared, SubscriptionDropped,
                User, bufferSize, autoAck);
        }

        private void SubscriptionDropped(EventStorePersistentSubscriptionBase eventStorePersistentSubscriptionBase,
            SubscriptionDropReason subscriptionDropReason, Exception ex)
        {
            Console.WriteLine("subs dropped");
            ConnectToSubscription();
        }

        private void EventAppeared(EventStorePersistentSubscriptionBase eventStorePersistentSubscriptionBase,
            ResolvedEvent resolvedEvent)
        {
            OnEventAppeared?.Invoke(this, resolvedEvent);
        }

    }
}
