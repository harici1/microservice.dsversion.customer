﻿using EventStore.ClientAPI;
using System;

namespace Microservice.EventQueue
{
    public sealed class EventStoreConnectionFactory
    {
        private static readonly Lazy<IEventStoreConnection> lazy = new Lazy<IEventStoreConnection>(() =>
        {
            var settingsBuilder = ConnectionSettings.Create()
                .KeepReconnecting()
                .FailOnNoServerResponse().UseDebugLogger();

            //var connectionString = "ConnectTo=tcp://altar:4rfv5TGB6yhn7UJM@172.21.1.83:1113;";
            var connectionString = "ConnectTo=tcp://admin:changeit@localhost:1113;";

            var conn = EventStoreConnection.Create(connectionString, settingsBuilder);
            
            conn.ConnectAsync().Wait();

            return conn;
        });

        public static IEventStoreConnection Instance { get { return lazy.Value; } }

        public EventStoreConnectionFactory() { }
    }
}
