﻿using Microservice.EventQueue.Models;
using Microsoft.Extensions.Logging;
using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microservice.EventQueue.Services
{
    public class EventQueueService : IEventQueueService
    {
        private readonly ILogger _logger;
        private readonly IElasticClient _elasticSearchClient;

        public EventQueueService(ILogger<EventQueueService> logger, IElasticClient elasticSearchClient)
        {
            _logger = logger;
            _elasticSearchClient = elasticSearchClient;
        }

        public IReadOnlyCollection<EventQueueModel> GetEventQueueDocuments()
        {
            var result = _elasticSearchClient.Search<EventQueueModel>(x => x.Query(q => q.Match(m => m.Field(f => f.Status).Query("0"))));

            return result.Documents;
        }

        public void UpdateEventQueueStatus(EventQueueModel model)
        {
            _elasticSearchClient.IndexDocumentAsync<EventQueueModel>(model);
        }
    }
}
