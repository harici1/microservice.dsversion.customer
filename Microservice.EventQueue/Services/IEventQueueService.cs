﻿using Microservice.EventQueue.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microservice.EventQueue.Services
{
    public interface IEventQueueService
    {
        IReadOnlyCollection<EventQueueModel> GetEventQueueDocuments();

        void UpdateEventQueueStatus(EventQueueModel model);
    }
}
