﻿using EventStore.ClientAPI;
using Microservice.EventQueue.Models;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Nest;
using Newtonsoft.Json.Linq;
using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace Microservice.EventQueue.Tasks
{
    public class EventQueueTask : BackgroundService
    {
        private readonly ILogger _logger;
        private readonly IElasticClient _elasticSearchClient;

        public EventQueueTask(ILogger<EventQueueTask> logger, IElasticClient elasticSearchClient)
        {
            _logger = logger;
            _elasticSearchClient = elasticSearchClient;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    _logger.LogInformation("EventQueueTask Background Service is working.");

                    var subscription = EventStoreConnectionFactory.Instance.ConnectToPersistentSubscription("$ce-EventQueueTest", "eventQueueTestGroup",
                                                          async (sub, e) =>
                                                          {
                                                              await Consumer(sub, e);
                                                          },
                                                     (sub, reason, ex) => { _logger.LogError(ex.Message); });
                    stoppingToken.WaitHandle.WaitOne();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                }
            }

            return Task.CompletedTask;
        }

        private async Task Consumer(EventStorePersistentSubscriptionBase sub, ResolvedEvent e)
        {
            try
            {
                if (ExistsInElastic(e.Event.EventId.ToString()))
                {
                    _logger.LogWarning("appeared multiple " + e.Event.EventId);
                }
                else
                {
                    var data = Encoding.UTF8.GetString(e.Event.Data);
                    var eventType = e.Event.EventType;

                    var json = JObject.Parse(data);

                    await _elasticSearchClient.IndexDocumentAsync(new EventQueueModel
                    {
                        EventId = e.Event.EventId,
                        EventData = Encoding.UTF8.GetString(e.Event.Data),
                        EventNo = e.Event.EventNumber,
                        StreamId = e.Event.EventStreamId,
                        Type = e.Event.EventType,
                        EventCreatedDate = e.Event.Created.ToString(CultureInfo.InvariantCulture),
                        QueueCreatedDate = DateTime.Now.ToString(CultureInfo.InvariantCulture),
                        Status = 0,
                        CreatedIp = GetIPAddress(),
                        EventOccurrenceDate = ""//(DateTime.Parse(json[eventType]["EventOccurrenceDate"] ?? DateTime.Now.ToString())).ToString(CultureInfo.InvariantCulture)
                    });

                    _logger.LogInformation("EventType {0}, Data {1}", e.Event.EventType, data);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }
        }

        private bool ExistsInElastic(string eventId)
        {
            var result = _elasticSearchClient.Search<EventQueueModel>(x => x.Query(q => q.Match(m => m.Field(f => f.EventId).Query(eventId))));

            return result.Documents.Any();
        }

        public static string GetIPAddress()
        {
            try
            {
                return Dns.GetHostAddresses(Dns.GetHostName()).Where(address => address.AddressFamily == AddressFamily.InterNetwork).FirstOrDefault()?.ToString() ?? string.Empty;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

    }
}
